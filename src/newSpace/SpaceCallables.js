import { functions } from '../firebase-config';

const createSpace = async (name, avatarUrl, spaceName) => {
  const callable = functions.httpsCallable('createSpace-createSpace');
  try {
    return await callable({
      name,
      avatarUrl,
      spaceName,
    });
  } catch (error) {
    throw new Error('Error calling createSpace-createSpace: ', error);
  }
};

export default createSpace;
