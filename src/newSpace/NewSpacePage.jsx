import React, {
  useState,
  useEffect,
  useContext,
  useRef,
} from 'react';

import { useHistory } from 'react-router-dom';
import {
  Form,
  Button,
  Spinner,
  Alert,
} from 'react-bootstrap';

import signOut from '../shared/SignOut';
import CombinedUserContext from '../currentUser/CombinedUserContext';
import createSpace from './SpaceCallables';
import createChat from '../chat/ChatCallables';
import './NewSpacePage.css';

const NewSpacePage = () => {
  document.title = 'Create new space';

  // https://dev.to/tusharkashyap63/use-refs-to-check-if-a-component-is-still-mounted-2gk7
  const mounted = useRef(false);
  mounted.current = true;

  const { combinedUser } = useContext(CombinedUserContext);
  const { avatarUrl, name, allowableSpaceNames } = combinedUser;

  const [spaceName, setSpaceName] = useState('');
  const [inputDisabled, setInputDisabled] = useState(false);
  const [buttonDisplay, setButtonDisplay] = useState('Create space');
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const [cancelButtonDisabled, setCancelButtonDisabled] = useState(false);
  const [warningMessage, setWarningMessage] = useState('You cannot rename a space after you create it.');

  const regex = new RegExp('^[a-zA-Z0-9 ]{2,20}$'); // Alphanumeric and spaces

  useEffect(() => {
    setButtonDisabled(
      !regex.test(spaceName.trim()),
    );
  }, [spaceName]);

  const updateButtonDisplay = (waitingMessage) => {
    setButtonDisplay(
      <>
        {waitingMessage && <Spinner animation="border" size="sm" />}
        <span>
          {waitingMessage && ` ${waitingMessage}`}
          {!waitingMessage && 'Create space'}
        </span>
      </>,
    );
  };

  const submit = async (e) => {
    e.preventDefault();
    let warning = null;
    setInputDisabled(true);
    setCancelButtonDisabled(true);
    setSpaceName(spaceName.trim());
    setWarningMessage(<span className="NewSpacePage__invisible-text-occupies-vertical-space">Invisible text</span>);
    setButtonDisabled(true);
    updateButtonDisplay(' Creating new space... takes a few moments');
    setTimeout(
      () => {
        if (!warning && mounted.current) {
          updateButtonDisplay(`Setting up ${spaceName.trim()}...`);
        }
      },
      1500,
    );
    setTimeout(
      () => {
        if (!warning && mounted.current) {
          updateButtonDisplay('Almost done...');
        }
      },
      3000,
    );
    const { data } = await createSpace(name, avatarUrl, spaceName.trim());
    const { warning: returnedWarning } = data;
    warning = returnedWarning;
    if (warning) {
      setWarningMessage(warning);
      setInputDisabled(false);
      setButtonDisabled(false);
      setCancelButtonDisabled(false);
      updateButtonDisplay('');
    }
    // else wait for allowableSpaceNames to get the newSpaceName before redirecting user to it
  };

  const history = useHistory();

  useEffect(() => {
    if (allowableSpaceNames && allowableSpaceNames.includes(spaceName.trim())) {
      mounted.current = false;
      history.push(`/-/${spaceName.trim()}`);
      createChat(spaceName.trim());
    }
  }, [allowableSpaceNames]);

  const cancel = () => {
    history.push('/');
  };

  return (
    <main className="NewSpacePage">
      <h3>Create a new space</h3>
      <Alert
        variant="warning"
        className="NewSpacePage__message"
      >
        A
        <span className="font-weight-bold"> space </span>
        is a private area for your team to brainstorm, plan, execute,
        and track work in
        <span className="font-weight-bold"> talks</span>
        , as well as collaborate in
        <span className="font-weight-bold"> docs</span>
        .
        <br />
        <br />
        Pick a space name to get started.
      </Alert>
      <Form autoComplete="off" onSubmit={submit}>
        <span>Use letters, numbers, and spaces. Keep it between 2 and 20 characters.</span>
        <Form.Control
          type="text"
          className="NewSpacePage__name"
          onChange={(e) => { setSpaceName(e.target.value); }}
          value={spaceName}
          placeholder="Space name"
          disabled={inputDisabled}
        />
        <div className="NewSpacePage__warning-message">{warningMessage}</div>
        <div className="NewSpace__save-cancel-buttons">
          <Button
            variant="primary"
            disabled={buttonDisabled}
            className="btn-sm"
            type="submit"
          >
            {buttonDisplay}
          </Button>
          {allowableSpaceNames.length > 0 && (
            <Button
              variant="outline-primary"
              className="btn-sm"
              onClick={cancel}
              disabled={cancelButtonDisabled}
            >
              Cancel
            </Button>
          )}
        </div>
        <div className="NewSpacePage__ensembl-community">
          Here for Ensembl Community instead?
          <a href="/-/Ensembl%20Community/join"> Join it</a>
        </div>
        {allowableSpaceNames.length === 0 && (
          <div className="NewSpacePage__sign-out-link">
            <button
              type="button"
              onClick={() => { signOut(); }}
            >
              Sign out
            </button>
          </div>
        )}
      </Form>
    </main>
  );
};

export default NewSpacePage;
