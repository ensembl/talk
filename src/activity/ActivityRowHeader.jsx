import React from 'react';

import PropTypes from 'prop-types';
import {
  OverlayTrigger,
  Tooltip,
} from 'react-bootstrap';
import TimeAgo from 'react-timeago';

import timeAgoFormatter from '../shared/TimeAgoFormatter';
import tooltipDatetimeFormatter from '../shared/TooltipDatetimeFormatter';
import './ActivityRowHeader.css';

const ActivityRowHeader = ({
  name,
  activityCreatedAtSeconds,
}) => (
  <div
    className="ActivityRowHeader text-muted"
  >
    <span className="font-weight-bold">
      {`${name} `}
    </span>
    <span>
      <OverlayTrigger
        delay={{ show: 50, hide: 50 }}
        overlay={<Tooltip>{tooltipDatetimeFormatter(activityCreatedAtSeconds)}</Tooltip>}
      >
        <TimeAgo date={new Date(activityCreatedAtSeconds * 1000)} minPeriod="60" formatter={timeAgoFormatter} />
      </OverlayTrigger>
    </span>
  </div>
);

ActivityRowHeader.propTypes = {
  name: PropTypes.string.isRequired,
  activityCreatedAtSeconds: PropTypes.number.isRequired,
};

export default ActivityRowHeader;
