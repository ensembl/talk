import React from 'react';

import dompurify from 'dompurify';
import PropTypes from 'prop-types';

import './ActivityRowBody.css';

const ActivityRowBody = ({
  htmlMessage,
  htmlCommentBody,
}) => (
  <>
    {/* eslint-disable-next-line react/no-danger */}
    <div dangerouslySetInnerHTML={{ __html: dompurify.sanitize(htmlMessage) }} />

    {htmlCommentBody && (
      <>
        <div
          className="ck-content ActivityRowBody__html-comment-body"
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{ __html: dompurify.sanitize(htmlCommentBody) }}
        />
      </>
    )}
  </>
);

ActivityRowBody.propTypes = {
  htmlMessage: PropTypes.string.isRequired,
  htmlCommentBody: PropTypes.string,
};

ActivityRowBody.defaultProps = {
  htmlCommentBody: '',
};
export default ActivityRowBody;
