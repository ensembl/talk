import React, { useEffect, useContext } from 'react';

import { useCollectionData } from 'react-firebase-hooks/firestore';
import { useParams } from 'react-router-dom';
import { Redirect } from 'react-router';

import { db } from '../firebase-config';
import CombinedUserContext from '../currentUser/CombinedUserContext';
import updateUserFirestore from '../shared/UserFirestore';
import PageLayout from '../shared/layout/PageLayout';
import PageLoadingSpinner from '../shared/PageLoadingSpinner';
import ActivityRow from './ActivityRow';
import PageNotFound from '../shared/PageNotFound';

import './ActivityPage.css';

const ActivityPage = () => {
  if (window.location.pathname.startsWith('/-/Ensembl%20Community')) {
    return <Redirect to="/" />;
  }

  const { spaceName: urlSpaceName } = useParams();

  const { combinedUser } = useContext(CombinedUserContext);
  const {
    uid,
    allowableSpaceNames,
  } = combinedUser;
  const [activities, activitiesLoading, activitiesError] = useCollectionData(db.collection(`spaces/${urlSpaceName}/activities`).orderBy('activityCreatedAt', 'desc').limit(200), { idField: 'id' });

  const onCommunitySpace = window.location.pathname.startsWith('/-/Ensembl%20Community');

  document.title = 'Activity';

  useEffect(() => {
    if (activities) {
      updateUserFirestore(uid, {
        lastVisitedSpaceName: urlSpaceName,
        activityPageLastVisitedAt: new Date(),
      });
    }
  }, [activities, activitiesLoading]);

  return (
    <PageLayout
      uid={uid}
      urlSpaceName={urlSpaceName}
      onCommunitySpace={onCommunitySpace}
      allowableSpaceNames={allowableSpaceNames}
      mobileHeaderTitle="Activity"
      mainContent={(
        <>
          {activitiesLoading && <PageLoadingSpinner />}
          {activitiesError && <PageNotFound />}
          {activities && (
            <>
              <h3 className="ActivityPage__title">Activity</h3>
              {activities.map((activity) => (
                <div className="ActivityPage__activityRow" key={activity.id}>
                  <ActivityRow
                    avatarUrl={activity.avatarUrl}
                    name={activity.name}
                    activityCreatedAtSeconds={activity.activityCreatedAt.seconds}
                    htmlMessage={activity.htmlMessage}
                    htmlCommentBody={activity.htmlCommentBody ? activity.htmlCommentBody : ''}
                  />
                </div>
              ))}
            </>
          )}
        </>
      )}
    />
  );
};

export default ActivityPage;
