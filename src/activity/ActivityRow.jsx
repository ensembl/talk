import React from 'react';

import PropTypes from 'prop-types';

import AvatarAnchoredContent from '../shared/AvatarAnchoredContent';
import ActivityRowHeader from './ActivityRowHeader';
import ActivityRowBody from './ActivityRowBody';

const Activity = ({
  avatarUrl,
  name,
  activityCreatedAtSeconds,
  htmlMessage,
  htmlCommentBody,
}) => (
  <AvatarAnchoredContent
    avatarUrl={avatarUrl}
    header={(
      <ActivityRowHeader
        name={name}
        activityCreatedAtSeconds={activityCreatedAtSeconds}
      />
    )}
    body={(
      <ActivityRowBody
        htmlMessage={htmlMessage}
        htmlCommentBody={htmlCommentBody}
      />
    )}
  />
);

Activity.propTypes = {
  avatarUrl: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  activityCreatedAtSeconds: PropTypes.number.isRequired,
  htmlMessage: PropTypes.string.isRequired,
  htmlCommentBody: PropTypes.string,
};

Activity.defaultProps = {
  htmlCommentBody: '',
};

export default Activity;
