import React, { useContext } from 'react';

import { useParams } from 'react-router-dom';

import CombinedUserContext from '../currentUser/CombinedUserContext';
import PageLayout from '../shared/layout/PageLayout';
import NewTalkForm from './NewTalkForm';

const NewTalkPage = () => {
  const { spaceName: urlSpaceName, type } = useParams();

  const { combinedUser } = useContext(CombinedUserContext);
  const {
    uid,
    name,
    avatarUrl,
    allowableSpaceNames,
  } = combinedUser;

  const onCommunitySpace = window.location.pathname.startsWith('/-/Ensembl%20Community');

  document.title = `New ${type}`;

  return (
    <PageLayout
      uid={uid}
      urlSpaceName={urlSpaceName}
      onCommunitySpace={onCommunitySpace}
      allowableSpaceNames={allowableSpaceNames}
      mobileHeaderTitle={`New ${type}`}
      mainContent={(
        <>
          <NewTalkForm
            spaceName={urlSpaceName}
            type={type}
            uid={uid}
            name={name}
            avatarUrl={avatarUrl}
          />
        </>
      )}
    />
  );
};

export default NewTalkPage;
