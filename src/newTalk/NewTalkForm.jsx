import React, { useEffect, useState } from 'react';

import PropTypes from 'prop-types';
import { Form, Button, Spinner } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

import { addTalkFirestore } from '../shared/TalkFirestore';
import './NewTalkForm.css';

const NewTalkForm = ({
  spaceName,
  type,
  uid,
  name,
  avatarUrl,
}) => {
  const [title, setTitle] = useState('');
  const [buttonDisplay, setButtonDisplay] = useState(`Create new ${type}`);
  const [creatingTalk, setCreatingTalk] = useState(false);

  const history = useHistory();
  const withName = (history.location.state && history.location.state.userName) || null;
  const newTalkTitle = `New ${type}${withName ? ` with ${withName}` : ''}`;

  useEffect(() => {
    setButtonDisplay(`Create new ${type}`);
  }, [type]);

  useEffect(() => {
    if (creatingTalk) {
      setButtonDisplay(
        <>
          <Spinner animation="border" size="sm" />
          <span> Creating...</span>
        </>,
      );
    }
  }, [creatingTalk]);

  const createTalk = async (e) => {
    e.preventDefault();
    setCreatingTalk(true);
    const nowDate = new Date();
    const newTalk = {
      title: title.trim(),
      htmlSubject: `<h3>${title}</h3>`,
      createdAt: nowDate,
      writtenAt: nowDate,
      createdByName: name,
      createdById: uid,
      createdByAvatarUrl: avatarUrl,
      state: 'Open',
      subscribedUids: [uid],
      mergeRequestUrls: [],
      ownerIds: [uid],
      isDoc: type === 'doc',
    };

    const newTalkRef = await addTalkFirestore(
      spaceName,
      newTalk,
    );
    history.replace({
      pathname: `/-/${spaceName}/${type}s/${newTalkRef.id}`,
      state: history.location.state,
    });
  };

  return (
    <div className="NewTalkForm">
      <h2 className="NewTalkForm__title">{newTalkTitle}</h2>
      <Form autoComplete="off" onSubmit={createTalk}>
        <Form.Control
          type="input"
          onChange={(e) => { setTitle(e.target.value); }}
          value={title}
          placeholder={`Enter ${type} title`}
          autoFocus
        />
        <div className="NewTalkForm__btns">
          <Button
            variant="primary"
            type="submit"
            disabled={title.trim().length === 0 || creatingTalk}
          >
            {buttonDisplay}
          </Button>
          <Button
            className="NewTalkForm__cancel-btn"
            variant="outline-primary"
            onClick={() => { history.replace('/home'); }}
          >
            Cancel
          </Button>
        </div>
      </Form>
    </div>
  );
};

NewTalkForm.propTypes = {
  spaceName: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['doc', 'talk']).isRequired,
  uid: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
};

export default NewTalkForm;
