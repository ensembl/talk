import React from 'react';

import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Avatar from '../shared/Avatar';
import './NewTalkAvatar.css';

const NewTalkAvatar = ({
  spaceName,
  user,
}) => (
  <div className="NewTalkAvatar">
    <Link
      className="center"
      to={{
        pathname: `/-/${spaceName}/new/talk`,
        state: {
          uid: user.id,
          userName: user.name,
          avatarUrl: user.avatarUrl,
        },
      }}
    >
      <Avatar
        avatarUrl={user.avatarUrl}
        size="md"
        tooltip={`Start a new talk with ${user.name}`}
        tooltipPlacement="bottom"
      />
    </Link>
  </div>
);

NewTalkAvatar.propTypes = {
  spaceName: PropTypes.string.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default NewTalkAvatar;
