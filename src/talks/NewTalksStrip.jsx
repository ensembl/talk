import React, { useContext } from 'react';

import PropTypes from 'prop-types';
import { Plus } from 'react-bootstrap-icons';
import { Link } from 'react-router-dom';
import { OverlayTrigger, Tooltip, Button } from 'react-bootstrap';

import CombinedUserContext from '../currentUser/CombinedUserContext';
import updateUserFirestore from '../shared/UserFirestore';
import OnboardingMessage from '../shared/OnboardingMessage';
import NewTalkAvatar from './NewTalkAvatar';
import InviteNewPersonMessage from './InviteNewPersonMessage';
import './NewTalksStrip.css';

const NewTalksStrip = ({
  spaceName,
  users,
}) => {
  const { combinedUser } = useContext(CombinedUserContext);
  const {
    uid,
    dismissedNewTalkOnboarding,
  } = combinedUser;

  const dismissNewTalkOnboarding = async () => {
    await updateUserFirestore(
      uid,
      {
        dismissedNewTalkOnboarding: true,
      },
    );
  };

  return (
    <>
      {!dismissedNewTalkOnboarding && users.length > 1 && (
        <OnboardingMessage
          title="Start a new talk 🗣️"
          body={(
            <p>
              Start a new talk with a teammate by clicking their picture below 👇. Or click
              <span className="font-weight-bold"> New talk </span>
              if you&apos;d rather just jot down some ideas first.
              You can pull in your teammates to the talk later.
            </p>
          )}
          dismissForever={dismissNewTalkOnboarding}
        />
      )}
      {users.length === 1 && <InviteNewPersonMessage spaceName={spaceName} />}
      <div className="NewTalksStrip">
        <OverlayTrigger
          placement="bottom"
          delay={{ show: 50, hide: 50 }}
          overlay={<Tooltip>Start a new talk</Tooltip>}
        >
          <Link to={`/-/${spaceName}/new/talk`}><Button variant="primary" className="NewTalksStrip__new-talk-button font-weight-bold"><Plus /></Button></Link>
        </OverlayTrigger>
        {users.map((user) => <NewTalkAvatar key={user.id} spaceName={spaceName} user={user} />)}
      </div>
    </>
  );
};

NewTalksStrip.propTypes = {
  spaceName: PropTypes.string.isRequired,
  users: PropTypes.instanceOf(Array).isRequired,
};

export default NewTalksStrip;
