import React from 'react';

import PropTypes from 'prop-types';

import TalkRow from './TalkRow';
import './TalkRows.css';

const TalkRows = ({
  talks,
  labels,
  spaceName,
  users,
}) => (
  <div className="TalkRows">
    {talks && talks.length === 0 && (
      <div
        className="TalkRows__no-results-message"
      >
        Nothing here. Start a new talk ☝️
      </div>
    )}
    {talks && talks.length > 0 && talks.filter((talk) => !talk.toDelete).map((talk) => (
      <TalkRow
        key={talk.id}
        talk={talk}
        labels={labels}
        spaceName={spaceName}
        users={users}
      />
    ))}
  </div>
);

TalkRows.propTypes = {
  talks: PropTypes.instanceOf(Array).isRequired,
  labels: PropTypes.instanceOf(Array).isRequired,
  spaceName: PropTypes.string.isRequired,
  users: PropTypes.instanceOf(Array).isRequired,
};

export default TalkRows;
