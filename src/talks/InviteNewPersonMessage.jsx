import React from 'react';

import { Alert } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const InviteNewPersonMessage = ({ spaceName }) => (
  <Alert variant="warning" className="InviteNewPersonMessage">
    <span>Ensembl Talk works best with more people. </span>
    <Link to="/people#invite">{`Invite a new person to ${spaceName}`}</Link>
  </Alert>
);

InviteNewPersonMessage.propTypes = {
  spaceName: PropTypes.string.isRequired,
};

export default InviteNewPersonMessage;
