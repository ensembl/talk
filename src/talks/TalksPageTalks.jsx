import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';
import { Alert } from 'react-bootstrap';
import { useCollection } from 'react-firebase-hooks/firestore';

import { db } from '../firebase-config';
import hash from '../shared/HashUtils';
import TalksPageSearch from './TalksPageSearch';
import TalksPageTabs from './TalksPageTabs';
import TalksPageFilterLabels from './TalksPageFilterLabels';
import TalksPagination from './TalksPagePagination';
import TalkRows from './TalkRows';

const TalksPageTalks = ({
  onCommunitySpace,
  spaceName,
  labels,
  users,
}) => {
  const [searchQuery, setSearchQuery] = useState('');
  const [filteredLabelIds, setFilteredLabelIds] = useState([]);
  const [tab, setTab] = useState('talks');
  const [searchHash, setSearchHash] = useState(null);
  const [page, setPage] = useState(1);
  const [talks, setTalks] = useState(null);

  // Keep track of docs in an array to use in pagination startAfter
  // Position 0 is not used, position 1 represents page 1 and is null
  // Position 2 represents page 2 with the first non-null startAfterDoc.
  const [startAfterDocs, setStartAfterDocs] = useState([null, null]);

  // Hash together the title and filteredIds in order
  // to match the searchHashes in updateTalkLabelHashes
  useEffect(() => {
    if (searchQuery || filteredLabelIds.length > 0) {
      const firstSpace = searchQuery.indexOf(' ');
      const titleHash = firstSpace < 0
        ? hash(searchQuery.toLowerCase())
        : hash(searchQuery.toLowerCase().slice(0, firstSpace));
      const filteredLabelsHash = filteredLabelIds.length === 0
        ? 0
        : filteredLabelIds.reduce((acc, elem) => (acc + hash(elem)), 0);
      setSearchHash(titleHash + filteredLabelsHash);
    } else {
      setSearchHash(null);
    }
  }, [searchQuery, filteredLabelIds]);

  // Get talksSnapshot
  const pageSize = 20;
  let query = db.collection(`spaces/${spaceName}/talks`);
  if (searchHash) {
    query = query.where('searchHashes', 'array-contains', searchHash);
  }
  query = query.where(`tabs.${tab}`, '==', true);
  query = query.orderBy('writtenAt', 'desc');
  if (startAfterDocs[page]) {
    query = query.startAfter(startAfterDocs[page]);
  }
  query = query.limit(pageSize);
  const [talksSnapshot, talksLoading, talksError] = useCollection(query, { idField: 'id' });

  // Generate talks from talksSnapshot
  // and update startAfterDocs as necessary
  useEffect(() => {
    if (!talksSnapshot) {
      setTalks(null);
      return;
    }
    const newStartAfterDocs = [...startAfterDocs];
    if (talksSnapshot.empty) {
      setTalks([]);
      newStartAfterDocs[page + 1] = null;
      setStartAfterDocs(newStartAfterDocs);
      return;
    }
    setTalks(
      talksSnapshot.docs.map((doc) => {
        const talk = doc.data();
        talk.id = doc.ref.id;
        return talk;
      }),
    );
    newStartAfterDocs[page + 1] = talksSnapshot.docs.length < pageSize
      ? null
      : talksSnapshot.docs[talksSnapshot.docs.length - 1];
    setStartAfterDocs(newStartAfterDocs);
  }, [talksSnapshot]);

  useEffect(() => {
    // Do not show a loading UI.
  }, [talksLoading]);

  return (
    <>
      <TalksPageSearch
        searchQuery={searchQuery}
        setSearchQuery={setSearchQuery}
        setPage={setPage}
      />
      <TalksPageTabs
        tab={tab}
        setTab={setTab}
        setPage={setPage}
      />
      <TalksPageFilterLabels
        onCommunitySpace={onCommunitySpace}
        labels={labels}
        filteredLabelIds={filteredLabelIds}
        setFilteredLabelIds={setFilteredLabelIds}
        setPage={setPage}
        spaceName={spaceName}
      />
      {talksLoading && <span />}
      {talksError && <Alert variant="warning">Something went wrong. Reload browser to try again.</Alert>}
      {talks && (
        <TalkRows
          talks={talks}
          labels={labels}
          spaceName={spaceName}
          users={users}
        />
      )}
      {talks && (
        <TalksPagination
          previousDisabled={!startAfterDocs[page - 1] && page !== 2}
          nextDisabled={!startAfterDocs[page + 1]}
          page={page}
          setPage={setPage}
        />
      )}
    </>
  );
};

TalksPageTalks.propTypes = {
  onCommunitySpace: PropTypes.bool.isRequired,
  spaceName: PropTypes.string.isRequired,
  labels: PropTypes.instanceOf(Array).isRequired,
  users: PropTypes.instanceOf(Array).isRequired,
};

export default TalksPageTalks;
