import React from 'react';

import { Alert } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const UnlockDocCommentsMessage = ({ spaceName }) => (
  <Alert variant="warning" className="UnlockDocCommentsMessage">
    <Link to="/-/Ensembl%20Community/docs/xMzhV6XseleduSPaVZdY">Doc comments </Link>
    <span>are unlocked when your space has at least two people. </span>
    <Link to="/people#invite">{`Invite a new person to ${spaceName}`}</Link>
  </Alert>
);

UnlockDocCommentsMessage.propTypes = {
  spaceName: PropTypes.string.isRequired,
};

export default UnlockDocCommentsMessage;
