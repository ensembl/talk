import React, { useState } from 'react';

import PropTypes from 'prop-types';

import NewTalksStrip from '../NewTalksStrip';
import StateBanner from './StateBanner';
import Subject from './Subject';
import Comments from './Comments';
import {
  addCommentFirestore,
  updateCommentFirestore,
} from './CommentFirestore';
import { updateTalkFirestore } from '../../shared/TalkFirestore';

import CommunityMessage from '../shared/CommunityMessage';

const TalkPageBody = ({
  usesReadOnlyEditor,
  onCommunitySpace,
  isSpaceMember,
  spaceName,
  talk,
  comments,
  users,
  save,
  contentEditorReady,
  setContentEditorReady,
}) => {
  const {
    id: talkId,
    isDoc = false,
    subscribedUids = [],
    state,
    changedStateById,
    changedStateByName,
    changedStateByAvatarUrl,
    changedStateAt,
  } = talk;

  const [addCommentAppendContent, setAddCommentAppendContent] = useState('');

  const changers = users.filter((user) => user.id === changedStateById);
  const changerAvatarUrl = changers.length > 0 ? changers[0].avatarUrl : changedStateByAvatarUrl;
  const changerName = changers.length > 0 ? changers[0].name : changedStateByName;

  const addComment = async (data) => {
    await addCommentFirestore(
      spaceName,
      talkId,
      data,
    );
  };

  const updateComment = async (commentId, data) => {
    await updateCommentFirestore(
      spaceName,
      talkId,
      commentId,
      data,
    );
  };

  const updateTalk = async (data) => {
    await updateTalkFirestore(
      spaceName,
      talkId,
      data,
    );
  };

  return (
    <>
      {!isDoc && !onCommunitySpace && (
        <NewTalksStrip
          spaceName={spaceName}
          users={users}
        />
      )}

      {onCommunitySpace && <CommunityMessage isSpaceMember={isSpaceMember} />}

      {['In progress', 'Resolved'].includes(state) && (
        <StateBanner
          state={state}
          changedByName={changerName}
          changedByAvatarUrl={changerAvatarUrl}
          changedAtSeconds={!changedStateAt ? -1 : changedStateAt.seconds}
        />
      )}
      <Subject
        usesReadOnlyEditor={usesReadOnlyEditor}
        onCommunitySpace={onCommunitySpace}
        spaceName={spaceName}
        talk={talk}
        users={users}
        save={save}
        contentEditorReady={contentEditorReady}
        setContentEditorReady={setContentEditorReady}
        setAddCommentAppendContent={setAddCommentAppendContent}
        updateTalk={updateTalk}
      />
      <Comments
        onCommunitySpace={onCommunitySpace}
        isSpaceMember={isSpaceMember}
        spaceName={spaceName}
        talkId={talk.id}
        comments={comments}
        addComment={addComment}
        updateComment={updateComment}
        addCommentAppendContent={addCommentAppendContent}
        setAddCommentAppendContent={setAddCommentAppendContent}
        users={users}
        subscribedUids={subscribedUids}
      />
    </>
  );
};

TalkPageBody.propTypes = {
  usesReadOnlyEditor: PropTypes.bool.isRequired,
  onCommunitySpace: PropTypes.bool.isRequired,
  isSpaceMember: PropTypes.bool.isRequired,
  spaceName: PropTypes.string.isRequired,
  talk: PropTypes.objectOf(PropTypes.any).isRequired,
  comments: PropTypes.arrayOf(PropTypes.any).isRequired,
  users: PropTypes.arrayOf(PropTypes.any).isRequired,
  save: PropTypes.exact({
    isSaving: PropTypes.bool.isRequired,
    setIsSaving: PropTypes.func.isRequired,
    isSaved: PropTypes.bool.isRequired,
    setIsSaved: PropTypes.func.isRequired,
  }).isRequired,
  contentEditorReady: PropTypes.bool.isRequired,
  setContentEditorReady: PropTypes.func.isRequired,
};

export default TalkPageBody;
