import React, { useContext, useState, useEffect } from 'react';

import PropTypes from 'prop-types';

import { CKEditorTokenContext } from '../../currentUser/CKEditorTokenProvider';
import CombinedUserContext from '../../currentUser/CombinedUserContext';
import PreviewCKEditor from '../../shared/ckeditor/PreviewCKEditor';
import TalkSubjectCKEditor from '../../shared/ckeditor/TalkSubjectCKEditor';
import DocSubjectCKEditor from '../../shared/ckeditor/DocSubjectCKEditor';
import './EditSubject.css';

const EditSubject = ({
  usesReadOnlyEditor,
  onCommunitySpace,
  spaceName,
  talkId,
  htmlSubject,
  users,
  setIsSaving,
  setIsSaved,
  type,
  contentEditorReady,
  setContentEditorReady,
  setComment,
  updateTalk,
  setShowThreadResolvedMessage,
}) => {
  const { ckeditorJwt } = useContext(CKEditorTokenContext);
  const { combinedUser } = useContext(CombinedUserContext);
  const {
    name,
    uid,
    avatarUrl,
  } = combinedUser;

  const [autoSaveContent, setAutoSaveContent] = useState(htmlSubject);

  useEffect(async () => {
    if (htmlSubject === autoSaveContent) {
      return;
    }
    let title;
    if (autoSaveContent.length === 0) {
      title = `Untitled ${type}`;
    } else {
      title = new DOMParser()
        .parseFromString(autoSaveContent, 'text/html').body.firstElementChild.textContent.trim();
    }
    setIsSaving(true);
    setIsSaved(false);
    const nowDate = new Date();
    await updateTalk(
      {
        title,
        htmlSubject: autoSaveContent,
        updatedAt: nowDate,
        writtenAt: nowDate,
        updatedByName: name,
        updatedById: uid,
        updatedByAvatarUrl: avatarUrl,
      },
    );
    setIsSaving(false);
    setIsSaved(true);
  }, [autoSaveContent]);

  const usesCollaborativeEditor = !onCommunitySpace && users.length > 1;

  return (
    <>
      {!contentEditorReady && (
        <PreviewCKEditor
          content={htmlSubject}
          type={type}
        />
      )}
      <div className={`${contentEditorReady ? '' : 'hidden'}`}>
        {type === 'talk' && (
          <TalkSubjectCKEditor
            usesCollaborativeEditor={usesCollaborativeEditor}
            usesReadOnlyEditor={usesReadOnlyEditor}
            content={htmlSubject}
            setAutoSaveContent={setAutoSaveContent}
            talkId={talkId}
            spaceName={spaceName}
            users={users}
            setContentEditorReady={setContentEditorReady}
            ckeditorJwt={ckeditorJwt}
          />
        )}
        {type === 'doc' && (
          <DocSubjectCKEditor
            usesCollaborativeEditor={usesCollaborativeEditor}
            usesReadOnlyEditor={usesReadOnlyEditor}
            content={htmlSubject}
            setAutoSaveContent={setAutoSaveContent}
            talkId={talkId}
            spaceName={spaceName}
            users={users}
            setContentEditorReady={setContentEditorReady}
            uid={uid}
            name={name}
            avatarUrl={avatarUrl}
            ckeditorJwt={ckeditorJwt}
            setComment={setComment}
            updateTalk={updateTalk}
            setShowThreadResolvedMessage={setShowThreadResolvedMessage}
          />
        )}
      </div>
    </>
  );
};

EditSubject.propTypes = {
  usesReadOnlyEditor: PropTypes.bool.isRequired,
  onCommunitySpace: PropTypes.bool.isRequired,
  spaceName: PropTypes.string.isRequired,
  talkId: PropTypes.string.isRequired,
  htmlSubject: PropTypes.string.isRequired,
  users: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    avatarUrl: PropTypes.string,
  })).isRequired,
  setIsSaving: PropTypes.func.isRequired,
  setIsSaved: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
  contentEditorReady: PropTypes.bool.isRequired,
  setContentEditorReady: PropTypes.func.isRequired,
  setComment: PropTypes.func,
  updateTalk: PropTypes.func.isRequired,
  setShowThreadResolvedMessage: PropTypes.func,
};

EditSubject.defaultProps = {
  setComment: null,
  setShowThreadResolvedMessage: null,
};

export default EditSubject;
