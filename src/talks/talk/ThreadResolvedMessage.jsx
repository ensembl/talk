import React from 'react';

import { Alert } from 'react-bootstrap';

const ThreadResolvedMessage = () => (
  <Alert variant="warning" className="ThreadResolvedMessage">
    Comment thread resolved. Click
    <span className="font-weight-bold"> Comment threads </span>
    below to view it.
  </Alert>
);

export default ThreadResolvedMessage;
