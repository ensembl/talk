import React, { useState } from 'react';

import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Subject from './Subject';
import {
  setCommentFirestore,
} from './CommentFirestore';
import { updateTalkFirestore } from '../../shared/TalkFirestore';

import CommunityMessage from '../shared/CommunityMessage';
import ThreadResolvedMessage from './ThreadResolvedMessage';
import StateBanner from './StateBanner';
import DocViewToggle from './DocViewToggle';
import CommentThreads from './CommentThreads';
import UnlockDocCommentsMessage from './UnlockDocCommentsMessage';

import './DocPageBody.css';

const DocPageBody = ({
  usesReadOnlyEditor,
  onCommunitySpace,
  isSpaceMember,
  spaceName,
  talk,
  comments,
  users,
  save,
  contentEditorReady,
  setContentEditorReady,
}) => {
  const {
    id: talkId,
    threadsMeta = {},
    state,
    changedStateById,
    changedStateByName,
    changedStateByAvatarUrl,
    changedStateAt,
  } = talk;

  const changers = users.filter((user) => user.id === changedStateById);
  const changerAvatarUrl = changers.length > 0 ? changers[0].avatarUrl : changedStateByAvatarUrl;
  const changerName = changers.length > 0 ? changers[0].name : changedStateByName;

  const hashComponents = window.location.hash.split('_');
  const [view, setView] = useState(
    !window.location.hash || hashComponents[0] === '#thread' ? 'doc' : 'comment-threads-only',
  );

  const [showThreadResolvedMessage, setShowThreadResolvedMessage] = useState(false);

  const setComment = async (commentId, data) => {
    await setCommentFirestore(
      spaceName,
      talkId,
      commentId,
      data,
    );
  };

  const updateTalk = async (data) => {
    await updateTalkFirestore(
      spaceName,
      talkId,
      data,
    );
  };

  const threadsCount = new Set(comments.map((comment) => comment.threadId)).size;

  return (
    <>
      {onCommunitySpace && <CommunityMessage isSpaceMember={isSpaceMember} />}

      {users.length === 1 && <UnlockDocCommentsMessage spaceName={spaceName} />}

      {['Archived'].includes(state) && (
        <StateBanner
          state={state}
          changedByName={changerName}
          changedByAvatarUrl={changerAvatarUrl}
          changedAtSeconds={!changedStateAt ? -1 : changedStateAt.seconds}
        />
      )}

      {showThreadResolvedMessage && <ThreadResolvedMessage />}

      <DocViewToggle
        view={view}
        setView={setView}
        threadsCount={threadsCount}
        setShowThreadResolvedMessage={setShowThreadResolvedMessage}
      />

      <div className={`${view === 'comment-threads-only' ? 'hidden' : ''}`}>
        <Subject
          usesReadOnlyEditor={usesReadOnlyEditor}
          onCommunitySpace={onCommunitySpace}
          spaceName={spaceName}
          talk={talk}
          users={users}
          save={save}
          contentEditorReady={contentEditorReady}
          setContentEditorReady={setContentEditorReady}
          setComment={setComment}
          updateTalk={updateTalk}
          setShowThreadResolvedMessage={setShowThreadResolvedMessage}
        />
      </div>

      {view === 'comment-threads-only' && (
        <>
          {threadsCount === 0
            ? (
              <div className="DocPageBody__comment-threads-empty-state">
                Comment threads appear here.
                <Link to="/-/Ensembl%20Community/docs/xMzhV6XseleduSPaVZdY"> Learn more</Link>
              </div>
            )
            : (
              <CommentThreads
                threadsMeta={threadsMeta}
                comments={comments}
                users={users}
              />
            )}
        </>
      )}
    </>
  );
};

DocPageBody.propTypes = {
  usesReadOnlyEditor: PropTypes.bool.isRequired,
  onCommunitySpace: PropTypes.bool.isRequired,
  isSpaceMember: PropTypes.bool.isRequired,
  spaceName: PropTypes.string.isRequired,
  talk: PropTypes.objectOf(PropTypes.any).isRequired,
  comments: PropTypes.arrayOf(PropTypes.any).isRequired,
  users: PropTypes.arrayOf(PropTypes.any).isRequired,
  save: PropTypes.exact({
    isSaving: PropTypes.bool.isRequired,
    setIsSaving: PropTypes.func.isRequired,
    isSaved: PropTypes.bool.isRequired,
    setIsSaved: PropTypes.func.isRequired,
  }).isRequired,
  contentEditorReady: PropTypes.bool.isRequired,
  setContentEditorReady: PropTypes.func.isRequired,
};

export default DocPageBody;
