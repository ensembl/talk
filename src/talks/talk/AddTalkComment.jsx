import React, { useEffect, useState } from 'react';

import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';

import AddComment from '../../shared/comment/AddComment';

const AddTalkComment = ({
  spaceName,
  users,
  talkId,
  uid,
  name,
  avatarUrl,
  addComment,
  subscribedUids,
  addCommentAppendContent,
  setAddCommentAppendContent,
}) => {
  const [starterMentionUserId, setStarterMentionUserId] = useState('');
  const [starterMentionUserName, setStarterMentionUserName] = useState('');
  const [starterMentionAvatarUrl, setStarterMentionAvatarUrl] = useState('');

  const history = useHistory();
  useEffect(() => {
    if (history.location.state) {
      setStarterMentionUserId(history.location.state.uid);
      setStarterMentionUserName(history.location.state.userName);
      setStarterMentionAvatarUrl(history.location.state.avatarUrl);
      window.history.replaceState({}, '');
    }
  }, [history.location.state]);

  return (
    <AddComment
      localStorageKey={`app.ensembl.so/-/${spaceName}/talks/${talkId}/addComment`}
      spaceName={spaceName}
      users={users}
      uid={uid}
      name={name}
      avatarUrl={avatarUrl}
      addComment={addComment}
      threadId={talkId}
      starterMentionUserId={starterMentionUserId}
      starterMentionUserName={starterMentionUserName}
      starterMentionAvatarUrl={starterMentionAvatarUrl}
      subscribedUids={subscribedUids}
      addCommentAppendContent={addCommentAppendContent}
      setAddCommentAppendContent={setAddCommentAppendContent}
    />
  );
};

AddTalkComment.propTypes = {
  spaceName: PropTypes.string.isRequired,
  users: PropTypes.instanceOf(Array).isRequired,
  talkId: PropTypes.string.isRequired,
  uid: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  subscribedUids: PropTypes.instanceOf(Array).isRequired,
  addCommentAppendContent: PropTypes.string.isRequired,
  setAddCommentAppendContent: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
};

export default AddTalkComment;
