import React, { useState, useEffect, useContext } from 'react';

import { useParams, useHistory } from 'react-router-dom';
import { useDocumentData, useCollectionData } from 'react-firebase-hooks/firestore';

import { db } from '../../firebase-config';
import CombinedUserContext from '../../currentUser/CombinedUserContext';
import PageLoadingSpinner from '../../shared/PageLoadingSpinner';
import PageLayout from '../../shared/layout/PageLayout';
import TalkPageBody from './TalkPageBody';
import DocPageBody from './DocPageBody';
import TalkPageSidebar from './talkPageSidebar/TalkPageSidebar';
import TalkPageDesktopHeader from './TalkPageDesktopHeader';
import updateUserFirestore from '../../shared/UserFirestore';
import PageNotFound from '../../shared/PageNotFound';
import './TalkPage.css';

const TalkPage = () => {
  const { spaceName: urlSpaceName, id } = useParams();

  const { combinedUser } = useContext(CombinedUserContext);
  const {
    uid,
    lastVisitedSpaceName,
    allowableSpaceNames,
    isAdmin,
  } = combinedUser;

  const [labels, labelsLoading, labelsError] = useCollectionData(db.collection(`spaces/${urlSpaceName}/labels`).orderBy('name', 'asc'), { idField: 'id' });
  const [users, usersLoading, usersError] = useCollectionData(db.collection(`spaces/${urlSpaceName}/copied-users`).orderBy('name', 'asc'), { idField: 'id' });

  const talkRef = db.collection(`spaces/${urlSpaceName}/talks`).doc(id);
  const [talk, talkLoading, talkError] = useDocumentData(talkRef, { idField: 'id' });

  const [comments, commentsLoading, commentsError] = useCollectionData(talkRef.collection('comments').orderBy('createdAt'), { idField: 'id' });

  const [isSaving, setIsSaving] = useState(false);
  const [isSaved, setIsSaved] = useState(false);
  const [isConvertingToDoc, setIsConvertingToDoc] = useState(false);

  const [title, setTitle] = useState('');
  const [isDoc, setIsDoc] = useState(false);
  const [usesReadOnlyEditor, setUsesReadOnlyEditor] = useState(false);
  const [contentEditorReady, setContentEditorReady] = useState(false);

  const [mobileHeaderTitle, setMobileHeaderTitle] = useState('');

  const history = useHistory();

  const onCommunitySpace = window.location.pathname.startsWith('/-/Ensembl%20Community');
  const isSpaceMember = allowableSpaceNames.includes(urlSpaceName);

  useEffect(async () => {
    if (talk) {
      if (uid) {
        updateUserFirestore(uid, { lastVisitedSpaceName: urlSpaceName });
      }
      const {
        title: talkTitle,
        isDoc: objectIsDoc,
        createdById,
      } = talk;

      document.title = talkTitle;
      setTitle(talkTitle);
      setIsDoc(objectIsDoc);
      setMobileHeaderTitle(talkTitle);

      setUsesReadOnlyEditor(
        onCommunitySpace && !isAdmin && (isDoc || (!isDoc && uid !== createdById)),
      );
    } else if (!talk && !talkLoading) {
      document.title = 'Page not found';
      setMobileHeaderTitle('Page not found');
    }
  }, [talk, talkLoading]);

  useEffect(() => {
    if (contentEditorReady) {
      const { hash } = history.location;
      const comment = document.getElementById(hash.substring(1));
      if (comment) {
        comment.scrollIntoView({ behavior: 'smooth', block: 'start' });
      }
    }
  }, [contentEditorReady]);

  return (
    <PageLayout
      uid={uid}
      onCommunitySpace={onCommunitySpace}
      urlSpaceName={urlSpaceName}
      lastVisitedSpaceName={lastVisitedSpaceName}
      allowableSpaceNames={allowableSpaceNames}
      mobileHeaderTitle={mobileHeaderTitle}
      mainContent={(
        <>
          {(labelsLoading || usersLoading || talkLoading || commentsLoading) && (
            <PageLoadingSpinner />
          )}
          {(labelsError || usersError || talkError || (!talkLoading && !talk) || commentsError) && (
            <PageNotFound />
          )}
          {labels && users && talk && comments && (
            <>
              <TalkPageDesktopHeader
                usesReadOnlyEditor={usesReadOnlyEditor}
                title={title}
                contentEditorReady={contentEditorReady}
                isSaving={isSaving}
                isSaved={isSaved}
              />
              <div className="ObjectPage__body">
                {!isDoc && !isConvertingToDoc && (
                  <TalkPageBody
                    usesReadOnlyEditor={usesReadOnlyEditor}
                    onCommunitySpace={onCommunitySpace}
                    isSpaceMember={isSpaceMember}
                    spaceName={urlSpaceName}
                    talk={talk}
                    comments={comments}
                    users={users}
                    save={{
                      isSaving,
                      setIsSaving,
                      isSaved,
                      setIsSaved,
                    }}
                    contentEditorReady={contentEditorReady}
                    setContentEditorReady={setContentEditorReady}
                  />
                )}
                {isDoc && !isConvertingToDoc && (
                  <DocPageBody
                    usesReadOnlyEditor={usesReadOnlyEditor}
                    onCommunitySpace={onCommunitySpace}
                    isSpaceMember={isSpaceMember}
                    spaceName={urlSpaceName}
                    talk={talk}
                    comments={comments}
                    users={users}
                    save={{
                      isSaving,
                      setIsSaving,
                      isSaved,
                      setIsSaved,
                    }}
                    contentEditorReady={contentEditorReady}
                    setContentEditorReady={setContentEditorReady}
                  />
                )}
              </div>
            </>
          )}
        </>
      )}
      talkSidebar={(
        <>
          {labels && users && talk && (
            <TalkPageSidebar
              onCommunitySpace={onCommunitySpace}
              isAdmin={isAdmin}
              isSpaceMember={isSpaceMember}
              spaceName={urlSpaceName}
              talk={talk}
              labels={labels}
              users={users}
              isConvertingToDoc={isConvertingToDoc}
              setIsConvertingToDoc={setIsConvertingToDoc}
            />
          )}
        </>
      )}
    />
  );
};

export default TalkPage;
