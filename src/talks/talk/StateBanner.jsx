import React from 'react';

import PropTypes from 'prop-types';
import StateBannerDisplay from './StateBannerDisplay';

const StateBanner = ({
  state,
  changedByName,
  changedByAvatarUrl,
  changedAtSeconds,
}) => {
  let message;
  switch (state) {
    case 'Archived':
      message = <span>&nbsp;archived&nbsp;</span>;
      break;
    case 'resolved-doc-comment-thread':
      message = <span>&nbsp;resolved&nbsp;</span>;
      break;
    case 'Converted':
      message = <span>&nbsp;converted from talk&nbsp;</span>;
      break;
    default:
      message = (
        <span>
          &nbsp;moved to
          <span className="font-weight-bold">
            &nbsp;
            {state}
            &nbsp;
          </span>
        </span>
      );
  }

  let alertVariant;
  switch (state) {
    case 'Archived':
    case 'In progress':
      alertVariant = 'warning';
      break;
    case 'Converted':
      alertVariant = 'secondary';
      break;
    default:
      alertVariant = 'success';
      break;
  }

  return (
    <div className="TalkStateBanner">
      <StateBannerDisplay
        alertVariant={alertVariant}
        message={message}
        changedByName={changedByName}
        changedByAvatarUrl={changedByAvatarUrl}
        changedAtSeconds={changedAtSeconds}
      />
    </div>
  );
};

StateBanner.propTypes = {
  // In progress and Resolved are for talks
  // Archived is for docs
  state: PropTypes.oneOf([
    'Archived', 'In progress', 'Resolved', 'resolved-doc-comment-thread', 'Converted']).isRequired,
  changedByName: PropTypes.string.isRequired,
  changedByAvatarUrl: PropTypes.string.isRequired,
  changedAtSeconds: PropTypes.number.isRequired,
};

export default StateBanner;
