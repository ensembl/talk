import React from 'react';

import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';

const NotifySubscribers = ({
  setAddCommentAppendContent,
}) => {
  const appendAddCommentAndScroll = () => {
    setAddCommentAppendContent('I just updated the talk proposal. Take a look and let me know what you think. Thanks!');
    const addComment = document.querySelector('.AddComment');
    if (addComment) {
      addComment.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
  };

  return (
    <Button
      className="btn-sm NotifySubscribers"
      variant="outline-primary"
      onClick={appendAddCommentAndScroll}
    >
      Notify subscribers of changes
    </Button>
  );
};

NotifySubscribers.propTypes = {
  setAddCommentAppendContent: PropTypes.func.isRequired,
};

export default NotifySubscribers;
