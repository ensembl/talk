import React from 'react';

import { Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

import './CommentsShowOlderComments.css';

const CommentsShowOlderComments = ({
  commentsLength, maxComments, showOlderComments, setShowOlderComments,
}) => (
  commentsLength > maxComments && !showOlderComments && (
    <div className="CommentsShowOlderComments">
      <Button
        type="button"
        onClick={() => setShowOlderComments(true)}
        variant="outline-primary"
        block
      >
        {`Show ${commentsLength - maxComments} older comment${commentsLength - maxComments === 1 ? '' : 's'}`}
      </Button>
    </div>
  )
);

CommentsShowOlderComments.propTypes = {
  commentsLength: PropTypes.number.isRequired,
  maxComments: PropTypes.number.isRequired,
  showOlderComments: PropTypes.bool.isRequired,
  setShowOlderComments: PropTypes.func.isRequired,
};

export default CommentsShowOlderComments;
