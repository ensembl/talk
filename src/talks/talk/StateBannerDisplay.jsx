import React from 'react';

import PropTypes from 'prop-types';
import {
  Alert,
  OverlayTrigger,
  Tooltip,
} from 'react-bootstrap';
import TimeAgo from 'react-timeago';

import Avatar from '../../shared/Avatar';
import timeAgoFormatter from '../../shared/TimeAgoFormatter';
import tooltipDatetimeFormatter from '../../shared/TooltipDatetimeFormatter';
import './StateBannerDisplay.css';

const StateBannerDisplay = ({
  alertVariant,
  message,
  changedByName,
  changedByAvatarUrl,
  changedAtSeconds,
}) => (
  <div className="StateBannerDisplay">
    <Alert variant={alertVariant}>
      <Avatar
        avatarUrl={changedByAvatarUrl}
      />
      <span className="font-weight-bold">{changedByName}</span>
      {message}
      <OverlayTrigger
        delay={{ show: 50, hide: 50 }}
        overlay={
          <Tooltip>{tooltipDatetimeFormatter(changedAtSeconds)}</Tooltip>
        }
      >
        <TimeAgo date={new Date(changedAtSeconds * 1000)} minPeriod="60" formatter={timeAgoFormatter} />
      </OverlayTrigger>
    </Alert>
  </div>
);

StateBannerDisplay.propTypes = {
  alertVariant: PropTypes.oneOf(['warning', 'success', 'secondary']).isRequired,
  message: PropTypes.node.isRequired,
  changedByName: PropTypes.string.isRequired,
  changedByAvatarUrl: PropTypes.string.isRequired,
  changedAtSeconds: PropTypes.number.isRequired,
};

export default StateBannerDisplay;
