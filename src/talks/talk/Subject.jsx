import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';

import SubjectTimestamps from '../shared/SubjectTimestamps';
import EditSubject from './EditSubject';
import IsSaving from './IsSaving';
import NotifySubscribers from './NotifySubscribers';
import '../../shared/ckeditor/CKEditorStyles.css';
import './Subject.css';

const Subject = ({
  usesReadOnlyEditor,
  onCommunitySpace,
  spaceName,
  talk,
  users,
  save,
  setAddCommentAppendContent,
  contentEditorReady,
  setContentEditorReady,
  setComment,
  updateTalk,
  setShowThreadResolvedMessage,
}) => {
  const {
    id: talkId,
    htmlSubject,
    createdById,
    createdByAvatarUrl,
    createdByName,
    createdAt,
    updatedById,
    updatedByAvatarUrl,
    updatedByName,
    updatedAt,
    isDoc = false,
  } = talk;

  const {
    isSaving,
    setIsSaving,
    isSaved,
    setIsSaved,
  } = save;

  const type = isDoc ? 'doc' : 'talk';

  const [savedOnce, setSavedOnce] = useState(false);

  useEffect(() => {
    if (isSaved) {
      setSavedOnce(true);
    }
  }, [isSaved]);

  const creators = users.filter((user) => user.id === createdById);
  const creatorAvatarUrl = creators.length > 0 ? creators[0].avatarUrl : createdByAvatarUrl;
  const creatorName = creators.length > 0 ? creators[0].name : createdByName;

  const updaters = users.filter((user) => user.id === updatedById);
  const updaterAvatarUrl = updaters.length > 0 ? updaters[0].avatarUrl : updatedByAvatarUrl;
  const updaterName = updaters.length > 0 ? updaters[0].name : updatedByName;

  return (
    <div className={`Subject__${type}`}>
      <div className="Subject__header">
        <SubjectTimestamps
          createdByAvatarUrl={creatorAvatarUrl}
          createdByName={creatorName}
          createdAtSeconds={createdAt.seconds}
          updatedByAvatarUrl={updaterAvatarUrl}
          updatedByName={updaterName}
          updatedAtSeconds={!updatedAt ? -1 : updatedAt.seconds}
        />
        {!usesReadOnlyEditor && (
          <IsSaving
            contentEditorReady={contentEditorReady}
            isSaving={isSaving}
            isSaved={isSaved}
          />
        )}
      </div>
      <EditSubject
        usesReadOnlyEditor={usesReadOnlyEditor}
        onCommunitySpace={onCommunitySpace}
        spaceName={spaceName}
        talkId={talkId}
        htmlSubject={htmlSubject}
        users={users}
        setIsSaving={setIsSaving}
        setIsSaved={setIsSaved}
        updatedByName={updatedByName}
        type={type}
        contentEditorReady={contentEditorReady}
        setContentEditorReady={setContentEditorReady}
        setComment={setComment}
        updateTalk={updateTalk}
        setShowThreadResolvedMessage={setShowThreadResolvedMessage}
      />
      <div className="Subject__footer">
        {setAddCommentAppendContent && savedOnce && (
          <NotifySubscribers
            setAddCommentAppendContent={setAddCommentAppendContent}
          />
        )}
      </div>
    </div>
  );
};

Subject.propTypes = {
  usesReadOnlyEditor: PropTypes.bool.isRequired,
  onCommunitySpace: PropTypes.bool.isRequired,
  spaceName: PropTypes.string.isRequired,
  talk: PropTypes.objectOf(PropTypes.any).isRequired,
  users: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    avatarUrl: PropTypes.string,
  })).isRequired,
  save: PropTypes.exact({
    isSaving: PropTypes.bool.isRequired,
    setIsSaving: PropTypes.func.isRequired,
    isSaved: PropTypes.bool.isRequired,
    setIsSaved: PropTypes.func.isRequired,
  }).isRequired,
  setAddCommentAppendContent: PropTypes.func,
  contentEditorReady: PropTypes.bool.isRequired,
  setContentEditorReady: PropTypes.func.isRequired,
  setComment: PropTypes.func,
  updateTalk: PropTypes.func.isRequired,
  setShowThreadResolvedMessage: PropTypes.func,
};

Subject.defaultProps = {
  setAddCommentAppendContent: null,
  setComment: null,
  setShowThreadResolvedMessage: null,
};

export default Subject;
