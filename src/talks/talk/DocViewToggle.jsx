import React from 'react';

import PropTypes from 'prop-types';
import Pills from '../shared/Pills';

import './DocViewToggle.css';

const DocViewToggle = ({
  view,
  setView,
  threadsCount,
  setShowThreadResolvedMessage,
}) => {
  const onChange = (event) => {
    if (event === 'comment-threads-only') {
      setShowThreadResolvedMessage(false);
    }
    setView(event);
  };

  return (
    <div className="DocViewToggle">
      <Pills
        pills={[
          { type: 'doc', name: 'Doc + comment threads' },
          { type: 'comment-threads-only', name: `Comment threads (${threadsCount})` },
        ]}
        activeKey={view}
        selectPill={onChange}
      />
    </div>

  );
};

DocViewToggle.propTypes = {
  view: PropTypes.oneOf(['doc', 'comment-threads-only']).isRequired,
  setView: PropTypes.func.isRequired,
  threadsCount: PropTypes.number.isRequired,
  setShowThreadResolvedMessage: PropTypes.func.isRequired,
};

export default DocViewToggle;
