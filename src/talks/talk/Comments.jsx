import React, { useState, useContext } from 'react';

import PropTypes from 'prop-types';

import updateUserFirestore from '../../shared/UserFirestore';
import CombinedUserContext from '../../currentUser/CombinedUserContext';
import OnboardingMessage from '../../shared/OnboardingMessage';
import Comment from '../../shared/comment/Comment';
import AddTalkComment from './AddTalkComment';
import CommentsShowOlderComments from './CommentsShowOlderComments';

import './Comments.css';

const Comments = ({
  onCommunitySpace,
  isSpaceMember,
  spaceName,
  talkId,
  comments,
  addComment,
  updateComment,
  addCommentAppendContent,
  setAddCommentAppendContent,
  users,
  subscribedUids,
}) => {
  const { combinedUser } = useContext(CombinedUserContext);
  const {
    uid,
    name,
    avatarUrl,
    dismissedCommentOnboarding,
  } = combinedUser;

  // Change this to change how many comments to display
  const maxComments = 15;

  const hashId = window.location.hash.split('_')[1];
  const commentIndex = comments.findIndex((comment) => comment.id === hashId);

  const [showOlderComments, setShowOlderComments] = useState(
    commentIndex >= 0 && commentIndex < (comments.length - maxComments),
  );

  const dismissCommentOnboarding = async () => {
    await updateUserFirestore(
      uid,
      {
        dismissedCommentOnboarding: true,
      },
    );
  };

  return (
    <div className="Comments">
      <CommentsShowOlderComments
        commentsLength={comments.length}
        maxComments={maxComments}
        showOlderComments={showOlderComments}
        setShowOlderComments={setShowOlderComments}
      />
      {comments && comments.filter((comment) => !comment.toDelete).map((comment, index) => {
        if (!showOlderComments) {
          return comments.length - maxComments <= index && (
            <Comment
              key={comment.id}
              spaceName={spaceName}
              comment={comment}
              users={users}
              updateComment={updateComment}
            />
          );
        }
        return (
          <Comment
            key={comment.id}
            spaceName={spaceName}
            comment={comment}
            users={users}
            updateComment={updateComment}
          />
        );
      })}
      {isSpaceMember && (
        <AddTalkComment
          spaceName={spaceName}
          talkId={talkId}
          users={users}
          uid={uid}
          name={name}
          avatarUrl={avatarUrl}
          subscribedUids={subscribedUids}
          addCommentAppendContent={addCommentAppendContent}
          setAddCommentAppendContent={setAddCommentAppendContent}
          addComment={addComment}
        />
      )}
      {onCommunitySpace && !isSpaceMember && (
        <div className="Comments__join-ensembl-community">
          <a
            href="/-/Ensembl%20Community/join"
          >
            Join Ensembl Community to write a comment
          </a>
        </div>
      )}
      {!dismissedCommentOnboarding && !onCommunitySpace && (
        <OnboardingMessage
          title="Send a message to a teammate 💬"
          body={(
            <p>
              Write a message ☝️ to a teammate by @ mentioning them,
              so that they get a notification.
              Keep it brief and to the point.
              Consider creating a new talk to keep this talk focused.
            </p>
          )}
          dismissForever={dismissCommentOnboarding}
        />
      )}
    </div>
  );
};

Comments.propTypes = {
  onCommunitySpace: PropTypes.bool.isRequired,
  isSpaceMember: PropTypes.bool.isRequired,
  spaceName: PropTypes.string.isRequired,
  talkId: PropTypes.string.isRequired,
  comments: PropTypes.instanceOf(Array).isRequired,
  addComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  addCommentAppendContent: PropTypes.string.isRequired,
  setAddCommentAppendContent: PropTypes.func.isRequired,
  users: PropTypes.instanceOf(Array).isRequired,
  subscribedUids: PropTypes.instanceOf(Array).isRequired,
};

export default Comments;
