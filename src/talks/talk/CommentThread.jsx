import React from 'react';

import PropTypes from 'prop-types';
import Comment from '../../shared/comment/Comment';
import StateBanner from './StateBanner';

import './CommentThread.css';

const CommentThread = ({
  users,
  comments,
  resolvedByName,
  resolvedByAvatarUrl,
  resolvedAtSeconds,
  convertedByName,
  convertedByAvatarUrl,
  convertedAtSeconds,
}) => {
  const { markerContent = '' } = comments[0];
  return (
    <div className="CommentThread">
      <div className="CommentThread__header">
        {convertedByName && <span>Comments from talk before it was converted to this doc</span>}
        {!convertedByName && (
          <>
            <span>Relates to:</span>
            <span className="CommentThread__message--highlight">{markerContent}</span>
          </>
        )}
      </div>
      {comments.map((comment) => (
        <Comment
          key={comment.id}
          comment={comment}
          users={users}
          readOnly
        />
      ))}
      {(resolvedByName || convertedByName) && (
        <StateBanner
          state={resolvedByName ? 'resolved-doc-comment-thread' : 'Converted'}
          changedByName={resolvedByName || convertedByName}
          changedByAvatarUrl={resolvedByAvatarUrl || convertedByAvatarUrl}
          changedAtSeconds={resolvedByName ? resolvedAtSeconds : convertedAtSeconds}
        />
      )}
    </div>
  );
};

CommentThread.propTypes = {
  users: PropTypes.arrayOf(
    PropTypes.shape({
      avatarUrl: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }),
  ).isRequired,
  comments: PropTypes.arrayOf(PropTypes.shape({
    threadId: PropTypes.string.isRequired,
    createdAt: PropTypes.objectOf(Date).isRequired,
    createdByAvatarUrl: PropTypes.string.isRequired,
    createdById: PropTypes.string.isRequired,
    createdByName: PropTypes.string.isRequired,
    htmlBody: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    markerContent: PropTypes.string,
    mentions: PropTypes.objectOf(PropTypes.any).isRequired,
  })).isRequired,
  resolvedByName: PropTypes.string.isRequired,
  resolvedByAvatarUrl: PropTypes.string.isRequired,
  resolvedAtSeconds: PropTypes.number.isRequired,
  convertedByName: PropTypes.string.isRequired,
  convertedByAvatarUrl: PropTypes.string.isRequired,
  convertedAtSeconds: PropTypes.number.isRequired,
};

export default CommentThread;
