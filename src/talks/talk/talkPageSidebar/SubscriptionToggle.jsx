import React, { useContext } from 'react';

import PropTypes from 'prop-types';
import firebase from 'firebase/app';

import { updateTalkFirestore } from '../../../shared/TalkFirestore';
import CombinedUserContext from '../../../currentUser/CombinedUserContext';
import SubscriptionToggleDisplay from './SubscriptionToggleDisplay';

const SubscriptionToggle = ({
  spaceName,
  talkId,
  subscribedUids,
}) => {
  const { combinedUser } = useContext(CombinedUserContext);
  const { uid } = combinedUser;

  const toggleSubscription = async (e) => {
    if (e.target.checked) {
      await updateTalkFirestore(
        spaceName,
        talkId,
        {
          subscribedUids: firebase.firestore.FieldValue.arrayUnion(uid),
        },
      );
    } else {
      await updateTalkFirestore(
        spaceName,
        talkId,
        {
          subscribedUids: firebase.firestore.FieldValue.arrayRemove(uid),
        },
      );
    }
  };

  return (
    <SubscriptionToggleDisplay
      isSubscribed={subscribedUids.includes(uid)}
      toggleSubscription={toggleSubscription}
    />
  );
};

SubscriptionToggle.propTypes = {
  spaceName: PropTypes.string.isRequired,
  talkId: PropTypes.string.isRequired,
  subscribedUids: PropTypes.arrayOf(PropTypes.any).isRequired,
};

export default SubscriptionToggle;
