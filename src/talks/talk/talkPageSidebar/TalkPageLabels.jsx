import React, { useEffect, useState } from 'react';

import PropTypes from 'prop-types';
import firebase from 'firebase/app';

import { updateTalkFirestore } from '../../../shared/TalkFirestore';
import TalkPageLabelsDisplay from './TalkPageLabelsDisplay';

const TalkPageLabels = ({
  canChange,
  spaceName,
  talkId,
  talkLabelIds,
  labels,
  isDoc,
}) => {
  const [addedLabels, setAddedLabels] = useState([]);
  const [notAddedLabels, setNotAddedLabels] = useState([]);

  useEffect(() => {
    setAddedLabels(
      labels.filter((label) => talkLabelIds.includes(label.id)),
    );
    setNotAddedLabels(
      labels.filter((label) => !talkLabelIds.includes(label.id)),
    );
  }, [talkLabelIds]);

  const addLabel = async (labelId) => {
    await updateTalkFirestore(
      spaceName,
      talkId,
      {
        labelIds: firebase.firestore.FieldValue.arrayUnion(labelId),
      },
    );
  };

  const removeLabel = async (labelId) => {
    await updateTalkFirestore(
      spaceName,
      talkId,
      {
        labelIds: firebase.firestore.FieldValue.arrayRemove(labelId),
      },
    );
  };

  return (
    <TalkPageLabelsDisplay
      canChange={canChange}
      addedLabels={addedLabels}
      notAddedLabels={notAddedLabels}
      addLabel={addLabel}
      removeLabel={removeLabel}
      isDoc={isDoc}
      spaceName={spaceName}
    />
  );
};

TalkPageLabels.propTypes = {
  canChange: PropTypes.bool.isRequired,
  spaceName: PropTypes.string.isRequired,
  talkId: PropTypes.string.isRequired,
  talkLabelIds: PropTypes.arrayOf(PropTypes.any).isRequired,
  labels: PropTypes.arrayOf(PropTypes.any).isRequired,
  isDoc: PropTypes.bool.isRequired,
};

export default TalkPageLabels;
