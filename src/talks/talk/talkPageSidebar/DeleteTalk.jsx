import React, { useState } from 'react';

import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

import { updateTalkFirestore } from '../../../shared/TalkFirestore';
import DeleteTalkDisplay from './DeleteTalkDisplay';

const DeleteTalk = ({
  spaceName,
  talkId,
  isDoc,
}) => {
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [isDeleting, setIsDeleting] = useState(false);

  const history = useHistory();

  const deleteFromModal = async () => {
    setIsDeleting(true);
    await updateTalkFirestore(
      spaceName,
      talkId,
      {
        toDelete: true,
      },
    );
    history.replace('/home');
  };

  return (
    <DeleteTalkDisplay
      showDeleteModal={showDeleteModal}
      setShowDeleteModal={setShowDeleteModal}
      isDeleting={isDeleting}
      deleteFromModal={deleteFromModal}
      type={isDoc ? 'doc' : 'talk'}
    />
  );
};

DeleteTalk.propTypes = {
  spaceName: PropTypes.string.isRequired,
  talkId: PropTypes.string.isRequired,
  isDoc: PropTypes.bool.isRequired,
};

export default DeleteTalk;
