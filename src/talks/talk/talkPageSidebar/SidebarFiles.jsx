import React from 'react';

import PropTypes from 'prop-types';

import SidebarFile from './SidebarFile';
import UploadSidebarFile from './UploadSidebarFile';
import './SidebarFiles.css';

const SidebarFiles = ({
  spaceName,
  talkId,
  files,
}) => (
  <div className="SidebarFiles">
    <div className="SidebarFiles__title">
      PowerPoint
    </div>
    <>
      {Object.keys(files).length > 0 && (
        <div className="SidebarFiles__files">
          {Object.keys(files).sort().map((key) => (
            <SidebarFile
              key={files[key].name}
              spaceName={spaceName}
              talkId={talkId}
              name={files[key].name}
              downloadUrl={files[key].downloadUrl}
            />
          ))}
        </div>
      )}
      {Object.keys(files).length < 7 && (
        <UploadSidebarFile
          spaceName={spaceName}
          talkId={talkId}
          fileNames={Object.values(files).map((value) => value.name)}
        />
      )}
    </>
  </div>
);

SidebarFiles.propTypes = {
  spaceName: PropTypes.string.isRequired,
  talkId: PropTypes.string.isRequired,
  files: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default SidebarFiles;
