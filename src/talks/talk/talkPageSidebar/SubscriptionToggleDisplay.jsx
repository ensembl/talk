import React from 'react';

import Toggle from 'react-toggle';
import 'react-toggle/style.css';
import PropTypes from 'prop-types';

import './SubscriptionToggleDisplay.css';

const SubscriptionToggleDisplay = ({
  isSubscribed,
  toggleSubscription,
}) => (
  <span className="subscription-toggle">
    <span className="subscription-toggle-label">Notifications</span>
    <Toggle
      checked={isSubscribed}
      name="talkSubscription"
      onChange={toggleSubscription}
    />
  </span>
);

SubscriptionToggleDisplay.propTypes = {
  isSubscribed: PropTypes.bool.isRequired,
  toggleSubscription: PropTypes.func.isRequired,
};

export default SubscriptionToggleDisplay;
