import React from 'react';

import PropTypes from 'prop-types';

import AvatarSet from '../../../shared/AvatarSet';
import AddSelf from '../../shared/AddSelf';
import './TalkPageOwnersDisplay.css';

const TalkPageOwnersDisplay = ({
  addedOwners,
  notAddedOwners,
  addOwner,
  removeOwner,
  spaceName,
  uid,
}) => (
  <>
    <div className="TalkPageOwnersDisplay__title">
      {addedOwners.length === 0 && <span>No owners</span>}
      {addedOwners.length > 0 && <span>Owners</span>}
    </div>
    <div className="TalkPageOwnersDisplay__selections">
      <AvatarSet
        addedAvatars={addedOwners}
        notAddedAvatars={notAddedOwners}
        addAvatar={addOwner}
        removeAvatar={removeOwner}
        addAvatarText="Add owner"
        spaceName={spaceName}
      />
      {notAddedOwners.map((avatar) => avatar.id).includes(uid) && (
        <AddSelf
          addSelf={addOwner}
          uid={uid}
        />
      )}
    </div>
  </>
);

TalkPageOwnersDisplay.propTypes = {
  addedOwners: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string.isRequired,
  })).isRequired,
  notAddedOwners: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string.isRequired,
  })).isRequired,
  addOwner: PropTypes.func.isRequired,
  removeOwner: PropTypes.func.isRequired,
  spaceName: PropTypes.string.isRequired,
  uid: PropTypes.string.isRequired,
};

export default TalkPageOwnersDisplay;
