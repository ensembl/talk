import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import ConvertTalkToDocDisplay from './ConvertTalkToDocDisplay';

import './ConvertTalkToDoc.css';

const ConvertTalkToDoc = ({
  updateTalk,
  uid,
  avatarUrl,
  name,
  spaceName,
  talkId,
  isConvertingToDoc,
  setIsConvertingToDoc,
}) => {
  const history = useHistory();

  const [showModal, setShowModal] = useState(false);

  const convertTalkToDocument = async () => {
    try {
      await updateTalk({
        convertedToDocAt: new Date(),
        convertedToDocById: uid,
        convertedToDocByName: name,
        convertedToDocByAvatarUrl: avatarUrl,
        threadsMeta: {
          [talkId]: {
            convertedById: uid,
            convertedByName: name,
            convertedByAvatarUrl: avatarUrl,
            convertedAt: new Date(),
          },
        },
        isDoc: true,
        state: 'Open',
      });
    } catch (error) {
      throw new Error('Error converting talk to doc');
    }
  };

  const onConfirm = async () => {
    setIsConvertingToDoc(true);
    await convertTalkToDocument();
    setIsConvertingToDoc(false);
    history.replace(`/-/${spaceName}/docs/${talkId}`);
  };

  const onCancel = () => {
    setShowModal(false);
  };

  return (
    <ConvertTalkToDocDisplay
      onConfirm={onConfirm}
      onCancel={onCancel}
      isConvertingToDoc={isConvertingToDoc}
      showModal={showModal}
      setShowModal={setShowModal}
    />
  );
};

ConvertTalkToDoc.propTypes = {
  updateTalk: PropTypes.func.isRequired,
  uid: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  talkId: PropTypes.string.isRequired,
  spaceName: PropTypes.string.isRequired,
  isConvertingToDoc: PropTypes.bool.isRequired,
  setIsConvertingToDoc: PropTypes.func.isRequired,
};

export default ConvertTalkToDoc;
