import React, { useState, useRef } from 'react';

import PropTypes from 'prop-types';
import { ProgressBar } from 'react-bootstrap';
import { Upload } from 'react-bootstrap-icons';

import { storage } from '../../../firebase-config';
import { updateTalkFirestore } from '../../../shared/TalkFirestore';

import './UploadSidebarFile.css';

const UploadSidebarFile = ({
  spaceName,
  talkId,
  fileNames,
}) => {
  const [uploading, setUploading] = useState(false);
  const [uploadProgress, setUploadProgress] = useState(null);
  const [hasError, setHasError] = useState(false);

  const inputEl = useRef(null);

  const removeExtension = (name) => name.split('.').slice(0, -1).join('.');
  const extension = (name) => name.split('.').slice(-1)[0];

  const uniqueName = (name) => {
    const baseNames = fileNames.map((fileName) => removeExtension(fileName));
    const starterBaseName = removeExtension(name);
    let candidateBaseName = starterBaseName;
    let i = 0;
    while (baseNames.includes(candidateBaseName)) {
      i += 1;
      candidateBaseName = `${starterBaseName}-${i}`;
    }
    return `${candidateBaseName}.${extension(name)}`;
  };

  const uploadFile = async (e) => {
    const file = e.target.files[0];
    const uniqueFileName = uniqueName(file.name);
    const fileRef = storage.ref().child(`uploads/${spaceName}/${talkId}/sidebar/${uniqueFileName}`);
    const uploadTask = fileRef.put(file);
    uploadTask.on('state_changed',
      (snapshot) => {
        setHasError(false);
        setUploading(true);
        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        setUploadProgress(progress);
      },
      (error) => {
        try {
          throw new Error('Error uploading file', error);
        } catch (err) {
          setHasError(true);
          setUploading(false);
        }
      },
      async () => {
        // Cannot use file name as key since dots break the write-directly-to-map-field notation
        const key = uniqueFileName.split('.').join('');
        let downloadUrl;
        try {
          downloadUrl = await uploadTask.snapshot.ref.getDownloadURL();
        } catch (error) {
          throw new Error('Error getting downloadUrl', error);
        }
        await updateTalkFirestore(spaceName, talkId, {
          [`sidebarFiles.${key}.name`]: uniqueFileName,
          [`sidebarFiles.${key}.downloadUrl`]: downloadUrl,
        });
        setUploading(false);
      });
  };

  const clickUploadLink = () => {
    inputEl.current.click();
  };

  return (
    <>
      {uploading && <ProgressBar animated now={uploadProgress} />}
      {!uploading && (
        <div className="UploadSidebarFile__upload">
          <button type="button" onClick={clickUploadLink}>
            <span className="UploadSidebarFile__upload--icon"><Upload /></span>
            <span>Upload a PowerPoint file</span>
          </button>
          <input ref={inputEl} type="file" onChange={uploadFile} hidden />
        </div>
      )}
      {hasError
        && (
          <span className="UploadSidebarFile__error">
            Try again. Upload a PowerPoint file up to 500 MB.
          </span>
        )}
    </>
  );
};

UploadSidebarFile.propTypes = {
  spaceName: PropTypes.string.isRequired,
  talkId: PropTypes.string.isRequired,
  fileNames: PropTypes.instanceOf(Array).isRequired,
};

export default UploadSidebarFile;
