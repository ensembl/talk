import React from 'react';

import PropTypes from 'prop-types';

import { Dropdown, Spinner } from 'react-bootstrap';

const MergeRequestButton = ({ isCreatingMergeRequest, createMergeRequest }) => {
  const onClick = (repoName) => {
    createMergeRequest(repoName);
  };

  return (
    <Dropdown>
      <Dropdown.Toggle disabled={isCreatingMergeRequest} variant="outline-primary" size="sm">
        {isCreatingMergeRequest && <Spinner animation="border" size="sm" />}
        Create Merge Request
      </Dropdown.Toggle>
      <Dropdown.Menu>
        <Dropdown.Item onClick={() => onClick('talk')}>Ensembl Talk</Dropdown.Item>
        <Dropdown.Item onClick={() => onClick('dev1on1')}>Dev 1 on 1</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
};

MergeRequestButton.propTypes = {
  isCreatingMergeRequest: PropTypes.bool.isRequired,
  createMergeRequest: PropTypes.func.isRequired,
};

export default MergeRequestButton;
