import React from 'react';

import { Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

import { updateTalkFirestore } from '../../../shared/TalkFirestore';

const DocStateChanger = ({
  spaceName,
  talkId,
  state,
  uid,
  name,
  avatarUrl,
}) => {
  const changeState = async (targetState) => {
    await updateTalkFirestore(
      spaceName,
      talkId,
      {
        state: targetState,
        changedStateById: uid,
        changedStateByName: name,
        changedStateByAvatarUrl: avatarUrl,
        changedStateAt: new Date(),
      },
    );
  };

  return (
    <Button
      onClick={() => { changeState(state === 'Archived' ? 'Open' : 'Archived'); }}
      className="btn-sm"
      variant="outline-primary"
    >
      {state === 'Archived' ? 'Unarchive doc' : 'Archive doc'}
    </Button>
  );
};

DocStateChanger.propTypes = {
  spaceName: PropTypes.string.isRequired,
  talkId: PropTypes.string.isRequired,
  state: PropTypes.oneOf(['Open', 'Archived']).isRequired,
  uid: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
};

export default DocStateChanger;
