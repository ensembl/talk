import React from 'react';

import PropTypes from 'prop-types';
import { DropdownButton, Dropdown, Alert } from 'react-bootstrap';

import './TalkStateChangerDisplay.css';

const TalkStateChangerDisplay = ({
  canChange,
  state,
  changeState,
}) => {
  let variant;
  switch (state) {
    case 'Open': variant = 'secondary'; break;
    case 'In progress': variant = 'warning'; break;
    case 'Resolved': variant = 'success'; break;
    default:
      throw new Error('Error state');
  }
  return (
    <>
      <div className="TalkStateChangerDisplay__state">
        <Alert variant={variant}>
          {state}
        </Alert>
      </div>
      <DropdownButton
        size="sm"
        variant="outline-primary"
        title="Change state"
        disabled={!canChange}
      >
        {['Open', 'In progress', 'Resolved'].map((eachState) => (
          <Dropdown.Item
            key={eachState}
            onClick={() => { changeState(eachState); }}
            disabled={eachState === state}
          >
            {`${eachState === state ? '\u2713' : ''} ${eachState}`}
          </Dropdown.Item>
        ))}
      </DropdownButton>
    </>
  );
};

TalkStateChangerDisplay.propTypes = {
  canChange: PropTypes.bool.isRequired,
  state: PropTypes.oneOf(['Open', 'In progress', 'Resolved']).isRequired,
  changeState: PropTypes.func.isRequired,
};

export default TalkStateChangerDisplay;
