import React from 'react';

import PropTypes from 'prop-types';

import DeleteModal from '../../../shared/DeleteModal';
import './DeleteTalkDisplay.css';

const DeleteTalkDisplay = ({
  showDeleteModal,
  setShowDeleteModal,
  isDeleting,
  deleteFromModal,
  type,
}) => (
  <>
    <button
      type="button"
      className="DeleteTalkDisplay__link"
      onClick={() => { setShowDeleteModal(true); }}
    >
      {`Delete ${type}`}
    </button>
    <DeleteModal
      showDeleteModal={showDeleteModal}
      setShowDeleteModal={setShowDeleteModal}
      isDeleting={isDeleting}
      deleteFromModal={deleteFromModal}
      modalTitle={`Delete ${type} forever`}
      modalBody={(
        <>
          <p>
            Are you sure? Deleted
            <span>{` ${type}s `}</span>
            cannot be recovered.
            Everything in the
            <span>{` ${type} `}</span>
            will be deleted, including everyone&apos;s comments.
          </p>
          {type === 'doc' && (
            <p>
              Consider archiving the doc instead.
            </p>
          )}
        </>
      )}
      modalDeleteButtonLabel={`Delete ${type} and comments forever`}
    />
  </>
);

DeleteTalkDisplay.propTypes = {
  showDeleteModal: PropTypes.bool.isRequired,
  setShowDeleteModal: PropTypes.func.isRequired,
  isDeleting: PropTypes.bool.isRequired,
  deleteFromModal: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
};

export default DeleteTalkDisplay;
