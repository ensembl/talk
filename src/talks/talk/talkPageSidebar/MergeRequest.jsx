import React, { useState } from 'react';

import PropTypes from 'prop-types';
import { functions } from '../../../firebase-config';
import MergeRequestButton from './MergeRequestButton';
import MergeRequestLinks from './MergeRequestLinks';

const MergeRequest = ({
  branchName, issueTitle, mergeRequestUrls, talkId, spaceName,
}) => {
  const [isCreatingMergeRequest, setIsCreatingMergeRequest] = useState(false);

  const createMergeRequest = async (repoName) => {
    setIsCreatingMergeRequest(true);
    const callable = functions.httpsCallable('gitlab-createMergeRequest');
    try {
      await callable({
        branchName,
        issueTitle,
        talkId,
        spaceName,
        repoName,
      });
    } catch (error) {
      throw new Error('Error creating merge request', error);
    }
    setIsCreatingMergeRequest(false);
  };

  return (
    <>
      <MergeRequestLinks
        mergeRequestUrls={mergeRequestUrls}
      />
      {mergeRequestUrls.length === 0 && (
        <MergeRequestButton
          isCreatingMergeRequest={isCreatingMergeRequest}
          createMergeRequest={createMergeRequest}
        />
      )}
    </>

  );
};

MergeRequest.propTypes = {
  branchName: PropTypes.string.isRequired,
  issueTitle: PropTypes.string.isRequired,
  mergeRequestUrls: PropTypes.arrayOf(PropTypes.string),
  talkId: PropTypes.string.isRequired,
  spaceName: PropTypes.string.isRequired,
};

MergeRequest.defaultProps = {
  mergeRequestUrls: [],
};

export default MergeRequest;
