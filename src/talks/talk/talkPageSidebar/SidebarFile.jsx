import React from 'react';

import firebase from 'firebase/app';
import PropTypes from 'prop-types';
import {
  OverlayTrigger, Tooltip,
} from 'react-bootstrap';
import { X } from 'react-bootstrap-icons';

import { storage } from '../../../firebase-config';
import { updateTalkFirestore } from '../../../shared/TalkFirestore';
import './SidebarFile.css';

const SidebarFile = ({
  spaceName,
  talkId,
  name,
  downloadUrl,
}) => {
  const deleteFile = async () => {
    const key = name.split('.').join('');
    await updateTalkFirestore(spaceName, talkId, {
      [`sidebarFiles.${key}`]: firebase.firestore.FieldValue.delete(),
    });
    try {
      await storage.ref().child(`uploads/${spaceName}/${talkId}/sidebar/${name}`).delete();
    } catch (error) {
      throw new Error('Error deleting file', error);
    }
  };

  return (
    <div className="SidebarFile">
      <a className="SidebarFile__url" href={downloadUrl}>
        {name}
      </a>
      <span className="SidebarFile__delete">
        <OverlayTrigger
          delay={{ show: 50, hide: 50 }}
          overlay={<Tooltip>Delete file</Tooltip>}
        >
          <X onClick={deleteFile} size="20" />
        </OverlayTrigger>
      </span>
    </div>
  );
};

SidebarFile.propTypes = {
  spaceName: PropTypes.string.isRequired,
  talkId: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  downloadUrl: PropTypes.string.isRequired,
};

export default SidebarFile;
