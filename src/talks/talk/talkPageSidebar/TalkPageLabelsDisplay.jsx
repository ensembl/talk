import React from 'react';

import PropTypes from 'prop-types';

import LabelSet from '../../shared/LabelSet';
import './TalkPageLabelsDisplay.css';

const TalkPageLabelsDisplay = ({
  canChange,
  addedLabels,
  notAddedLabels,
  addLabel,
  removeLabel,
  isDoc,
  spaceName,
}) => (
  <>
    <div className="TalkPageLabelsDisplay__title">
      {addedLabels.length === 0 && <span>No labels</span>}
      {addedLabels.length > 0 && <span>Labels</span>}
    </div>
    <LabelSet
      canChange={canChange}
      addedLabels={addedLabels}
      notAddedLabels={notAddedLabels}
      addLabel={addLabel}
      addLabelText="Attach label"
      removeLabel={removeLabel}
      type={isDoc ? 'doc' : 'talk'}
      dropdownOnNewline
      spaceName={spaceName}
    />
  </>
);

TalkPageLabelsDisplay.propTypes = {
  canChange: PropTypes.bool.isRequired,
  addedLabels: PropTypes.instanceOf(Array).isRequired,
  notAddedLabels: PropTypes.instanceOf(Array).isRequired,
  addLabel: PropTypes.func.isRequired,
  removeLabel: PropTypes.func.isRequired,
  isDoc: PropTypes.bool.isRequired,
  spaceName: PropTypes.string.isRequired,
};

export default TalkPageLabelsDisplay;
