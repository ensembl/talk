import React from 'react';

import { Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

import AvatarSet from '../../../shared/AvatarSet';
import './ApprovalsDisplay.css';

const ApprovalsDisplay = ({
  canApprove,
  addedApprovers,
  isApproved,
  toggleApproval,
}) => (
  <div className="ApprovalsDisplay">
    <div className="ApprovalsDisplay__title">
      {addedApprovers.length === 0 && <span>No approvals</span>}
      {addedApprovers.length > 0 && <span>Approvals</span>}
    </div>
    {addedApprovers.length > 0 && (
      <AvatarSet
        addedAvatars={addedApprovers}
      />
    )}
    {canApprove && (
      <div>
        {!isApproved && <Button onClick={toggleApproval} className="btn-sm" variant="outline-primary">Approve</Button>}
        {isApproved && (
          <button
            type="button"
            className="ApprovalsDisplay__remove-approval-link"
            onClick={toggleApproval}
          >
            Remove approval
          </button>
        )}
      </div>
    )}
  </div>
);

ApprovalsDisplay.propTypes = {
  canApprove: PropTypes.bool.isRequired,
  addedApprovers: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string.isRequired,
  })).isRequired,
  isApproved: PropTypes.bool.isRequired,
  toggleApproval: PropTypes.func.isRequired,
};

export default ApprovalsDisplay;
