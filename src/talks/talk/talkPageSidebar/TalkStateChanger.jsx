import React from 'react';

import PropTypes from 'prop-types';

import { updateTalkFirestore } from '../../../shared/TalkFirestore';
import TalkStateChangerDisplay from './TalkStateChangerDisplay';

const TalkStateChanger = ({
  canChange,
  spaceName,
  talkId,
  state,
  uid,
  name,
  avatarUrl,
}) => {
  const changeState = async (targetState) => {
    await updateTalkFirestore(
      spaceName,
      talkId,
      {
        state: targetState,
        changedStateById: uid,
        changedStateByName: name,
        changedStateByAvatarUrl: avatarUrl,
        changedStateAt: new Date(),
      },
    );
  };

  return (
    <TalkStateChangerDisplay
      canChange={canChange}
      state={state}
      changeState={changeState}
    />
  );
};

TalkStateChanger.propTypes = {
  canChange: PropTypes.bool.isRequired,
  spaceName: PropTypes.string.isRequired,
  talkId: PropTypes.string.isRequired,
  state: PropTypes.oneOf(['Open', 'In progress', 'Resolved']).isRequired,
  uid: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
};

export default TalkStateChanger;
