import React, { useEffect, useState, useContext } from 'react';

import PropTypes from 'prop-types';
import firebase from 'firebase/app';

import { updateTalkFirestore } from '../../../shared/TalkFirestore';
import CombinedUserContext from '../../../currentUser/CombinedUserContext';
import ApprovalsDisplay from './ApprovalsDisplay';

const Approvals = ({
  canApprove,
  spaceName,
  talkId,
  approverIds,
  users,
}) => {
  const { combinedUser } = useContext(CombinedUserContext);
  const {
    uid,
  } = combinedUser;

  const [addedApprovers, setAddedApprovers] = useState([]);
  const [isApproved, setIsApproved] = useState(false);

  useEffect(() => {
    setIsApproved(approverIds.includes(uid));
  }, [approverIds]);

  useEffect(() => {
    setAddedApprovers(
      users.filter((user) => approverIds.includes(user.id)),
    );
  }, [approverIds]);

  const toggleApproval = async () => {
    if (isApproved) {
      await updateTalkFirestore(
        spaceName,
        talkId,
        {
          approverIds: firebase.firestore.FieldValue.arrayRemove(uid),
        },
      );
    } else {
      await updateTalkFirestore(
        spaceName,
        talkId,
        {
          approverIds: firebase.firestore.FieldValue.arrayUnion(uid),
        },
      );
    }
  };

  return (
    <ApprovalsDisplay
      canApprove={canApprove}
      addedApprovers={addedApprovers}
      isApproved={isApproved}
      toggleApproval={toggleApproval}
    />
  );
};

Approvals.propTypes = {
  canApprove: PropTypes.bool.isRequired,
  spaceName: PropTypes.string.isRequired,
  talkId: PropTypes.string.isRequired,
  approverIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  users: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string.isRequired,
  })).isRequired,
};

export default Approvals;
