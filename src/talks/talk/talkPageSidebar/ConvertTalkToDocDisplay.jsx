import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';

import GenericModal from '../../../shared/layout/GenericModal';

const ConvertTalkToDocDisplay = ({
  onConfirm,
  onCancel,
  isConvertingToDoc,
  showModal,
  setShowModal,
}) => {
  const modalBody = (
    <>
      <p>
        Are you sure? The talk proposal will become the content of the new doc.
        Talk comments will appear in the
        <b> Comment threads </b>
        view of the new doc.
      </p>
      <p>You cannot go back once you convert.</p>
    </>
  );

  return (
    <div className="ConvertTalkToDocDisplay">
      <Button
        onClick={() => { setShowModal(true); }}
        disabled={isConvertingToDoc}
        size="sm"
        variant="outline-primary"
      >
        Convert to doc
      </Button>
      <GenericModal
        show={showModal}
        modalTitle="Convert talk to doc"
        modalBody={modalBody}
        onConfirm={onConfirm}
        onCancel={onCancel}
        isConfirming={isConvertingToDoc}
        confirmButtonVariant="primary"
        cancelButtonVariant="outline-primary"
        loadingLabel="Converting..."
        confirmLabel="Convert talk to doc"
      />
    </div>
  );
};

ConvertTalkToDocDisplay.propTypes = {
  onConfirm: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  isConvertingToDoc: PropTypes.bool.isRequired,
  showModal: PropTypes.bool.isRequired,
  setShowModal: PropTypes.func.isRequired,
};

export default ConvertTalkToDocDisplay;
