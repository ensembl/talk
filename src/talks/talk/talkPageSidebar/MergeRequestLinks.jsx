import React from 'react';

import PropTypes from 'prop-types';

import './MergeRequestLinks.css';

const MergeRequestLinks = (
  { mergeRequestUrls },
) => mergeRequestUrls.map((url) => (
  <div className="MergeRequestLinks__link" key={url}>
    <a
      href={url}
      target="_blank"
      rel="noreferrer"
    >
      Merge request
    </a>
  </div>
));

MergeRequestLinks.propTypes = {
  mergeRequestUrls: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default MergeRequestLinks;
