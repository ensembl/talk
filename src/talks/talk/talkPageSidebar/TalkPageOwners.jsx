import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';
import firebase from 'firebase/app';

import { updateTalkFirestore } from '../../../shared/TalkFirestore';
import TalkPageOwnersDisplay from './TalkPageOwnersDisplay';

const TalkPageOwners = ({
  spaceName, talkId, ownerIds, users, uid,
}) => {
  const [addedOwners, setAddedOwners] = useState([]);
  const [notAddedOwners, setNotAddedOwners] = useState([]);

  useEffect(() => {
    setAddedOwners(
      users.filter((user) => ownerIds.includes(user.id)),
    );
    setNotAddedOwners(
      users.filter((user) => !ownerIds.includes(user.id)),
    );
  }, [ownerIds]);

  const addOwner = async (userId) => {
    await updateTalkFirestore(
      spaceName,
      talkId,
      {
        ownerIds: firebase.firestore.FieldValue.arrayUnion(userId),
        subscribedUids: firebase.firestore.FieldValue.arrayUnion(userId),
      },
    );
  };

  const removeOwner = async (userId) => {
    await updateTalkFirestore(
      spaceName,
      talkId,
      {
        ownerIds: firebase.firestore.FieldValue.arrayRemove(userId),
      },
    );
  };

  return (
    <TalkPageOwnersDisplay
      addedOwners={addedOwners}
      notAddedOwners={notAddedOwners}
      addOwner={addOwner}
      removeOwner={removeOwner}
      spaceName={spaceName}
      uid={uid}
    />
  );
};

TalkPageOwners.propTypes = {
  spaceName: PropTypes.string.isRequired,
  talkId: PropTypes.string.isRequired,
  ownerIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  users: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string.isRequired,
  })).isRequired,
  uid: PropTypes.string.isRequired,
};

export default TalkPageOwners;
