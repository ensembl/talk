import React, { useContext } from 'react';

import PropTypes from 'prop-types';

import CombinedUserContext from '../../../currentUser/CombinedUserContext';
import TalkStateChanger from './TalkStateChanger';
import DocStateChanger from './DocStateChanger';
import TalkPageLabels from './TalkPageLabels';
import Approvals from './Approvals';
import SubscriptionToggle from './SubscriptionToggle';
import DeleteTalk from './DeleteTalk';
import MergeRequest from './MergeRequest';
import TalkPageOwners from './TalkPageOwners';
import SidebarFiles from './SidebarFiles';
import ConvertTalkToDoc from './ConvertTalkToDoc';
import { updateTalkFirestore } from '../../../shared/TalkFirestore';
import './TalkPageSidebar.css';

const TalkPageSidebar = ({
  onCommunitySpace,
  isAdmin,
  isSpaceMember,
  spaceName,
  talk,
  labels,
  users,
  isConvertingToDoc,
  setIsConvertingToDoc,
}) => {
  const { combinedUser } = useContext(CombinedUserContext);
  const { uid, avatarUrl, name } = combinedUser;
  const {
    id,
    state,
    createdById,
    ownerIds,
    isDoc = false,
    approverIds = [],
    labelIds: talkLabelIds = [],
    subscribedUids = [],
    title,
    mergeRequestUrls = [],
    sidebarFiles = {},
  } = talk;
  const isTalkCreator = createdById === uid || createdById === 'Ensembl Bot';

  const updateTalk = async (data) => {
    await updateTalkFirestore(
      spaceName,
      id,
      data,
    );
  };

  return (
    <div className="TalkPageSidebar">
      {!isDoc && (
        <div className="TalkPageSidebar__item">
          <TalkStateChanger
            canChange={isAdmin || isTalkCreator || !onCommunitySpace}
            spaceName={spaceName}
            talkId={id}
            state={state}
            uid={uid}
            name={name}
            avatarUrl={avatarUrl}
          />
        </div>
      )}
      {!onCommunitySpace && (
        <div className="TalkPageSidebar__item">
          <TalkPageOwners
            spaceName={spaceName}
            talkId={id}
            ownerIds={ownerIds || []}
            users={users}
            uid={uid}
          />
        </div>
      )}
      <div className="TalkPageSidebar__item">
        <Approvals
          canApprove={isSpaceMember}
          spaceName={spaceName}
          talkId={id}
          approverIds={approverIds}
          users={users}
        />
      </div>
      <div className="TalkPageSidebar__item">
        <TalkPageLabels
          canChange={isAdmin || !onCommunitySpace}
          spaceName={spaceName}
          talkId={id}
          talkLabelIds={talkLabelIds}
          labels={labels}
          isDoc={isDoc}
        />
      </div>
      {['Ensembl', 'CBCR Worship', 'Apple'].includes(spaceName) && !isDoc && (
        <div className="TalkPageSidebar__item">
          <SidebarFiles
            spaceName={spaceName}
            talkId={id}
            files={sidebarFiles}
          />
        </div>
      )}
      {spaceName === 'Ensembl' && !isDoc && (
        <div className="TalkPageSidebar__item">
          <MergeRequest
            branchName={title}
            issueTitle={title}
            mergeRequestUrls={mergeRequestUrls}
            talkId={id}
            spaceName={spaceName}
          />
        </div>
      )}
      {isDoc && (
        <div className="TalkPageSidebar__item">
          <DocStateChanger
            spaceName={spaceName}
            talkId={id}
            state={state}
            uid={uid}
            name={name}
            avatarUrl={avatarUrl}
          />
        </div>
      )}
      {!isDoc && (
        <div className="TalkPageSidebar__item">
          <ConvertTalkToDoc
            updateTalk={updateTalk}
            avatarUrl={avatarUrl}
            name={name}
            uid={uid}
            talkId={id}
            spaceName={spaceName}
            isConvertingToDoc={isConvertingToDoc}
            setIsConvertingToDoc={setIsConvertingToDoc}
          />
        </div>
      )}
      {isSpaceMember && (
        <div className="TalkPageSidebar__item">
          <SubscriptionToggle
            spaceName={spaceName}
            talkId={id}
            subscribedUids={subscribedUids}
          />
        </div>
      )}
      {isTalkCreator && (
        <div className="TalkPageSidebar__item">
          <DeleteTalk
            spaceName={spaceName}
            talkId={id}
            isDoc={isDoc}
          />
        </div>
      )}
    </div>
  );
};

TalkPageSidebar.propTypes = {
  onCommunitySpace: PropTypes.bool.isRequired,
  isAdmin: PropTypes.bool.isRequired,
  isSpaceMember: PropTypes.bool.isRequired,
  spaceName: PropTypes.string.isRequired,
  talk: PropTypes.objectOf(PropTypes.any).isRequired,
  labels: PropTypes.arrayOf(PropTypes.any).isRequired,
  users: PropTypes.arrayOf(PropTypes.any).isRequired,
  isConvertingToDoc: PropTypes.bool.isRequired,
  setIsConvertingToDoc: PropTypes.func.isRequired,
};

export default TalkPageSidebar;
