import React from 'react';
import PropTypes from 'prop-types';
import IsSaving from './IsSaving';

import './TalkPageDesktopHeader.css';

const TalkPageDesktopHeader = ({
  usesReadOnlyEditor,
  title,
  contentEditorReady,
  isSaving,
  isSaved,
}) => (
  <header className="TalkPageDesktopHeader">
    <h1 className="TalkPageDesktopHeader__title font-weight-bold">{title}</h1>
    <div className="TalkPageDesktopHeader__is-saving">
      {!usesReadOnlyEditor && (
        <IsSaving
          contentEditorReady={contentEditorReady}
          isSaving={isSaving}
          isSaved={isSaved}
        />
      )}
    </div>
  </header>
);

TalkPageDesktopHeader.propTypes = {
  usesReadOnlyEditor: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  contentEditorReady: PropTypes.bool.isRequired,
  isSaving: PropTypes.bool.isRequired,
  isSaved: PropTypes.bool.isRequired,
};

export default TalkPageDesktopHeader;
