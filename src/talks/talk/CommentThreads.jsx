import React from 'react';

import PropTypes from 'prop-types';
import CommentThread from './CommentThread';

const CommentThreads = ({
  users, threadsMeta, comments,
}) => {
  const threadIds = [...new Set(comments.map((comment) => comment.threadId))];
  const sortedResolvedThreadIds = Object.keys(threadsMeta)
    .filter((metaThreadId) => threadIds.includes(metaThreadId))
    .sort((a, b) => {
      const aFinishedAt = threadsMeta[a].resolvedAt
        ? threadsMeta[a].resolvedAt.seconds : threadsMeta[a].convertedAt.seconds;
      const bFinishedAt = threadsMeta[b].resolvedAt
        ? threadsMeta[b].resolvedAt.seconds : threadsMeta[b].convertedAt.seconds;
      return aFinishedAt - bFinishedAt;
    });
  const nonResolvedThreadIds = threadIds
    .filter((threadId) => !sortedResolvedThreadIds.includes(threadId));
  const sortedThreadIds = [...sortedResolvedThreadIds, ...nonResolvedThreadIds];

  return (
    <div className="CommentThreads">
      {sortedThreadIds.map((threadId) => {
        const filteredComments = comments
          .filter((comment) => comment.threadId === threadId)
          .sort((a, b) => a.createdAt.seconds - b.createdAt.seconds);
        return (
          <CommentThread
            key={threadId}
            comments={filteredComments}
            users={users}
            resolvedByName={
              threadId in threadsMeta && threadsMeta[threadId].resolvedByName
                ? threadsMeta[threadId].resolvedByName
                : ''
            }
            resolvedByAvatarUrl={
              threadId in threadsMeta && threadsMeta[threadId].resolvedByAvatarUrl
                ? threadsMeta[threadId].resolvedByAvatarUrl
                : ''
            }
            resolvedAtSeconds={
              threadId in threadsMeta && threadsMeta[threadId].resolvedAt
                ? threadsMeta[threadId].resolvedAt.seconds
                : -1
            }
            convertedByName={
              threadId in threadsMeta && threadsMeta[threadId].convertedByName
                ? threadsMeta[threadId].convertedByName
                : ''
            }
            convertedByAvatarUrl={
              threadId in threadsMeta && threadsMeta[threadId].convertedByAvatarUrl
                ? threadsMeta[threadId].convertedByAvatarUrl
                : ''
            }
            convertedAtSeconds={
              threadId in threadsMeta && threadsMeta[threadId].convertedAt
                ? threadsMeta[threadId].convertedAt.seconds
                : -1
            }
          />
        );
      })}
    </div>
  );
};

CommentThreads.propTypes = {
  users: PropTypes.arrayOf(
    PropTypes.shape({
      avatarUrl: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }),
  ).isRequired,
  threadsMeta: PropTypes.objectOf(PropTypes.shape({
    resolvedAt: PropTypes.objectOf(Date),
    resolvedByAvatarUrl: PropTypes.string,
    resolvedById: PropTypes.string,
    resolvedByName: PropTypes.string,
    convertedAt: PropTypes.objectOf(Date),
    convertedByAvatarUrl: PropTypes.string,
    convertedById: PropTypes.string,
    convertedByName: PropTypes.string,
  })).isRequired,
  comments: PropTypes.arrayOf(PropTypes.shape({
    threadId: PropTypes.string.isRequired,
    createdAt: PropTypes.objectOf(Date).isRequired,
    createdByAvatarUrl: PropTypes.string.isRequired,
    createdById: PropTypes.string.isRequired,
    createdByName: PropTypes.string.isRequired,
    htmlBody: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    markerContent: PropTypes.string,
    mentions: PropTypes.objectOf(PropTypes.any).isRequired,
  })).isRequired,
};

export default CommentThreads;
