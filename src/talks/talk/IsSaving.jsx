import React from 'react';

import PropTypes from 'prop-types';

import './IsSaving.css';

const IsSaving = ({
  contentEditorReady,
  isSaving,
  isSaved,
}) => {
  let message;
  if (!contentEditorReady) {
    message = <span className="IsSaving__message IsSaving__message--loading">Loading...</span>;
  } else if (contentEditorReady && !isSaving && !isSaved) {
    message = <span className="IsSaving__message IsSaving__message--done">{'Ready for autosave editing \u2713'}</span>;
  } else if (contentEditorReady && isSaving) {
    message = <span className="IsSaving__message text-muted">Saving...</span>;
  } else if (contentEditorReady && isSaved) {
    message = <span className="IsSaving__message IsSaving__message--done">{'Saved \u2713'}</span>;
  }
  return (
    <span className="IsSaving">{message}</span>
  );
};

IsSaving.propTypes = {
  contentEditorReady: PropTypes.bool.isRequired,
  isSaving: PropTypes.bool.isRequired,
  isSaved: PropTypes.bool.isRequired,
};

export default IsSaving;
