import { db } from '../../firebase-config';

const addCommentFirestore = async (spaceName, talkId, data) => {
  try {
    await db.collection(`spaces/${spaceName}/talks/${talkId}/comments`).add(data);
  } catch (error) {
    throw new Error('Error adding comment:', error, spaceName, talkId, data);
  }
};

const setCommentFirestore = async (spaceName, talkId, id, data) => {
  try {
    await db.collection(`spaces/${spaceName}/talks/${talkId}/comments`).doc(id).set(data);
  } catch (error) {
    throw new Error('Error setting comment:', error, spaceName, talkId, id, data);
  }
};

const updateCommentFirestore = async (spaceName, talkId, id, data) => {
  try {
    await db.collection(`spaces/${spaceName}/talks/${talkId}/comments`).doc(id).update(data);
  } catch (error) {
    throw new Error('Error updating comment:', error, spaceName, talkId, id, data);
  }
};

export { addCommentFirestore, setCommentFirestore, updateCommentFirestore };
