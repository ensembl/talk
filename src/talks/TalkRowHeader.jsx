import React from 'react';

import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { FileEarmarkText } from 'react-bootstrap-icons';

import SubjectTimestamps from './shared/SubjectTimestamps';
import TalkRowLabels from './TalkRowLabels';
import './TalkRowHeader.css';

const TalkRowHeader = ({
  spaceName,
  createdByAvatarUrl,
  createdByName,
  createdAtSeconds,
  updatedByAvatarUrl,
  updatedByName,
  updatedAtSeconds,
  talkLabelIds,
  labels,
  type,
  talkId,
  title,
}) => (
  <div className="TalkRowHeader">
    <div className="TalkRowHeader__timestamps-labels">
      <div className="TalkRowHeader__timestamps">
        <SubjectTimestamps
          createdByAvatarUrl={createdByAvatarUrl}
          createdByName={createdByName}
          createdAtSeconds={createdAtSeconds}
          updatedByAvatarUrl={updatedByAvatarUrl}
          updatedByName={updatedByName}
          updatedAtSeconds={updatedAtSeconds}
        />
      </div>
      <TalkRowLabels
        talkLabelIds={talkLabelIds}
        labels={labels}
        type={type}
      />
    </div>
    <Link to={`/-/${spaceName}/${type}s/${talkId}`} className="not-user-generated-link">
      <h2 className="TalkRowHeader__title font-weight-bold">
        {type === 'doc' && <FileEarmarkText size={16} />}
        <span>{title}</span>
      </h2>
    </Link>
  </div>
);

TalkRowHeader.propTypes = {
  spaceName: PropTypes.string.isRequired,
  createdByAvatarUrl: PropTypes.string.isRequired,
  createdByName: PropTypes.string.isRequired,
  createdAtSeconds: PropTypes.number.isRequired,
  updatedByAvatarUrl: PropTypes.string.isRequired,
  updatedByName: PropTypes.string.isRequired,
  updatedAtSeconds: PropTypes.number.isRequired,
  talkLabelIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  labels: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    color: PropTypes.string,
    name: PropTypes.string,
  })).isRequired,
  type: PropTypes.string.isRequired,
  talkId: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

export default TalkRowHeader;
