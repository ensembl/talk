import React, { useEffect } from 'react';

import PropTypes from 'prop-types';
// Since IE doesn't support https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams
// https://www.npmjs.com/package/url-search-params-polyfill
import 'url-search-params-polyfill';
import { useHistory } from 'react-router-dom';

import LabelSet from './shared/LabelSet';
import './TalksPageFilterLabels.css';

const TalksPageFilterLabels = ({
  onCommunitySpace,
  labels,
  filteredLabelIds,
  setFilteredLabelIds,
  setPage,
  spaceName,
}) => {
  const history = useHistory();

  const addedLabels = labels.filter((label) => filteredLabelIds.includes(label.id));
  const notAddedLabels = labels.filter((label) => !filteredLabelIds.includes(label.id));

  const updateUrlParams = (isAdd, labelId) => {
    const { search } = history.location;
    const params = new URLSearchParams(!search ? '' : search);
    let newLabelIds = params.getAll('labelId');
    if (isAdd) {
      newLabelIds.push(labelId);
    } else {
      newLabelIds = newLabelIds.filter((newLabelId) => newLabelId !== labelId);
    }
    params.delete('labelId');
    newLabelIds.forEach((newLabelId) => {
      params.append('labelId', newLabelId);
    });
    history.push({ search: params.toString() });
  };

  const addLabel = (labelId) => {
    updateUrlParams(true, labelId);
  };

  const removeLabel = (labelId) => {
    updateUrlParams(false, labelId);
  };

  useEffect(() => {
    const { search } = history.location;
    const params = new URLSearchParams(!search ? '' : search);
    const labelIds = params.getAll('labelId');
    setFilteredLabelIds(labelIds);
    setPage(1);
  }, [history.location]);

  return (
    <div className="TalksPageFilterLabels">
      <LabelSet
        onCommunitySpace={onCommunitySpace}
        addedLabels={addedLabels}
        notAddedLabels={notAddedLabels}
        addLabel={addLabel}
        addLabelText="Filter by label"
        removeLabel={removeLabel}
        spaceName={spaceName}
      />
    </div>
  );
};

TalksPageFilterLabels.propTypes = {
  onCommunitySpace: PropTypes.bool.isRequired,
  labels: PropTypes.instanceOf(Array).isRequired,
  filteredLabelIds: PropTypes.instanceOf(Array).isRequired,
  setFilteredLabelIds: PropTypes.func.isRequired,
  setPage: PropTypes.func.isRequired,
  spaceName: PropTypes.string.isRequired,
};

export default TalksPageFilterLabels;
