import React, { useEffect, useContext } from 'react';

import { useCollectionData } from 'react-firebase-hooks/firestore';
import { useParams } from 'react-router-dom';

import { db } from '../firebase-config';
import CombinedUserContext from '../currentUser/CombinedUserContext';
import PageLoadingSpinner from '../shared/PageLoadingSpinner';
import NewTalksStrip from './NewTalksStrip';
import PageLayout from '../shared/layout/PageLayout';
import TalksPageTalks from './TalksPageTalks';
import updateUserFirestore from '../shared/UserFirestore';
import PageNotFound from '../shared/PageNotFound';
import CommunityMessage from './shared/CommunityMessage';
import './TalksPage.css';

const TalksPage = () => {
  const { spaceName: urlSpaceName } = useParams();

  const { combinedUser } = useContext(CombinedUserContext);
  const {
    uid, allowableSpaceNames,
  } = combinedUser;

  const [users, usersLoading, usersError] = useCollectionData(db.collection(`spaces/${urlSpaceName}/copied-users`).orderBy('name', 'asc'), { idField: 'id' });
  const [labels, labelsLoading, labelsError] = useCollectionData(db.collection(`spaces/${urlSpaceName}/labels`).orderBy('name', 'asc'), { idField: 'id' });

  const onCommunitySpace = window.location.pathname.startsWith('/-/Ensembl%20Community');
  const isSpaceMember = allowableSpaceNames.includes(urlSpaceName);

  useEffect(() => {
    if (users && labels && uid) {
      updateUserFirestore(uid, { lastVisitedSpaceName: urlSpaceName });
    }
  }, [users, labels]);

  // Desktop document.title controlled in TalksPageTabs.jsx

  return (
    <PageLayout
      uid={uid}
      onCommunitySpace={onCommunitySpace}
      urlSpaceName={urlSpaceName}
      allowableSpaceNames={allowableSpaceNames}
      mobileHeaderTitle="Home"
      mainContent={(
        <>
          {(usersLoading || labelsLoading) && <PageLoadingSpinner />}
          {(usersError || labelsError) && <PageNotFound />}
          {users && labels && (
            <div className="TalksPage">
              {!onCommunitySpace && (
                <NewTalksStrip
                  spaceName={urlSpaceName}
                  users={users}
                />
              )}
              {onCommunitySpace && <CommunityMessage isSpaceMember={isSpaceMember} />}
              <h3>Talks + Docs</h3>
              <TalksPageTalks
                onCommunitySpace={onCommunitySpace}
                spaceName={urlSpaceName}
                labels={labels}
                users={users}
              />
            </div>
          )}
        </>
      )}
    />
  );
};

export default TalksPage;
