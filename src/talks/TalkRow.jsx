import React from 'react';

import PropTypes from 'prop-types';

import TalkRowHeader from './TalkRowHeader';
import TalkRowNewestComment from './TalkRowNewestComment';
import './TalkRow.css';

const TalkRow = ({
  talk,
  labels,
  spaceName,
  users,
}) => {
  const {
    createdById,
    createdByAvatarUrl,
    createdByName,
    createdAt,
    updatedById = '',
    updatedByAvatarUrl = '',
    updatedByName = '',
    updatedAt = null,
    labelIds: talkLabelIds = [],
    isDoc,
    id: talkId,
    title,
    newestCommentHtmlBody = '',
    newestCommentId = '',
    newestCommentCreatedByAvatarUrl = '',
    newestCommentCreatedByName = '',
    newestCommentCreatedById = '',
  } = talk;

  const creators = users.filter((user) => user.id === createdById);
  const creatorAvatarUrl = creators.length > 0 ? creators[0].avatarUrl : createdByAvatarUrl;
  const creatorName = creators.length > 0 ? creators[0].name : createdByName;

  const updaters = users.filter((user) => user.id === updatedById);
  const updaterAvatarUrl = updaters.length > 0 ? updaters[0].avatarUrl : updatedByAvatarUrl;
  const updaterName = updaters.length > 0 ? updaters[0].name : updatedByName;

  const commentors = users.filter((user) => user.id === newestCommentCreatedById);
  const commentorAvatarUrl = commentors.length > 0
    ? commentors[0].avatarUrl : newestCommentCreatedByAvatarUrl;
  const commentorName = commentors.length > 0 ? commentors[0].name : newestCommentCreatedByName;

  return (
    <div className="TalkRow">
      <TalkRowHeader
        spaceName={spaceName}
        createdByAvatarUrl={creatorAvatarUrl}
        createdByName={creatorName}
        createdAtSeconds={createdAt.seconds}
        updatedByAvatarUrl={updaterAvatarUrl}
        updatedByName={updaterName}
        updatedAtSeconds={updatedAt ? updatedAt.seconds : -1}
        talkLabelIds={talkLabelIds}
        labels={labels}
        type={isDoc ? 'doc' : 'talk'}
        talkId={talkId}
        title={title}
      />
      {newestCommentHtmlBody && !isDoc && (
        <TalkRowNewestComment
          spaceName={spaceName}
          talkId={talkId}
          commentId={newestCommentId}
          avatarUrl={commentorAvatarUrl}
          createdByName={commentorName}
          htmlBody={newestCommentHtmlBody}
        />
      )}
    </div>
  );
};

TalkRow.propTypes = {
  talk: PropTypes.objectOf(PropTypes.any).isRequired,
  labels: PropTypes.instanceOf(Array).isRequired,
  spaceName: PropTypes.string.isRequired,
  users: PropTypes.instanceOf(Array).isRequired,
};

export default TalkRow;
