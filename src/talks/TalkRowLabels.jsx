import React from 'react';

import PropTypes from 'prop-types';

import LabelSet from './shared/LabelSet';

const TalkRowLabels = ({
  talkLabelIds,
  labels,
  type,
}) => {
  const talkLabels = talkLabelIds && labels
    ? labels.filter((label) => talkLabelIds.includes(label.id)) : [];

  return (
    talkLabels.length > 0 && (
      <LabelSet
        addedLabels={talkLabels}
        type={type}
      />
    )
  );
};

TalkRowLabels.propTypes = {
  talkLabelIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  labels: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    color: PropTypes.string,
    name: PropTypes.string,
  })).isRequired,
  type: PropTypes.string.isRequired,
};

export default TalkRowLabels;
