import React from 'react';

import { Link } from 'react-router-dom';
import dompurify from 'dompurify';
import PropTypes from 'prop-types';

import AvatarAnchoredContent from '../shared/AvatarAnchoredContent';

import './TalkRowNewestComment.css';
import '../shared/ckeditor/CKEditorStyles.css';

const TalkRowNewestComment = ({
  spaceName,
  talkId,
  commentId,
  avatarUrl,
  createdByName,
  htmlBody,
}) => (
  <Link
    data-testid="newest-comment-link"
    to={`/-/${spaceName}/talks/${talkId}#comment_${commentId}`}
    className="text-dark not-user-generated-link"
  >
    <div className="TalkRowNewestComment">
      <AvatarAnchoredContent
        avatarUrl={avatarUrl}
        header={(
          <div className="text-muted comment-header">
            <span>Latest by </span>
            <span className="font-weight-bold">{createdByName}</span>
          </div>
        )}
        body={(
          <div
            className="TalkRowNewestComment__comment-body ck-content"
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{ __html: dompurify.sanitize(htmlBody) }}
          />
        )}
      />
    </div>
  </Link>
);

TalkRowNewestComment.propTypes = {
  spaceName: PropTypes.string.isRequired,
  talkId: PropTypes.string.isRequired,
  commentId: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  createdByName: PropTypes.string.isRequired,
  htmlBody: PropTypes.string.isRequired,
};

export default TalkRowNewestComment;
