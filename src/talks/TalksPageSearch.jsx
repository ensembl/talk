import React from 'react';

import PropTypes from 'prop-types';
import { Form } from 'react-bootstrap';

import './TalksPageSearch.css';

const TalksPageSearch = ({
  searchQuery,
  setSearchQuery,
  setPage,
}) => (
  <Form className="TalksPageSearch">
    <Form.Control
      className="one-line-input"
      type="search"
      value={searchQuery}
      onChange={(e) => {
        setPage(1);
        setSearchQuery(e.target.value);
      }}
      placeholder="Search..."
      autoFocus
    />
  </Form>
);

TalksPageSearch.propTypes = {
  searchQuery: PropTypes.string.isRequired,
  setSearchQuery: PropTypes.func.isRequired,
  setPage: PropTypes.func.isRequired,
};

export default TalksPageSearch;
