import React from 'react';

import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';

import './TalksPagePagination.css';

const TalksPagination = ({
  previousDisabled,
  nextDisabled,
  page,
  setPage,
}) => (
  <div className="TalksPagePagination">
    <div>
      <Button
        variant="outline-primary"
        className="btn-sm"
        disabled={previousDisabled}
        onClick={() => {
          setPage(page - 1);
        }}
      >
        Prev
      </Button>
      <span className="font-weight-bold TalksPagePagination_page-label">
        {`Page ${page}`}
      </span>
      <Button
        variant="outline-primary"
        className="btn-sm"
        disabled={nextDisabled}
        onClick={() => {
          setPage(page + 1);
        }}
      >
        Next
      </Button>
    </div>
  </div>
);

TalksPagination.propTypes = {
  previousDisabled: PropTypes.bool.isRequired,
  nextDisabled: PropTypes.bool.isRequired,
  page: PropTypes.number.isRequired,
  setPage: PropTypes.func.isRequired,
};

export default TalksPagination;
