import React, { useEffect } from 'react';

import PropTypes from 'prop-types';
// Since IE doesn't support https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams
// https://www.npmjs.com/package/url-search-params-polyfill
import 'url-search-params-polyfill';
import { useHistory } from 'react-router-dom';

import Pills from './shared/Pills';
import './TalksPageTabs.css';

const TalksPageTabs = ({
  tab,
  setTab,
  setPage,
}) => {
  const history = useHistory();

  const updateUrlParams = (eventKey) => {
    const { search } = history.location;
    const params = new URLSearchParams(!search ? '' : search);
    params.delete('tab');
    params.append('tab', eventKey);
    history.push({ search: params.toString() });
  };

  useEffect(() => {
    const { search } = history.location;
    const params = new URLSearchParams(!search ? '' : search);
    const urlParamTab = params.get('tab');
    if (urlParamTab) {
      setTab(urlParamTab);
      setPage(1);
      switch (urlParamTab) {
        case 'talks':
          document.title = 'Talks';
          break;
        case 'inProgress':
          document.title = 'In progress talks';
          break;
        case 'resolvedTalks':
          document.title = 'Resolved talks';
          break;
        case 'docs':
          document.title = 'Docs';
          break;
        case 'archivedDocs':
          document.title = 'Archived docs';
          break;
        default:
          throw new Error('Error matching urlParamTab');
      }
    } else {
      document.title = 'Talks';
    }
  }, [history.location]);

  const talksTypeTabSelected = ['talks', 'inProgress', 'resolvedTalks'].includes(tab);

  return (
    <div className="TalksPageTabs">
      <div>
        <div className="TalksPageTabs__section-title font-weight-bold">Type</div>
        <Pills
          pills={[
            { type: 'talks', name: 'Talks' },
            { type: 'docs', name: 'Docs' },
          ]}
          activeKey={talksTypeTabSelected ? 'talks' : 'docs'}
          selectPill={updateUrlParams}
        />
      </div>
      {talksTypeTabSelected && (
        <div className="TalksPagTabs__state-section">
          <div className="TalksPageTabs__section-title font-weight-bold">Talk state</div>
          <Pills
            pills={[
              { type: 'talks', name: 'Open' },
              { type: 'inProgress', name: 'In progress' },
              { type: 'resolvedTalks', name: 'Resolved' },
            ]}
            activeKey={tab}
            selectPill={updateUrlParams}
          />
        </div>
      )}
      {!talksTypeTabSelected && (
        <div className="TalksPagTabs__state-section">
          <div className="TalksPageTabs__section-title font-weight-bold">Doc state</div>
          <Pills
            pills={[
              { type: 'docs', name: 'Open' },
              { type: 'archivedDocs', name: 'Archived' },
            ]}
            activeKey={tab}
            selectPill={updateUrlParams}
          />
        </div>
      )}
    </div>
  );
};

TalksPageTabs.propTypes = {
  tab: PropTypes.string.isRequired,
  setTab: PropTypes.func.isRequired,
  setPage: PropTypes.func.isRequired,
};

export default TalksPageTabs;
