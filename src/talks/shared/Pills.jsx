import React from 'react';

import PropTypes from 'prop-types';
import { Nav } from 'react-bootstrap';

import './Pills.css';

const Pills = ({
  pills,
  activeKey,
  selectPill,
}) => (
  <Nav
    variant="pills"
    activeKey={activeKey}
    onSelect={selectPill}
    className="Pills"
  >
    {pills.map(({ type, name }) => (
      <Nav.Item key={type}>
        <Nav.Link
          className="btn-sm"
          eventKey={type}
        >
          {name}
        </Nav.Link>
      </Nav.Item>
    ))}
  </Nav>
);

Pills.propTypes = {
  pills: PropTypes.instanceOf(Array).isRequired,
  activeKey: PropTypes.string.isRequired,
  selectPill: PropTypes.func.isRequired,
};

export default Pills;
