import React from 'react';

import PropTypes from 'prop-types';
import { DropdownButton, Dropdown } from 'react-bootstrap';

import Label from '../../shared/Label';
import './LabelsDropdown.css';

const LabelsDropdown = ({
  onCommunitySpace,
  display,
  labels,
  selectLabel,
  spaceName,
}) => (
  <DropdownButton
    className="LabelsDropdown"
    size="sm"
    variant="outline-primary"
    title={display}
  >
    <div className="LabelsDropdown__items">
      {labels.map((label) => (
        <Dropdown.Item
          key={label.id}
          onClick={() => { selectLabel(label.id); }}
        >
          <Label
            name={label.name}
            color={label.color}
          />
        </Dropdown.Item>
      ))}
    </div>
    {!onCommunitySpace && (
      <>
        {labels.length > 0 && <Dropdown.Divider />}
        <Dropdown.Item href={`/-/${spaceName}/labels`}>Create a new label</Dropdown.Item>
      </>
    )}
  </DropdownButton>
);

LabelsDropdown.propTypes = {
  onCommunitySpace: PropTypes.bool,
  display: PropTypes.string.isRequired,
  labels: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    color: PropTypes.string,
  })).isRequired,
  selectLabel: PropTypes.func.isRequired,
  spaceName: PropTypes.string.isRequired,
};

LabelsDropdown.defaultProps = {
  onCommunitySpace: false,
};

export default LabelsDropdown;
