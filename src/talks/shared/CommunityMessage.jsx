import React from 'react';

import PropTypes from 'prop-types';
import { Alert } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import './CommunityMessage.css';

const CommunityMessage = ({ isSpaceMember }) => (
  <Alert
    className="CommunityMessage"
    variant="warning"
  >
    <span>This is a page in</span>
    <span className="font-weight-bold"> Ensembl Community</span>
    <span>, a special internet-public version of an Ensembl Talk space. </span>
    <Link to="/-/Ensembl%20Community/docs/SrwKEBolh6DCEOisZh0s">
      {isSpaceMember ? 'Learn more' : 'Learn more and join'}
    </Link>
  </Alert>
);

CommunityMessage.propTypes = {
  isSpaceMember: PropTypes.bool.isRequired,
};

export default CommunityMessage;
