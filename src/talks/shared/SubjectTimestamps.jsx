import React from 'react';

import PropTypes from 'prop-types';
import SubjectTimestamp from './SubjectTimestamp';
import './SubjectTimestamps.css';

const SubjectTimestamps = ({
  createdByAvatarUrl,
  createdByName,
  createdAtSeconds,
  updatedByAvatarUrl,
  updatedByName,
  updatedAtSeconds,
}) => (
  <div className="SubjectTimestamps">
    <SubjectTimestamp
      avatarUrl={createdByAvatarUrl}
      name={createdByName}
      seconds={createdAtSeconds}
      type="created"
    />
    {updatedByName && (
      <>
        <span className="SubjectTimestamps__separation-dot">·</span>
        <SubjectTimestamp
          avatarUrl={updatedByAvatarUrl || 'https://placekitten.com/73/73'}
          name={updatedByName}
          seconds={updatedAtSeconds}
          type="updated"
        />
      </>
    )}
  </div>
);

SubjectTimestamps.propTypes = {
  createdByAvatarUrl: PropTypes.string.isRequired,
  createdByName: PropTypes.string.isRequired,
  createdAtSeconds: PropTypes.number.isRequired,
  updatedByAvatarUrl: PropTypes.string,
  updatedByName: PropTypes.string,
  updatedAtSeconds: PropTypes.number,
};

SubjectTimestamps.defaultProps = {
  updatedByAvatarUrl: '',
  updatedByName: '',
  updatedAtSeconds: -1,
};

export default SubjectTimestamps;
