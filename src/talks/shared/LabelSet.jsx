import React from 'react';

import PropTypes from 'prop-types';

import Label from '../../shared/Label';
import LabelsDropdown from './LabelsDropdown';

import './LabelSet.css';

const LabelSet = ({
  onCommunitySpace,
  canChange,
  addedLabels,
  notAddedLabels,
  addLabel,
  addLabelText,
  removeLabel,
  type,
  dropdownOnNewline,
  spaceName,
}) => (
  <div className={`${dropdownOnNewline ? 'LabelSet__column' : 'LabelSet__row'}`}>
    {addedLabels.length > 0 && (
      <div className="LabelSet__labels">
        {addedLabels.map((label) => (
          <span key={label.id} className="LabelSet__label">
            <Label
              name={label.name}
              color={label.color}
              id={label.id}
              remove={canChange && removeLabel}
              type={type}
            />
          </span>
        ))}
      </div>
    )}
    <div className="LabelSet__dropdown">
      {addLabel && canChange && (
        <LabelsDropdown
          onCommunitySpace={onCommunitySpace}
          display={addLabelText}
          labels={notAddedLabels}
          selectLabel={addLabel}
          spaceName={spaceName}
        />
      )}
    </div>
  </div>
);

LabelSet.propTypes = {
  onCommunitySpace: PropTypes.bool,
  canChange: PropTypes.bool,
  addedLabels: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    color: PropTypes.string,
  })).isRequired,
  notAddedLabels: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    color: PropTypes.string,
  })),
  addLabel: PropTypes.func,
  addLabelText: PropTypes.string,
  removeLabel: PropTypes.func,
  type: PropTypes.string,
  dropdownOnNewline: PropTypes.bool,
  spaceName: PropTypes.string,
};

LabelSet.defaultProps = {
  onCommunitySpace: false,
  canChange: true,
  notAddedLabels: [],
  addLabel: null,
  addLabelText: '',
  removeLabel: null,
  type: '',
  dropdownOnNewline: false,
  spaceName: '',
};

export default LabelSet;
