import React from 'react';
import { Button } from 'react-bootstrap';

import PropTypes from 'prop-types';

import './AddSelf.css';

const AddSelf = ({ addSelf, uid }) => (
  <Button
    className="AddSelf"
    variant="outline-primary"
    size="sm"
    onClick={() => addSelf(uid)}
  >
    Add yourself
  </Button>
);

AddSelf.propTypes = {
  addSelf: PropTypes.func.isRequired,
  uid: PropTypes.string.isRequired,
};

export default AddSelf;
