import React from 'react';

import PropTypes from 'prop-types';
import {
  OverlayTrigger,
  Tooltip,
} from 'react-bootstrap';
import TimeAgo from 'react-timeago';

import timeAgoFormatter from '../../shared/TimeAgoFormatter';
import tooltipDatetimeFormatter from '../../shared/TooltipDatetimeFormatter';
import Avatar from '../../shared/Avatar';
import './SubjectTimestamp.css';

const SubjectTimestamp = ({
  avatarUrl,
  name,
  seconds,
  type,
}) => {
  const descriptionText = type === 'created' ? 'Created by' : 'Edited by';

  return (
    <div className="SubjectTimestamp text-muted">
      {type === 'updated' && (
        <span className="SubjectTimestamp__description">{descriptionText}</span>
      )}
      {avatarUrl && <Avatar avatarUrl={avatarUrl} />}
      <span
        className="SubjectTimestamp__name font-weight-bold"
      >
        {name}
      </span>
      <span>
        <OverlayTrigger
          delay={{ show: 50, hide: 50 }}
          overlay={<Tooltip>{tooltipDatetimeFormatter(seconds)}</Tooltip>}
        >
          <TimeAgo date={new Date(seconds * 1000)} minPeriod="60" formatter={timeAgoFormatter} />
        </OverlayTrigger>
      </span>
    </div>
  );
};

SubjectTimestamp.propTypes = {
  avatarUrl: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  seconds: PropTypes.number.isRequired,
  type: PropTypes.oneOf(['created', 'updated']).isRequired,
};

export default SubjectTimestamp;
