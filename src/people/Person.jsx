import React from 'react';

import PropTypes from 'prop-types';

import Avatar from '../shared/Avatar';
import './Person.css';

const Person = ({
  name,
  email,
  avatarUrl,
}) => (
  <div className="Person">
    <Avatar
      avatarUrl={!avatarUrl ? 'https://placekitten.com/59/59' : avatarUrl}
      size="md"
    />
    <span className="Person__name-email">{`${name} · ${email}`}</span>
  </div>
);

Person.propTypes = {
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string,
};

Person.defaultProps = {
  avatarUrl: '',
};

export default Person;
