import React, { useState, useContext } from 'react';

import PropTypes from 'prop-types';

import resendInvite from './InviteeCallables';
import CombinedUserContext from '../currentUser/CombinedUserContext';
import InviteeRowDisplay from './InviteeRowDisplay';

const InviteeRow = ({
  email,
  spaceName,
}) => {
  const [reInvited, setReInvited] = useState(false);

  const { combinedUser } = useContext(CombinedUserContext);
  const { name } = combinedUser;

  const resendInvitation = async () => {
    setReInvited(true);
    await resendInvite(name, spaceName, email);
  };

  return (
    <InviteeRowDisplay
      email={email}
      resendInvite={resendInvitation}
      reInvited={reInvited}
    />
  );
};

InviteeRow.propTypes = {
  email: PropTypes.string.isRequired,
  spaceName: PropTypes.string.isRequired,
};

export default InviteeRow;
