import React from 'react';

import PropTypes from 'prop-types';

import Person from './Person';
import './People.css';

const People = ({ users }) => (
  <>
    <h3 className="people">People</h3>
    {users.map((user) => (
      <Person
        key={user.id}
        name={user.name}
        email={user.email}
        avatarUrl={user.avatarUrl}
      />
    ))}
  </>
);

People.propTypes = {
  users: PropTypes.instanceOf(Array).isRequired,
};

export default People;
