import React, { useEffect, useRef } from 'react';

import PropTypes from 'prop-types';
import { Form, Button } from 'react-bootstrap';

import './InvitePersonForm.css';

const InvitePersonForm = ({
  spaceName,
  email,
  updateEmail,
  addInvitee,
  inviteButtonIsDisabled,
  isInviteLink,
}) => {
  const inputEl = useRef(null);

  useEffect(() => {
    if (isInviteLink) inputEl.current.focus();
  }, [isInviteLink]);

  return (
    <>
      <h3 className="invite-a-new-person">{`Invite a new person to ${spaceName}`}</h3>
      <Form autoComplete="off" onSubmit={addInvitee}>
        <Form.Control
          type="input"
          className="invite-person-email-input one-line-input"
          onChange={updateEmail}
          value={email}
          placeholder="Email address of person"
          ref={inputEl}
        />
        <Button
          variant="primary"
          disabled={inviteButtonIsDisabled}
          className="btn-sm"
          type="submit"
        >
          Send an invite email
        </Button>
      </Form>
    </>
  );
};

InvitePersonForm.propTypes = {
  spaceName: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  updateEmail: PropTypes.func.isRequired,
  addInvitee: PropTypes.func.isRequired,
  inviteButtonIsDisabled: PropTypes.bool.isRequired,
  isInviteLink: PropTypes.bool.isRequired,
};

export default InvitePersonForm;
