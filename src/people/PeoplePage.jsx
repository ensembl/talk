import React, { useEffect, useContext } from 'react';

import { useCollectionData } from 'react-firebase-hooks/firestore';
import { useLocation, useParams } from 'react-router-dom';
import { Redirect } from 'react-router';

import { db } from '../firebase-config';
import CombinedUserContext from '../currentUser/CombinedUserContext';
import InvitePerson from './InvitePerson';
import Invitees from './Invitees';
import People from './People';
import PageLayout from '../shared/layout/PageLayout';
import PageLoadingSpinner from '../shared/PageLoadingSpinner';
import updateUserFirestore from '../shared/UserFirestore';
import PageNotFound from '../shared/PageNotFound';

const PeoplePage = () => {
  if (window.location.pathname.startsWith('/-/Ensembl%20Community')) {
    return <Redirect to="/" />;
  }
  const { spaceName: urlSpaceName } = useParams();

  const { combinedUser } = useContext(CombinedUserContext);
  const {
    uid,
    allowableSpaceNames,
  } = combinedUser;
  const [users, usersLoading, usersError] = useCollectionData(
    db.collection(`spaces/${urlSpaceName}/copied-users`).orderBy('name', 'asc'), { idField: 'id' },
  );
  const [invitees, inviteesLoading, inviteesError] = useCollectionData(
    db.collection(`spaces/${urlSpaceName}/invitees`).where('userLoggedIn', '==', false).orderBy('email', 'asc'), { idField: 'id' },
  );

  const onCommunitySpace = window.location.pathname.startsWith('/-/Ensembl%20Community');

  document.title = 'People';

  const location = useLocation();

  useEffect(() => {
    if (users && invitees) {
      updateUserFirestore(uid, { lastVisitedSpaceName: urlSpaceName });
    }
  }, [users, usersLoading, invitees]);

  return (
    <PageLayout
      uid={uid}
      urlSpaceName={urlSpaceName}
      onCommunitySpace={onCommunitySpace}
      allowableSpaceNames={allowableSpaceNames}
      mobileHeaderTitle="People"
      mainContent={(
        <>
          {(usersLoading || inviteesLoading) && <PageLoadingSpinner />}
          {(usersError || inviteesError) && <PageNotFound />}
          {users && invitees && (
            <>
              <InvitePerson
                invitees={invitees}
                users={users}
                spaceName={urlSpaceName}
                isInviteLink={location.hash === '#invite'}
              />
              {invitees.length > 0 && (
                <Invitees
                  invitees={invitees}
                  spaceName={urlSpaceName}
                />
              )}
              <People
                users={users}
              />
            </>
          )}
        </>
      )}
    />
  );
};

export default PeoplePage;
