import React from 'react';

import PropTypes from 'prop-types';

import InviteeRow from './InviteeRow';
import './Invitees.css';

const Invitees = ({
  invitees,
  spaceName,
}) => (
  <>
    <h3 className="invitees">Invited</h3>
    {invitees.map((invitee) => (
      <InviteeRow
        key={invitee.id}
        email={invitee.email}
        spaceName={spaceName}
      />
    ))}
  </>
);

Invitees.propTypes = {
  invitees: PropTypes.instanceOf(Array).isRequired,
  spaceName: PropTypes.string.isRequired,
};

export default Invitees;
