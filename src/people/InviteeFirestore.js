import { db } from '../firebase-config';

const addInviteeFirestore = async (spaceName, data) => {
  try {
    await db.collection(`spaces/${spaceName}/invitees`).add(data);
  } catch (error) {
    throw new Error('Error adding invitee:', error);
  }
};

export default addInviteeFirestore;
