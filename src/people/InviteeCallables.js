import { functions } from '../firebase-config';

const resendInvite = async (invitedByName, spaceName, email) => {
  const callable = functions.httpsCallable('invitees-resendInvite');
  try {
    await callable({
      invitedByName,
      spaceName,
      email,
    });
  } catch (error) {
    throw new Error('Error calling invitees-resendInvite: ', error);
  }
};

export default resendInvite;
