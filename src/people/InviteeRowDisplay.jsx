import React from 'react';

import PropTypes from 'prop-types';
import { Alert } from 'react-bootstrap';

import './InviteeRowDisplay.css';

const InviteeRowDisplay = ({
  email,
  resendInvite,
  reInvited,
}) => (
  <div className="invitee-row">
    <div>
      <span>{`${email} · `}</span>
      <button
        type="button"
        onClick={resendInvite}
        className="resend-invite-link btn-sm"
        variant="outline-primary"
      >
        Resend invite
      </button>
    </div>
    {reInvited && <Alert className="invite-re-sent-alert" variant="success">{`Invite re-sent to ${email}`}</Alert>}
  </div>
);

InviteeRowDisplay.propTypes = {
  email: PropTypes.string.isRequired,
  resendInvite: PropTypes.func.isRequired,
  reInvited: PropTypes.bool.isRequired,
};

export default InviteeRowDisplay;
