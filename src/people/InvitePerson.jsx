import React, { useState, useEffect, useContext } from 'react';

import PropTypes from 'prop-types';

import addInviteeFirestore from './InviteeFirestore';
import CombinedUserContext from '../currentUser/CombinedUserContext';
import InvitePersonForm from './InvitePersonForm';

const InvitePerson = ({
  invitees,
  users,
  spaceName,
  isInviteLink,
}) => {
  const { combinedUser } = useContext(CombinedUserContext);
  const { name } = combinedUser;
  const [email, setEmail] = useState('');
  const [inviteButtonIsDisabled, setInviteButtonIsDisabled] = useState(true);

  // eslint-disable-next-line no-useless-escape
  const emailRegex = new RegExp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$');

  useEffect(() => {
    setInviteButtonIsDisabled(
      !emailRegex.test(email)
      || invitees.map((invitee) => invitee.email).includes(email)
      || users.map((user) => user.email).includes(email),
    );
  }, [email, invitees, users]);

  const updateEmail = (e) => {
    setEmail(e.target.value);
  };

  const addInvitee = async (e) => {
    e.preventDefault();
    await addInviteeFirestore(
      spaceName,
      {
        email: email.toLowerCase(),
        invitedByName: name,
        userLoggedIn: false,
        createdAt: new Date(),
      },
    );
    setEmail('');
  };

  return (
    <>
      <InvitePersonForm
        spaceName={spaceName}
        email={email}
        updateEmail={updateEmail}
        addInvitee={addInvitee}
        inviteButtonIsDisabled={inviteButtonIsDisabled}
        isInviteLink={isInviteLink}
      />
    </>
  );
};

InvitePerson.propTypes = {
  invitees: PropTypes.instanceOf(Array).isRequired,
  users: PropTypes.instanceOf(Array).isRequired,
  spaceName: PropTypes.string.isRequired,
  isInviteLink: PropTypes.bool.isRequired,
};

export default InvitePerson;
