import React from 'react';

import PropTypes from 'prop-types';

const DownloadInitialsImage = ({ name }) => (
  <div className="DownloadInitialsImage">
    <span>Don&apos;t have an image? </span>
    <a
      href={`https://ui-avatars.com/api/?name=${name.replace(' ', '+')}&background=random&size=512`}
      target="_blank"
      rel="noreferrer"
    >
      Download your initials as an image.
    </a>
  </div>
);

DownloadInitialsImage.propTypes = {
  name: PropTypes.string.isRequired,
};

export default DownloadInitialsImage;
