import React, { useState, useContext } from 'react';

import { useHistory } from 'react-router-dom';
import {
  Form,
  Button,
  Spinner,
} from 'react-bootstrap';
import { v4 as uuidv4 } from 'uuid';
import { storage } from '../firebase-config';

import signOut from '../shared/SignOut';
import CombinedUserContext from '../currentUser/CombinedUserContext';
import updateUserFirestore from '../shared/UserFirestore';
import ProfileName from './ProfileName';
import ProfileImage from './ProfileImage';
import DownloadInitialsImage from './DownloadInitialsImage';
import './ProfilePage.css';

const ProfilePage = () => {
  document.title = 'Profile';

  const { combinedUser } = useContext(CombinedUserContext);
  const { uid, name: oldName = '', avatarUrl } = combinedUser;

  const [name, setName] = useState(oldName || '');
  const [previewImage, setPreviewImage] = useState(null);
  const [saving, setSaving] = useState(false);
  const [hasError, setHasError] = useState(false);

  const history = useHistory();

  const submit = async (e) => {
    e.preventDefault();
    setSaving(true);
    if (previewImage) { // User wants to upload a new image
      const imageRef = storage.ref().child(`users/${uid}/${uuidv4()}/${previewImage.name}`);
      const uploadTask = imageRef.put(previewImage);
      uploadTask.on('state_changed',
        () => { },
        (error) => {
          try {
            throw new Error('Error uploading profile picture', error);
          } catch (err) {
            setHasError(true);
            setSaving(false);
          }
        },
        async () => {
          const imageUrl = await uploadTask.snapshot.ref.getDownloadURL();
          await updateUserFirestore(
            uid,
            {
              name: name.trim(),
              avatarUrl: imageUrl,
            },
          );
          history.push('/');
        });
    } else { // Image unchanged, avatarUrl must be non-null since submit button is enabled
      await updateUserFirestore(
        uid,
        {
          name: name.trim(),
          avatarUrl,
        },
      );
      history.push('/');
    }
  };

  const cancel = () => {
    if (history.length > 1) {
      history.goBack();
    } else {
      history.push('/');
    }
  };

  return (
    <main className="ProfilePage">
      <h3>How do you want to appear in Ensembl Talk?</h3>
      <div className="ProfilePage__form">
        <Form autoComplete="off" onSubmit={submit}>
          <ProfileName
            name={name}
            setName={setName}
            saving={saving}
          />
          <ProfileImage
            setPreviewImage={setPreviewImage}
            avatarUrl={avatarUrl}
            saving={saving}
          />
          {name && (
            <DownloadInitialsImage
              name={name}
            />
          )}
          <div className="ProfilePage__save-cancel-buttons">
            <Button
              variant="primary"
              disabled={name.trim() === '' || (!previewImage && !avatarUrl) || saving}
              className="btn-sm"
              type="submit"
            >
              {!saving && <span>Save profile</span>}
              {saving && (
                <>
                  <Spinner animation="border" size="sm" />
                  <span> Saving...</span>
                </>
              )}
            </Button>
            {oldName && avatarUrl && (
              <Button
                variant="outline-primary"
                className="btn-sm"
                onClick={cancel}
                disabled={saving}
              >
                Cancel
              </Button>
            )}
          </div>
          {hasError && (
            <div className="ProfilePage__error">
              Something went wrong. Try again.
            </div>
          )}
          {(!oldName || !avatarUrl) && (
            <button
              type="button"
              className="ProfilePage__sign-out-link"
              onClick={() => { signOut(); }}
            >
              Sign out
            </button>
          )}
        </Form>
      </div>
    </main>
  );
};

export default ProfilePage;
