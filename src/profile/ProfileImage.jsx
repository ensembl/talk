import React, { useState, useRef } from 'react';

import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';

import Avatar from '../shared/Avatar';
import './ProfileImage.css';

const ProfileImage = ({ setPreviewImage, avatarUrl, saving }) => {
  const inputEl = useRef(null);

  const [localImageUrl, setLocalImageUrl] = useState(null);
  const [isImageValid, setIsImageValid] = useState(true);

  const onFileReaderLoad = (e) => {
    setLocalImageUrl(e.target.result);
  };

  const validateImage = (image) => {
    if (image.size > 5000000) {
      setLocalImageUrl(null);
      setIsImageValid(false);
      return false;
    }
    setIsImageValid(true);
    return true;
  };

  const onChange = (e) => {
    if (e.target.files.length > 0 && validateImage(e.target.files[0])) {
      const file = e.target.files[0];
      const fr = new FileReader();
      fr.onload = onFileReaderLoad;
      fr.readAsDataURL(file);
      setPreviewImage(e.target.files[0]);
    }
  };

  const onClick = () => {
    inputEl.current.click();
  };

  return (
    <div className="ProfileImage">
      <h5>Profile image</h5>

      <div className="ProfileImage__avatar">
        {avatarUrl && !localImageUrl && (
          <Avatar
            avatarUrl={avatarUrl}
            size="lg"
          />
        )}
        {localImageUrl && (
          <Avatar
            avatarUrl={localImageUrl}
            size="lg"
          />
        )}
      </div>
      <Button
        variant={`${(avatarUrl || localImageUrl) ? 'outline-primary' : 'primary'}`}
        size="sm"
        onClick={onClick}
        disabled={saving}
      >
        {(avatarUrl || localImageUrl) ? 'Replace image' : 'Upload image'}
      </Button>

      <div className="ProfileImage__warning-message">
        {isImageValid && <span>Square image that&apos;s less than 5 MB.</span>}
        {!isImageValid && <span>Too big. Upload an image smaller than 5 MB.</span>}
      </div>

      <input
        onChange={onChange}
        ref={inputEl}
        type="file"
        accept="image/*"
        hidden
      />
    </div>
  );
};

ProfileImage.propTypes = {
  setPreviewImage: PropTypes.func.isRequired,
  avatarUrl: PropTypes.string,
  saving: PropTypes.bool.isRequired,
};

ProfileImage.defaultProps = {
  avatarUrl: null,
};

export default ProfileImage;
