import React from 'react';

import PropTypes from 'prop-types';
import { Form } from 'react-bootstrap';

import './ProfileName.css';

const ProfileName = ({
  name,
  setName,
  saving,
}) => (
  <div className="ProfileName">
    <h5>Name</h5>
    <Form.Control
      type="text"
      onChange={(e) => { setName(e.target.value); }}
      value={name}
      placeholder="Name"
      disabled={saving}
    />
  </div>
);

ProfileName.propTypes = {
  name: PropTypes.string.isRequired,
  setName: PropTypes.func.isRequired,
  saving: PropTypes.bool.isRequired,
};

export default ProfileName;
