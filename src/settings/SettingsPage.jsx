import React, { useContext, useEffect, useState } from 'react';

import { useDocumentData } from 'react-firebase-hooks/firestore';
import { useParams } from 'react-router-dom';

import { db } from '../firebase-config';
import PageLayout from '../shared/layout/PageLayout';
import SlackIntegration from './SlackIntegration';
import CombinedUserContext from '../currentUser/CombinedUserContext';
import updateSpaceFirestore from '../shared/SpaceFirestore';
import updateUserFirestore from '../shared/UserFirestore';
import PageLoadingSpinner from '../shared/PageLoadingSpinner';
import PageNotFound from '../shared/PageNotFound';

import './SettingsPage.css';

const SettingsPage = () => {
  const { combinedUser } = useContext(CombinedUserContext);
  const {
    uid,
    allowableSpaceNames,
    isIntegratedWithSlack: isUserIntegrated,
  } = combinedUser;
  const { spaceName: urlSpaceName } = useParams();

  const onCommunitySpace = window.location.pathname.startsWith('/-/Ensembl%20Community');

  const spaceRef = db.collection('spaces').doc(urlSpaceName);
  const [space, spaceLoading, spaceError] = useDocumentData(spaceRef, { idField: 'id' });
  const [isSpaceIntegrated, setIsSpaceIntegrated] = useState(false);

  useEffect(() => {
    if (space) {
      const { isIntegratedWithSlack } = space;
      setIsSpaceIntegrated(isIntegratedWithSlack);
    }
  }, [space]);

  document.title = 'Connect to Slack';

  const updateSpace = async (data) => {
    await updateSpaceFirestore(urlSpaceName, data);
  };

  const updateUser = async (data) => {
    await updateUserFirestore(uid, data);
  };

  return (
    <PageLayout
      uid={uid}
      urlSpaceName={urlSpaceName}
      onCommunitySpace={onCommunitySpace}
      allowableSpaceNames={allowableSpaceNames}
      mobileHeaderTitle="Connect to Slack"
      mainContent={(
        <>
          {spaceLoading && <PageLoadingSpinner />}
          {spaceError && <PageNotFound />}
          {space && (
            <>
              <h3 className="SettingsPage__title">Connect to Slack</h3>
              <SlackIntegration
                updateSpace={updateSpace}
                updateUser={updateUser}
                spaceName={urlSpaceName}
                isSpaceIntegrated={isSpaceIntegrated}
                isUserIntegrated={isUserIntegrated}
              />
            </>
          )}
        </>
      )}
    />
  );
};

export default SettingsPage;
