import React, { useState, useContext, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import PageLoadingSpinner from '../shared/PageLoadingSpinner';

import { functions } from '../firebase-config';

import CombinedUserContext from '../currentUser/CombinedUserContext';

import './SlackOAuthRedirectPage.css';

const SlackOAuthRedirectPage = () => {
  const { oAuthType } = useParams();
  const history = useHistory();
  const { search } = history.location;
  const params = new URLSearchParams(!search ? '' : search);
  const code = params.get('code');
  const state = params.get('state');
  const error = params.get('error');

  // Accessing this path without being redirected from Slack will fail
  if (state !== sessionStorage.getItem('slackState')) {
    history.push('/');
    return '';
  }

  const { combinedUser } = useContext(CombinedUserContext);
  const {
    lastVisitedSpaceName: spaceName,
  } = combinedUser;

  // User canceled
  if (error === 'access_denied') {
    history.push(`/-/${spaceName}/settings`);
  }

  const callable = functions.httpsCallable('slackOAuth-slackOAuth');

  const [isLoading, setIsLoading] = useState(true);
  const [message, setMessage] = useState(null);

  useEffect(() => {
    callable({ oAuthType, code, spaceName }).then((res) => {
      const { data } = res;
      const { status } = data;
      setMessage(
        status === 'ok'
          ? 'Connected to Slack. Redirecting back...'
          : 'Something went wrong. Try again later. Redirecting back...',
      );
      setIsLoading(false);
    });
  }, []);

  useEffect(() => {
    if (message) {
      setTimeout(() => { history.push(`/-/${spaceName}/settings`); }, 2000);
    }
  }, [message]);

  return (
    <div className="SlackOAuthRedirectPage">
      <PageLoadingSpinner
        message={
          isLoading
            ? 'Connecting to Slack...'
            : `${message}`
        }
      />
    </div>
  );
};

export default SlackOAuthRedirectPage;
