import React from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'react-bootstrap';

import SlackConnection from './SlackConnection';

const SlackIntegration = ({
  spaceName, isSpaceIntegrated, updateSpace, updateUser, isUserIntegrated,
}) => (
  <>
    <ul>
      <li>Get notified of new talks and docs in a specified Slack channel</li>
      <li>View previews of talk links and doc links in Slack</li>
      <li>Create a talk in Slack</li>
    </ul>
    <Alert
      variant={(isSpaceIntegrated && isUserIntegrated && 'success') || 'warning'}
    >
      {
        (isSpaceIntegrated && isUserIntegrated && 'Ensembl Talk is connected to Slack')
        || 'Complete two steps to connect Ensembl Talk to Slack'
      }
    </Alert>
    <SlackConnection
      spaceName={spaceName}
      isIntegrated={isSpaceIntegrated}
      updateIsIntegrated={updateSpace}
    />
    {isSpaceIntegrated && (
      <SlackConnection
        isIntegrated={isUserIntegrated}
        updateIsIntegrated={updateUser}
      />
    )}
  </>
);

SlackIntegration.propTypes = {
  spaceName: PropTypes.string.isRequired,
  isSpaceIntegrated: PropTypes.bool.isRequired,
  updateSpace: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired,
  isUserIntegrated: PropTypes.bool.isRequired,
};

export default SlackIntegration;
