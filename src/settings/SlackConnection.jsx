import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';

import SlackConnectionDisplay from './SlackConnectionDisplay';
import PageLoadingSpinner from '../shared/PageLoadingSpinner';

const SlackConnection = ({
  spaceName,
  isIntegrated,
  updateIsIntegrated,
}) => {
  const connectTitle = spaceName
    ? `1. Connect ${spaceName} to your Slack workspace`
    : '2. Connect your Ensembl Talk account to your Slack account';

  const connectBtnDisplay = spaceName
    ? (
      <img
        alt="Add to Slack"
        height="40"
        width="139"
        src="https://platform.slack-edge.com/img/add_to_slack.png"
        srcSet="https://platform.slack-edge.com/img/add_to_slack.png 1x, https://platform.slack-edge.com/img/add_to_slack@2x.png 2x"
      />
    )
    : (
      <img
        alt="Sign in with Slack"
        height="40"
        width="172"
        src="https://platform.slack-edge.com/img/sign_in_with_slack.png"
        srcSet="https://platform.slack-edge.com/img/sign_in_with_slack.png 1x, https://platform.slack-edge.com/img/sign_in_with_slack@2x.png 2x"
      />
    );

  const connectLocation = spaceName
    ? 'https://slack.com/oauth/v2/authorize?client_id=1843757088518.2112458372355&scope=chat:write,chat:write.public,commands,incoming-webhook,links:read,links:write&user_scope=chat:write,links:read,links:write&redirect_uri=https://app.ensembl.so/slack-oauth-redirect/space'
    : 'https://slack.com/oauth/v2/authorize?client_id=1843757088518.2112458372355&scope=chat:write,chat:write.public,commands,links:read,links:write&user_scope=chat:write,links:read,links:write&redirect_uri=https://app.ensembl.so/slack-oauth-redirect/user';

  const onConnect = () => {
    const state = uuidv4();
    sessionStorage.setItem('slackState', state);
    window.location = `${connectLocation}&state=${state}`;
  };

  const disconnectTitle = spaceName
    ? `1. ${spaceName} is connected to your Slack workspace ✅`
    : '2. Your Ensembl Talk account is connected to your Slack account ✅';

  const [isLoading, setIsLoading] = useState(false);
  const onDisconnect = async () => {
    setIsLoading(true);
    try {
      await updateIsIntegrated({ isIntegratedWithSlack: false });
    } catch (error) {
      throw new Error('Error disconnect user from Slack');
    }
    setIsLoading(false);
  };

  return (
    <div className="SlackConnection">
      {isLoading && <PageLoadingSpinner />}
      {!isIntegrated && !isLoading && (
        <SlackConnectionDisplay
          title={connectTitle}
          btnDisplay={connectBtnDisplay}
          btnOnClick={onConnect}
          isSlackButton
        />
      )}
      {isIntegrated && !isLoading && (
        <SlackConnectionDisplay
          title={disconnectTitle}
          btnVariant="outline-primary"
          btnDisplay={<span>Disconnect from Slack</span>}
          btnOnClick={onDisconnect}
        />
      )}
    </div>
  );
};

SlackConnection.propTypes = {
  spaceName: PropTypes.string,
  isIntegrated: PropTypes.bool.isRequired,
  updateIsIntegrated: PropTypes.func.isRequired,
};

SlackConnection.defaultProps = {
  spaceName: '',
};

export default SlackConnection;
