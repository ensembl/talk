import React from 'react';

import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';

import './SlackConnectionDisplay.css';

const SlackConnectionDisplay = ({
  title,
  btnVariant,
  btnDisplay,
  btnOnClick,
  isSlackButton,
}) => (
  <div className="SlackConnectionDisplay">
    <h5>{title}</h5>
    <Button
      className={`btn-sm ${isSlackButton ? 'slackButton' : ''}`}
      variant={btnVariant}
      onClick={btnOnClick}
    >
      {btnDisplay}
    </Button>
  </div>
);

SlackConnectionDisplay.propTypes = {
  title: PropTypes.string.isRequired,
  btnVariant: PropTypes.string,
  btnDisplay: PropTypes.node.isRequired,
  btnOnClick: PropTypes.func.isRequired,
  isSlackButton: PropTypes.bool,
};

SlackConnectionDisplay.defaultProps = {
  btnVariant: 'link',
  isSlackButton: false,
};

export default SlackConnectionDisplay;
