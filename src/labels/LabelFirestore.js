import { db } from '../firebase-config';

const addLabelFirestore = async (spaceName, data) => {
  try {
    await db.collection(`spaces/${spaceName}/labels`).add(data);
  } catch (error) {
    throw new Error('Error adding label:', error);
  }
};

const updateLabelFirestore = async (spaceName, id, data) => {
  try {
    await db.collection(`spaces/${spaceName}/labels`).doc(id).update(data);
  } catch (error) {
    throw new Error('Error updating label:', error);
  }
};

export { addLabelFirestore, updateLabelFirestore };
