import React from 'react';

import PropTypes from 'prop-types';
import { Form, Nav } from 'react-bootstrap';

import '../shared/Label.css';
import './EditLabel.css';

const EditLabel = ({
  setName,
  setColor,
  name,
  color,
}) => (
  <>
    <Form.Control
      type="text"
      className="one-line-input EditLabel__name-input"
      onChange={(e) => { setName(e.target.value); }}
      value={name}
      placeholder="Name"
    />

    <Nav
      onSelect={(eventKey) => { setColor(eventKey); }}
      className="EditLabel__color-selects"
    >
      <Nav.Item>
        <Nav.Link className={`EditLabel__color-select Label__color-blue ${color === 'blue' ? 'active' : ''}`} eventKey="blue" />
      </Nav.Item>
      <Nav.Item>
        <Nav.Link className={`EditLabel__color-select Label__color-green ${color === 'green' ? 'active' : ''}`} eventKey="green" />
      </Nav.Item>
      <Nav.Item>
        <Nav.Link className={`EditLabel__color-select Label__color-red ${color === 'red' ? 'active' : ''}`} eventKey="red" />
      </Nav.Item>
      <Nav.Item>
        <Nav.Link className={`EditLabel__color-select Label__color-purple ${color === 'purple' ? 'active' : ''}`} eventKey="purple" />
      </Nav.Item>
      <Nav.Item>
        <Nav.Link className={`EditLabel__color-select Label__color-orange ${color === 'orange' ? 'active' : ''}`} eventKey="orange" />
      </Nav.Item>
    </Nav>
  </>
);

EditLabel.propTypes = {
  setName: PropTypes.func.isRequired,
  setColor: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
};

export default EditLabel;
