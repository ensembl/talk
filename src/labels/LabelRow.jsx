import React, { useState } from 'react';

import PropTypes from 'prop-types';
import { Form, Button } from 'react-bootstrap';

import { updateLabelFirestore } from './LabelFirestore';
import Label from '../shared/Label';
import EditLabel from './EditLabel';
import EditContentButtons from '../shared/EditContentButtons';
import DeleteModal from '../shared/DeleteModal';
import './LabelRow.css';

const LabelRow = ({
  labelId,
  labelName,
  labelColor,
  spaceName,
}) => {
  const [name, setName] = useState(labelName);
  const [color, setColor] = useState(labelColor);

  const [isEditMode, setIsEditMode] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [isDeleting, setIsDeleting] = useState(false);

  const enterEditMode = () => {
    setName(labelName);
    setColor(labelColor);
    setIsEditMode(true);
  };

  const updateLabel = async (e) => {
    e.preventDefault();
    setIsEditMode(false);
    await updateLabelFirestore(
      spaceName,
      labelId,
      {
        name: name.trim(),
        color,
      },
    );
  };

  const deleteFromModal = async () => {
    setIsDeleting(true);
    await updateLabelFirestore(
      spaceName,
      labelId,
      {
        toDelete: true,
      },
    );
  };

  return (
    <div className="LabelRow text-muted">
      {!isEditMode && (
        <div className="LabelRow__view">
          <Label
            name={labelName}
            color={labelColor}
            id={labelId}
            type="talk"
          />
          <Button
            onClick={enterEditMode}
            className="btn-sm"
            variant="outline-primary"
          >
            Edit
          </Button>
        </div>
      )}
      {isEditMode && (
        <Form autoComplete="off" onSubmit={updateLabel}>
          <EditLabel
            setName={setName}
            setColor={setColor}
            name={name}
            color={color}
          />
          <EditContentButtons
            setIsEditMode={setIsEditMode}
            saveButtonIsDisabled={name.trim() === '' || (name === labelName && color === labelColor)}
            showDeleteLink
            setShowDeleteModal={setShowDeleteModal}
            deleteButtonLabel="Delete label"
          />
        </Form>
      )}
      <DeleteModal
        showDeleteModal={showDeleteModal}
        setShowDeleteModal={setShowDeleteModal}
        isDeleting={isDeleting}
        deleteFromModal={deleteFromModal}
        modalTitle="Delete label forever"
        modalBody={(
          <>
            <p>
              Are you sure? Deleted labels cannot be recovered.
              This label will be removed from all currently attached talks and docs.
            </p>
            <p>
              Consider renaming the label instead.
            </p>
          </>
        )}
        modalDeleteButtonLabel="Delete label forever"
      />
    </div>
  );
};

LabelRow.propTypes = {
  labelId: PropTypes.string.isRequired,
  labelName: PropTypes.string.isRequired,
  labelColor: PropTypes.string.isRequired,
  spaceName: PropTypes.string.isRequired,
};

export default LabelRow;
