import React from 'react';

import PropTypes from 'prop-types';
import { Form, Button } from 'react-bootstrap';

import './AddLabelDisplay.css';

const AddLabelDisplay = ({
  add,
  setName,
  name,
}) => (
  <div className="AddLabelDisplay">
    <Form autoComplete="off" onSubmit={add}>
      <Form.Control
        type="text"
        className="AddLabelDisplay__input one-line-input"
        onChange={(e) => { setName(e.target.value); }}
        value={name}
        placeholder="Label name"
      />
      <Button
        variant="primary"
        disabled={name.trim() === ''}
        className="btn-sm"
        type="submit"
      >
        Add label
      </Button>
    </Form>

  </div>
);

AddLabelDisplay.propTypes = {
  add: PropTypes.func.isRequired,
  setName: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
};

export default AddLabelDisplay;
