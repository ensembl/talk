import React from 'react';

import PropTypes from 'prop-types';

import LabelRow from './LabelRow';

const LabelRows = ({
  labels,
  spaceName,
}) => (
  <>
    {labels.filter((label) => !label.toDelete).map((label) => (
      <LabelRow
        key={label.id}
        labelId={label.id}
        labelName={label.name}
        labelColor={label.color}
        spaceName={spaceName}
      />
    ))}
  </>
);

LabelRows.propTypes = {
  labels: PropTypes.instanceOf(Array).isRequired,
  spaceName: PropTypes.string.isRequired,
};

export default LabelRows;
