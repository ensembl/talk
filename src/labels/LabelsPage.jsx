import React, { useEffect, useContext } from 'react';

import { useCollectionData } from 'react-firebase-hooks/firestore';
import { useParams, Link } from 'react-router-dom';
import { Redirect } from 'react-router';

import { db } from '../firebase-config';
import CombinedUserContext from '../currentUser/CombinedUserContext';
import PageLoadingSpinner from '../shared/PageLoadingSpinner';
import AddLabel from './AddLabel';
import LabelRows from './LabelRows';
import PageLayout from '../shared/layout/PageLayout';
import updateUserFirestore from '../shared/UserFirestore';
import PageNotFound from '../shared/PageNotFound';

import './LabelsPage.css';

const LabelsPage = () => {
  if (window.location.pathname.startsWith('/-/Ensembl%20Community')) {
    return <Redirect to="/" />;
  }

  const { spaceName: urlSpaceName } = useParams();

  const { combinedUser } = useContext(CombinedUserContext);
  const { uid, allowableSpaceNames } = combinedUser;
  const [labels, labelsLoading, labelsError] = useCollectionData(db.collection(`spaces/${urlSpaceName}/labels`).orderBy('name', 'asc'), { idField: 'id' });

  const onCommunitySpace = window.location.pathname.startsWith('/-/Ensembl%20Community');

  document.title = 'Labels';

  useEffect(() => {
    if (labels) {
      updateUserFirestore(uid, { lastVisitedSpaceName: urlSpaceName });
    }
  }, [labels, labelsLoading]);

  return (
    <PageLayout
      uid={uid}
      urlSpaceName={urlSpaceName}
      onCommunitySpace={onCommunitySpace}
      allowableSpaceNames={allowableSpaceNames}
      mobileHeaderTitle="Labels"
      mainContent={(
        <>
          {labelsLoading && <PageLoadingSpinner />}
          {labelsError && <PageNotFound />}
          {labels && (
            <>
              <h3 className="LabelsPage__add-label-title">{`Add a new label to ${urlSpaceName}`}</h3>
              <AddLabel
                spaceName={urlSpaceName}
              />
              {labels.length === 0 && (
                <div className="LabelsPage__empty-state">
                  <span>
                    Use labels to categorize talks and docs together.
                    Filter by labels on the home page.
                  </span>
                  <Link to="/-/Ensembl%20Community/docs/Yw3OO9ii4IbKXcm0R2UR"> Learn more</Link>
                  <div>Create your first label ☝️</div>
                </div>
              )}
              {labels.length > 0 && <h3>Labels</h3>}
              {labels.filter((label) => !label.toDelete).length > 0 && (
                <LabelRows
                  labels={labels}
                  spaceName={urlSpaceName}
                />
              )}
            </>
          )}
        </>
      )}
    />
  );
};

export default LabelsPage;
