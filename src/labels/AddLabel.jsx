import React, { useState } from 'react';

import PropTypes from 'prop-types';

import { addLabelFirestore } from './LabelFirestore';
import AddLabelDisplay from './AddLabelDisplay';

const AddLabel = ({
  spaceName,
}) => {
  const [name, setName] = useState('');

  const addObject = async (e) => {
    e.preventDefault();
    const data = {
      name: name.trim(),
    };
    setName('');
    switch (Math.floor(Math.random() * 5)) {
      case 0: data.color = 'blue'; break;
      case 1: data.color = 'green'; break;
      case 2: data.color = 'red'; break;
      case 3: data.color = 'purple'; break;
      default: data.color = 'orange';
    }
    await addLabelFirestore(
      spaceName,
      data,
    );
  };

  return (
    <AddLabelDisplay
      add={addObject}
      setName={setName}
      name={name}
    />
  );
};

AddLabel.propTypes = {
  spaceName: PropTypes.string.isRequired,
};

export default AddLabel;
