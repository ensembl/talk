import React, { useState, useContext, useEffect } from 'react';

import PropTypes from 'prop-types';

import { AuthContext } from './AuthProvider';
import { setJwt } from '../shared/ckeditor/CKEditorTokenCallables';

export const CKEditorTokenContext = React.createContext();

export const CKEditorTokenProvider = ({ children }) => {
  const [ckeditorJwt, setCkeditorJwt] = useState('');

  const { currentUser } = useContext(AuthContext);

  useEffect(() => {
    if (currentUser) {
      setJwt(setCkeditorJwt);
    } else {
      setCkeditorJwt('');
    }
  }, [currentUser]);

  return (
    <CKEditorTokenContext.Provider value={{ ckeditorJwt }}>
      {children}
    </CKEditorTokenContext.Provider>
  );
};

CKEditorTokenProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default CKEditorTokenProvider;
