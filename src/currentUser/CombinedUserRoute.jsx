import React, { useEffect, useState, useContext } from 'react';

import { Route, useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useDocumentData } from 'react-firebase-hooks/firestore';

import { AuthContext } from './AuthProvider';
import PageLoadingPage from '../shared/layout/PageLoadingPage';
import CombinedUserContext from './CombinedUserContext';
import { db } from '../firebase-config';
import { setHeapProperties } from '../shared/HeapUtils';
import packageJson from '../../package.json';

const CombinedUserRoute = ({
  component: RouteComponent,
  path,
  ...rest
}) => {
  const { currentUser } = useContext(AuthContext);
  const [firestoreUser] = useDocumentData(db.collection('users').doc(currentUser ? currentUser.uid : '-1'), { idField: 'id' });
  const [combinedUser, setCombinedUser] = useState({
    uid: currentUser ? currentUser.uid : '',
    name: '',
    avatarUrl: '',
    lastVisitedSpaceName: '',
    allowableSpaceNames: [],
  });
  const [copiedFirestoreUser, setCopiedFirestoreUser] = useState(false);

  const history = useHistory();

  useEffect(async () => {
    if (firestoreUser) {
      const {
        email,
        name = '',
        avatarUrl = '',
        activityPageLastVisitedAt = null,
        dismissedNewTalkOnboarding = false,
        dismissedCommentOnboarding = false,
        lastVisitedSpaceName,
        allowableSpaceNames = [],
        allowableSpaceNamesUpdatedAt = null,
        isIntegratedWithSlack = false,
      } = firestoreUser;

      // Wait until allowableSpaceNamesUpdatedAt is set
      // before processing the Firestore user
      if (allowableSpaceNamesUpdatedAt) {
        allowableSpaceNames.sort();

        let safeLastVisitedSpaceName = lastVisitedSpaceName;
        if (!allowableSpaceNames.includes(lastVisitedSpaceName) && allowableSpaceNames.length > 0) {
          [safeLastVisitedSpaceName] = allowableSpaceNames;
        }

        if (!name || !avatarUrl) {
          history.push('/profile');
        } else if (allowableSpaceNames.length === 0 && !window.location.pathname.startsWith('/-/Ensembl%20Community')) {
          history.push('/new-space');
        }

        setCombinedUser({
          ...combinedUser,
          email,
          name,
          avatarUrl,
          activityPageLastVisitedAt,
          dismissedNewTalkOnboarding,
          dismissedCommentOnboarding,
          lastVisitedSpaceName: safeLastVisitedSpaceName,
          allowableSpaceNames,
          isIntegratedWithSlack,
          isAdmin: email === 'victor@ensembl.so' || email === 'ed@ensembl.so',
        });
        setCopiedFirestoreUser(true);
      }
    }
  }, [firestoreUser]);

  // currentUser is null if window.location.pathname.startsWith('/-/Ensembl%20Community')
  if (!currentUser || copiedFirestoreUser) {
    const {
      uid,
      lastVisitedSpaceName,
      email,
    } = combinedUser;

    setHeapProperties(uid, lastVisitedSpaceName, email, packageJson.version);

    return (
      <CombinedUserContext.Provider value={{ combinedUser }}>
        <Route
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...rest}
          // eslint-disable-next-line react/jsx-props-no-spreading
          render={(routeProps) => <RouteComponent {...routeProps} />}
        />
      </CombinedUserContext.Provider>
    );
  }
  return <PageLoadingPage />;
};

CombinedUserRoute.propTypes = {
  component: PropTypes.elementType.isRequired,
  path: PropTypes.string.isRequired,
};

export default CombinedUserRoute;
