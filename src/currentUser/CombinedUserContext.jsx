import React from 'react';

const CombinedUserContext = React.createContext();

export default CombinedUserContext;
