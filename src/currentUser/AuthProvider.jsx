import React, { useState } from 'react';

import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import firebase from 'firebase/app';
import 'firebase/auth';

import PageLoadingPage from '../shared/layout/PageLoadingPage';

export const AuthContext = React.createContext();

export const AuthProvider = ({ children }) => {
  const [currentUser, setCurrentUser] = useState(null);
  const [loading, setLoading] = useState(true);
  const history = useHistory();

  firebase.auth().onAuthStateChanged((user) => {
    if (
      !user
      && !firebase.auth().isSignInWithEmailLink(window.location.href)
      && window.location.pathname !== '/signup'
      && !window.location.pathname.startsWith('/-/Ensembl%20Community')
    ) {
      history.push('/login');
    }
    setCurrentUser(user);
    setLoading(false);
  });

  return (
    <>
      {loading && <PageLoadingPage />}
      {!loading && (
        <AuthContext.Provider
          value={{ currentUser }}
        >
          {children}
        </AuthContext.Provider>
      )}
    </>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AuthProvider;
