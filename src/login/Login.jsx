import React, { useEffect, useContext } from 'react';

import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';
import firebase from 'firebase/app';
import * as firebaseui from 'firebaseui';
import 'firebaseui/dist/firebaseui.css';

import { AuthContext } from '../currentUser/AuthProvider';

import './Login.css';

const Login = () => {
  const { currentUser } = useContext(AuthContext);

  if (currentUser) {
    return <Redirect to="/" />;
  }

  const uiConfig = {
    signInFlow: 'popup',
    signInSuccessUrl: '/',
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      {
        provider: 'microsoft.com',
        providerName: 'Microsoft',
        customParameters: {
          redirect_uri: 'https://auth.app.ensembl.so/__/auth/handler',
        },
      },
      {
        provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
        signInMethod: firebase.auth.EmailAuthProvider.EMAIL_LINK_SIGN_IN_METHOD,
      },
    ],
    callbacks: {
      signInSuccessWithAuthResult: () => true,
    },
    privacyPolicyUrl: 'https://ensembl.so/privacy-policy',
    tosUrl: 'https://ensembl.so/terms-of-service',
  };

  useEffect(() => {
    // https://github.com/firebase/firebaseui-web/issues/216#issuecomment-459302414
    const ui = firebaseui.auth.AuthUI.getInstance() || new firebaseui.auth.AuthUI(firebase.auth());
    ui.start('#firebaseui-auth-container', uiConfig);
  }, []);

  return (
    <main className="Login">
      <h1 className="Login__logo-header">
        <img className="Login__logo" src="/logo512.png" alt="logo" />
        <span>Ensembl Talk</span>
      </h1>

      {window.location.pathname === '/login' && <h4 className="Login__title">Sign in</h4>}
      {window.location.pathname === '/signup' && <h4 className="Login__sign-up-title">Sign up</h4>}
      <div id="firebaseui-auth-container" />
      {window.location.pathname === '/signup' && (
        <div className="Login__login-message">
          <span>Have an account? </span>
          <Link to="/login">Sign in</Link>
        </div>
      )}
    </main>
  );
};

export default Login;
