import React, { useContext } from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { Redirect } from 'react-router';
import CombinedUserContext from '../currentUser/CombinedUserContext';
import JoinEnsemblCommunity from '../shared/JoinEnsemblCommunity';

import './JoinPage.css';

const JoinPage = () => {
  const { combinedUser } = useContext(CombinedUserContext);
  const { uid } = combinedUser;
  const { email, allowableSpaceNames } = combinedUser;
  const history = useHistory();

  const communityHomePagePath = '/-/Ensembl%20Community/home';
  const communityDocPath = '/-/Ensembl%20Community/docs/SrwKEBolh6DCEOisZh0s';

  if (allowableSpaceNames && allowableSpaceNames.includes('Ensembl Community')) {
    return <Redirect to={communityHomePagePath} />;
  }

  const signIn = () => {
    history.push('/login');
  };

  return (
    <div className="JoinPage">
      <img className="JoinPage__logo" src="/logo512.png" alt="logo" />
      <h1 className="JoinPage__header">Ensembl Community</h1>

      {!uid && (
        <Button
          variant="primary"
          onClick={signIn}
        >
          Sign in to Ensembl Talk and join
        </Button>
      )}
      {uid && <JoinEnsemblCommunity email={email} />}

      <a className="JoinPage__visit" href={communityHomePagePath}>Visit as guest</a>
      <a className="JoinPage__what-is" href={communityDocPath}>What is Ensembl Community?</a>
    </div>
  );
};

export default JoinPage;
