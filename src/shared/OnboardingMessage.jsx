import React from 'react';

import PropTypes from 'prop-types';
import { Button, Alert } from 'react-bootstrap';

import './OnboardingMessage.css';

const OnboardingMessage = ({
  title,
  body,
  dismissForever,
}) => (
  <Alert
    className="onboarding-message"
    variant="warning"
  >
    <Alert.Heading>
      {title}
    </Alert.Heading>
    {body}
    <hr />
    <div className="d-flex justify-content-end">
      <Button
        className="btn-sm"
        onClick={dismissForever}
        variant="primary"
      >
        Got it, dismiss forever
      </Button>
    </div>
  </Alert>
);

OnboardingMessage.propTypes = {
  title: PropTypes.string.isRequired,
  body: PropTypes.objectOf(PropTypes.any).isRequired,
  dismissForever: PropTypes.func.isRequired,
};

export default OnboardingMessage;
