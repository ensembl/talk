import { db } from '../firebase-config';

const updateUserFirestore = async (id, data) => {
  try {
    await db.collection('users').doc(id).update(data);
  } catch (error) {
    throw new Error('Error updating user:', error);
  }
};

export default updateUserFirestore;
