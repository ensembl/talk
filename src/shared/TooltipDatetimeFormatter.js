const tooltipDatetimeFormatter = (datetimeSeconds) => {
  try {
    return new Date(datetimeSeconds * 1000).toLocaleTimeString(
      'en-us',
      {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: 'numeric',
        minute: '2-digit',
        hour12: true,
      },
    );
  } catch (error) {
    // Error creating toLocaleTimeString
    return datetimeSeconds;
  }
};

export default tooltipDatetimeFormatter;
