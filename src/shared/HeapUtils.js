const setHeapProperties = (userId, spaceName, email, version) => {
  window.heap.identify(userId);
  window.heap.addUserProperties({
    spaceName,
    email,
    version,
  });
};

const resetHeapProperties = () => {
  window.heap.resetIdentity();
};

export { setHeapProperties, resetHeapProperties };
