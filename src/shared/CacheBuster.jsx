import React, { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import packageJson from '../../package.json';

const CacheBuster = ({ children }) => {
  const location = useLocation();

  const validateCache = () => {
    fetch('/meta.json').then(async (result) => {
      const metaJson = await result.json();
      if (metaJson.version !== packageJson.version) {
        localStorage.setItem('app.ensembl.so/should-bust-cache', 'yes');
      }
    }).catch((error) => {
      throw new Error('Error fetching /meta.json: ', error);
    });
  };

  useEffect(() => {
    // Don't force reload more often than 10 minutes and be robust against infinite reloading loops
    const lastReloadDateUnix = localStorage.getItem('app.ensembl.so/reload-date-unix') || 0;
    const nowDateUnix = new Date().getTime();
    if (
      localStorage.getItem('app.ensembl.so/should-bust-cache') === 'yes'
      && ((nowDateUnix - lastReloadDateUnix) > 600000) // 10 minutes
    ) {
      localStorage.setItem('app.ensembl.so/should-bust-cache', 'no');
      localStorage.setItem('app.ensembl.so/reload-date-unix', nowDateUnix);
      window.location.reload();
    } else {
      validateCache();
    }
  }, [location]);

  return (
    <>
      {children}
    </>
  );
};

CacheBuster.propTypes = {
  children: PropTypes.node.isRequired,
};

export default CacheBuster;
