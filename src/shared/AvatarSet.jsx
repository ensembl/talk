import React from 'react';

import PropTypes from 'prop-types';

import Avatar from './Avatar';
import AvatarsDropdown from './AvatarsDropdown';
import './AvatarSet.css';

const AvatarSet = ({
  addedAvatars,
  notAddedAvatars,
  addAvatar,
  removeAvatar,
  addAvatarText,
  spaceName,
}) => (
  <div className="AvatarSet">
    {addedAvatars.length > 0 && (
      <div className="AvatarSet__avatars">
        {addedAvatars.map((avatar) => (
          <div key={avatar.id}>
            {removeAvatar && (
              <Avatar
                avatarUrl={avatar.avatarUrl}
                tooltip={avatar.name}
                remove={() => removeAvatar(avatar.id)}
              />
            )}
            {!removeAvatar && (
              <Avatar
                avatarUrl={avatar.avatarUrl}
                tooltip={avatar.name}
              />
            )}
          </div>
        ))}
      </div>
    )}
    {addAvatar && (
      <div className="AvatarSet__dropdown">
        <AvatarsDropdown
          notAddedAvatars={notAddedAvatars}
          addAvatar={addAvatar}
          addAvatarText={addAvatarText}
          spaceName={spaceName}
        />
      </div>
    )}
  </div>
);

AvatarSet.propTypes = {
  addedAvatars: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string.isRequired,
  })).isRequired,
  notAddedAvatars: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string,
  })),
  addAvatar: PropTypes.func,
  removeAvatar: PropTypes.func,
  addAvatarText: PropTypes.string,
  spaceName: PropTypes.string,
};

AvatarSet.defaultProps = {
  notAddedAvatars: [],
  addAvatar: null,
  removeAvatar: null,
  addAvatarText: '',
  spaceName: '',
};

export default AvatarSet;
