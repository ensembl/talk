import { db } from '../firebase-config';

const addTalkFirestore = async (spaceName, data) => {
  try {
    return await db.collection(`spaces/${spaceName}/talks`).add(data);
  } catch (error) {
    throw new Error('Error adding talk:', error);
  }
};

const updateTalkFirestore = async (spaceName, id, data) => {
  try {
    await db.collection(`spaces/${spaceName}/talks`).doc(id).update(data);
  } catch (error) {
    throw new Error('Error updating talk:', error);
  }
};

export { addTalkFirestore, updateTalkFirestore };
