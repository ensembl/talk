import firebase from 'firebase/app';
import { resetHeapProperties } from './HeapUtils';

const signOut = () => {
  firebase.auth().signOut();
  resetHeapProperties();
};

export default signOut;
