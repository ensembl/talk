import React from 'react';

import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';

import './EditContentButtons.css';

const EditContentButtons = ({
  setIsEditMode,
  saveButtonIsDisabled,
  showDeleteLink,
  setShowDeleteModal,
  deleteButtonLabel,
}) => (
  <div className="EditContentButtons">
    <Button
      className="btn-sm"
      disabled={saveButtonIsDisabled}
      type="submit"
    >
      Save
    </Button>
    <Button
      variant="outline-primary"
      className="btn-sm EditContentButtons__cancel-button"
      onClick={() => { setIsEditMode(false); }}
    >
      Cancel
    </Button>
    {showDeleteLink && (
      <button
        type="button"
        className="EditContentButtons__delete-button"
        onClick={() => { setShowDeleteModal(true); }}
      >
        {deleteButtonLabel}
      </button>
    )}
  </div>
);

EditContentButtons.propTypes = {
  setIsEditMode: PropTypes.func.isRequired,
  saveButtonIsDisabled: PropTypes.bool.isRequired,
  showDeleteLink: PropTypes.bool.isRequired,
  setShowDeleteModal: PropTypes.func.isRequired,
  deleteButtonLabel: PropTypes.string.isRequired,
};

export default EditContentButtons;
