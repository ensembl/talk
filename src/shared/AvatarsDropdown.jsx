import React from 'react';

import PropTypes from 'prop-types';
import { DropdownButton, Dropdown } from 'react-bootstrap';

import Avatar from './Avatar';
import './AvatarsDropdown.css';

const AvatarsDropdown = ({
  notAddedAvatars,
  addAvatar,
  addAvatarText,
  spaceName,
}) => (
  <div className="AvatarsDropdown">
    <DropdownButton
      title={addAvatarText}
      variant="outline-primary"
      size="sm"
    >
      <div className="AvatarsDropdown__items">
        {notAddedAvatars.map((avatar) => (
          <Dropdown.Item
            key={avatar.id}
            onClick={() => addAvatar(avatar.id)}
          >
            <div className="AvatarsDropdown__item">
              <Avatar
                avatarUrl={avatar.avatarUrl}
              />
              <span className="AvatarsDropdown__item-name">{avatar.name}</span>
            </div>
          </Dropdown.Item>
        ))}
      </div>
      {notAddedAvatars.length > 0 && <Dropdown.Divider />}
      <Dropdown.Item href={`/-/${spaceName}/people#invite`}>Invite a new person</Dropdown.Item>
    </DropdownButton>
  </div>
);

AvatarsDropdown.propTypes = {
  notAddedAvatars: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string.isRequired,
  })).isRequired,
  addAvatar: PropTypes.func.isRequired,
  addAvatarText: PropTypes.string.isRequired,
  spaceName: PropTypes.string.isRequired,
};

export default AvatarsDropdown;
