import React from 'react';

import { Spinner } from 'react-bootstrap';
import PropTypes from 'prop-types';

import './PageLoadingSpinner.css';

const PageLoadingSpinner = ({
  message,
}) => (
  <div className="PageLoadingSpinner">
    <Spinner animation="border" variant="secondary" />
    <h5 className="PageLoadingSpinner__message font-weight-bold">{message}</h5>
  </div>
);

PageLoadingSpinner.propTypes = {
  message: PropTypes.string,
};

PageLoadingSpinner.defaultProps = {
  message: '',
};

export default PageLoadingSpinner;
