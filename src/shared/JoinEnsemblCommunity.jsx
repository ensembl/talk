import React, { useState } from 'react';
import { Button, Spinner } from 'react-bootstrap';
import PropTypes from 'prop-types';

import addInviteeFirestore from '../people/InviteeFirestore';

const JoinEnsemblCommunity = ({ email }) => {
  const [isJoiningCommunity, setIsJoiningCommunity] = useState(false);

  const onAddEnsemblCommunity = async () => {
    setIsJoiningCommunity(true);
    try {
      await addInviteeFirestore('Ensembl Community', {
        email: email.toLowerCase(),
        invitedByName: 'Ensembl Team',
        userLoggedIn: true,
        createdAt: new Date(),
      });
    } catch (error) {
      throw new Error('Error joining Ensembl Community', error);
    }
  };

  return (
    <Button
      disabled={isJoiningCommunity}
      onClick={onAddEnsemblCommunity}
      variant="primary"
      size="sm"
    >
      {isJoiningCommunity && <Spinner animation="border" size="sm" />}
      <span>{isJoiningCommunity ? ' Joining...' : 'Join Ensembl Community'}</span>
    </Button>
  );
};

JoinEnsemblCommunity.propTypes = {
  email: PropTypes.string.isRequired,
};

export default JoinEnsemblCommunity;
