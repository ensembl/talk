import React from 'react';

import { Alert } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import './PageNotFound.css';

const PageNotFound = () => (
  <Alert
    className="PageNotFound__alert"
    variant="warning"
  >
    <span>
      {'Page not found or something went wrong. The page may have been deleted, or you may not have permissions to view it. Reload browser to try again. '}
    </span>
    <Link to="/home">Go back to Talks</Link>
  </Alert>
);

export default PageNotFound;
