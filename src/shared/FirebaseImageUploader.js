import { v4 as uuidv4 } from 'uuid';
import { storage } from '../firebase-config';

class FirebaseImageUploader {
  constructor(loader, spaceName) {
    this.loader = loader;
    this.spaceName = spaceName;
  }

  async upload() {
    const file = await this.loader.file;
    return new Promise((resolve, reject) => {
      const storageRef = storage.ref();
      const imageRef = storageRef.child(`uploads/${this.spaceName}/${uuidv4()}/${file.name}`);
      this.uploadTask = imageRef.put(file);
      this.uploadTask.on('state_changed',
        (snapshot) => {
          // Observe state change events such as progress, pause, and resume
          this.loader.uploadTotal = snapshot.totalBytes;
          this.loader.uploaded = snapshot.bytesTransferred;
        },
        (error) => {
          reject(new Error('Error uploading file', error.code));
        },
        async () => {
          // Handle successful uploads on complete
          const downloadUrl = await this.uploadTask.snapshot.ref.getDownloadURL();
          resolve({ default: downloadUrl });
        });
    });
  }

  abort() {
    this.uploadTask.cancel();
  }
}

export default FirebaseImageUploader;
