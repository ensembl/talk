import React from 'react';

import PropTypes from 'prop-types';

import Avatar from './Avatar';
import './AvatarAnchoredContent.css';

const AvatarAnchoredContent = ({
  avatarUrl,
  header,
  body,
}) => (
  <div className="AvatarAnchoredContent">
    <div className="AvatarAnchoredContent__left">
      <Avatar avatarUrl={avatarUrl} size="md" />
    </div>
    <div className="AvatarAnchoredContent__right">
      <div className="AvatarAnchoredContent__header">
        {header}
      </div>
      <div>
        {body}
      </div>
    </div>
  </div>
);

AvatarAnchoredContent.propTypes = {
  avatarUrl: PropTypes.string.isRequired,
  header: PropTypes.node.isRequired,
  body: PropTypes.node.isRequired,
};

export default AvatarAnchoredContent;
