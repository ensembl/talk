const formatAsMentionUsers = (users) => (
  users.map((user) => (
    {
      id: `@${user.name}`,
      uid: user.id,
      userName: user.name,
      avatarUrl: user.avatarUrl,
    }
  ))
);

const mentionsFromHtml = (html, beforeMentions, defaultType) => {
  const mentionElements = Array.from(new DOMParser().parseFromString(html, 'text/html').getElementsByClassName('mention'));
  const mentions = {};
  mentionElements.forEach((elem) => {
    const userId = elem.getAttribute('data-uid');
    mentions[userId] = {
      // If this mention already existed before, take it's type, otherwise take the default
      type: Object.prototype.hasOwnProperty.call(beforeMentions, userId)
        ? beforeMentions[userId].type
        : defaultType,
      userName: elem.getAttribute('data-user-name'),
      avatarUrl: elem.getAttribute('data-avatar-url'),
    };
  });
  return mentions;
};

const mentionsWithEveryone = (users, mentions) => {
  const everyoneMentions = users.reduce(
    (acc, current) => (
      {
        ...acc,
        [current.id]: { type: 'notify', userName: current.name, avatarUrl: current.avatarUrl },
      }
    ), {},
  );
  const newMentions = { ...everyoneMentions, ...mentions };
  delete newMentions.Everyone;
  return newMentions;
};

export {
  formatAsMentionUsers,
  mentionsWithEveryone,
  mentionsFromHtml,
};
