import React from 'react';

import { Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

import './AddCommentButton.css';

const AddCommentButton = ({
  sendButtonIsDisabled,
}) => (
  <Button
    disabled={sendButtonIsDisabled}
    className="btn-sm add-comment-button"
    type="submit"
  >
    Send
  </Button>
);

AddCommentButton.propTypes = {
  sendButtonIsDisabled: PropTypes.bool.isRequired,
};

export default AddCommentButton;
