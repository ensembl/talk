import React, { useState, useContext } from 'react';

import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

import CombinedUserContext from '../../currentUser/CombinedUserContext';
import AvatarAnchoredContent from '../AvatarAnchoredContent';
import CommentHeader from './CommentHeader';
import CommentBody from './CommentBody';
import CommentResponses from './responses/CommentResponses';
import EditComment from './EditComment';
import HeartButton from './HeartButton';
import './Comment.css';
import '../ckeditor/CKEditorStyles.css';

const Comment = ({
  spaceName,
  comment,
  users,
  updateComment,
  readOnly,
}) => {
  const {
    id: commentId,
    mentions,
    createdAt,
    createdByAvatarUrl,
    createdByName,
    createdById,
    updatedAt,
    htmlBody,
    heartedUids = [],
  } = comment;
  const { combinedUser } = useContext(CombinedUserContext);
  const { uid } = combinedUser;
  const [isEditMode, setIsEditMode] = useState(false);

  const history = useHistory();

  const creators = users.filter((user) => user.id === createdById);
  const creatorAvatarUrl = creators.length > 0 ? creators[0].avatarUrl : createdByAvatarUrl;
  const creatorName = creators.length > 0 ? creators[0].name : createdByName;

  return (
    <>
      <div className={`
        Comment 
        ${!isEditMode ? 'Comment__hover' : ''} 
        ${history.location.hash && history.location.hash.substring(9) === comment.id && !isEditMode ? 'Comment__anchor-highlight' : ''} `}
      >
        <div id={`comment_${comment.id}`} className="Comment__anchor" />

        <AvatarAnchoredContent
          avatarUrl={creatorAvatarUrl}
          header={(
            <CommentHeader
              createdByName={creatorName}
              createdAt={createdAt}
              commentId={commentId}
              updatedAtSeconds={updatedAt ? updatedAt.seconds : -1}
              isEditMode={isEditMode}
              setIsEditMode={setIsEditMode}
              canEditComment={createdById === uid && !readOnly}
            />
          )}
          body={(
            <>
              {!isEditMode && (
                <CommentBody
                  htmlBody={htmlBody}
                />
              )}
              {isEditMode && (
                <EditComment
                  spaceName={spaceName}
                  setIsEditMode={setIsEditMode}
                  commentId={commentId}
                  htmlBody={htmlBody}
                  users={users}
                  updateComment={updateComment}
                />
              )}
              {!readOnly && (
                <CommentResponses
                  mentions={mentions}
                  createdByName={createdByName}
                  commentId={commentId}
                  updateComment={updateComment}
                  uid={uid}
                />
              )}
              {!readOnly && (
                <HeartButton
                  updateComment={updateComment}
                  commentId={commentId}
                  uid={uid}
                  heartedUids={heartedUids}
                  users={users}
                />
              )}
            </>
          )}
        />
      </div>
    </>
  );
};

Comment.propTypes = {
  spaceName: PropTypes.string,
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  users: PropTypes.instanceOf(Array).isRequired,
  updateComment: PropTypes.func,
  readOnly: PropTypes.bool,
};

Comment.defaultProps = {
  readOnly: false,
  spaceName: null,
  updateComment: null,
};

export default Comment;
