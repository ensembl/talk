import React, { useState, useEffect, useContext } from 'react';

import { Form } from 'react-bootstrap';
import PropTypes from 'prop-types';

import CombinedUserContext from '../../currentUser/CombinedUserContext';
import EditContentButtons from '../EditContentButtons';
import DeleteModal from '../DeleteModal';
import { removeNbsp } from '../ckeditor/CKEditorUtils';
import CommentCKEditor from '../ckeditor/CommentCKEditor';
import './EditComment.css';

const EditComment = ({
  spaceName,
  setIsEditMode,
  commentId,
  htmlBody,
  users,
  updateComment,
}) => {
  const { combinedUser } = useContext(CombinedUserContext);
  const {
    name,
    uid,
    avatarUrl,
  } = combinedUser;

  const [editorContent, setEditorContent] = useState(htmlBody);
  const [saveButtonIsDisabled, setSaveButtonIsDisabled] = useState(true);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [isDeleting, setIsDeleting] = useState(false);

  useEffect(async () => {
    setSaveButtonIsDisabled(editorContent === '' || htmlBody.localeCompare(editorContent) === 0);
  }, [editorContent]);

  const saveComment = async (e) => {
    e.preventDefault();
    setIsEditMode(false);
    await updateComment(
      commentId,
      {
        htmlBody: removeNbsp(editorContent),
        updatedAt: new Date(),
        updatedByName: name,
        updatedById: uid,
        updatedByAvatarUrl: avatarUrl,
      },
    );
  };

  // Deleting comment
  const deleteFromModal = async () => {
    setIsDeleting(true);
    await updateComment(
      commentId,
      {
        toDelete: true,
      },
    );
  };

  return (
    <Form className="EditComment" autoComplete="off" onSubmit={saveComment}>
      <Form.Group>
        <CommentCKEditor
          content={editorContent}
          setContent={setEditorContent}
          spaceName={spaceName}
          users={users}
        />
        <EditContentButtons
          setIsEditMode={setIsEditMode}
          saveButtonIsDisabled={saveButtonIsDisabled}
          showDeleteLink
          setShowDeleteModal={setShowDeleteModal}
          deleteButtonLabel="Delete comment"
        />
        <DeleteModal
          showDeleteModal={showDeleteModal}
          setShowDeleteModal={setShowDeleteModal}
          isDeleting={isDeleting}
          deleteFromModal={deleteFromModal}
          modalTitle="Delete comment forever"
          modalBody={(
            <>
              <p>
                Are you sure? Deleted comments cannot be recovered.
              </p>
              <p>
                Consider editing your comment instead.
              </p>
            </>
          )}
          modalDeleteButtonLabel="Delete comment forever"
        />
      </Form.Group>
    </Form>
  );
};

EditComment.propTypes = {
  spaceName: PropTypes.string.isRequired,
  setIsEditMode: PropTypes.func.isRequired,
  commentId: PropTypes.string.isRequired,
  htmlBody: PropTypes.string.isRequired,
  users: PropTypes.instanceOf(Array).isRequired,
  updateComment: PropTypes.func.isRequired,
};

export default EditComment;
