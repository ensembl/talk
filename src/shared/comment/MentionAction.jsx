import React from 'react';

import PropTypes from 'prop-types';
import { OverlayTrigger, Tooltip, Form } from 'react-bootstrap';
import { QuestionCircle } from 'react-bootstrap-icons';

import Avatar from '../Avatar';
import './MentionAction.css';

const MentionAction = ({
  uid,
  userName,
  avatarUrl,
  mentionType,
  mentionTypeChangeHandler,
}) => (
  <div className="MentionAction">
    <Form.Control
      as="select"
      className="MentionAction__select"
      onChange={(e) => { mentionTypeChangeHandler(uid, userName, avatarUrl, e.target.value); }}
    >
      <option value="ack">Request ACK from</option>
      <option value="answer">Request answer from</option>
      <option value="notify">Notify</option>
    </Form.Control>
    <Avatar
      avatarUrl={avatarUrl}
    />
    <span className="MentionAction__user-name">{userName}</span>
    <span className="MentionAction__question">
      <OverlayTrigger
        className="MentionAction__question"
        delay={{ show: 50, hide: 50 }}
        overlay={(
          <Tooltip>
            {mentionType === 'ack' && <span>{`Will send ${userName} an email notification, requesting they ACK (acknowledge) this comment`}</span>}
            {mentionType === 'answer' && <span>{`Will send ${userName} an email notification, requesting they answer this comment`}</span>}
            {mentionType === 'notify' && <span>{`Will send ${userName} an email, notifying them of this comment`}</span>}
          </Tooltip>
        )}
      >
        <QuestionCircle />
      </OverlayTrigger>
    </span>
  </div>
);
export default MentionAction;

MentionAction.propTypes = {
  uid: PropTypes.string.isRequired,
  userName: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  mentionType: PropTypes.string.isRequired,
  mentionTypeChangeHandler: PropTypes.func.isRequired,
};
