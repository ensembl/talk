import React from 'react';
import PropTypes from 'prop-types';
import MentionAction from './MentionAction';

const MentionActions = ({
  mentions,
  mentionTypeChangeHandler,
}) => (
  <div className="comment-mention-actions">
    {
      Object.keys(mentions).filter((uid) => uid !== 'Everyone').map((uid) => (
        <MentionAction
          key={uid}
          uid={uid}
          userName={mentions[uid].userName}
          avatarUrl={mentions[uid].avatarUrl}
          mentionType={mentions[uid].type}
          mentionTypeChangeHandler={mentionTypeChangeHandler}
        />
      ))
    }
  </div>
);

export default MentionActions;

MentionActions.propTypes = {
  mentions: PropTypes.objectOf(PropTypes.any).isRequired,
  mentionTypeChangeHandler: PropTypes.func.isRequired,
};
