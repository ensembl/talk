import React, { useState } from 'react';

import PropTypes from 'prop-types';

import CommentHeaderDisplay from './CommentHeaderDisplay';

const CommentHeader = ({
  createdByName,
  createdAt,
  commentId,
  updatedAtSeconds,
  isEditMode,
  setIsEditMode,
  canEditComment,
}) => {
  const [copyLinkTooltipMessage, setCopyLinkTooltipMessage] = useState('Copy link to comment');

  const copyLink = async () => {
    try {
      await navigator.clipboard.writeText(`${window.location.href.split('#')[0]}#comment_${commentId}`);
    } catch (error) {
      throw new Error('Error copying comment link:', error);
    }
    setCopyLinkTooltipMessage('Copied to clipboard 😊');
    setTimeout(() => setCopyLinkTooltipMessage('Copy link to comment'), 2000);
  };

  return (
    <CommentHeaderDisplay
      createdByName={createdByName}
      createdAt={createdAt}
      commentId={commentId}
      updatedAtSeconds={updatedAtSeconds}
      isEditMode={isEditMode}
      setIsEditMode={setIsEditMode}
      copyLinkTooltipMessage={copyLinkTooltipMessage}
      copyLink={copyLink}
      canEditComment={canEditComment}
    />
  );
};

CommentHeader.propTypes = {
  createdByName: PropTypes.string.isRequired,
  createdAt: PropTypes.objectOf(PropTypes.any).isRequired,
  commentId: PropTypes.string.isRequired,
  updatedAtSeconds: PropTypes.number.isRequired,
  isEditMode: PropTypes.bool.isRequired,
  setIsEditMode: PropTypes.func.isRequired,
  canEditComment: PropTypes.bool.isRequired,
};

export default CommentHeader;
