import React from 'react';

import PropTypes from 'prop-types';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { Heart, HeartFill } from 'react-bootstrap-icons';
import firebase from 'firebase/app';

import './HeartButton.css';

const HeartButton = ({
  updateComment,
  commentId,
  uid,
  heartedUids,
  users,
}) => {
  const isHearted = heartedUids.includes(uid);
  const names = users
    .filter((mentionUser) => heartedUids.includes(mentionUser.id))
    .map((mentionUser) => mentionUser.name);

  const tooltipMessage = names.length > 0
    ? `Hearted by ${names.join(', ')}`
    : 'No hearts yet';

  const toggleHeart = async () => {
    if (isHearted) {
      await updateComment(
        commentId,
        {
          heartedUids: firebase.firestore.FieldValue.arrayRemove(uid),
        },
      );
    } else {
      await updateComment(
        commentId,
        {
          heartedUids: firebase.firestore.FieldValue.arrayUnion(uid),
        },
      );
    }
  };

  return (
    <div className="HeartButton">
      <OverlayTrigger
        delay={{ show: 50, hide: 50 }}
        overlay={<Tooltip>{tooltipMessage}</Tooltip>}
      >
        <Button
          onClick={toggleHeart}
          size="sm"
          variant="outline-primary"
        >
          <span>
            {heartedUids.length}
            &nbsp;
          </span>
          {heartedUids.length === 0 && <Heart size={13} />}
          {heartedUids.length > 0 && <HeartFill size={13} />}
        </Button>
      </OverlayTrigger>
    </div>
  );
};

HeartButton.propTypes = {
  updateComment: PropTypes.func.isRequired,
  commentId: PropTypes.string.isRequired,
  uid: PropTypes.string.isRequired,
  heartedUids: PropTypes.instanceOf(Array).isRequired,
  users: PropTypes.instanceOf(Array).isRequired,
};

export default HeartButton;
