import React from 'react';

import PropTypes, { string } from 'prop-types';

import './ResponseWaiting.css';

const ResponseWaiting = ({ hasPrecedingDot, type, names }) => (
  <span className="text-muted ResponseWaiting" data-testid="ack-waiting-message">
    {hasPrecedingDot && '• '}
    Requesting
    <span className="font-weight-bold">
      {type === 'ack' && <span> ACK </span>}
      {type === 'answer' && <span> answer </span>}
    </span>
    {`from ${names.join(', ')}`}
  </span>
);

ResponseWaiting.propTypes = {
  hasPrecedingDot: PropTypes.bool.isRequired,
  type: PropTypes.oneOf(['answer', 'ack']).isRequired,
  names: PropTypes.arrayOf(string).isRequired,
};

export default ResponseWaiting;
