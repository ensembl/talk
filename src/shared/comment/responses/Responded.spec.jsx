import React from 'react';
import { render } from '@testing-library/react';
import Responded from './Responded';

// Create a render function that will be reused for rendering the component
const renderComponent = ({ type, names }) => render(
  <Responded type={type} names={names} />,
);

describe('<Responded>', () => {
  test('should render plurals', () => {
    // Arrage
    const type = 'ack';
    const names = ['Bob', 'Jane', 'Mary'];
    const data = { type, names };
    const { getByTestId } = renderComponent(data);
    const respondedCount = getByTestId('responded-count');
    // Assert
    expect(respondedCount).toHaveTextContent('3 ACKs');
  });
  test('should render singular', () => {
    // Arrage
    const type = 'ack';
    const names = ['Bob'];
    const data = { type, names };
    const { getByTestId } = renderComponent(data);
    const respondedCount = getByTestId('responded-count');
    // Assert
    expect(respondedCount).toHaveTextContent('1 ACK');
  });
});
