import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import CommentResponses from './CommentResponses';

const renderComponent = ({
  mentions, createdByName, updateComment, commentId, uid,
}) => render(
  <CommentResponses
    mentions={mentions}
    createdByName={createdByName}
    commentId={commentId}
    updateComment={updateComment}
    uid={uid}
  />,
);

const updateComment = jest.fn();

describe('<CommentResponses>', () => {
  test('should render acked and waiting list', () => {
    const data = {
      mentions: {
        testId: {
          type: 'ack',
          userName: 'Ed',
        },
        testId2: {
          type: 'ack',
          userName: 'Victor',
        },
        testId3: {
          type: 'ack',
          userName: 'Bob',
          acked: true,
        },
        testId4: {
          type: 'ack',
          userName: 'Bob',
          acked: true,
        },
        testId5: {
          type: 'notify',
          userName: 'Bob',
        },
      },
      createdByName: 'creator',
      updateComment,
      commentId: '123',
      uid: 'testId',
    };
    const { getByTestId } = renderComponent(data);
    const commentResponses = getByTestId('comment-responses');
    expect(commentResponses.textContent).toEqual('2 ACKs• Requesting ACK from Ed, VictorACK');
  });

  test('non-mentioned should not see ack button', () => {
    const data = {
      mentions: {
        testId: {
          type: 'ack',
          userName: 'Ed',
        },
      },
      createdByName: 'creator',
      updateComment,
      commentId: '123',
      uid: 'nonId',
    };
    const { queryByTestId } = renderComponent(data);
    const respondedButton = queryByTestId('responded-button');
    expect(respondedButton).not.toBeInTheDocument();
  });

  test('can mentioned user ack', async () => {
    const data = {
      mentions: {
        yesid: {
          type: 'ack',
          userName: 'Ed',
        },
      },
      createdByName: 'creator',
      updateComment,
      commentId: '123',
      uid: 'yesid',
    };
    const { getByTestId } = renderComponent(data);
    const respondButton = getByTestId('response-button');
    await fireEvent.click(respondButton);
    expect(updateComment).toHaveBeenCalledWith('123', { 'mentions.yesid.acked': true });
  });
});
