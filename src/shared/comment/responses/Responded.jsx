import React from 'react';

import PropTypes, { string } from 'prop-types';
import {
  OverlayTrigger, Tooltip,
} from 'react-bootstrap';

import './Responded.css';

const Responded = ({ type, names }) => (
  <OverlayTrigger
    delay={{ show: 50, hide: 50 }}
    overlay={<Tooltip>{`${type === 'ack' ? 'Acknowledged ' : 'Answered '} by ${names.join(', ')}`}</Tooltip>}
  >
    <span className="Responded font-weight-bold" data-testid="responded-count">
      {`${names.length} ${type === 'ack' ? 'ACK' : 'answer'}${names.length > 1 ? 's' : ''}`}
    </span>
  </OverlayTrigger>
);

Responded.propTypes = {
  type: PropTypes.oneOf(['answer', 'ack']).isRequired,
  names: PropTypes.arrayOf(string).isRequired,
};

export default Responded;
