import React from 'react';
import {
  fireEvent, render,
} from '@testing-library/react';
import ResponseButton from './ResponseButton';

const renderComponent = ({ type, handleClick, createdByName }) => render(
  <ResponseButton
    type={type}
    handleClick={handleClick}
    createdByName={createdByName}
  />,
);

// mock function
const handleClick = jest.fn();

describe('<ResponseButton>', () => {
  test('should render', () => {
    const data = {
      type: 'ack',
      handleClick,
      createdByName: 'Ed',
    };
    const { getByTestId } = renderComponent(data);
    const ackButton = getByTestId('response-button');
    expect(ackButton).toHaveTextContent('ACK');
  });

  test('should trigger click event', async () => {
    const data = {
      type: 'ack',
      handleClick,
      createdByName: 'Ed',
    };
    const { getByTestId } = renderComponent(data);
    await fireEvent.click(getByTestId('response-button'));
    expect(handleClick).toBeCalled();
  });
});
