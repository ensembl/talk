import React from 'react';
import PropTypes from 'prop-types';

import Responded from './Responded';
import ResponseWaiting from './ResponseWaiting';
import ResponseButton from './ResponseButton';
import './CommentResponses.css';

const CommentResponses = ({
  mentions,
  createdByName,
  commentId,
  updateComment,
  uid,
}) => {
  const getRespondedNames = (type) => Object.entries(mentions)
    .filter(([, map]) => `${type}ed` in map && map.type === type)
    .map(([, map]) => map.userName);

  const getWaitingNames = (type) => Object.entries(mentions)
    .filter(([, map]) => !(`${type}ed` in map) && map.type === type)
    .map(([, map]) => map.userName);

  const answeredNames = mentions ? getRespondedNames('answer') : [];
  const ackedNames = mentions ? getRespondedNames('ack') : [];

  const answerWaitingNames = mentions ? getWaitingNames('answer') : [];
  const ackWaitingNames = mentions ? getWaitingNames('ack') : [];

  const canRespond = (type) => mentions
    && Object.entries(mentions).find(
      ([userId, map]) => uid === userId && map.type === type && !(`${type}ed` in map),
    ) !== undefined;
  const canAnswer = canRespond('answer');
  const canAck = canRespond('ack');

  const handleClick = async (type) => {
    await updateComment(
      commentId,
      {
        [`mentions.${uid}.${type}ed`]: true,
      },
    );
  };
  const handleClickAck = async () => handleClick('ack');
  const handleClickAnswer = async () => handleClick('answer');

  return (
    <div
      className="CommentResponses"
      data-testid="comment-responses"
    >
      {(answeredNames.length > 0 || answerWaitingNames.length > 0 || canAnswer) && (
        <div className="CommentResponses__section">
          {answeredNames.length > 0 && (
            <Responded
              type="answer"
              names={answeredNames}
            />
          )}
          {answerWaitingNames.length > 0 && (
            <ResponseWaiting
              hasPrecedingDot={answeredNames.length > 0}
              type="answer"
              names={answerWaitingNames}
            />
          )}
          {canAnswer && (
            <ResponseButton
              type="answer"
              handleClick={handleClickAnswer}
              createdByName={createdByName}
            />
          )}
        </div>
      )}
      {(ackedNames.length > 0 || ackWaitingNames.length > 0 || canAck) && (
        <div className="CommentResponses__section">
          {ackedNames.length > 0 && (
            <Responded
              type="ack"
              names={ackedNames}
            />
          )}
          {ackWaitingNames.length > 0 && (
            <ResponseWaiting
              hasPrecedingDot={ackedNames.length > 0}
              type="ack"
              names={ackWaitingNames}
            />
          )}
          {canAck && (
            <ResponseButton
              type="ack"
              handleClick={handleClickAck}
              createdByName={createdByName}
            />
          )}
        </div>
      )}
    </div>
  );
};

CommentResponses.propTypes = {
  mentions: PropTypes.objectOf(
    PropTypes.shape({
      type: PropTypes.oneOf(['answer', 'ack', 'notify']).isRequired,
      userName: PropTypes.string.isRequired,
      acked: PropTypes.bool,
    }),
  ).isRequired,
  createdByName: PropTypes.string.isRequired,
  commentId: PropTypes.string.isRequired,
  updateComment: PropTypes.func.isRequired,
  uid: PropTypes.string.isRequired,
};

export default CommentResponses;
