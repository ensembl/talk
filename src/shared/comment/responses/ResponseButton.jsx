import React from 'react';

import PropTypes from 'prop-types';
import {
  OverlayTrigger, Tooltip, Button,
} from 'react-bootstrap';

import './ResponseButton.css';

const ResponseButton = ({
  type,
  handleClick,
  createdByName,
}) => (
  <OverlayTrigger
    delay={{ show: 50, hide: 50 }}
    overlay={(
      <Tooltip>
        {type === 'answer' && <span>Click to show you answered</span>}
        {type === 'ack' && <span>{`Acknowledge ${createdByName} and send them a notification`}</span>}
      </Tooltip>
    )}
  >
    <Button
      className="ReponseButton"
      variant="primary"
      onClick={handleClick}
      data-testid="response-button"
    >
      {type === 'answer' && <span>I answered</span>}
      {type === 'ack' && <span>ACK</span>}
    </Button>
  </OverlayTrigger>
);

ResponseButton.propTypes = {
  type: PropTypes.oneOf(['answer', 'ack']).isRequired,
  handleClick: PropTypes.func.isRequired,
  createdByName: PropTypes.string.isRequired,
};

export default ResponseButton;
