import React from 'react';

import {
  OverlayTrigger, Tooltip,
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import TimeAgo from 'react-timeago';

import tooltipDatetimeFormatter from '../TooltipDatetimeFormatter';
import timeAgoFormatter from '../TimeAgoFormatter';
import './CommentHeaderDisplay.css';

const CommentHeaderDisplay = ({
  createdByName,
  createdAt,
  commentId,
  updatedAtSeconds,
  isEditMode,
  setIsEditMode,
  copyLinkTooltipMessage,
  copyLink,
  canEditComment,
}) => (
  <div className="comment-header text-muted">
    <span className="font-weight-bold">{`${createdByName} `}</span>
    <span>
      <OverlayTrigger
        delay={{ show: 50, hide: 50 }}
        overlay={
          <Tooltip>{tooltipDatetimeFormatter(createdAt.seconds)}</Tooltip>
        }
      >
        <Link className="comment-anchor-link" to={`#comment_${commentId}`}>
          <TimeAgo date={new Date(createdAt.seconds * 1000)} minPeriod="60" formatter={timeAgoFormatter} />
        </Link>
      </OverlayTrigger>
      {updatedAtSeconds > 0 && <span> · Edited </span>}
      {updatedAtSeconds > 0 && (
        <OverlayTrigger
          delay={{ show: 50, hide: 50 }}
          overlay={
            <Tooltip>{tooltipDatetimeFormatter(updatedAtSeconds)}</Tooltip>
          }
        >
          <TimeAgo date={new Date(updatedAtSeconds * 1000)} minPeriod="60" formatter={timeAgoFormatter} />
        </OverlayTrigger>
      )}
      {!isEditMode && (
        <>
          <OverlayTrigger
            delay={{ show: 50, hide: 50 }}
            overlay={<Tooltip>{copyLinkTooltipMessage}</Tooltip>}
          >
            <span className="comment-copy-comment-link">
              <span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>
              <button type="button" onClick={copyLink}>Copy link</button>
            </span>
          </OverlayTrigger>
          {canEditComment && (
            <span className="comment-edit">
              <span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>
              <button type="button" onClick={() => { setIsEditMode(true); }}>Edit</button>
            </span>
          )}
        </>
      )}
    </span>
  </div>
);

CommentHeaderDisplay.propTypes = {
  createdByName: PropTypes.string.isRequired,
  createdAt: PropTypes.objectOf(PropTypes.any).isRequired,
  commentId: PropTypes.string.isRequired,
  updatedAtSeconds: PropTypes.number.isRequired,
  isEditMode: PropTypes.bool.isRequired,
  setIsEditMode: PropTypes.func.isRequired,
  copyLinkTooltipMessage: PropTypes.string.isRequired,
  copyLink: PropTypes.func.isRequired,
  canEditComment: PropTypes.bool.isRequired,
};

export default CommentHeaderDisplay;
