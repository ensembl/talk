import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';
import { Form } from 'react-bootstrap';

import NotsSentMessage from './NotsSentMessage';
import MentionActions from './MentionActions';
import CommentNotify from './CommentNotify';
import AddCommentButton from './AddCommentButton';
import { mentionsWithEveryone, mentionsFromHtml } from '../MentionsUtils';
import CommentCKEditor from '../ckeditor/CommentCKEditor';
import { removeNbsp } from '../ckeditor/CKEditorUtils';
import './AddComment.css';

const AddComment = ({
  localStorageKey,
  spaceName,
  users,
  uid,
  name,
  avatarUrl,
  addComment,
  threadId,
  starterMentionUserId,
  starterMentionUserName,
  starterMentionAvatarUrl,
  subscribedUids,
  addCommentAppendContent,
  setAddCommentAppendContent,
  shortCommentCKEditor,
}) => {
  const [editorContent, setEditorContent] = useState(localStorage.getItem(localStorageKey) || '');
  const [sendButtonIsDisabled, setSendButtonIsDisabled] = useState(true);
  const [showNotsSentMessage, setShowNotsSentMessage] = useState(false);
  const [mentionsForNotsMessage, setMentionsForNotsMessage] = useState({});

  const [mentions, setMentions] = useState({});

  const mentionTypeChangeHandler = (userId, userName, userAvatarUrl, type) => {
    setMentions({
      ...mentions,
      [userId]:
      {
        type,
        userName,
        avatarUrl: userAvatarUrl,
      },
    });
  };

  useEffect(async () => {
    localStorage.setItem(localStorageKey, editorContent);
    setSendButtonIsDisabled(editorContent === '');
    setMentions(
      mentionsFromHtml(editorContent, mentions, 'ack'),
    );
  }, [editorContent]);

  useEffect(() => {
    if (addCommentAppendContent) {
      setEditorContent(
        `${editorContent} <p>${addCommentAppendContent}</p>`,
      );
      setAddCommentAppendContent('');
    }
  }, [addCommentAppendContent]);

  const addNewComment = async (e) => {
    e.preventDefault();
    const newHtmlBody = removeNbsp(editorContent);
    setEditorContent('');
    const mentionsForNewComment = 'Everyone' in mentions
      ? mentionsWithEveryone(users.filter((user) => user.id !== uid), mentions)
      : mentions;
    await addComment(
      {
        htmlBody: newHtmlBody,
        createdAt: new Date(),
        createdByName: name,
        createdById: uid,
        createdByAvatarUrl: avatarUrl,
        mentions: mentionsForNewComment,
        threadId,
      },
    );
    setShowNotsSentMessage(true);
    setMentionsForNotsMessage(mentionsForNewComment);
    setMentions({});
    localStorage.removeItem(localStorageKey);
  };

  return (
    <>
      {showNotsSentMessage && (
        <NotsSentMessage
          users={users}
          mentions={mentionsForNotsMessage}
          subscribedUids={subscribedUids}
        />
      )}
      <Form className="AddComment" autoComplete="off" onSubmit={addNewComment}>
        <Form.Group>
          <CommentCKEditor
            starterMentionUserId={starterMentionUserId}
            starterMentionUserName={starterMentionUserName}
            starterMentionAvatarUrl={starterMentionAvatarUrl}
            content={editorContent}
            setContent={setEditorContent}
            spaceName={spaceName}
            users={users}
            shortCommentCKEditor={shortCommentCKEditor}
          />
          {Object.keys(mentions).length > 0 && (
            <MentionActions
              mentions={mentions}
              mentionTypeChangeHandler={mentionTypeChangeHandler}
            />
          )}
          {!sendButtonIsDisabled && (
            <CommentNotify
              users={users}
              uid={uid}
              mentions={mentions}
              subscribedUids={subscribedUids}
            />
          )}
          <AddCommentButton
            sendButtonIsDisabled={sendButtonIsDisabled}
          />
        </Form.Group>
      </Form>
    </>
  );
};

AddComment.propTypes = {
  localStorageKey: PropTypes.string.isRequired,
  spaceName: PropTypes.string.isRequired,
  users: PropTypes.arrayOf(PropTypes.any).isRequired,
  uid: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  addComment: PropTypes.func.isRequired,
  threadId: PropTypes.string,
  starterMentionUserId: PropTypes.string,
  starterMentionUserName: PropTypes.string,
  starterMentionAvatarUrl: PropTypes.string,
  subscribedUids: PropTypes.instanceOf(Array),
  addCommentAppendContent: PropTypes.string,
  setAddCommentAppendContent: PropTypes.func,
  shortCommentCKEditor: PropTypes.bool,
};

AddComment.defaultProps = {
  threadId: '',
  starterMentionUserId: '',
  starterMentionUserName: '',
  starterMentionAvatarUrl: '',
  subscribedUids: [],
  addCommentAppendContent: '',
  setAddCommentAppendContent: null,
  shortCommentCKEditor: false,
};

export default AddComment;
