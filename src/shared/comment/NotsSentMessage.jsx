import React, { useContext } from 'react';

import PropTypes from 'prop-types';
import { Alert } from 'react-bootstrap';

import AvatarSet from '../AvatarSet';
import CombinedUserContext from '../../currentUser/CombinedUserContext';

import './NotsSentMessage.css';

const NotsSentMessage = ({ users, mentions, subscribedUids }) => {
  const { combinedUser } = useContext(CombinedUserContext);
  const {
    uid,
  } = combinedUser;

  const mentionTypeUids = (type) => Object.entries(mentions)
    .filter(([, map]) => map.type === type).map(([userId]) => userId);

  const requestAnswerUids = mentionTypeUids('answer');
  const requestAckUids = mentionTypeUids('ack');
  const notifyUids = mentionTypeUids('notify');

  const requestAnswerUsers = users
    .filter((user) => requestAnswerUids.includes(user.id))
    .filter((user) => user.id !== uid);

  const requestAckUsers = users
    .filter((user) => requestAckUids.includes(user.id))
    .filter((user) => user.id !== uid);

  const notifyUsers = users
    .filter((user) => notifyUids.includes(user.id) || subscribedUids.includes(user.id))
    .filter((user) => !requestAckUids.includes(user.id))
    .filter((user) => !requestAnswerUids.includes(user.id))
    .filter((user) => user.id !== uid);

  return (
    <>
      {(requestAnswerUsers.length > 0 || requestAckUsers.length > 0 || notifyUsers.length > 0) && (
        <Alert variant="success" className="NotsSentMessage">
          {requestAnswerUsers.length > 0 && (
            <div className="NotsSentMessage__section">
              <span>Requested</span>
              <span className="font-weight-bold">&nbsp;answer&nbsp;</span>
              <span>from</span>
              <AvatarSet
                addedAvatars={requestAnswerUsers}
              />
            </div>
          )}
          {requestAckUsers.length > 0 && (
            <div className="NotsSentMessage__section">
              <span>Requested</span>
              <span className="font-weight-bold">&nbsp;ACK&nbsp;</span>
              <span>from</span>
              <AvatarSet
                addedAvatars={requestAckUsers}
              />
            </div>
          )}
          {notifyUsers.length > 0 && (
            <div className="NotsSentMessage__section">
              <span>Notified</span>
              <AvatarSet
                addedAvatars={notifyUsers}
              />
            </div>
          )}
        </Alert>
      )}
    </>
  );
};

NotsSentMessage.propTypes = {
  users: PropTypes.instanceOf(Array).isRequired,
  mentions: PropTypes.objectOf(PropTypes.any).isRequired,
  subscribedUids: PropTypes.instanceOf(Array),
};

NotsSentMessage.defaultProps = {
  subscribedUids: [],
};

export default NotsSentMessage;
