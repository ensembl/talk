import React from 'react';

import PropTypes from 'prop-types';

import AvatarSet from '../AvatarSet';
import './CommentNotify.css';

const CommentNotify = ({
  users,
  uid,
  mentions,
  subscribedUids,
}) => {
  if ('Everyone' in mentions) {
    return (
      <div className="CommentNotify CommentNotify__everyone">
        <span className="CommentNotify__everyone-label">Notify everyone</span>
        <AvatarSet
          addedAvatars={users.filter((user) => user.id !== uid)}
        />
      </div>
    );
  }

  if ('Everyone' in mentions) {
    return (
      <div className="CommentNotify CommentNotify__everyone">
        <span>Notify everyone</span>
        <AvatarSet
          addedAvatars={users.filter((user) => user.id !== uid)}
        />
      </div>
    );
  }

  const filteredSubscribers = users
    .filter((user) => subscribedUids.includes(user.id))
    .filter((user) => !Object.keys(mentions).includes(user.id))
    .filter((user) => user.id !== uid);

  if (filteredSubscribers.length > 0) {
    return (
      <div className="CommentNotify CommentNotify__subscribers">
        <span className="CommentNotify__subscribers-label">
          {`Will auto-notify subscriber${filteredSubscribers.length > 1 ? 's' : ''}`}
        </span>
        <AvatarSet
          addedAvatars={filteredSubscribers}
        />
        <span className="CommentNotify__subscribers-notify-more-label"> @ to notify more</span>
      </div>
    );
  }

  if (Object.keys(mentions).length === 0) {
    return (
      <div className="CommentNotify CommentNotify__nobody">
        <span>Won&apos;t notify anyone, @ to notify someone</span>
      </div>
    );
  }

  return ''; // There is at least one @ user in the comment box and no filtered subscribers
};

CommentNotify.propTypes = {
  users: PropTypes.instanceOf(Array).isRequired,
  uid: PropTypes.string.isRequired,
  mentions: PropTypes.objectOf(PropTypes.any).isRequired,
  subscribedUids: PropTypes.instanceOf(Array).isRequired,
};

export default CommentNotify;
