import React from 'react';

import PropTypes from 'prop-types';
import dompurify from 'dompurify';

import '../ckeditor/CKEditorStyles.css';
import './CommentBody.css';

const CommentBody = ({
  htmlBody,
}) => (
  <div
    className="CommentBody ck-content dont-break-out"
    // eslint-disable-next-line react/no-danger
    dangerouslySetInnerHTML={{ __html: dompurify.sanitize(htmlBody) }}
  />
);

CommentBody.propTypes = {
  htmlBody: PropTypes.string.isRequired,
};

export default CommentBody;
