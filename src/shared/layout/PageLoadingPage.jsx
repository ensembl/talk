import React from 'react';

import PageLoadingSpinner from '../PageLoadingSpinner';
import './PageLoadingPage.css';

const PageLoadingPage = () => (
  <div className="PageLoadingPage">
    <PageLoadingSpinner message="Loading..." />
  </div>
);

export default PageLoadingPage;
