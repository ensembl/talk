import React from 'react';

import PropTypes from 'prop-types';
import { Button, Modal, Spinner } from 'react-bootstrap';

import './GenericModal.css';

const GenericModal = ({
  show,
  animation,
  modalTitle,
  modalBody,
  loadingLabel,
  onConfirm,
  confirmLabel,
  isConfirming,
  onCancel,
  cancelLabel,
  confirmButtonVariant,
  cancelButtonVariant,
}) => (
  <Modal className="GenericModal" show={show} animation={animation}>
    <Modal.Header>
      <Modal.Title>{modalTitle}</Modal.Title>
    </Modal.Header>
    <Modal.Body>{modalBody}</Modal.Body>
    <Modal.Footer className="GenericModal__footer">
      <Button disabled={isConfirming} className="btn-sm" variant={cancelButtonVariant} onClick={onCancel}>{cancelLabel}</Button>
      <Button disabled={isConfirming} className="btn-sm" variant={confirmButtonVariant} onClick={onConfirm}>
        {!isConfirming && <span>{confirmLabel}</span>}
        {isConfirming
          && (
            <span>
              <Spinner animation="border" size="sm" />
              <span className="GenericModal__loading-label">{loadingLabel}</span>
            </span>
          )}
      </Button>
    </Modal.Footer>
  </Modal>
);

GenericModal.propTypes = {
  show: PropTypes.bool.isRequired,
  modalTitle: PropTypes.string.isRequired,
  modalBody: PropTypes.oneOfType(
    [PropTypes.string, PropTypes.element],
  ).isRequired,
  onConfirm: PropTypes.func.isRequired,
  isConfirming: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  confirmButtonVariant: PropTypes.string.isRequired,
  cancelButtonVariant: PropTypes.string.isRequired,
  animation: PropTypes.bool,
  loadingLabel: PropTypes.string,
  confirmLabel: PropTypes.string,
  cancelLabel: PropTypes.string,
};

GenericModal.defaultProps = {
  animation: false,
  loadingLabel: 'Loading...',
  confirmLabel: 'Confirm',
  cancelLabel: 'Cancel',
};

export default GenericModal;
