import React from 'react';

import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { PlusSquareFill } from 'react-bootstrap-icons';

import SpaceSelectDropdown from './SpaceSelectDropdown';
import ActivityDot from './ActivityDot';
import signOut from '../SignOut';
import './LeftNav.css';

const LeftNav = ({
  uid,
  onCommunitySpace,
  spaceName,
  allowableSpaceNames,
}) => (
  <div className="LeftNav">
    <nav>
      <ul className="LeftNav__links">
        <li className="LeftNav__url-space-name font-weight-bold">
          {spaceName}
        </li>
        {uid && (
          <li>
            <SpaceSelectDropdown
              spaceName={spaceName}
              allowableSpaceNames={allowableSpaceNames}
            />
          </li>
        )}
        <li className="LeftNav__link LeftNav__link-divider-above">
          <NavLink
            to={`/-/${spaceName}/home?tab=talks`}
            isActive={(match, location) => (location.pathname.includes('home') && location.search === '')
              || location.search.includes('tab=talks')
              || location.search.includes('tab=resolvedTalks')
              || location.pathname.includes('/talks')}
          >
            <span>Talks</span>
          </NavLink>
        </li>
        <li className="LeftNav__link">
          <NavLink
            to={`/-/${spaceName}/home?tab=docs`}
            isActive={(match, location) => location.search.includes('tab=docs')
              || location.pathname.includes('/docs')}
          >
            <span>Docs</span>
          </NavLink>
        </li>
        {!onCommunitySpace && (
          <li className="LeftNav__link">
            <NavLink exact to={`/-/${spaceName}/chat`}>
              <span>Chat</span>
            </NavLink>
          </li>
        )}
        {!onCommunitySpace && (
          <li className="LeftNav__link LeftNav__link-divider-below">
            <NavLink exact to={`/-/${spaceName}/activity`}>
              <span>Activity</span>
              <span className="LeftNav__activity-dot">
                <ActivityDot spaceName={spaceName} />
              </span>
            </NavLink>
          </li>
        )}
        {
          (
            !onCommunitySpace
            || (onCommunitySpace && allowableSpaceNames.includes('Ensembl Community'))
          ) && (
            <li className="LeftNav__link">
              <NavLink exact to={`/-/${onCommunitySpace ? 'Ensembl Community' : spaceName}/new/talk`} isActive={() => false}>
                <PlusSquareFill size={15} />
                <span className="LeftNav__new-text font-weight-bold">New talk</span>
              </NavLink>
            </li>
          )
        }
        {!onCommunitySpace && (
          <>
            <li className="LeftNav__link">
              <NavLink exact to={`/-/${spaceName}/new/doc`} isActive={() => false}>
                <PlusSquareFill size={15} />
                <span className="LeftNav__new-text font-weight-bold">New doc</span>
              </NavLink>
            </li>
            <li className="LeftNav__link LeftNav__link-divider-below">
              <NavLink exact to={`/-/${spaceName}/people#invite`} isActive={() => false}>
                <PlusSquareFill size={15} />
                <span className="LeftNav__new-text font-weight-bold">Invite person</span>
              </NavLink>
            </li>
            <li className="LeftNav__link">
              <NavLink exact to={`/-/${spaceName}/people`}>
                <span>People</span>
              </NavLink>
            </li>
            <li className="LeftNav__link">
              <NavLink exact to={`/-/${spaceName}/labels`}>
                <span>Labels</span>
              </NavLink>
            </li>
            <li className="LeftNav__link">
              <NavLink exact to={`/-/${spaceName}/settings`}>Connect to Slack</NavLink>
            </li>
          </>
        )}
        {uid && (
          <li className="LeftNav__link LeftNav__link-divider-above">
            <NavLink exact to="/profile">
              <span>Profile</span>
            </NavLink>
          </li>
        )}
        <li className="LeftNav__link">
          <NavLink exact to="/-/Ensembl%20Community/docs/7VHsfIh7kDEIh6mpm9pU">
            <span>Help</span>
          </NavLink>
        </li>
        <li className="LeftNav__sign-in-out-link">
          {uid && (
            <NavLink exact to="/#" onClick={() => { signOut(); }}>
              <span>Sign out</span>
            </NavLink>
          )}
          {!uid && <a href="/">Sign in</a>}
        </li>
      </ul>
    </nav>
  </div>
);

LeftNav.propTypes = {
  uid: PropTypes.string.isRequired,
  onCommunitySpace: PropTypes.bool.isRequired,
  spaceName: PropTypes.string,
  allowableSpaceNames: PropTypes.arrayOf(PropTypes.string).isRequired,
};

LeftNav.defaultProps = {
  spaceName: '',
};

export default LeftNav;
