import React from 'react';

import PropTypes from 'prop-types';
import { ChevronLeft, List } from 'react-bootstrap-icons';

import './MobileHeader.css';

const MobileHeader = ({
  title,
  isTalk,
  setIsLeftDrawerOpen,
  setIsRightDrawerOpen,
}) => (
  <div className="MobileHeader">
    <button
      className="MobileHeader__menu-button"
      type="button"
      onClick={() => {
        setIsLeftDrawerOpen(true);
        setIsRightDrawerOpen(false);
      }}
    >
      <List size={25} />
    </button>
    <h1
      className="MobileHeader__title font-weight-bold"
    >
      {title}
    </h1>
    {isTalk && (
      <button
        className="MobileHeader__chevron-button"
        type="button"
        onClick={() => {
          setIsRightDrawerOpen(true);
          setIsLeftDrawerOpen(false);
        }}
      >
        <ChevronLeft size={25} />
      </button>
    )}
  </div>
);

MobileHeader.propTypes = {
  title: PropTypes.string.isRequired,
  isTalk: PropTypes.bool.isRequired,
  setIsLeftDrawerOpen: PropTypes.func.isRequired,
  setIsRightDrawerOpen: PropTypes.func.isRequired,
};

export default MobileHeader;
