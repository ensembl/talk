import React from 'react';
import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types';

import './LeftSideDrawer.css';

const LeftSideDrawer = ({ show, children }) => (
  <CSSTransition
    in={show}
    timeout={200}
    classNames="slide-in-left"
    mountOnEnter
    unmountOnExit
  >
    <aside className="LeftSideDrawer">
      {children}
    </aside>
  </CSSTransition>
);

LeftSideDrawer.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  show: PropTypes.bool.isRequired,
};

export default LeftSideDrawer;
