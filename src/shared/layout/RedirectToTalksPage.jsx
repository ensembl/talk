import React from 'react';
import { Redirect } from 'react-router-dom';

const RedirectToTalksPage = () => <Redirect to={`${window.location.pathname.replace(/\/+$/, '')}/home?tab=talks`} />;

export default RedirectToTalksPage;
