import React, { useEffect, useState, useContext } from 'react';

import { useDocumentData } from 'react-firebase-hooks/firestore';
import PropTypes from 'prop-types';
import { CircleFill } from 'react-bootstrap-icons';

import { db } from '../../firebase-config';
import CombinedUserContext from '../../currentUser/CombinedUserContext';

const ActivityDot = ({ spaceName }) => {
  const { combinedUser } = useContext(CombinedUserContext);
  const {
    activityPageLastVisitedAt = new Date('2020-01-01'),
  } = combinedUser;
  const [space] = useDocumentData(db.collection('spaces').doc(spaceName), { idField: 'id' });
  const [showActivityDot, setShowActivityDot] = useState(false);

  useEffect(async () => {
    if (space) {
      const { lastActivityCreatedAt = null } = space;
      if (lastActivityCreatedAt && lastActivityCreatedAt > activityPageLastVisitedAt) {
        setShowActivityDot(true);
      } else {
        setShowActivityDot(false);
      }
    }
  }, [space, activityPageLastVisitedAt]);

  return (
    <>{showActivityDot && <CircleFill size={6} />}</>
  );
};

ActivityDot.propTypes = {
  spaceName: PropTypes.string.isRequired,
};

export default ActivityDot;
