import React, { useContext } from 'react';
import { Redirect } from 'react-router-dom';

import CombinedUserContext from '../../currentUser/CombinedUserContext';

const RedirectPage = () => {
  const { combinedUser } = useContext(CombinedUserContext);
  const { lastVisitedSpaceName } = combinedUser;

  return (
    <Redirect to={`/-/${lastVisitedSpaceName}${window.location.pathname}${window.location.search}${window.location.hash}`} />
  );
};

export default RedirectPage;
