import React, { useState } from 'react';

import PropTypes from 'prop-types';

import Backdrop from './Backdrop';
import LeftSideDrawer from './LeftSideDrawer';
import RightSideDrawer from './RightSideDrawer';
import MobileHeader from './MobileHeader';
import LeftNav from './LeftNav';
import './PageLayout.css';

const PageLayout = ({
  uid,
  email,
  onCommunitySpace,
  urlSpaceName,
  allowableSpaceNames,
  mobileHeaderTitle,
  mainContent,
  talkSidebar,
}) => {
  const [isLeftDrawerOpen, setIsLeftDrawerOpen] = useState(false);
  const [isRightDrawerOpen, setIsRightDrawerOpen] = useState(false);

  return (
    <div className="PageLayout">
      <LeftSideDrawer show={isLeftDrawerOpen}>
        <LeftNav
          uid={uid}
          email={email}
          onCommunitySpace={onCommunitySpace}
          spaceName={urlSpaceName}
          allowableSpaceNames={allowableSpaceNames}
        />
      </LeftSideDrawer>
      {talkSidebar && (
        <RightSideDrawer show={isRightDrawerOpen}>
          {talkSidebar}
        </RightSideDrawer>
      )}
      <header className="PageLayout__mobile-header">
        <MobileHeader
          title={mobileHeaderTitle}
          isTalk={!!talkSidebar}
          setIsLeftDrawerOpen={setIsLeftDrawerOpen}
          setIsRightDrawerOpen={setIsRightDrawerOpen}
        />
      </header>
      <div className="PageLayout__body">
        {(isLeftDrawerOpen || isRightDrawerOpen) && (
          <Backdrop
            onClick={() => {
              setIsLeftDrawerOpen(false);
              setIsRightDrawerOpen(false);
            }}
          />
        )}
        <aside className="PageLayout__left-aside">
          <div className="PageLayout__left-aside-inner">
            <LeftNav
              uid={uid}
              email={email}
              onCommunitySpace={onCommunitySpace}
              spaceName={urlSpaceName}
              allowableSpaceNames={allowableSpaceNames}
            />
          </div>
        </aside>
        <main className="PageLayout__content">
          <div className="PageLayout__content-inner">
            {mainContent}
            <div className="PageLayout__content-bottom-div">
              Invisible text
            </div>
          </div>
        </main>
        {talkSidebar && (
          <aside className="PageLayout__right-aside">
            <div className="PageLayout__right-aside-inner">
              {talkSidebar}
            </div>
          </aside>
        )}
      </div>
    </div>
  );
};

PageLayout.propTypes = {
  uid: PropTypes.string,
  email: PropTypes.string,
  onCommunitySpace: PropTypes.bool.isRequired,
  urlSpaceName: PropTypes.string.isRequired,
  allowableSpaceNames: PropTypes.arrayOf(PropTypes.string).isRequired,
  mobileHeaderTitle: PropTypes.string.isRequired,
  mainContent: PropTypes.node.isRequired,
  talkSidebar: PropTypes.node,
};

PageLayout.defaultProps = {
  uid: null,
  email: null,
  talkSidebar: '',
};

export default PageLayout;
