import React from 'react';
import ReactDOM from 'react-dom';

import './Backdrop.css';

const Backdrop = ({ onClick }) => ReactDOM.createPortal(
  /* eslint-disable */
  <div className="backdrop" onClick={onClick} />,
  document.getElementById('backdrop-hook'),
);

export default Backdrop;
