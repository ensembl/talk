import React, { useState } from 'react';

import PropTypes from 'prop-types';
import { DropdownButton, Dropdown, Spinner } from 'react-bootstrap';

import './SpaceSelectDropdown.css';

const SpaceSelectDropdown = ({
  spaceName,
  allowableSpaceNames,
}) => {
  const [buttonDisabled, setButtonDisabled] = useState(false);

  return (
    <div className="SpaceSelectDropdown">
      <DropdownButton
        size="sm"
        variant="outline-primary"
        title={buttonDisabled ? <Spinner animation="border" size="sm" /> : <span className="SpaceSelectDropdown__title">Go to space</span>}
        disabled={buttonDisabled}
      >
        {allowableSpaceNames.map((allowableSpaceName) => (
          <Dropdown.Item
            key={allowableSpaceName}
            onSelect={() => { setButtonDisabled(true); }}
            href={`/-/${allowableSpaceName}/home`}
            disabled={allowableSpaceName === spaceName}
          >
            {`${allowableSpaceName === spaceName ? '\u2713' : ''} ${allowableSpaceName}`}
          </Dropdown.Item>
        ))}
        {allowableSpaceNames.length > 0 && <Dropdown.Divider />}
        <Dropdown.Item href="/new-space">New space</Dropdown.Item>
      </DropdownButton>
    </div>
  );
};
SpaceSelectDropdown.propTypes = {
  spaceName: PropTypes.string.isRequired,
  allowableSpaceNames: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default SpaceSelectDropdown;
