import React from 'react';
import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types';

import './RightSideDrawer.css';

const RightSideDrawer = ({ show, children }) => (
  <CSSTransition
    in={show}
    timeout={200}
    classNames="slide-in-right"
    mountOnEnter
    unmountOnExit
  >
    <aside className="RightSideDrawer">
      {children}
    </aside>
  </CSSTransition>
);

RightSideDrawer.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  show: PropTypes.bool.isRequired,
};

export default RightSideDrawer;
