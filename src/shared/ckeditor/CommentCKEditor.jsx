import React from 'react';

import PropTypes from 'prop-types';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import { Editor } from 'ckeditor5-custom-build/build/ckeditor';
import FirebaseImageUploader from '../FirebaseImageUploader';
import { formatAsMentionUsers } from '../MentionsUtils';

import {
  getToolbarItems,
  getToolbarViewportTopOffset,
  getImageStyles,
  getImageToolbar,
  getTableContentToolbar,
  getFeedItems,
} from './CKEditorUtils';
import './CommentCKEditor.css';

const CommentCKEditor = ({
  starterMentionUserId,
  starterMentionUserName,
  starterMentionAvatarUrl,
  content,
  setContent,
  spaceName,
  users,
  shortCommentCKEditor,
}) => {
  const config = {
    placeholder: 'Write message · @ to notify people · drag and drop images',
    toolbar: {
      items: getToolbarItems(),
      viewportTopOffset: getToolbarViewportTopOffset(),
    },
    image: {
      styles: getImageStyles(),
      toolbar: getImageToolbar(),
    },
    table: {
      contentToolbar: getTableContentToolbar(),
    },
    mention: {
      feeds: [{
        marker: '@',
        feed: (queryText) => getFeedItems(
          queryText,
          formatAsMentionUsers(users),
          spaceName,
          true,
        ),
        minimumCharacters: 0,
      }],
    },
  };

  return (
    <div className={`${shortCommentCKEditor ? 'CommentCKEditor__short' : 'CommentCKEditor'}`}>
      <CKEditor
        editor={Editor}
        config={config}
        data={content}
        onReady={(editor) => {
          if (editor) {
            // eslint-disable-next-line no-param-reassign
            editor.plugins.get('FileRepository').createUploadAdapter = (loader) => new FirebaseImageUploader(loader, spaceName);

            // New talk with @ mention already in initial comment
            if (starterMentionUserId) {
              editor.execute('mention', {
                marker: '@',
                mention: {
                  id: `@${starterMentionUserName}`,
                  userName: starterMentionUserName,
                  avatarUrl: starterMentionAvatarUrl,
                  uid: starterMentionUserId,
                },
              });
            }
          }
        }}
        onChange={(event, editor) => {
          if (setContent) {
            setContent(editor.getData());
          }
        }}
      />
    </div>
  );
};

CommentCKEditor.propTypes = {
  starterMentionUserId: PropTypes.string,
  starterMentionUserName: PropTypes.string,
  starterMentionAvatarUrl: PropTypes.string,
  content: PropTypes.string.isRequired,
  setContent: PropTypes.func.isRequired,
  spaceName: PropTypes.string.isRequired,
  users: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    avatarUrl: PropTypes.string,
  })).isRequired,
  shortCommentCKEditor: PropTypes.bool,
};

CommentCKEditor.defaultProps = {
  starterMentionUserId: '',
  starterMentionUserName: '',
  starterMentionAvatarUrl: '',
  shortCommentCKEditor: false,
};

export default CommentCKEditor;
