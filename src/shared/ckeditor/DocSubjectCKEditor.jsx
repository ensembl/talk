import React from 'react';

import PropTypes from 'prop-types';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import { Editor, CollaborativeEditor } from 'ckeditor5-custom-build/build/ckeditor';
import { useLocation } from 'react-router-dom';

import { formatAsMentionUsers } from '../MentionsUtils';
import FirebaseImageUploader from '../FirebaseImageUploader';
import { getJwt } from './CKEditorTokenCallables';
import {
  getToolbarItems,
  getToolbarViewportTopOffset,
  getImageStyles,
  getImageToolbar,
  getTableContentToolbar,
  uploadUrl,
  webSocketUrl,
  getFeedItems,
} from './CKEditorUtils';
import {
  onCommentResolved,
  onAddComment,
} from './CKEditorCommentsCallbacks';
import './DocSubjectCKEditor.css';

const DocSubjectCKEditor = ({
  usesCollaborativeEditor,
  usesReadOnlyEditor,
  content,
  setAutoSaveContent,
  talkId,
  spaceName,
  users,
  setContentEditorReady,
  uid,
  name,
  avatarUrl,
  ckeditorJwt,
  setComment,
  updateTalk,
  setShowThreadResolvedMessage,
}) => {
  const MentionPlugin = CollaborativeEditor.builtinPlugins.find((plugin) => plugin.pluginName === 'Mention');
  const MentionCustomizationPlugin = CollaborativeEditor.builtinPlugins.find(
    (plugin) => plugin.pluginName === 'MentionCustomization',
  );

  const toolbarItems = getToolbarItems();

  const config = {
    placeholder: 'Write title and content · drag and drop images',
    toolbar: {
      items: toolbarItems,
      viewportTopOffset: getToolbarViewportTopOffset(),
    },
    image: {
      styles: getImageStyles(),
      toolbar: getImageToolbar(),
    },
    table: {
      contentToolbar: getTableContentToolbar(),
    },
    mention: {
      feeds: [{
        marker: '@',
        feed: (queryText) => getFeedItems(
          queryText,
          formatAsMentionUsers(users),
          spaceName,
          false,
        ),
        minimumCharacters: 0,
      }],
    },
    initialData: content,
    autosave: {
      save(editor) {
        setAutoSaveContent(editor.getData());
      },
    },
  };

  if (usesCollaborativeEditor) {
    toolbarItems.splice(1, 0, '|', 'comment', '|');

    config.cloudServices = {
      // ckeditorJwt should already be available (and non-null) since it was
      // retrieved in CKEditorTokenProvider earlier, and a talk takes
      // several moments to load anyways. But if it isn't ready or failed
      // retrieve it again here with getJwt.
      tokenUrl: async () => (ckeditorJwt || getJwt()),
      uploadUrl,
      webSocketUrl,
    };
    config.collaboration = {
      channelId: talkId,
    };
    config.comments = {
      editorConfig: {
        extraPlugins: [MentionPlugin, MentionCustomizationPlugin],
        mention: {
          feeds: [{
            marker: '@',
            feed: (queryText) => getFeedItems(
              queryText,
              formatAsMentionUsers(users),
              spaceName,
              false,
            ),
            minimumCharacters: 0,
          }],
        },
      },
    };
  }

  const location = useLocation();

  const showCommentThread = (editor) => {
    const hash = location.hash.substring(1);
    const threadId = hash.split('_')[1];
    try {
      const markerList = Array.from(editor.model.markers.getMarkersGroup('comment'));
      const filteredList = markerList.filter(
        (marker) => marker.name.includes(threadId),
      );
      if (filteredList.length > 0) {
        const hashedMarkerId = filteredList[0].name;
        const range = editor.model.markers.get(
          hashedMarkerId,
        ).getRange();
        setTimeout(() => {
          editor.model.change((writer) => writer.setSelection(range));
        }, 0);
        // ? Probably a better way to focus on the editor
        const el = document.querySelector(`[data-comment="${threadId}"]`);
        if (el) {
          el.dispatchEvent(new Event('focus'));
          setTimeout(() => {
            el.scrollIntoView({ behavior: 'smooth', block: 'center' });
          }, 0);
        }
      }
    } catch (error) {
      throw new Error('Error displaying inline comment in doc', error);
    }
  };

  return (
    <div className="DocSubjectCKEditor">
      <CKEditor
        editor={usesCollaborativeEditor ? CollaborativeEditor : Editor}
        config={config}
        onReady={(editor) => {
          if (editor) {
            // eslint-disable-next-line no-param-reassign
            editor.isReadOnly = usesReadOnlyEditor;

            if (!usesReadOnlyEditor) {
              // eslint-disable-next-line no-param-reassign
              editor.plugins.get('FileRepository').createUploadAdapter = (loader) => new FirebaseImageUploader(loader, spaceName);
            }

            if (usesCollaborativeEditor) {
              // Resolve comment thread by removing the marker
              window.addEventListener('resolveCommentThread', (event) => {
                onCommentResolved({
                  editor,
                  threadId: event.detail.threadId,
                  setShowThreadResolvedMessage,
                  updateTalk,
                  uid,
                  name,
                  avatarUrl,
                });
              });

              if (location.hash) {
                showCommentThread(editor);
              }

              // Send notification if mention at doc comment
              editor.plugins.get('CommentsRepository').on('addComment', (event, data) => {
                onAddComment({
                  data,
                  editor,
                  setComment,
                  name,
                  uid,
                  avatarUrl,
                });
              }, { priority: 'high' });
            }

            setContentEditorReady(true);
          }
        }}
      />
    </div>
  );
};

DocSubjectCKEditor.propTypes = {
  usesCollaborativeEditor: PropTypes.bool.isRequired,
  usesReadOnlyEditor: PropTypes.bool.isRequired,
  content: PropTypes.string.isRequired,
  setAutoSaveContent: PropTypes.func.isRequired,
  talkId: PropTypes.string.isRequired,
  spaceName: PropTypes.string.isRequired,
  users: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    avatarUrl: PropTypes.string,
  })).isRequired,
  setContentEditorReady: PropTypes.func.isRequired,
  uid: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  ckeditorJwt: PropTypes.string.isRequired,
  setComment: PropTypes.func.isRequired,
  updateTalk: PropTypes.func.isRequired,
  setShowThreadResolvedMessage: PropTypes.func.isRequired,
};

export default DocSubjectCKEditor;
