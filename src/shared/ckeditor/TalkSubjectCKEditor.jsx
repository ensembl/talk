import React from 'react';

import PropTypes from 'prop-types';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import { Editor, CollaborativeEditor } from 'ckeditor5-custom-build/build/ckeditor';
import FirebaseImageUploader from '../FirebaseImageUploader';
import { getJwt } from './CKEditorTokenCallables';
import { formatAsMentionUsers } from '../MentionsUtils';

import {
  getToolbarItems,
  getToolbarViewportTopOffset,
  getImageStyles,
  getImageToolbar,
  getTableContentToolbar,
  uploadUrl,
  webSocketUrl,
  getFeedItems,
  getTalkFeedItems,
} from './CKEditorUtils';
import './TalkSubjectCKEditor.css';

const TalkSubjectCKEditor = ({
  usesCollaborativeEditor,
  usesReadOnlyEditor,
  content,
  setAutoSaveContent,
  talkId,
  spaceName,
  users,
  setContentEditorReady,
  ckeditorJwt,
}) => {
  const config = {
    placeholder: 'Write title and content · drag and drop images',
    toolbar: {
      items: getToolbarItems(),
      viewportTopOffset: getToolbarViewportTopOffset(),
    },
    image: {
      styles: getImageStyles(),
      toolbar: getImageToolbar(),
    },
    table: {
      contentToolbar: getTableContentToolbar(),
    },
    mention: {
      feeds: [{
        marker: '@',
        feed: (queryText) => getFeedItems(
          queryText,
          formatAsMentionUsers(users),
          spaceName,
          false,
        ),
        minimumCharacters: 0,
      }],
    },
    initialData: content,
    autosave: {
      save(editor) {
        setAutoSaveContent(editor.getData());
      },
    },
  };

  if (spaceName === 'Ensembl' || spaceName === 'Ensembl Two') {
    config.mention.feeds.push({
      marker: '#',
      feed: (queryText) => getTalkFeedItems(queryText, spaceName),
      minimumCharacters: 0,
    });
  }

  if (usesCollaborativeEditor) {
    config.cloudServices = {
      // ckeditorJwt should already be available (and non-null) since it was
      // retrieved in CKEditorTokenProvider earlier, and a talk takes
      // several moments to load anyways. But if it isn't ready or failed
      // retrieve it again here with getJwt.
      tokenUrl: async () => (ckeditorJwt || getJwt()),
      uploadUrl,
      webSocketUrl,
    };
    config.collaboration = {
      channelId: talkId,
    };
  }

  return (
    <div className="TalkSubjectCKEditor">
      <CKEditor
        editor={usesCollaborativeEditor ? CollaborativeEditor : Editor}
        config={config}
        onReady={(editor) => {
          if (editor) {
            // eslint-disable-next-line no-param-reassign
            editor.isReadOnly = usesReadOnlyEditor;

            if (!usesReadOnlyEditor) {
              // eslint-disable-next-line no-param-reassign
              editor.plugins.get('FileRepository').createUploadAdapter = (loader) => new FirebaseImageUploader(loader, spaceName);
            }

            setContentEditorReady(true);
          }
        }}
      />
    </div>
  );
};

TalkSubjectCKEditor.propTypes = {
  usesCollaborativeEditor: PropTypes.bool.isRequired,
  usesReadOnlyEditor: PropTypes.bool.isRequired,
  content: PropTypes.string.isRequired,
  setAutoSaveContent: PropTypes.func.isRequired,
  talkId: PropTypes.string.isRequired,
  spaceName: PropTypes.string.isRequired,
  users: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    avatarUrl: PropTypes.string,
  })).isRequired,
  setContentEditorReady: PropTypes.func.isRequired,
  ckeditorJwt: PropTypes.string.isRequired,
};

export default TalkSubjectCKEditor;
