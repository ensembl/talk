import { db } from '../../firebase-config';
import hash from '../HashUtils';

const removeNbsp = (content) => content.replace(/<p>&nbsp;<\/p>/g, '');

const getToolbarItems = () => ([
  'heading',
  '|',
  'bold',
  'italic',
  'underline',
  'link',
  'imageinsert',
  'blockquote',
  'fontcolor',
  'fontbackgroundcolor',
  '|',
  'superscript',
  'subscript',
  'strikethrough',
  '|',
  'bulletedList',
  'numberedList',
  'outdent',
  'indent',
  '|',
  'horizontalline',
  'inserttable',
  'alignment',
  '|',
  'code',
  'codeblock',
  '|',
  'undo',
  'redo',
  'removeformat',
]);

const getToolbarViewportTopOffset = () => {
  if (window.innerWidth >= 900) {
    return 40; // Desktop header height
  }
  return 45; // Mobile header height
};

const getImageStyles = () => ([
  'alignLeft',
  'alignCenter',
  'alignRight',
]);

const getImageToolbar = () => ([
  'imageStyle:alignLeft',
  'imageStyle:alignCenter',
  'imageStyle:alignRight',
  '|',
  'linkImage',
  'imageTextAlternative',
]);

const getTableContentToolbar = () => ([
  'tableColumn',
  'tableRow',
  'mergeTableCells',
  'tableProperties',
  'tableCellProperties',
]);

const uploadUrl = 'https://80145.cke-cs.com/easyimage/upload/';

const webSocketUrl = 'wss://80145.cke-cs.com/ws';

const getFeedItems = (queryText, mentionUsers, spaceName, withEnsemblBot) => {
  const filteredUsers = mentionUsers.filter(
    (user) => {
      const searchString = queryText.toLowerCase();
      return user.userName.toLowerCase().includes(searchString);
    },
  );
  if (spaceName !== 'Ensembl Community' && mentionUsers.length > 1 && !queryText) {
    filteredUsers.unshift(
      { id: `@Everyone in ${spaceName}`, uid: 'Everyone', userName: 'Everyone' },
    );
  }
  if (withEnsemblBot) {
    filteredUsers.push(
      {
        uid: 'Ensembl Bot',
        id: '@Ensembl Bot',
        userName: 'Ensembl Bot',
        avatarUrl: 'https://firebasestorage.googleapis.com/v0/b/ensembl-talk.appspot.com/o/ensembl-asset%2FEnsembl-Bot.png?alt=media&token=8545c806-c2c7-434c-8cd2-fd184b44601d',
      },
    );
  }
  return filteredUsers;
};

const getTalkFeedItems = async (queryText, spaceName) => {
  const ref = await db.collection(`spaces/${spaceName}/talks`)
    .where('searchHashes', 'array-contains', hash(queryText)).orderBy('writtenAt', 'desc').limit(6);
  const data = await ref.get();

  return data.docs.map((doc) => {
    const { title, isDoc } = doc.data();
    const type = isDoc ? 'doc' : 'talk';
    return {
      title, id: `#${title}`, link: `https://app.ensembl.so/-/${spaceName}/${type}s/${doc.id}`, talkId: doc.id,
    };
  });
};

export {
  removeNbsp,
  getToolbarItems,
  getToolbarViewportTopOffset,
  getImageStyles,
  getImageToolbar,
  getTableContentToolbar,
  uploadUrl,
  webSocketUrl,
  getFeedItems,
  getTalkFeedItems,
};
