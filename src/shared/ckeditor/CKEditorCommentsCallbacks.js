import { v4 as uuidv4 } from 'uuid';
import { mentionsFromHtml } from '../MentionsUtils';

const onCommentResolved = async ({
  editor,
  threadId,
  setShowThreadResolvedMessage,
  updateTalk,
  uid,
  name,
  avatarUrl,
}) => {
  const markerList = Array.from(editor.model.markers.getMarkersGroup('comment'));
  if (markerList.length > 0) {
    const filteredList = markerList.filter(
      (marker) => marker.name.includes(threadId),
    );
    editor.model.change((writer) => writer.removeMarker(filteredList[0]));
  }
  setShowThreadResolvedMessage(true);
  await updateTalk({
    [`threadsMeta.${threadId}`]: {
      resolvedById: uid,
      resolvedByName: name,
      resolvedByAvatarUrl: avatarUrl,
      resolvedAt: new Date(),
    },
  });
};

const onAddComment = async ({
  data,
  editor,
  setComment,
  name,
  uid,
  avatarUrl,
}) => {
  const {
    content: commentContent,
    threadId,
    commentId,
    authorId,
  } = data;

  const markerList = Array.from(editor.model.markers.getMarkersGroup('comment'));
  const foundMarker = markerList.find(
    (marker) => marker.name.includes(threadId),
  );
  const items = foundMarker.getRange().getItems();
  const markerContent = items.next().value.data;

  if (authorId !== uid && !window.location.hostname.includes('localhost')) {
    return;
  }
  // https://github.com/ckeditor/ckeditor5/issues/9640#issuecomment-834570527
  let firestoreCommentId = uuidv4();
  if (!commentId) {
    // eslint-disable-next-line no-param-reassign
    data.commentId = firestoreCommentId;
  } else {
    firestoreCommentId = commentId;
  }
  const mentions = mentionsFromHtml(commentContent, {}, 'notify');
  await setComment(
    firestoreCommentId,
    {
      htmlBody: commentContent,
      createdAt: new Date(),
      createdByName: name,
      createdById: uid,
      createdByAvatarUrl: avatarUrl,
      mentions,
      threadId,
      markerContent,
    },
  );
};

export {
  onCommentResolved,
  onAddComment,
};
