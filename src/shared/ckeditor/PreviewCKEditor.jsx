import React from 'react';

import PropTypes from 'prop-types';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import { Editor } from 'ckeditor5-custom-build/build/ckeditor';

import {
  getToolbarItems,
  getToolbarViewportTopOffset,
  getImageStyles,
  getImageToolbar,
  getTableContentToolbar,
} from './CKEditorUtils';
import './PreviewCKEditor.css';

const PreviewCKEditor = ({
  content,
  type,
}) => {
  const config = {
    toolbar: {
      items: getToolbarItems(),
      viewportTopOffset: getToolbarViewportTopOffset(),
    },
    image: {
      styles: getImageStyles(),
      toolbar: getImageToolbar(),
    },
    table: {
      contentToolbar: getTableContentToolbar(),
    },
  };

  return (
    <div
      className={`PreviewCKEditor PreviewCKEditor__${type}`}
    >
      <CKEditor
        editor={Editor}
        config={config}
        data={content}
        onReady={(editor) => {
          // https://github.com/ckeditor/ckeditor5-react/issues/29#issuecomment-413574185
          if (editor) {
            // eslint-disable-next-line no-param-reassign
            editor.isReadOnly = true;
          }
        }}
      />
    </div>
  );
};

PreviewCKEditor.propTypes = {
  content: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['doc', 'talk']).isRequired,
};

export default PreviewCKEditor;
