import { functions } from '../../firebase-config';

const setJwt = (setCkeditorJwt) => {
  const callable = functions.httpsCallable('ckeditor-getCkeditorJWT');

  callable().then((result) => {
    setCkeditorJwt(result.data);
  }).catch(() => {
    callable().then((result) => {
      setCkeditorJwt(result.data);
    }).catch(() => {
      callable().then((result) => {
        setCkeditorJwt(result.data);
      });
    });
  });
  // Fall back on TalkSubjectCKEditor / DocSubjectCKEditor to
  // retrieve the token if fail after all tries
};

const getJwt = async () => {
  const callable = functions.httpsCallable('ckeditor-getCkeditorJWT');

  let result;
  try {
    result = await callable();
  } catch (e1) {
    try {
      result = await callable();
    } catch (e2) {
      try {
        result = await callable();
      } catch (e3) {
        throw new Error('Failed to get token for ckeditor in getJwt', e3);
      }
    }
  }
  return result.data;
};

export { setJwt, getJwt };
