const timeAgoFormatter = (value, unit) => {
  if (unit === 'second' && value < 60) return 'just now';
  return value === 1 ? `1 ${unit} ago` : `${value} ${unit}s ago`;
};

export default timeAgoFormatter;
