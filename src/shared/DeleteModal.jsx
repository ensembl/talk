import React from 'react';

import PropTypes from 'prop-types';
import { Button, Modal, Spinner } from 'react-bootstrap';

import './DeleteModal.css';

const DeleteModal = ({
  showDeleteModal,
  setShowDeleteModal,
  isDeleting,
  deleteFromModal,
  modalTitle,
  modalBody,
  modalDeleteButtonLabel,
}) => (
  <Modal show={showDeleteModal} animation={false}>
    <Modal.Header>
      <Modal.Title>{modalTitle}</Modal.Title>
    </Modal.Header>
    <Modal.Body>{modalBody}</Modal.Body>
    <Modal.Footer>
      <Button className="btn-sm" variant="outline-primary" onClick={() => { setShowDeleteModal(false); }}>Cancel</Button>
      <Button disabled={isDeleting} className="btn-sm delete-confirm-button" variant="danger" onClick={deleteFromModal}>
        { !isDeleting && <span>{modalDeleteButtonLabel}</span> }
        { isDeleting
          && (
            <span>
              <Spinner animation="border" size="sm" />
              <span> Deleting...</span>
            </span>
          )}
      </Button>

    </Modal.Footer>
  </Modal>
);

DeleteModal.propTypes = {
  showDeleteModal: PropTypes.bool.isRequired,
  setShowDeleteModal: PropTypes.func.isRequired,
  isDeleting: PropTypes.bool.isRequired,
  deleteFromModal: PropTypes.func.isRequired,
  modalTitle: PropTypes.string.isRequired,
  modalBody: PropTypes.objectOf(PropTypes.any).isRequired,
  modalDeleteButtonLabel: PropTypes.string.isRequired,
};

export default DeleteModal;
