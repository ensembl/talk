import React from 'react';

import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { X } from 'react-bootstrap-icons';

import './Label.css';

const Label = ({
  name,
  color,
  id,
  remove,
  type,
}) => {
  const labelName = !type
    ? <div className="Label__name">{name}</div>
    : (
      <Link
        to={{
          pathname: '/home',
          search: `?tab=${type}s&labelId=${id}`,
        }}
      >
        <div className="Label__name">{name}</div>
      </Link>
    );

  return (
    <div className={`Label Label__color-${color}`}>
      {labelName}
      {remove && <Button className="Label__remove-label-x-button" onClick={() => { remove(id); }}><X size={16} /></Button>}
    </div>
  );
};

Label.propTypes = {
  name: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  id: PropTypes.string,
  remove: PropTypes.func,
  type: PropTypes.string,
};

Label.defaultProps = {
  id: '',
  remove: null,
  type: '',
};

export default Label;
