import React from 'react';

import PropTypes from 'prop-types';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';

import { X } from 'react-bootstrap-icons';

import './Avatar.css';

const Avatar = ({
  avatarUrl,
  tooltip,
  tooltipPlacement,
  remove,
  size,
}) => (
  <div className={`Avatar Avatar__size-${size}`}>
    {tooltip && (
      <OverlayTrigger
        placement={tooltipPlacement}
        delay={{ show: 50, hide: 50 }}
        overlay={<Tooltip>{tooltip}</Tooltip>}
      >
        <img
          src={!avatarUrl ? 'https://placekitten.com/55/55' : avatarUrl}
          alt="User"
        />
      </OverlayTrigger>
    )}
    {!tooltip && (
      <img
        src={avatarUrl}
        alt="User"
      />
    )}
    {remove && (
      <div className="Avatar__remove-x">
        <X
          size={16}
          onClick={remove}
        />
      </div>
    )}
  </div>
);

Avatar.propTypes = {
  avatarUrl: PropTypes.string.isRequired,
  tooltip: PropTypes.string,
  tooltipPlacement: PropTypes.string,
  remove: PropTypes.func,
  size: PropTypes.string,
};

Avatar.defaultProps = {
  tooltip: '',
  tooltipPlacement: 'top',
  remove: null,
  size: '',
};

export default Avatar;
