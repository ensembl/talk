import { db } from '../firebase-config';

const updateSpaceFirestore = async (spaceName, data) => {
  try {
    await db.collection('spaces').doc(spaceName).update(data);
  } catch (error) {
    throw new Error('Error updating talk:', error);
  }
};

export default updateSpaceFirestore;
