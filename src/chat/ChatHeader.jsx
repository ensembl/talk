import React from 'react';

import { Alert } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import './ChatHeader.css';

const ChatHeader = () => (
  <div className="ChatHeader">
    <h1 className="ChatHeader__title">Chat</h1>
    <Alert
      variant="warning"
    >
      <span>
        {'Use chat for sending quick messages to teammates, like a Zoom link or telling the team that you won\'t be at work today. If in doubt, always '}
      </span>
      <Link to="/new/talk">start a new talk</Link>
      <span> instead.</span>
    </Alert>
  </div>
);

export default ChatHeader;
