import React, { useEffect, useState, useRef } from 'react';

import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

import ChatHeader from './ChatHeader';
import AddChatComment from './AddChatComment';
import Comment from '../shared/comment/Comment';
import { addChatCommentFirestore, updateChatCommentFirestore } from './ChatCommentFirestore';

import './ChatPageComments.css';

const ChatPageComments = ({
  spaceName,
  comments,
  users,
  uid,
  name,
  avatarUrl,
}) => {
  const history = useHistory();

  const addComment = async (data) => {
    await addChatCommentFirestore(
      spaceName,
      data,
    );
  };

  const updateComment = async (commentId, data) => {
    await updateChatCommentFirestore(
      spaceName,
      commentId,
      data,
    );
  };

  const el = useRef(null);
  const [commentsLength, setCommentsLength] = useState(comments.length);

  const { hash } = history.location;

  useEffect(() => {
    if (commentsLength !== comments.length) {
      el.current.scrollIntoView({ block: 'end', behavior: 'instant' });
      setCommentsLength(comments.length);
    }
  }, [comments]);

  useEffect(() => {
    const hashedComment = document.getElementById(hash.substring(1));
    if (hashedComment) {
      hashedComment.scrollIntoView({ behavior: 'smooth', block: 'start' });
    } else {
      el.current.scrollIntoView({ block: 'end', behavior: 'instant' });
    }
  }, []);

  return (
    <div className="ChatPageComments">
      <div className="ChatPageComments__comments">
        <div className="ChatPageComments__scroll-anchor" ref={el} />
        {comments && comments.filter((comment) => !comment.toDelete).map((comment) => (
          <Comment
            key={comment.id}
            spaceName={spaceName}
            comment={comment}
            users={users}
            updateComment={updateComment}
          />
        ))}
        {comments.length < 4 && <ChatHeader />}
      </div>
      <AddChatComment
        spaceName={spaceName}
        users={users}
        uid={uid}
        name={name}
        avatarUrl={avatarUrl}
        addComment={addComment}
      />
    </div>
  );
};

ChatPageComments.propTypes = {
  spaceName: PropTypes.string.isRequired,
  comments: PropTypes.arrayOf(PropTypes.any).isRequired,
  users: PropTypes.arrayOf(PropTypes.any).isRequired,
  uid: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
};

export default ChatPageComments;
