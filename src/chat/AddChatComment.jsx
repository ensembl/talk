import React from 'react';

import PropTypes from 'prop-types';

import AddComment from '../shared/comment/AddComment';

const AddChatComment = ({
  spaceName,
  users,
  uid,
  name,
  avatarUrl,
  addComment,
}) => (
  <AddComment
    localStorageKey={`app.ensembl.so/-/${spaceName}/chat/addComment`}
    spaceName={spaceName}
    users={users}
    uid={uid}
    name={name}
    avatarUrl={avatarUrl}
    addComment={addComment}
    shortCommentCKEditor
  />
);

AddChatComment.propTypes = {
  spaceName: PropTypes.string.isRequired,
  users: PropTypes.arrayOf(PropTypes.any).isRequired,
  uid: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  addComment: PropTypes.func.isRequired,
};

export default AddChatComment;
