import { db } from '../firebase-config';

const addChatCommentFirestore = async (spaceName, data) => {
  try {
    await db.collection(`spaces/${spaceName}/chats/0/comments`).add(data);
  } catch (error) {
    throw new Error('Error adding comment:', error, spaceName, data);
  }
};

const updateChatCommentFirestore = async (spaceName, id, data) => {
  try {
    await db.collection(`spaces/${spaceName}/chats/0/comments`).doc(id).update(data);
  } catch (error) {
    throw new Error('Error updating comment:', error, spaceName, id, data);
  }
};

export { addChatCommentFirestore, updateChatCommentFirestore };
