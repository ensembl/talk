import { functions } from '../firebase-config';

const createChat = async (spaceName) => {
  const callable = functions.httpsCallable('createChat-createChat');
  try {
    return await callable({ spaceName });
  } catch (error) {
    throw new Error('Error calling createChat-createChat: ', error);
  }
};

export default createChat;
