import React, { useContext } from 'react';

import { useCollectionData } from 'react-firebase-hooks/firestore';
import { useParams } from 'react-router-dom';

import { db } from '../firebase-config';
import CombinedUserContext from '../currentUser/CombinedUserContext';
import PageLayout from '../shared/layout/PageLayout';
import ChatPageComments from './ChatPageComments';
import PageLoadingSpinner from '../shared/PageLoadingSpinner';
import PageNotFound from '../shared/PageNotFound';

const ChatPage = () => {
  const { spaceName: urlSpaceName } = useParams();

  const { combinedUser } = useContext(CombinedUserContext);
  const {
    uid,
    name,
    avatarUrl,
    allowableSpaceNames,
  } = combinedUser;

  const onCommunitySpace = window.location.pathname.startsWith('/-/Ensembl%20Community');

  document.title = 'Chat';

  const [users, usersLoading, usersError] = useCollectionData(db.collection(`spaces/${urlSpaceName}/copied-users`).orderBy('name', 'asc'), { idField: 'id' });
  const [comments, commentsLoading, commentsError] = useCollectionData(
    db.collection(`spaces/${urlSpaceName}/chats/0/comments`).orderBy('createdAt', 'desc').limit(500), { idField: 'id' },
  );

  return (
    <PageLayout
      uid={uid}
      onCommunitySpace={onCommunitySpace}
      urlSpaceName={urlSpaceName}
      allowableSpaceNames={allowableSpaceNames}
      mobileHeaderTitle="Chat"
      mainContent={(
        <>
          {(usersLoading || commentsLoading) && (
            <PageLoadingSpinner />
          )}
          {(usersError || commentsError) && (
            <PageNotFound />
          )}
          {users && comments && (
            <ChatPageComments
              spaceName={urlSpaceName}
              comments={comments}
              users={users}
              uid={uid}
              name={name}
              avatarUrl={avatarUrl}
            />
          )}
        </>
      )}
    />
  );
};

export default ChatPage;
