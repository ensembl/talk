import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/functions';
import 'firebase/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyBBUDTAMjliUDBC-o_AJRR0In-Jod9flrA',
  authDomain: 'auth.app.ensembl.so',
  projectId: 'ensembl-talk',
  storageBucket: 'ensembl-talk.appspot.com',
  messagingSenderId: '702323845840',
  appId: '1:702323845840:web:00da8fe05e9472e62d4cd5',
  measurementId: 'G-DQ7W33VT40',
};

firebase.initializeApp(firebaseConfig);

const functions = firebase.functions();
const db = firebase.firestore();
const auth = firebase.auth();
const storage = firebase.storage();

if (window.location.hostname === 'localhost') {
  db.useEmulator('localhost', 8081);
  functions.useEmulator('localhost', 5001);
  auth.useEmulator('http://localhost:9099');
  console.warn('You are running in the emulator environment.'); // eslint-disable-line no-console
}

export {
  db, functions, storage, firebase,
};
