import React from 'react';

import { Redirect } from 'react-router';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import dompurify from 'dompurify';
import { AuthProvider } from './currentUser/AuthProvider';
import { CKEditorTokenProvider } from './currentUser/CKEditorTokenProvider';
import CombinedUserRoute from './currentUser/CombinedUserRoute';
import TalksPage from './talks/TalksPage';
import TalkPage from './talks/talk/TalkPage';
import NewTalkPage from './newTalk/NewTalkPage';
import ChatPage from './chat/ChatPage';
import ActivityPage from './activity/ActivityPage';
import PeoplePage from './people/PeoplePage';
import LabelsPage from './labels/LabelsPage';
import ProfilePage from './profile/ProfilePage';
import NewSpacePage from './newSpace/NewSpacePage';
import RedirectPage from './shared/layout/RedirectPage';
import RedirectToTalksPage from './shared/layout/RedirectToTalksPage';
import Login from './login/Login';
import CacheBuster from './shared/CacheBuster';
import JoinPage from './join/JoinPage';
import SettingsPage from './settings/SettingsPage';
import SlackOAuthRedirectPage from './settings/SlackOAuthRedirectPage';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

dompurify.addHook('afterSanitizeAttributes', (node) => {
  if ('target' in (node) && !node.href.includes('https://app.ensembl.so')) {
    node.setAttribute('rel', 'noopener noreferrer');
    node.setAttribute('target', '_blank');
  }
});

const App = () => (
  <Router>
    <CacheBuster>
      <AuthProvider>
        <CKEditorTokenProvider>
          <Switch>
            <CombinedUserRoute exact path="/-/Ensembl Community/join" component={JoinPage} />
            <CombinedUserRoute exact path="/-/:spaceName/new/:type" component={NewTalkPage} />
            <CombinedUserRoute exact path="/new/:type" component={RedirectPage} />

            <CombinedUserRoute exact path="/-/:spaceName/talks/:id" component={TalkPage} />
            <CombinedUserRoute exact path="/talks/:id" component={RedirectPage} />

            <CombinedUserRoute exact path="/-/:spaceName/docs/:id" component={TalkPage} />
            <CombinedUserRoute exact path="/docs/:id" component={RedirectPage} />

            <CombinedUserRoute exact path="/-/:spaceName/home" component={TalksPage} />
            <CombinedUserRoute exact path="/home" component={RedirectPage} />
            <Route exact path="/talks"><Redirect to="/home?tab=talks" /></Route>
            <Route exact path="/docs"><Redirect to="/home?tab=docs" /></Route>
            <Route exact path="/"><Redirect to="/home?tab=talks" /></Route>

            <CombinedUserRoute exact path="/-/:spaceName/chat" component={ChatPage} />

            <CombinedUserRoute exact path="/-/:spaceName/activity" component={ActivityPage} />
            <CombinedUserRoute exact path="/activity" component={RedirectPage} />

            <CombinedUserRoute exact path="/-/:spaceName/people" component={PeoplePage} />
            <CombinedUserRoute exact path="/people" component={RedirectPage} />

            <CombinedUserRoute exact path="/-/:spaceName/labels" component={LabelsPage} />
            <CombinedUserRoute exact path="/labels" component={RedirectPage} />

            <CombinedUserRoute exact path="/-/:spaceName/settings" component={SettingsPage} />
            <CombinedUserRoute exact path="/settings" component={RedirectPage} />

            <Route exact path="/-/:spaceName" component={RedirectToTalksPage} />
            <CombinedUserRoute exact path="/profile" component={ProfilePage} />

            <CombinedUserRoute exact path="/new-space" component={NewSpacePage} />

            <CombinedUserRoute
              exact
              path="/slack-oauth-redirect/:oAuthType"
              component={SlackOAuthRedirectPage}
            />

            <Route exact path="/login" component={Login} />
            <Route exact path="/signup" component={Login} />
          </Switch>
        </CKEditorTokenProvider>
      </AuthProvider>
    </CacheBuster>
  </Router>
);
export default App;
