rules_version = '2';
service cloud.firestore {
  match /databases/{database}/documents {

    function isAdmin() {
      return request.auth.uid == 'kvdclBTDaiXibufaQMjV6MvRebJ3' || request.auth.uid == 'XPVqAu7gMDZSDzPSQxPpiU74s8b2'
    }

    function isSignedIn() {
      return request.auth.uid != null;      
    }

    function isInSecuredSpace(spaceName) {
      return spaceName in get(/databases/$(database)/documents/users/$(request.auth.uid)/secured/spaceNames).data.allowable 
    }

    function isInCommunitySpace(spaceName) {
      return spaceName == 'Ensembl Community';   
    }

    function isTalkCreator(spaceName, talkId) {
      return get(/databases/$(database)/documents/spaces/$(spaceName)/talks/$(talkId))
        .data.createdById == request.auth.uid;
    }

    function isCreatedByEnsemblBot(spaceName, talkId) {
      return get(/databases/$(database)/documents/spaces/$(spaceName)/talks/$(talkId))
        .data.createdById == 'Ensembl Bot';
    }

    function isCommentCreator(spaceName, talkId, commentId) {
      return get(/databases/$(database)/documents/spaces/$(spaceName)/talks/$(talkId)/comments/$(commentId))
        .data.createdById == request.auth.uid;
    }

    function isCommentMentioned(spaceName, talkId, commentId) {
      return get(/databases/$(database)/documents/spaces/$(spaceName)/talks/$(talkId)/comments/$(commentId))
        .data.mentions.keys().hasAll([request.auth.uid]);
    }

    function isChatCommentCreator(spaceName, commentId) {
      return get(/databases/$(database)/documents/spaces/$(spaceName)/chats/0/comments/$(commentId))
        .data.createdById == request.auth.uid;
    }

    function isChatCommentMentioned(spaceName, commentId) {
      return get(/databases/$(database)/documents/spaces/$(spaceName)/chats/0/comments/$(commentId))
        .data.mentions.keys().hasAll([request.auth.uid]);
    }    

    match /custom-claims-refresh-times/{userId} {
      allow read: if 
        isSignedIn()
        && request.auth.uid == userId;
    }

    match /users/{userId} {
      allow read: if
        isSignedIn() 
        && request.auth.uid == userId;
      allow update: if
        isSignedIn()
        && request.auth.uid == userId
        && (request.resource.data.diff(resource.data).affectedKeys()
          .hasOnly([
            'avatarUrl',
            'email',
            'name',
            'lastVisitedSpaceName',
            'activityPageLastVisitedAt',
            'dismissedCommentOnboarding', 
            'dismissedNewTalkOnboarding',
            'isIntegratedWithSlack'
          ]));
    }

    // For lastActivityCreatedAt
    match /spaces/{spaceName} {
      allow read: if
        isSignedIn()
        && isInSecuredSpace(spaceName);
      allow update: if
        isSignedIn()
        && isInSecuredSpace(spaceName)
        && (request.resource.data.diff(resource.data).affectedKeys()
          .hasOnly(['isIntegratedWithSlack']));   
    }

    match /spaces/{spaceName}/copied-users/{userId} {
      allow read: if
        isInCommunitySpace(spaceName)
      allow read: if
        isSignedIn()
        && isInSecuredSpace(spaceName);
    }

    match /spaces/{spaceName}/talks/{talkId} {
      // Non-community spaces
      allow read, create: if
        isSignedIn()
        && isInSecuredSpace(spaceName);      
      allow update: if
        isSignedIn()
        && isInSecuredSpace(spaceName)
        && !isInCommunitySpace(spaceName)
        && (!request.resource.data.diff(resource.data).affectedKeys()
          .hasAny(['toDelete']));
      allow update: if
        isSignedIn()
        && isInSecuredSpace(spaceName)
        && !isInCommunitySpace(spaceName)
        && (isTalkCreator(spaceName, talkId) || isCreatedByEnsemblBot(spaceName, talkId))
        && (request.resource.data.diff(resource.data).affectedKeys()
          .hasOnly(['toDelete']));       
      
      // Community space
      allow read: if
        isInCommunitySpace(spaceName)
      allow update: if
        isSignedIn()
        && isInSecuredSpace(spaceName)
        && isInCommunitySpace(spaceName)
        && isTalkCreator(spaceName, talkId)
      allow update: if
        isSignedIn()
        && isInSecuredSpace(spaceName)
        && isInCommunitySpace(spaceName)
        && isAdmin()
      allow update: if
        isSignedIn()
        && isInSecuredSpace(spaceName)
        && isInCommunitySpace(spaceName)
        && (request.resource.data.diff(resource.data).affectedKeys()
          .hasAny(['subscribedUids', 'approverIds']));        
    }
  
    match /spaces/{spaceName}/talks/{talkId}/comments/{commentId} {
      allow read: if
        isInCommunitySpace(spaceName)  
      allow read, create: if
        isSignedIn()
        && isInSecuredSpace(spaceName)
      allow update: if
        isSignedIn()
        && isInSecuredSpace(spaceName)
        && isCommentMentioned(spaceName, talkId, commentId)
        && (request.resource.data.diff(resource.data).affectedKeys()
        .hasOnly(['mentions']));
      allow update: if
        isSignedIn()
        && isInSecuredSpace(spaceName)
        && (request.resource.data.diff(resource.data).affectedKeys()
        .hasOnly(['heartedUids']));
      allow update: if
        isSignedIn()
        && isInSecuredSpace(spaceName)
        && isCommentCreator(spaceName, talkId, commentId);
    }

    match /spaces/{spaceName}/chats/0/comments/{commentId} {
      allow read, create: if
        isSignedIn()
        && isInSecuredSpace(spaceName)
      allow update: if
        isSignedIn()
        && isInSecuredSpace(spaceName)
        && isChatCommentMentioned(spaceName, commentId)
        && (request.resource.data.diff(resource.data).affectedKeys()
        .hasOnly(['mentions']));
      allow update: if
        isSignedIn()
        && isInSecuredSpace(spaceName)
        && (request.resource.data.diff(resource.data).affectedKeys()
        .hasOnly(['heartedUids']));
      allow update: if
        isSignedIn()
        && isInSecuredSpace(spaceName)
        && isChatCommentCreator(spaceName, commentId);        
    }

    match /spaces/{spaceName}/invitees/{inviteeId} {
      // Can only invite yourself to Ensembl Community
      allow create: if 
        spaceName == 'Ensembl Community' 
        && isSignedIn()
        && request.resource.data.email == request.auth.token.email
      allow read, create: if
        isSignedIn()
        && isInSecuredSpace(spaceName);
    }

    match /spaces/{spaceName}/activities/{activityId} {
      allow read: if
        isSignedIn()
        && isInSecuredSpace(spaceName);
    }

    match /spaces/{spaceName}/labels/{labelId} {
      allow read: if
        isInCommunitySpace(spaceName)
      allow read, write: if
        isSignedIn()
        && isInSecuredSpace(spaceName);
    }
  
    match /spaces/{spaceName}/stages/{stageId} {
      allow read, write: if
        isSignedIn()
        && isInSecuredSpace(spaceName);
    }    
  }
}
