/* eslint-disable */
import CommentThreadView from '@ckeditor/ckeditor5-comments/src/comments/ui/view/commentthreadview';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';

export default class DocCommentThreadView extends CommentThreadView {
  constructor(...args) {
    super(...args);
    // Get the default definition
    const templateDefinition = super._getTemplate();
    // Add the button to the default view
    templateDefinition.children[0].children.unshift(this._createImportantButtonView());
    // Setting the new template
    this.setTemplate(templateDefinition);
  }

  _createImportantButtonView() {
    // Create a new button view.
    const button = new ButtonView(this.locale);

    // Create an icon for the button view.
    const checkIcon = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check2" viewBox="0 0 16 16"><path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z"/></svg>';

    // Use the localization service.
    // The feature will be translatable.
    const { t } = this.locale;

    // Set the label and the icon for the button.
    button.set({
      icon: checkIcon,
      isToggleable: true,
      label: 'Resolve comment thread',
      withText: true,
    });

    // bind the button visibility to the length of the comment thread
    button.bind('isVisible').to(this, 'length', length => length > 0)

    // Add a class to the button to style it.
    button.extendTemplate({
      attributes: {
        class: 'ck-button--resolve-comment-thread',
      },
    });
    // When the button is clicked, dispatch custom event
    button.on('execute', (event) => {
      window.dispatchEvent(
        new CustomEvent('resolveCommentThread',
          { detail: { threadId: this._model.id } }))
    });

    return button;
  }
}

