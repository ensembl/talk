import Plugin from '@ckeditor/ckeditor5-core/src/plugin';

class MentionCustomization extends Plugin {
  // declare the plugin name
  static get pluginName() {
    return 'MentionCustomization';
  }

  // When getting the data from DB, cast element from HTML element into
  // ckeditor's data object
  init() {
    const { editor } = this;
    /* # THIS PORTION IS FOR # TALK */
    editor.conversion.for('upcast').elementToAttribute({
      view: {
        name: 'a',
        key: 'data-mention',
        classes: 'mention',
        attributes: {
          'data-title': true,
          'data-mention': true,
          'data-talk-id': true,
          link: true,
        },
      },
      model: {
        key: 'mention',
        value: (viewItem) => {
          // The mention feature expects that the mention attribute value
          // in the model is a plain object with a set of additional attributes.
          // In order to create a proper object use the toMentionAttribute() helper method:
          const mentionAttribute = editor.plugins.get('Mention').toMentionAttribute(viewItem, {
            // Add any other properties that you need.
            title: viewItem.getAttribute('data-title'),
            talkId: viewItem.getAttribute('data-talk-id'),
            id: viewItem.getAttribute('data-mention'),
            link: viewItem.getAttribute('data-link'),
          });

          return mentionAttribute;
        },
      },
      converterPriority: 'high',
    });
    // Downcast the model 'mention' text attribute to a view <a> element.
    editor.conversion.for('downcast').attributeToElement({
      model: 'mention',
      view: (modelAttributeValue, { writer }) => {
        // Do not convert empty attributes (lack of value means no mention) or non-talk mention
        if (!modelAttributeValue || !modelAttributeValue.talkId) {
          return;
        }
        // Disable eslint to follow official documentation from ckeditor
        /* eslint-disable consistent-return */
        return writer.createAttributeElement('a', {
          class: 'mention',
          'data-mention': modelAttributeValue.id,
          'data-talk-id': modelAttributeValue.talkId,
          'data-title': modelAttributeValue.title,
          href: modelAttributeValue.link,
        }, {
          // Make mention attribute to be wrapped by other attribute elements.
          priority: 20,
          // Prevent merging mentions together.
          id: modelAttributeValue.uid,
        });
      },
      converterPriority: 'high',
    });
    /* END OF # TALK */

    /* @ THIS IS FOR @ MENTION USER */
    editor.conversion.for('upcast').elementToAttribute({
      view: {
        name: 'a',
        key: 'data-mention',
        classes: 'mention',
        attributes: {
          'data-uid': true,
          'data-user-name': true,
          'data-avatar-url': true,
          'data-mention': true,
        },
      },
      model: {
        key: 'mention',
        value: (viewItem) => {
          // The mention feature expects that the mention attribute value
          // in the model is a plain object with a set of additional attributes.
          // In order to create a proper object use the toMentionAttribute() helper method:
          const mentionAttribute = editor.plugins.get('Mention').toMentionAttribute(viewItem, {
            // Add any other properties that you need.
            uid: viewItem.getAttribute('data-uid'),
            userName: viewItem.getAttribute('data-user-name'),
            avatarUrl: viewItem.getAttribute('data-avatar-url'),
            id: viewItem.getAttribute('data-mention'),
          });

          return mentionAttribute;
        },
      },
      converterPriority: 'high',
    });

    // Downcast the model 'mention' text attribute to a view <a> element.
    editor.conversion.for('downcast').attributeToElement({
      model: 'mention',
      view: (modelAttributeValue, { writer }) => {
        // Do not convert empty attributes (lack of value means no mention) or non user mention
        if (!modelAttributeValue || !modelAttributeValue.uid) {
          return;
        }
        // Disable eslint to follow official documentation from ckeditor
        /* eslint-disable consistent-return */
        return writer.createAttributeElement('a', {
          class: 'mention',
          'data-mention': modelAttributeValue.id,
          'data-uid': modelAttributeValue.uid,
          'data-user-name': modelAttributeValue.userName,
          'data-avatar-url': modelAttributeValue.avatarUrl,
        }, {
          // Make mention attribute to be wrapped by other attribute elements.
          priority: 20,
          // Prevent merging mentions together.
          id: modelAttributeValue.uid,
        });
      },
      converterPriority: 'high',
    });
    /* END OF @ USER */
  }
}

export default MentionCustomization;
