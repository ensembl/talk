const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.subscribeToTalkOnApproval = functions.firestore.document('spaces/{space}/talks/{talkId}')
  .onUpdate(async (change, context) => {
    const { space, talkId } = context.params;
    functions.logger.info('subscribeToTalkOnApproval fired: ',
      { space, talkId });

    const { approverIds: beforeApproverIds = [] } = change.before.data();
    const { approverIds = [] } = change.after.data();

    const addedUids = approverIds.filter((uid) => !beforeApproverIds.includes(uid));
    const removedUids = beforeApproverIds.filter((uid) => !approverIds.includes(uid));

    if (addedUids.length === 0 && removedUids.length === 0) {
      functions.logger.info('No change in approvals');
      return;
    }

    const [uid] = addedUids.length === 1 ? addedUids : removedUids;

    try {
      await db.collection(`spaces/${space}/talks`).doc(talkId).update({
        subscribedUids: admin.firestore.FieldValue.arrayUnion(uid),
      });
    } catch (error) {
      functions.logger.error('Error subscribing user to talk', {
        error, space, talkId, uid,
      });
    }
  });
