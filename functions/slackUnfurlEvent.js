const functions = require('firebase-functions');
const axios = require('axios');
const { convert } = require('html-to-text');

const { admin } = require('./admin');
const { slackCredentialsFromSlackUserId, slackCredentialsFromSlackTeamId } = require('./slackCredentials');
const {
  connectETAccountBlocks, previewNotAvailableBlocks, talkBlocks, commentBlocks,
} = require('./slackBlocks');

const db = admin.firestore();

const allowableSpaceNames = async ({ uid }) => {
  let spaceNamesDoc;
  try {
    spaceNamesDoc = await db.collection(`users/${uid}/secured`).doc('spaceNames').get();
  } catch (error) {
    functions.logger.error('Error getting spaceNames doc in allowableSpaceNames', { error, uid });
  }
  if (spaceNamesDoc.exists) {
    const { allowable } = spaceNamesDoc.data();
    return allowable;
  }
  return [];
};

const spaceNameFromUrl = ({ url }) => url
  .split('/-/')
  .pop()
  .split('/')
  .shift()
  .replace('%20', ' ');

const generateUnfurlPreview = async ({ url, uid }) => {
  const allowable = await allowableSpaceNames({ uid });
  const spaceName = spaceNameFromUrl({ url });

  if (!allowable.includes(spaceName)) {
    return { blocks: previewNotAvailableBlocks({ url }) };
  }

  const urlEnding = url.split('/').pop();
  let talkId;
  let commentId;
  if (urlEnding.includes('#comment_')) {
    [talkId, commentId] = urlEnding.split('#comment_');
  } else {
    talkId = urlEnding;
  }

  let talkDoc;
  try {
    talkDoc = await db.collection(`spaces/${spaceName}/talks`).doc(talkId).get();
  } catch (error) {
    functions.logger.error('Error getting talk/doc', { error, spaceName, talkId });
  }
  if (!talkDoc.exists) {
    return { blocks: previewNotAvailableBlocks({ url }) };
  }

  const {
    title, createdByAvatarUrl, createdByName, createdAt, isDoc, state,
  } = talkDoc.data();
  const talkUnfurlBlocks = talkBlocks({
    url, spaceName, title, createdByAvatarUrl, createdByName, createdAt, state, type: isDoc ? 'doc' : 'talk',
  });

  let commentUnfurlBlocks = [];
  if (commentId) {
    let commentDoc;
    try {
      commentDoc = await db.collection(`spaces/${spaceName}/talks/${talkId}/comments`).doc(commentId).get();
    } catch (error) {
      functions.logger.error('Error getting talk comment', {
        error, spaceName, talkId, commentId,
      });
    }
    if (commentDoc.exists) {
      const {
        htmlBody,
        createdByAvatarUrl: commentCreatedByAvatarUrl,
        createdByName: commentCreatedByName,
        createdAt: commentCreatedAt,
      } = commentDoc.data();
      const convertedBody = convert(htmlBody, {
        selectors: [{
          selector: 'a',
          options: { ignoreHref: true },
        }],
      });
      commentUnfurlBlocks = commentBlocks({
        url, convertedBody, commentCreatedByAvatarUrl, commentCreatedByName, commentCreatedAt,
      });
    }
  }
  return { blocks: [...talkUnfurlBlocks, ...commentUnfurlBlocks] };
};

exports.slackUnfurlEvent = functions.https.onRequest(async (req, res) => {
  functions.logger.info('slackUnfurlEvent endpoint fired');

  const {
    challenge,
    team_id: slackTeamId,
    event,
  } = req.body;

  if (challenge) {
    return res.send(challenge);
  }

  const {
    user: slackUserId,
    channel,
    message_ts: messageTs,
    links,
  } = event;

  const {
    accessToken: userAccessToken,
    uid,
  } = await slackCredentialsFromSlackUserId({ slackUserId });

  let spaceAccessToken;
  if (!userAccessToken) {
    const { accessToken } = await slackCredentialsFromSlackTeamId({ slackTeamId });
    spaceAccessToken = accessToken;
  }

  for (let i = 0; i < links.length; i += 1) {
    const { url } = links[i];
    let postBody = {
      channel,
      ts: messageTs,
      unfurls: {
        // eslint-disable-next-line no-await-in-loop
        [url]: userAccessToken ? await generateUnfurlPreview({ url, uid }) : '',
      },
    };
    if (!userAccessToken) {
      postBody = { ...postBody, user_auth_blocks: connectETAccountBlocks() };
    }
    try {
      await axios.post( // eslint-disable-line no-await-in-loop
        'https://slack.com/api/chat.unfurl',
        postBody,
        {
          headers: {
            Authorization: `Bearer ${userAccessToken || spaceAccessToken}`,
          },
          'content-type': 'application/json',
        },
      );
    } catch (err) {
      functions.logger.error('Error posting unfurl in slackUnfurlEvent', {
        err, postBody, userAccessToken, spaceAccessToken,
      });
    }
  }

  return res.sendStatus(200);
});
