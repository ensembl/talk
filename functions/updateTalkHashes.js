const functions = require('firebase-functions');

const { admin } = require('./admin');

const db = admin.firestore();

// https://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript
// eslint-disable-next-line
const hash = (s) => s.split('').reduce((a,b)=>{a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);

const combos = (array) => {
  // https://codereview.stackexchange.com/questions/7001/generating-all-combinations-of-an-array
  const results = [[]];
  for (const value of array) { // eslint-disable-line
    const copy = [...results];
    for (const prefix of copy) { // eslint-disable-line
      results.push(prefix.concat(value));
    }
  }
  return results;
};

const updateTalkHashes = async (spaceName, talkId, title, labelIds) => {
  // Lowercase, split on whitespace, and only keep alphanumeric
  const terms = title.toLowerCase().split(/[ ]+/).map((term) => term.replace(/[^0-9a-z]/gi, ''));

  // If the title is `This is the title", then match on
  // t, th, thi, this, i, is, t, th, ...
  // We want a title hash of 0 if a user filters on labels without entering a search term
  const titleHashes = [0];
  terms.forEach((term) => {
    for (let i = 0; i < term.length; i += 1) {
      titleHashes.push(hash(term.slice(0, i + 1)));
    }
  });

  // Find all combos of labelIds
  // For each combo, hash them together (sum of hash of each labelId in each combo)
  // labelSetHashes will be [0] if there are no labels
  const labelSetHashes = combos(labelIds).map((combo) => (
    combo.reduce((acc, elem) => (acc + hash(elem)), 0)
  ));

  // For each (titleHash, labelSetHash), sum together, and that becomes what we match for
  const searchHashes = [];
  titleHashes.forEach((titleHash) => {
    labelSetHashes.forEach((labelHash) => {
      searchHashes.push(titleHash + labelHash);
    });
  });
  try {
    await db.collection(`spaces/${spaceName}/talks`).doc(talkId).update({
      searchHashes,
    });
  } catch (error) {
    functions.logger.error('Error updating talk: ', {
      error, spaceName, talkId,
    });
  }
};

const updateTalkHashesAll = async () => {
  let snapshot;
  try {
    snapshot = await db.collectionGroup('talks').get();
  } catch (error) {
    functions.logger.error('Error getting talks for updateTalkHashesAll', { error });
  }
  for (let i = 0; i < snapshot.docs.length; i += 1) {
    const talkRef = snapshot.docs[i].ref;
    const talk = snapshot.docs[i].data();
    const { title, labelIds = [] } = talk;
    // eslint-disable-next-line no-await-in-loop
    await updateTalkHashes(talkRef.parent.parent.id, talkRef.id, title, labelIds);
  }
};

exports.updateTalkHashesOnCreate = functions.firestore.document('spaces/{spaceName}/talks/{talkId}')
  .onCreate(async (snapshot, context) => {
    const { spaceName, talkId } = context.params;
    functions.logger.info('updateTalkTabsOnCreate fired: ', {
      spaceName, talkId,
    });
    const talk = snapshot.data();
    const { title, labelIds = [] } = talk;
    await updateTalkHashes(spaceName, talkId, title, labelIds);
  });

exports.updateTalkHashesOnUpdate = functions.firestore.document('spaces/{spaceName}/talks/{talkId}')
  .onUpdate(async (change, context) => {
    const { spaceName, talkId } = context.params;
    functions.logger.info('updateTalkLabelHashesOnUpdate fired: ', {
      spaceName, talkId,
    });
    const talk = change.after.data();
    const { title, labelIds = [] } = talk;
    const beforeTalk = change.before.data();
    const { title: beforeTitle, labelIds: beforeLabelIds = [] } = beforeTalk;
    const addedLabelIds = labelIds.filter(
      (labelId) => !beforeLabelIds.includes(labelId),
    );
    const removedLabelIds = beforeLabelIds.filter(
      (beforeLabelId) => !labelIds.includes(beforeLabelId),
    );
    if (title === beforeTitle && addedLabelIds.length === 0 && removedLabelIds.length === 0) {
      functions.logger.info('No labels added or removed and title unchanged, do nothing: ', {
        spaceName, talkId, title, labelIds,
      });
      return;
    }
    await updateTalkHashes(spaceName, talkId, title, labelIds);
  });

exports.updateTalkHashesAllNightly = functions.pubsub.schedule('55 23 * * *').onRun(async () => {
  functions.logger.info('updateTalkHashesAllNightly fired: ');
  await updateTalkHashesAll();
});

// Run on command line for development
// node -e 'require("./functions/updateTalkHashes").refreshDevTalkHashes()'
module.exports.refreshDevTalkHashes = async () => {
  await updateTalkHashesAll();
};
