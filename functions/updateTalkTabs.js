const functions = require('firebase-functions');

const { admin } = require('./admin');

const db = admin.firestore();

const updateTalkTabs = async (spaceName, talkId, state, isDoc) => {
  const tabs = {};
  if (!isDoc && state === 'Open') {
    tabs.talks = true;
  } else if (!isDoc && state === 'In progress') {
    tabs.inProgress = true;
  } else if (!isDoc && state === 'Resolved') {
    tabs.resolvedTalks = true;
  } else if (isDoc && state === 'Open') {
    tabs.docs = true;
  } else if (isDoc && state === 'Archived') {
    tabs.archivedDocs = true;
  }
  try {
    await db.collection(`spaces/${spaceName}/talks`).doc(talkId).update({
      tabs,
    });
  } catch (error) {
    functions.logger.error('Error updating talk: ', {
      error, spaceName, talkId,
    });
  }
};

const updateTalkTabsAll = async () => {
  let snapshot;
  try {
    snapshot = await db.collectionGroup('talks').get();
  } catch (error) {
    functions.logger.error('Error getting talks for updateTalkTabsAll', { error });
  }
  for (let i = 0; i < snapshot.docs.length; i += 1) {
    const talkRef = snapshot.docs[i].ref;
    const talk = snapshot.docs[i].data();
    const { state, isDoc } = talk;
    // eslint-disable-next-line no-await-in-loop
    await updateTalkTabs(talkRef.parent.parent.id, talkRef.id, state, isDoc);
  }
};

exports.updateTalkTabsOnUpdate = functions.firestore.document('spaces/{spaceName}/talks/{talkId}')
  .onUpdate(async (change, context) => {
    const { spaceName, talkId } = context.params;
    functions.logger.info('updateTalkTabsOnUpdate fired: ', {
      spaceName, talkId,
    });
    const talk = change.after.data();
    const { state, isDoc } = talk;
    const beforeTalk = change.before.data();
    const { state: beforeState, isDoc: beforeIsDoc } = beforeTalk;
    if (state === beforeState && isDoc === beforeIsDoc) {
      functions.logger.info('state and isDoc both unchanged, do nothing: ', {
        spaceName, talkId, state, isDoc,
      });
      return;
    }
    await updateTalkTabs(spaceName, talkId, state, isDoc);
  });

exports.updateTalkTabsOnCreate = functions.firestore.document('spaces/{spaceName}/talks/{talkId}')
  .onCreate(async (snapshot, context) => {
    const { spaceName, talkId } = context.params;
    functions.logger.info('updateTalkTabsOnCreate fired: ', {
      spaceName, talkId,
    });
    const talk = snapshot.data();
    const { state, isDoc } = talk;
    await updateTalkTabs(spaceName, talkId, state, isDoc);
  });

exports.updateTalkTabsAllNightly = functions.pubsub.schedule('50 23 * * *').onRun(async () => {
  functions.logger.info('updateTalkTabsAllNightly fired: ');
  await updateTalkTabsAll();
});

// Run on command line for development
// node -e 'require("./functions/updateTalkTabs").refreshDevTalkTabs()'
module.exports.refreshDevTalkTabs = async () => {
  await updateTalkTabsAll();
};
