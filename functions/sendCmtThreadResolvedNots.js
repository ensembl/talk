const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.sendCmtThreadResolvedNots = functions.firestore.document('spaces/{spaceName}/talks/{talkId}')
  .onUpdate(async (change, context) => {
    const { spaceName, talkId } = context.params;
    functions.logger.info('sendCmtThreadResolvedNots fired: ',
      { spaceName, talkId });

    const {
      threadsMeta: beforeThreadsMeta,
    } = change.before.data();
    const {
      isDoc,
      threadsMeta,
      subscribedUids,
      title,
    } = change.after.data();

    if (!isDoc) { return; }

    const beforeThreadIds = beforeThreadsMeta ? Object.keys(beforeThreadsMeta) : [];
    const threadIds = threadsMeta ? Object.keys(threadsMeta) : [];
    const addedThreadId = threadIds.find((threadId) => !beforeThreadIds.includes(threadId));
    if (!addedThreadId) { return; }

    const { resolvedById, resolvedByName } = threadsMeta[addedThreadId];

    const filteredSubscribedUids = subscribedUids
      .filter((uid) => uid !== resolvedById);

    const snapshot = await db.collection(`spaces/${spaceName}/talks/${talkId}/comments`)
      .where('threadId', '==', addedThreadId)
      .orderBy('createdAt', 'desc')
      .limit(1)
      .get();

    const docId = await snapshot.docs[0].ref.id;
    const { markerContent } = await (await snapshot.docs[0].ref.get()).data();

    if (filteredSubscribedUids.length > 0) {
      try {
        await db.collection('mail').add({
          createdAt: new Date(),
          bccUids: filteredSubscribedUids,
          from: `${resolvedByName} - Ensembl Talk <ensembl@mg.ensembl.so>`,
          template: {
            name: 'docCommentResolvedNotification',
            data: {
              spaceName,
              title,
              resolvedByName,
              talkId,
              markerContent,
              commentId: docId,
            },
          },
        });
      } catch (error) {
        functions.logger.error('Error adding mail for notifyUids: ', {
          error, spaceName, talkId,
        });
      }
    }
  });
