const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.deleteLabelOnUpdate = functions.firestore.document('spaces/{space}/labels/{labelId}')
  .onUpdate(async (change, context) => {
    const { space, labelId } = context.params;
    functions.logger.info('deleteLabelOnUpdate fired: ', {
      space, labelId,
    });

    if (change.after.get('toDelete')) {
      try {
        await db.collection(`spaces/${space}/labels`).doc(labelId).delete();
      } catch (error) {
        functions.logger.error('Error deleting label: ', { error, labelId });
      }

      let snapshot;
      try {
        snapshot = await db.collection(`spaces/${space}/talks`).where('labelIds', 'array-contains', labelId).get();
      } catch (error) {
        functions.logger.error('Error getting talks: ', { error, space });
      }
      for (let i = 0; i < snapshot.docs.length; i += 1) {
        const talkRef = snapshot.docs[i].ref;
        try {
          // eslint-disable-next-line no-await-in-loop
          await talkRef.update({
            labelIds: admin.firestore.FieldValue.arrayRemove(labelId),
          });
        } catch (error) {
          functions.logger.error('Error updating talk: ', { error, talkId: snapshot.docs[i].id });
        }
      }
    }
  });
