const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.subscribeToTalkOnCmtCreate = functions.firestore.document('spaces/{space}/talks/{talkId}/comments/{commentId}')
  .onCreate(async (snapshot, context) => {
    const { space, talkId, commentId } = context.params;
    functions.logger.info('subscribeToTalkOnCmtCreate fired: ',
      { space, talkId, commentId });

    const createdById = snapshot.get('createdById');

    try {
      await db.collection(`spaces/${space}/talks`).doc(talkId).update({
        subscribedUids: admin.firestore.FieldValue.arrayUnion(createdById),
      });
    } catch (error) {
      functions.logger.error('Error subscribing user to Talk', {
        error, space, talkId, commentId, createdById,
      });
    }
  });
