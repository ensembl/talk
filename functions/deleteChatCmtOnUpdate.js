const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.deleteChatCmtOnUpdate = functions.firestore.document('spaces/{spaceName}/chats/0/comments/{commentId}')
  .onUpdate(async (change, context) => {
    const { spaceName, commentId } = context.params;
    functions.logger.info('deleteChatCmtOnUpdate fired: ', {
      spaceName, commentId,
    });
    if (change.after.get('toDelete')) {
      try {
        await db.collection(`spaces/${spaceName}/chats/0/comments/`).doc(commentId).delete();
      } catch (error) {
        functions.logger.error('Error deleting Comment: ', { error, commentId });
      }
    }
  });
