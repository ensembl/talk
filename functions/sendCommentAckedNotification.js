const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.sendCommentAckedNotification = functions.firestore.document('spaces/{spaceName}/talks/{talkId}/comments/{commentId}')
  .onUpdate(async (change, context) => {
    const { spaceName, talkId, commentId } = context.params;
    functions.logger.info('sendCommentAckedNotification fired: ',
      { spaceName, talkId, commentId });

    const beforeMentions = change.before.data().mentions;
    const { createdById, mentions, htmlBody } = change.after.data();

    const waitingUids = Object.entries(beforeMentions)
      .filter(([, map]) => map.type === 'ack' && !('acked' in map)).map(([uid]) => uid);
    const sentAckUserNames = Object.entries(mentions)
      .filter(([uid, map]) => waitingUids.includes(uid) && map.type === 'ack' && 'acked' in map).map(([, map]) => map.userName);
    // For a comment being changed because of an ack, there should only be one match.
    // Exit the cloud function if this isn't the case, since it was triggered by a different
    // reason or there's an error.
    if (sentAckUserNames.length !== 1 || sentAckUserNames[0] === 'Ensembl Bot') {
      return;
    }
    const sentAckUserName = sentAckUserNames[0];

    let talkDoc;
    try {
      talkDoc = await db.collection(`spaces/${spaceName}/talks`).doc(talkId).get();
    } catch (error) {
      functions.logger.error('Error getting talk: ', {
        error, spaceName, talkId, commentId,
      });
      return;
    }
    const { title } = talkDoc.data();

    try {
      await db.collection('mail').add({
        createdAt: new Date(),
        bccUids: [createdById],
        from: `${sentAckUserName} - Ensembl Talk <ensembl@mg.ensembl.so>`,
        template: {
          name: 'sentAckNotification',
          data: {
            spaceName,
            title,
            sentAckUserName,
            htmlBody,
            talkId,
            commentId,
          },
        },
      });
    } catch (error) {
      functions.logger.error('Error adding mail: ', {
        error, spaceName, talkId, commentId,
      });
    }
  });
