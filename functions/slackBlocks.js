const jsAgo = require('js-ago');

const connectETAccountBlocks = () => [
  {
    type: 'section',
    text: {
      type: 'plain_text',
      text: 'Connect your Ensembl Talk account to your Slack account and try this action again.',
    },
  },
  {
    type: 'actions',
    elements: [
      {
        type: 'button',
        text: {
          type: 'plain_text',
          text: 'Connect',
        },
        action_id: 'connectETAccountBlocks',
        url: 'https://app.ensembl.so/settings',
        style: 'primary',
      },
    ],
  },
];

const createdTalkBlocks = ({
  createdByName, type, spaceName, talkId, title,
}) => [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `*${createdByName}* created a ${type} in ${spaceName}: *<https://app.ensembl.so/-/${spaceName}/${type}s/${talkId}|${title}>*`,
    },
  },
];

const previewNotAvailableBlocks = ({ url }) => [
  {
    type: 'header',
    text: {
      type: 'plain_text',
      text: 'Preview not available',
      emoji: true,
    },
  },
  {
    type: 'actions',
    elements: [
      {
        type: 'button',
        text: {
          type: 'plain_text',
          emoji: true,
          text: 'View link',
        },
        action_id: 'previewNotAvailableBlocks',
        url: `${url}`,
        style: 'primary',
      },
    ],
  },
];

const talkBlocks = ({
  url, spaceName, title, createdByAvatarUrl, createdByName, createdAt, state, type,
}) => [
  {
    type: 'header',
    text: {
      type: 'plain_text',
      text: `${spaceName} | ${title}`,
      emoji: true,
    },
  },
  {
    type: 'context',
    elements: [
      {
        type: 'image',
        image_url: `${createdByAvatarUrl}`,
        alt_text: `${createdByName}`,
      },
      {
        type: 'mrkdwn',
        text: `*${createdByName}* created ${jsAgo(createdAt.seconds, { format: 'long' })} \`${state} ${type}\``,
      },
    ],
  },
  {
    type: 'actions',
    elements: [
      {
        type: 'button',
        text: {
          type: 'plain_text',
          emoji: true,
          text: 'View in Ensembl Talk',
        },
        action_id: 'talkBlocks',
        url: `${url}`,
        style: 'primary',
      },
    ],
  },
];

const commentBlocks = ({
  url, convertedBody, commentCreatedByAvatarUrl, commentCreatedByName, commentCreatedAt,
}) => [
  {
    type: 'divider',
  },
  {
    type: 'section',
    text: {
      type: 'plain_text',
      text: convertedBody,
    },
  },
  {
    type: 'context',
    elements: [
      {
        type: 'image',
        image_url: `${commentCreatedByAvatarUrl}`,
        alt_text: `${commentCreatedByName}`,
      },
      {
        type: 'mrkdwn',
        text: `*${commentCreatedByName}* commented ${jsAgo(commentCreatedAt.seconds, { format: 'long' })}`,
      },
    ],
  },
  {
    type: 'actions',
    elements: [
      {
        type: 'button',
        text: {
          type: 'plain_text',
          emoji: true,
          text: 'View comment in Ensembl Talk',
        },
        action_id: 'commentBlocks',
        url: `${url}`,
        style: 'primary',
      },
    ],
  },
];

module.exports = {
  connectETAccountBlocks,
  createdTalkBlocks,
  previewNotAvailableBlocks,
  talkBlocks,
  commentBlocks,
};
