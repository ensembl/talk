const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

const bot = {
  name: 'Ensembl Bot',
  id: 'Ensembl Bot',
  avatarUrl: 'https://firebasestorage.googleapis.com/v0/b/ensembl-talk.appspot.com/o/ensembl-asset%2FEnsembl-Bot.png?alt=media&token=8545c806-c2c7-434c-8cd2-fd184b44601d',
};

exports.createSpace = functions.https.onCall(async (data, context) => {
  const { uid } = context.auth;
  const { avatarUrl, spaceName, name } = data;

  functions.logger.info('createSpace fired: ', {
    uid, spaceName,
  });

  // It has to be in exact format that CKEditor codes it in
  // app.ensembl.so/-/Ensembl/talks/6W8z3WcAxuswlkfDueC4#comment_B3fI5pw1q84fELGMJAHB
  const starterTalkText = `<h3>Hey ${name}, welcome to Ensembl Talk 😊</h3><p>This is your very first talk in <strong>${spaceName}</strong>. You're looking at the talk proposal area. Add some content in the space below 👇. (Drag in images too.) It saves automatically.</p><p><span style="color:hsl(0,0%,60%);">[Add content here]</span></p><p>&nbsp;</p><p>Done with this talk? Change the state of the talk to <strong>Resolved</strong> at the top right ↗️.</p><p><a href="https://app.ensembl.so/-/Ensembl%20Community/docs/BOUmmLEM9VD0gbi4vA3v">Read the docs to learn more about talks.</a></p>`;

  const starterCommentText = `
    <p>Hey <a class="mention" data-mention="@${name}" data-uid="${uid}" data-user-name="${name}" data-avatar-url="${avatarUrl}">@${name}</a>, what do you think about the talk proposal above?</p>
    <p>Click the <b>ACK</b> button below 👇.</p>
    <p/>
    <p>Oh, and please @ mention me in a new comment. (I don't usually get much atttention.) Just type @ in the box, pick my name and send the comment.</p>
  `;

  const reducedSpaceName = spaceName.split(' ').join('').toLowerCase();

  let snapshot;
  try {
    snapshot = await db.collection('spaces').where('reducedSpaceName', '==', reducedSpaceName).get();
  } catch (error) {
    functions.logger.error('Error getting space: ', { reducedSpaceName });
  }
  if (!snapshot.empty) {
    return {
      warning: 'Name taken. Choose another name.',
    };
  }

  let spaceRef;
  try {
    spaceRef = db.collection('spaces').doc(spaceName);
    await spaceRef.set({
      reducedSpaceName,
      createdAt: new Date(),
    });
  } catch (error) {
    functions.logger.error('Error setting space: ', { reducedSpaceName });
  }

  try {
    await db.collection(`users/${uid}/secured/`).doc('spaceNames').update({
      allowable: admin.firestore.FieldValue.arrayUnion(spaceName),
    });
  } catch (error) {
    functions.logger.error('Error setting secured spaceName: ', { reducedSpaceName });
  }

  const now = new Date();
  let talkRef;
  try {
    talkRef = await spaceRef.collection('talks').add({
      title: `Hey ${name}, welcome to Ensembl Talk 😊`,
      htmlSubject: starterTalkText,
      createdAt: now,
      writtenAt: now,
      createdByName: bot.name,
      createdById: bot.id,
      createdByAvatarUrl: bot.avatarUrl,
      state: 'Open',
      subscribedUids: [],
      mergeRequestUrls: [],
      ownerIds: [],
      isDoc: false,
    });
  } catch (error) {
    functions.logger.error('Error creating starter talk: ', { reducedSpaceName });
  }

  try {
    await talkRef.collection('comments').add({
      htmlBody: starterCommentText,
      createdAt: now,
      createdByName: bot.name,
      createdById: bot.id,
      createdByAvatarUrl: bot.avatarUrl,
      threadId: talkRef.id,
      mentions: {
        [uid]: {
          avatarUrl,
          type: 'ack',
          userName: name,
        },
      },
    });
  } catch (error) {
    functions.logger.error('Error creating starter comment: ', { reducedSpaceName });
  }

  return {
    warning: '',
  };
});
