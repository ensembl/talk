const admin = require('firebase-admin');
const serviceAccount = require('./firebase-adminsdk-token.json');

const useEmulator = false;

const settings = {
  projectId: 'ensembl-talk',
};

// Uncomment this line if you're intended to make changes
// to the production setting with a local script

// settings.credential = admin.credential.cert(serviceAccount);

if (useEmulator) {
  settings.credential = admin.credential.cert(serviceAccount);
  process.env.FIRESTORE_EMULATOR_HOST = 'localhost:8081';
  process.env.FIREBASE_AUTH_EMULATOR_HOST = 'localhost:9099';
}

admin.initializeApp(settings);

module.exports = {
  admin,
};
