const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.subscribeToTalkOnSubjUpdate = functions.firestore.document('spaces/{space}/talks/{talkId}')
  .onUpdate(async (change, context) => {
    const { space, talkId } = context.params;
    functions.logger.info('subscribeToTalkOnSubjUpdate fired: ',
      { space, talkId });

    const { htmlSubject: beforeHtmlSubject } = change.before.data();
    const {
      htmlSubject,
      updatedById,
    } = change.after.data();

    if (htmlSubject === beforeHtmlSubject) {
      functions.logger.info('No change to htmlSubject: ', { htmlSubject });
      return;
    }

    try {
      await db.collection(`spaces/${space}/talks`).doc(talkId).update({
        subscribedUids: admin.firestore.FieldValue.arrayUnion(updatedById),
      });
    } catch (error) {
      functions.logger.error('Error subscribing user to talk', {
        error, space, talkId, updatedById,
      });
    }
  });
