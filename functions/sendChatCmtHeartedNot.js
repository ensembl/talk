const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.sendChatCmtHeartedNot = functions.firestore.document('spaces/{spaceName}/chats/0/comments/{commentId}')
  .onUpdate(async (change, context) => {
    const { spaceName, commentId } = context.params;
    functions.logger.info('sendChatCmtHeartedNot fired: ',
      { spaceName, commentId });

    const { heartedUids: beforeHeartedUids = [] } = change.before.data();
    const {
      createdById,
      heartedUids = [],
    } = change.after.data();

    const addedUids = heartedUids.filter((uid) => !beforeHeartedUids.includes(uid));
    if (addedUids.length === 0) {
      functions.logger.info('No additions to heartedUids');
      return;
    }

    const heartedUid = addedUids[0];
    let userDoc;
    try {
      userDoc = await db.collection(`spaces/${spaceName}/copied-users`).doc(heartedUid).get();
    } catch (error) {
      functions.logger.error('Error getting user: ', {
        error, spaceName, heartedUid,
      });
      return;
    }
    const { name: heartedName } = userDoc.data();

    try {
      await db.collection('mail').add({
        createdAt: new Date(),
        bccUids: [createdById],
        from: `${heartedName} - Ensembl Talk <ensembl@mg.ensembl.so>`,
        template: {
          name: 'chatCommentHeartedNotification',
          data: {
            spaceName,
            commentId,
            name: heartedName,
          },
        },
      });
    } catch (error) {
      functions.logger.error('Error adding mail for chatCommentHeartedNotification: ', {
        error, createdById, heartedName, spaceName, commentId,
      });
    }
  });
