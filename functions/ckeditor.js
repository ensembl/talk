const functions = require('firebase-functions');
const jwt = require('jsonwebtoken');
const { admin } = require('./admin');

const db = admin.firestore();

const generateJWT = (
  accessKey,
  environmentId,
  uid,
  user,
) => {
  const payload = {
    aud: environmentId,
    sub: process.env.FIRESTORE_EMULATOR_HOST === 'localhost:8081'
      ? 'kvdclBTDaiXibufaQMjV6MvRebJ3'
      : uid,
    user,
    auth: {
      collaboration: {
        '*': {
          role: 'writer',
        },
      },
    },
  };

  const result = jwt.sign(
    payload, accessKey, { algorithm: 'HS256', expiresIn: '72h' },
  );
  return result;
};

exports.getCkeditorJWT = functions.https.onCall(async (data, context) => {
  const { uid } = context.auth;

  let userDoc;
  try {
    userDoc = await db.collection('users').doc(uid).get();
  } catch (error) {
    functions.logger.error('Error getting user',
      { error, uid });
  }
  const {
    name = '',
    email = '',
    avatarUrl = '',
  } = userDoc.data();

  // If user doesn't have name or avatarUrl yet, don't return a token
  // When the user opens a talk or doc, it will try to get the token again
  // in TalkSubjectCKEditor/DocSubjectCKEditor
  if (!name || !avatarUrl || !email) {
    return '';
  }
  return generateJWT(
    functions.config().ckeditor.access_key,
    '71lhujXrI5Nj0uUsLm71',
    uid,
    {
      email,
      name,
      avatar: avatarUrl,
    },
  );
});
