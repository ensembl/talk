const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

const upsertStartTalkActivity = async (
  type,
  createdAt,
  createdByAvatarUrl,
  createdByName,
  talkId,
  title,
  spaceName,
) => {
  const data = {
    activityCreatedAt: createdAt,
    avatarUrl: createdByAvatarUrl,
    name: createdByName,
    htmlMessage: `<b>Started</b> a new ${type} <a href="https://app.ensembl.so/-/${spaceName}/${type}s/${talkId}">${title}</a>`,
  };

  try {
    await db.collection(`spaces/${spaceName}/activities`).doc(`${talkId}-new`).set(data, { merge: true });
  } catch (error) {
    functions.logger.error('Error setting activity: ', {
      error, spaceName, talkId,
    });
  }
};

const addTalkStateChangedActivity = async (
  changedStateAt,
  changedStateByAvatarUrl,
  changedStateByName,
  state,
  talkId,
  title,
  spaceName,
) => {
  const data = {
    activityCreatedAt: changedStateAt,
    avatarUrl: changedStateByAvatarUrl,
    name: changedStateByName,
    htmlMessage: `Moved <a href="https://app.ensembl.so/-/${spaceName}/talks/${talkId}">${title}</a> to <b>${state}</b>`,
  };

  try {
    await db.collection(`spaces/${spaceName}/activities`).add(data);
  } catch (error) {
    functions.logger.error('Error adding activity: ', {
      error, spaceName,
    });
  }
};

const updateSpaceLastActivityCreatedAt = async (
  spaceName,
  createdAt,
) => {
  try {
    await db.collection('spaces').doc(spaceName).set(
      {
        lastActivityCreatedAt: createdAt,
      },
      {
        merge: true,
      },
    );
  } catch (error) {
    functions.logger.error('Error setting space: ', {
      error, spaceName,
    });
  }
};

exports.talkActivityOnTalkUpdate = functions.firestore.document('spaces/{spaceName}/talks/{talkId}')
  .onUpdate(async (change, context) => {
    const { spaceName, talkId } = context.params;
    functions.logger.info('talkActivityOnTalkUpdate fired: ',
      { spaceName, talkId });

    const {
      isDoc,
      title,
      state,
      createdAt,
      createdByName,
      createdByAvatarUrl,
      changedStateAt,
      changedStateByName = '',
      changedStateByAvatarUrl = '',
    } = change.after.data();

    const { title: beforeTitle, state: beforeState } = change.before.data();

    // If title changed, upsert the talk started at activity item
    if (title !== beforeTitle) {
      await upsertStartTalkActivity(
        isDoc ? 'doc' : 'talk',
        createdAt,
        createdByAvatarUrl,
        createdByName,
        talkId,
        title,
        spaceName,
      );
      await updateSpaceLastActivityCreatedAt(
        spaceName,
        createdAt,
      );
    // If talk changed state, add a new activity item
    } else if (state !== beforeState) {
      await addTalkStateChangedActivity(
        changedStateAt,
        changedStateByAvatarUrl,
        changedStateByName,
        state,
        talkId,
        title,
        spaceName,
      );
      await updateSpaceLastActivityCreatedAt(
        spaceName,
        changedStateAt,
      );
    }
  });
