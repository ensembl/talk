const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

const setSecuredSpaceNameAndLoggedInIfAlreadyUser = async (email, spaceName, inviteeId) => {
  let snapshot;
  try {
    snapshot = await db.collection('users').where('email', '==', email).get();
  } catch (error) {
    functions.logger.error('Error getting users with email: ', {
      error, email,
    });
  }
  if (snapshot.empty) {
    return;
  }
  const userDoc = snapshot.docs[0];

  try {
    await db.collection(`users/${userDoc.id}/secured/`).doc('spaceNames').update({
      allowable: admin.firestore.FieldValue.arrayUnion(spaceName),
    });
  } catch (error) {
    functions.logger.error('Error updating allowable', error);
  }
  try {
    await db.collection(`spaces/${spaceName}/invitees/`).doc(inviteeId).update({
      userLoggedIn: true,
    });
  } catch (error) {
    functions.logger.error('Error updating invitee', error);
  }
};

const addEmail = async (invitedByName, spaceName, email) => {
  try {
    await db.collection('mail').add({
      createdAt: new Date(),
      to: email,
      from: `${invitedByName} - Ensembl Talk <ensembl@mg.ensembl.so>`,
      template: {
        name: 'inviteUser',
        data: {
          invitedByName,
          spaceName,
          email,
        },
      },
    });
  } catch (error) {
    functions.logger.error('Error adding mail: ', {
      error, email, invitedByName,
    });
  }
};

exports.sendInvite = functions.firestore.document('spaces/{spaceName}/invitees/{inviteeId}').onCreate(async (snapshot, context) => {
  const { spaceName, inviteeId } = context.params;
  functions.logger.info('sendInvite fired', { spaceName, inviteeId });

  const {
    email,
    invitedByName,
  } = snapshot.data();

  await setSecuredSpaceNameAndLoggedInIfAlreadyUser(email, spaceName, inviteeId);

  if (spaceName !== 'Ensembl Community') {
    await addEmail(invitedByName, spaceName, email);
  }
});

exports.resendInvite = functions.https.onCall(async (data) => {
  const {
    invitedByName = '',
    spaceName = '',
    email = '',
  } = data;
  functions.logger.info('resendInvite fired: ', {
    invitedByName, spaceName, email,
  });
  await addEmail(invitedByName, spaceName, email);
});
