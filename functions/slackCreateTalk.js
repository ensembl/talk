const functions = require('firebase-functions');
const axios = require('axios');

const { admin } = require('./admin');
const { slackCredentialsFromSlackUserId, slackCredentialsFromSlackTeamId } = require('./slackCredentials');
const { connectETAccountBlocks } = require('./slackBlocks');

const db = admin.firestore();

const pingSlackCreateTalk = async ({ endpoint }) => {
  try {
    await axios.get(endpoint, { params: { warmPing: true } });
  } catch (err) {
    functions.logger.error('Error warm pinging in pingSlackCreateTalk', { err });
  }
};

const isWarmPing = ({ req }) => {
  const { query } = req;
  const { warmPing } = query;
  return !!warmPing;
};

const jsonPayload = ({ req }) => {
  const { body } = req;
  const { payload: rawPayload } = body;
  return JSON.parse(rawPayload);
};

const flatPayload = ({ req }) => {
  const payload = jsonPayload({ req });
  const {
    response_url: responseUrl, user, team, channel, message,
  } = payload;
  const { id: slackUserId } = user;
  const { id: slackTeamId } = team;
  const { id: channelId } = channel;
  const { text } = message;
  return {
    responseUrl, slackUserId, slackTeamId, channelId, text,
  };
};

const isMessageAction = ({ req }) => {
  const payload = jsonPayload({ req });
  const { type } = payload;
  return type === 'message_action';
};

const postConnectETAccountPrompt = async ({ responseUrl }) => {
  try {
    await axios.post(
      responseUrl,
      {
        response_type: 'ephemeral',
        blocks: connectETAccountBlocks(),
      },
    );
  } catch (err) {
    functions.logger.error('Error posting in postConnectETAccountPrompt', { responseUrl });
  }
};

const postCreatingTalkMessage = async ({ responseUrl }) => {
  try {
    await axios.post(responseUrl, {
      text: 'Creating new talk...',
      response_type: 'ephemeral',
    });
  } catch (err) {
    functions.logger.error('Error posting in postCreatingTalkMessage', { responseUrl });
  }
};

const etUser = async ({ uid }) => {
  let userDoc;
  try {
    userDoc = await db.collection('users').doc(uid).get();
  } catch (error) {
    functions.logger.error('Error getting user doc in etUser', { error, uid });
  }
  if (userDoc.exists) {
    const { name, avatarUrl } = userDoc.data();
    return { name, avatarUrl };
  }
  return {};
};

const createTalk = async ({
  uid, slackTeamId, channelId, text,
}) => {
  const { name, avatarUrl } = await etUser({ uid });
  const { spaceName } = await slackCredentialsFromSlackTeamId({ slackTeamId });
  const nowDate = new Date();
  await db.collection(`spaces/${spaceName}/talks`).add({
    title: text,
    htmlSubject: `<h3>${text}</h3>`,
    createdAt: nowDate,
    writtenAt: nowDate,
    createdByName: name,
    createdById: uid,
    createdByAvatarUrl: avatarUrl,
    state: 'Open',
    subscribedUids: [],
    mergeRequestUrls: [],
    ownerIds: [],
    isDoc: false,
    createdInSlackChannelId: channelId,
  });
};

exports.slackCreateTalk = functions.https.onRequest(async (req, res) => {
  functions.logger.info('slackCreateTalk fired');

  if (isWarmPing({ req })) {
    functions.logger.info('Warm ping received');
    return res.status(200).send();
  }

  if (!isMessageAction({ req })) {
    functions.logger.info('Not a message action');
    return res.status(200).send();
  }

  const {
    responseUrl, slackUserId, slackTeamId, channelId, text,
  } = flatPayload({ req });

  const {
    accessToken: userAccessToken, uid,
  } = await slackCredentialsFromSlackUserId({ slackUserId });

  if (!userAccessToken) {
    await postConnectETAccountPrompt({ responseUrl });
    return res.status(200).send();
  }

  await postCreatingTalkMessage({ responseUrl });

  await createTalk({
    uid, slackTeamId, channelId, text,
  });

  return res.status(200).send();
});

exports.scheduledPingSlackCreateTalk = functions.pubsub.schedule('*/15 * * * *').onRun(async () => {
  functions.logger.info('scheduledPingSlackCreateTalk fired');
  await pingSlackCreateTalk({ endpoint: 'https://us-central1-ensembl-talk.cloudfunctions.net/slackCreateTalk-slackCreateTalk' });
});

// Run on command line for development
// node -e 'require("./functions/slackCreateTalk").devPingSlackCreateTalk()'
module.exports.devPingSlackCreateTalk = async () => {
  await pingSlackCreateTalk({ endpoint: 'http://localhost:5001/ensembl-talk/us-central1/slackCreateTalk-slackCreateTalk' });
};
