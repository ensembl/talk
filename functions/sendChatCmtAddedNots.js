const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

const mentionTypeUids = (mentions, type) => Object.entries(mentions)
  .filter(([, map]) => map.type === type).map(([uid]) => uid);

exports.sendChatCmtAddedNots = functions.firestore.document('spaces/{spaceName}/chats/0/comments/{commentId}')
  .onCreate(async (snapshot, context) => {
    const { spaceName, commentId } = context.params;
    functions.logger.info('sendChatCmtAddedNots fired: ',
      { spaceName, commentId });

    const {
      htmlBody, createdByName, mentions,
    } = snapshot.data();

    const requestAnswerUids = mentionTypeUids(mentions, 'answer');
    const requestAckUids = mentionTypeUids(mentions, 'ack');
    const notifyUids = mentionTypeUids(mentions, 'notify');

    if (notifyUids.length > 0) {
      try {
        await db.collection('mail').add({
          createdAt: new Date(),
          bccUids: notifyUids,
          from: `${createdByName} - Ensembl Talk <ensembl@mg.ensembl.so>`,
          template: {
            name: 'chatCommentAddedNotification',
            data: {
              spaceName,
              createdByName,
              htmlBody,
              senderAction: 'notifies you with',
              receiverAction: 'View and respond',
              commentId,
            },
          },
        });
      } catch (error) {
        functions.logger.error('Error adding mail for notifyUids: ', {
          error, spaceName, commentId,
        });
      }
    }

    if (requestAnswerUids.length > 0) {
      try {
        await db.collection('mail').add({
          createdAt: new Date(),
          bccUids: requestAnswerUids,
          from: `${createdByName} - Ensembl Talk <ensembl@mg.ensembl.so>`,
          template: {
            name: 'chatCommentAddedNotification',
            data: {
              spaceName,
              createdByName,
              htmlBody,
              senderAction: 'requests your <b>answer</b> on',
              receiverAction: 'View and <b>answer</b>',
              commentId,
            },
          },
        });
      } catch (error) {
        functions.logger.error('Error adding mail for requestAckUids: ', {
          error, spaceName, commentId,
        });
      }
    }

    if (requestAckUids.length > 0) {
      try {
        await db.collection('mail').add({
          createdAt: new Date(),
          bccUids: requestAckUids,
          from: `${createdByName} - Ensembl Talk <ensembl@mg.ensembl.so>`,
          template: {
            name: 'chatCommentAddedNotification',
            data: {
              spaceName,
              createdByName,
              htmlBody,
              senderAction: 'requests your <b>ACK</b> (acknowledgment) on',
              receiverAction: 'View and click <b>ACK</b>',
              commentId,
            },
          },
        });
      } catch (error) {
        functions.logger.error('Error adding mail for requestAckUids: ', {
          error, spaceName, commentId,
        });
      }
    }
  });
