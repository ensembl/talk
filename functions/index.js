// Firetore backups
exports.backupFirestoreHourly = require('./backupFirestoreHourly');
exports.backupFirestoreNightly = require('./backupFirestoreNightly');

// Automatically subscribe to notifications
exports.subscribeToTalkOnCmtAck = require('./subscribeToTalkOnCmtAck');
exports.subscribeToTalkOnCmtCreate = require('./subscribeToTalkOnCmtCreate');
exports.subscribeToTalkOnCmtMentioned = require('./subscribeToTalkOnCmtMentioned');
exports.subscribeToTalkOnApproval = require('./subscribeToTalkOnApproval');
exports.subscribeToTalkOnSubjUpdate = require('./subscribeToTalkOnSubjUpdate');

// Email notifications
exports.sendCmtAddedNots = require('./sendCmtAddedNots');
exports.sendCommentAckedNotification = require('./sendCommentAckedNotification');
exports.sendCmtHeartedNot = require('./sendCmtHeartedNot');
exports.sendTalkApprovalNots = require('./sendTalkApprovalNots');
exports.sendTalkDocStateChangedNots = require('./sendTalkDocStateChangedNots');
exports.sendCmtThreadResolvedNots = require('./sendCmtThreadResolvedNots');

// Chat email notifications
exports.sendChatCmtAddedNots = require('./sendChatCmtAddedNots');
exports.sendChatCmtAckedNot = require('./sendChatCmtAckedNot');
exports.sendChatCmtHeartedNot = require('./sendChatCmtHeartedNot');

// Talk and comment deletes
exports.deleteCmtOnUpdate = require('./deleteCmtOnUpdate');
exports.deleteTalkOnUpdate = require('./deleteTalkOnUpdate');
exports.deleteChatCmtOnUpdate = require('./deleteChatCmtOnUpdate');

// Talk updates
exports.updateTalkNewestCmt = require('./updateTalkNewestCmt');
exports.updateTalkTabs = require('./updateTalkTabs');
exports.updateTalkHashes = require('./updateTalkHashes');

// Chat
exports.createChat = require('./createChat');

// Label deletes
exports.deleteLabelOnUpdate = require('./deleteLabelOnUpdate');

// Activity
exports.talkActivityOnTalkUpdate = require('./talkActivityOnTalkUpdate');
exports.cmtActivityOnCmtCreate = require('./cmtActivityOnCmtCreate');

// User management
exports.copyUserToSpaces = require('./copyUserToSpaces');
exports.copySpaceNamesToUsers = require('./copySpaceNamesToUsers');
exports.createUserOnFirebaseAuthCreate = require('./createUserOnFirebaseAuthCreate');
exports.invitees = require('./invitees');

// Space management
exports.createSpace = require('./createSpace');

// Gitlab merge request
exports.gitlab = require('./gitlab');

// CKEditor access token
exports.ckeditor = require('./ckeditor');

// Measure usage
exports.usageInWeek = require('./usageInWeek');

// Respond to Bot request
exports.respondToCmtAdded = require('./respondToCmtAdded');

// Update state when merge request is created
exports.updateTalkStateOnMR = require('./updateTalkStateOnMR');

// Slack integration
exports.slackOAuth = require('./slackOAuth');
exports.slackUnfurlEvent = require('./slackUnfurlEvent');
exports.slackNotOnTalkCreate = require('./slackNotOnTalkCreate');
exports.slackCreateTalk = require('./slackCreateTalk');
