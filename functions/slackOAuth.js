const functions = require('firebase-functions');
const axios = require('axios');
const { admin } = require('./admin');

const db = admin.firestore();

exports.slackOAuth = functions.https.onCall(async (data, context) => {
  const { oAuthType, code, spaceName } = data;
  const { uid } = context.auth;

  // Make sure user has access to this space
  let spaceNamesDoc;
  try {
    spaceNamesDoc = await db.collection(`users/${uid}/secured`).doc('spaceNames').get();
  } catch (error) {
    functions.logger.error('Error getting spaceNamesDoc', { error, uid });
    return { status: 'bad code' };
  }
  const { allowable } = spaceNamesDoc.data();
  if (!allowable.includes(spaceName)) {
    functions.logger.error('User unauthorized access', { spaceName, uid });
    return { status: 'unauthorized access' };
  }

  const slackOAuthEndpoint = 'https://slack.com/api/oauth.v2.access';
  const clientId = functions.config().slack.client_id;
  const clientSecret = functions.config().slack.client_secret;
  const redirectUri = `https://app.ensembl.so/slack-oauth-redirect/${oAuthType}`;

  let oAuthRes;
  try {
    oAuthRes = await axios.post(slackOAuthEndpoint, `client_id=${clientId}&client_secret=${clientSecret}&code=${code}&redirect_uri=${redirectUri}`);
  } catch (error) {
    functions.logger.error('Error posting to slackOAuthEndpoint', {
      slackOAuthEndpoint, clientId, clientSecret, code, redirectUri,
    });
    return { status: 'bad code' };
  }

  const { data: oAuthData } = oAuthRes;
  const {
    ok,
    // Space fields
    team,
    incoming_webhook: incomingWebhook,
    access_token: spaceAccessToken,
    scope: spaceScope,
    // User fields
    authed_user: authedUser,
  } = oAuthData;

  if (!ok) {
    functions.logger.error('Not ok received from posting to slackOAuthEndpoint', {
      slackOAuthEndpoint, clientId, clientSecret, code, redirectUri,
    });
    return { status: 'bad code' };
  }

  const {
    id: slackUserId,
    access_token: userAccessToken,
    scope: userScope,
  } = authedUser;

  const firestoreId = oAuthType === 'space' ? spaceName : uid;
  const slackDocData = oAuthType === 'space'
    ? {
      team,
      incomingWebhook,
      accessToken: spaceAccessToken,
      scope: spaceScope,
    }
    : {
      slackUserId,
      accessToken: userAccessToken,
      scope: userScope,
    };

  const batch = db.batch();
  batch.set(
    db.collection(`${oAuthType}s/${firestoreId}/integrations/`).doc('slack'),
    slackDocData,
  );
  batch.update(
    db.collection(`${oAuthType}s`).doc(`${firestoreId}`),
    { isIntegratedWithSlack: true },
  );
  try {
    await batch.commit();
  } catch (error) {
    functions.logger.error('Error setting slackDocData and isIntegratedWithSlack', { oAuthType, firestoreId, slackDocData });
    return { status: 'bad code' };
  }
  return { status: 'ok' };
});

const deleteSlackDocIfNotInt = async ({ change }) => {
  const { before, after } = change;
  const { isIntegratedWithSlack: beforeIsIntegratedWithSlack } = before.data();
  const { isIntegratedWithSlack } = after.data();
  if (beforeIsIntegratedWithSlack === isIntegratedWithSlack) {
    functions.logger.info('No change in isIntegratedWithSlack');
    return;
  }
  if (!isIntegratedWithSlack) {
    try {
      const { ref: docRef } = after;
      await docRef.collection('integrations').doc('slack').delete();
    } catch (error) {
      functions.logger.error('Error deleting Slack integration doc');
    }
  }
};

exports.deleteSpaceSlackDocOnIntChange = functions.firestore.document('spaces/{spaceName}').onUpdate(async (change, context) => {
  const { params } = context;
  const { spaceName } = params;
  functions.logger.info('deleteSpaceSlackDocOnIntChange fired', { spaceName });
  await deleteSlackDocIfNotInt({ change });
});

exports.deleteUserSlackDocOnIntChange = functions.firestore.document('users/{uid}').onUpdate(async (change, context) => {
  const { params } = context;
  const { uid } = params;
  functions.logger.info('deleteUserSlackDocOnIntChange fired', { uid });
  await deleteSlackDocIfNotInt({ change });
});
