/* eslint-disable */
const functions = require('firebase-functions');

const { admin } = require('./admin');

const db = admin.firestore();

// Uncomment settings.credential in admin.js
// Run locally: node -e 'require("./functions/usageInWeek").usageInWeek()'
module.exports.usageInWeek = async () => {
  functions.logger.info('usageInWeek fired: ');

  const startDate = '2021-04-04'; // Sunday
  const endDatePlusOne = '2021-04-11'; // Sunday

  // const snapshot = await db.collection('spaces/Pui Tak Youth/talks')
  //   .where('createdAt', '>=', new Date(startDate))
  //   .where('createdAt', '<', new Date(endDatePlusOne))
  //   .get();
  // snapshot.docs.forEach((talk) => {
  //   console.log(talk.data().createdByName);
  // });

  const snapshot = await db.collectionGroup('comments')
    .where('createdAt', '>=', new Date(startDate))
    .where('createdAt', '<', new Date(endDatePlusOne))
    .get();
  snapshot.docs.forEach((comment) => {
    if (comment.ref.parent.parent.parent.parent.id === 'Pui Tak Youth') {
      console.log(comment.data().createdByName);
    }
  });
};
