const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.deleteTalkOnUpdate = functions.firestore.document('spaces/{space}/talks/{talkId}')
  .onUpdate(async (change, context) => {
    const { space, talkId } = context.params;
    functions.logger.info('deleteTalkOnUpdate fired: ', {
      space, talkId,
    });

    if (change.after.get('toDelete')) {
      const comments = await db.collection(`spaces/${space}/talks/${talkId}/comments`).get();
      // Set delete flag for nested comments
      comments.forEach((comment) => {
        try {
          comment.ref.update({
            toDelete: true,
          });
        } catch (error) {
          functions.logger.error('Error setting toDelete to comment', {
            error, talkId, comment,
          });
        }
      });
      // Delete the Talk
      try {
        await change.after.ref.delete();
      } catch (error) {
        functions.logger.error('Error deleting Talk: ', { error, talkId });
      }
    }
  });
