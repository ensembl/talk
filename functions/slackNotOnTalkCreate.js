const functions = require('firebase-functions');
const axios = require('axios');

const { slackCredentialsFromUid, slackCredentialsFromSpaceName } = require('./slackCredentials');
const { createdTalkBlocks } = require('./slackBlocks');

const postMessage = async ({
  accessToken, channelId, createdByName, type, spaceName, talkId, title,
}) => {
  const postBody = {
    channel: channelId,
    blocks: createdTalkBlocks({
      createdByName, type, spaceName, talkId, title,
    }),
  };
  try {
    await axios.post(
      'https://slack.com/api/chat.postMessage',
      postBody,
      {
        headers: { Authorization: `Bearer ${accessToken}` },
        'content-type': 'application/json',
      },
    );
  } catch (err) {
    functions.logger.error('Error posting message in postMessage', {
      err, postBody, accessToken,
    });
  }
};

exports.slackNotOnTalkCreate = functions.firestore.document('spaces/{spaceName}/talks/{talkId}').onCreate(async (snapshot, context) => {
  const { spaceName, talkId } = context.params;

  functions.logger.info('slackNotOnTalkCreate fired', { spaceName, talkId });

  const {
    accessToken: spaceToken, channelId: defaultChannelId,
  } = await slackCredentialsFromSpaceName({ spaceName });
  if (!spaceToken) {
    functions.logger.info('Space is not connected to Slack', { spaceName });
    return;
  }

  const {
    createdByName,
    createdById,
    title,
    isDoc,
    createdInSlackChannelId = null,
  } = snapshot.data();

  const { accessToken: userToken } = await slackCredentialsFromUid({ uid: createdById });
  if (!userToken) {
    functions.logger.info('Talk creator is not connected to Slack', { createdById });
    return;
  }

  const notChannelIds = [defaultChannelId];
  if (createdInSlackChannelId && createdInSlackChannelId !== defaultChannelId) {
    notChannelIds.push(createdInSlackChannelId);
  }

  for (let i = 0; i < notChannelIds.length; i += 1) {
    // eslint-disable-next-line no-await-in-loop
    await postMessage({
      accessToken: spaceToken,
      channelId: notChannelIds[i],
      createdByName,
      type: isDoc ? 'doc' : 'talk',
      spaceName,
      talkId,
      title,
    });
  }
});
