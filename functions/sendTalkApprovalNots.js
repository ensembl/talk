const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.sendTalkApprovalNots = functions.firestore.document('spaces/{spaceName}/talks/{talkId}')
  .onUpdate(async (change, context) => {
    const { spaceName, talkId } = context.params;
    functions.logger.info('sendTalkApprovalNots fired: ', {
      spaceName, talkId,
    });

    const { approverIds: beforeApproverIds = [] } = change.before.data();
    const {
      approverIds = [],
      subscribedUids,
      title,
      isDoc = false,
    } = change.after.data();

    const addedUids = approverIds.filter((id) => !beforeApproverIds.includes(id));
    const removedUids = beforeApproverIds.filter((id) => !approverIds.includes(id));

    if (addedUids.length === 0 && removedUids.length === 0) {
      functions.logger.info('No change in approvals');
      return;
    }

    let uid;
    let action;
    if (addedUids.length === 1) {
      [uid] = addedUids;
      action = 'approved';
    } else {
      [uid] = removedUids;
      action = 'removed their approval for';
    }

    let userDoc;
    try {
      userDoc = await db.collection('users').doc(uid).get();
    } catch (error) {
      functions.logger.error('Error getting user: ', {
        error, spaceName, talkId,
      });
    }
    const { name } = userDoc.data();

    const filteredSubscribedUids = subscribedUids.filter((userId) => userId !== uid);
    if (filteredSubscribedUids.length > 0) {
      try {
        await db.collection('mail').add({
          createdAt: new Date(),
          bccUids: filteredSubscribedUids,
          from: `${name} - Ensembl Talk <ensembl@mg.ensembl.so>`,
          template: {
            name: 'talkApprovalNotification',
            data: {
              spaceName,
              title,
              talkId,
              type: isDoc ? 'doc' : 'talk',
              name,
              action,
            },
          },
        });
      } catch (error) {
        functions.logger.error('Error adding mail for talkApprovalNotification: ', {
          error, spaceName, talkId,
        });
      }
    }
  });
