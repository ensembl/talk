const functions = require('firebase-functions');
const axios = require('axios');
const { admin } = require('./admin');

const db = admin.firestore();

const toBranch = (title) => title.replace(/\s+/g, '-').toLowerCase();

const createBranchFromMaster = async (headerOptions, branchName, repoInfo) => {
  const endPoint = `https://gitlab.com/api/v4/projects/${repoInfo.number}/repository/branches?branch=${toBranch(branchName)}&ref=${repoInfo.defaultBranch}`;
  const response = await axios.post(
    endPoint, {}, headerOptions,
  );
  return response.data.web_url;
};

const createMergeRequestFromBranch = async (
  headerOptions, branchName, title, talkId, repoInfo,
) => {
  const endPoint = `https://gitlab.com/api/v4/projects/${repoInfo.number}/merge_requests?source_branch=${toBranch(branchName)}&target_branch=${repoInfo.defaultBranch}&title=Draft: ${title}&description=https://app.ensembl.so/-/Ensembl/talks/${talkId}`;
  const response = await axios.post(
    endPoint, {}, headerOptions,
  );
  return response.data.web_url;
};

const updateDb = async (mergeRequestUrl, talkId, uid, spaceName) => {
  const userDoc = await db.collection('users').doc(uid).get();
  const {
    name,
    avatarUrl,
  } = userDoc.data();
  try {
    await db.collection(`spaces/${spaceName}/talks/`).doc(talkId).update({
      mergeRequestUrls: admin.firestore.FieldValue.arrayUnion(mergeRequestUrl),
      mergeRequestCreatedById: uid,
      mergeRequestCreatedByName: name,
      mergeRequestCreatedByAvatarUrl: avatarUrl,
      mergeRequestCreatedAt: new Date(),
    });
  } catch (error) {
    functions.logger.error('Error updating merge request urls', error);
  }
};

exports.createMergeRequest = functions.https.onCall(async (
  {
    branchName, issueTitle, talkId, spaceName, repoName,
  },
  context,
) => {
  const { uid } = context.auth;
  let accessToken;

  if (uid === 'XPVqAu7gMDZSDzPSQxPpiU74s8b2') {
    accessToken = functions.config().gitlab.ed_accesstoken;
  } else if (uid === 'kvdclBTDaiXibufaQMjV6MvRebJ3') {
    accessToken = functions.config().gitlab.victor_accesstoken;
  } else {
    functions.logger.error('Unathorized user trying to create merge request', uid);
    throw new functions.https.HttpsError('failed-precondition', `Unathorized user trying to create merge request ${uid}`);
  }
  const headerOptions = {
    headers: { 'PRIVATE-TOKEN': accessToken },
  };

  // 23752226 = Ensembl Talk, 27790422 = Dev 1on1
  const repoInfo = {
    number: repoName === 'talk' ? '23752226' : '27790422',
    defaultBranch: repoName === 'talk' ? 'master' : 'main',
  };

  const branchUrl = await createBranchFromMaster(headerOptions, branchName, repoInfo);
  const mergeRequestUrl = await createMergeRequestFromBranch(
    headerOptions, branchName, issueTitle, talkId, repoInfo,
  );

  await updateDb(mergeRequestUrl, talkId, uid, spaceName);
  return { branchUrl, mergeRequestUrl };
});
