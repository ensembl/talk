const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

const mentionTypeUids = (mentions, type) => Object.entries(mentions)
  .filter(([, map]) => map.type === type).map(([uid]) => uid);

exports.sendCmtAddedNots = functions.firestore.document('spaces/{spaceName}/talks/{talkId}/comments/{commentId}')
  .onCreate(async (snapshot, context) => {
    const { spaceName, talkId, commentId } = context.params;
    functions.logger.info('sendCmtAddedNots fired: ',
      { spaceName, talkId, commentId });

    const {
      htmlBody, createdById, createdByName, mentions, threadId,
    } = snapshot.data();

    if (createdByName === 'Ensembl Bot') return;

    const requestAnswerUids = mentionTypeUids(mentions, 'answer');
    const requestAckUids = mentionTypeUids(mentions, 'ack');
    const notifyUids = mentionTypeUids(mentions, 'notify');

    let talkDoc;
    try {
      talkDoc = await db.collection(`spaces/${spaceName}/talks`).doc(talkId).get();
    } catch (error) {
      functions.logger.error('Error getting talk: ', {
        error, spaceName, talkId, commentId,
      });
      return;
    }
    const { title, subscribedUids, isDoc } = talkDoc.data();

    const filteredSubscribedUids = subscribedUids
      .filter((uid) => !requestAnswerUids.includes(uid))
      .filter((uid) => !requestAckUids.includes(uid))
      .filter((uid) => !notifyUids.includes(uid))
      .filter((uid) => uid !== createdById);

    const type = isDoc ? 'doc' : 'talk';
    const hash = isDoc ? `thread_${threadId}` : `comment_${commentId}`;

    if (notifyUids.length > 0) {
      try {
        await db.collection('mail').add({
          createdAt: new Date(),
          bccUids: notifyUids,
          from: `${createdByName} - Ensembl Talk <ensembl@mg.ensembl.so>`,
          template: {
            name: 'commentAddedNotification',
            data: {
              spaceName,
              title,
              createdByName,
              htmlBody,
              talkId,
              senderAction: 'notifies you with',
              receiverAction: 'View and respond',
              hash,
              type,
            },
          },
        });
      } catch (error) {
        functions.logger.error('Error adding mail for notifyUids: ', {
          error, spaceName, talkId, hash,
        });
      }
    }

    if (requestAnswerUids.length > 0) {
      try {
        await db.collection('mail').add({
          createdAt: new Date(),
          bccUids: requestAnswerUids,
          from: `${createdByName} - Ensembl Talk <ensembl@mg.ensembl.so>`,
          template: {
            name: 'commentAddedNotification',
            data: {
              spaceName,
              title,
              createdByName,
              htmlBody,
              talkId,
              hash,
              type,
              senderAction: 'requests your <b>answer</b> on',
              receiverAction: 'View and <b>answer</b>',
            },
          },
        });
      } catch (error) {
        functions.logger.error('Error adding mail for requestAckUids: ', {
          error, spaceName, talkId, hash,
        });
      }
    }

    if (requestAckUids.length > 0) {
      try {
        await db.collection('mail').add({
          createdAt: new Date(),
          bccUids: requestAckUids,
          from: `${createdByName} - Ensembl Talk <ensembl@mg.ensembl.so>`,
          template: {
            name: 'commentAddedNotification',
            data: {
              spaceName,
              title,
              createdByName,
              htmlBody,
              talkId,
              hash,
              type,
              senderAction: 'requests your <b>ACK</b> (acknowledgment) on',
              receiverAction: 'View and click <b>ACK</b>',
            },
          },
        });
      } catch (error) {
        functions.logger.error('Error adding mail for requestAckUids: ', {
          error, spaceName, talkId, hash,
        });
      }
    }

    if (filteredSubscribedUids.length > 0) {
      try {
        await db.collection('mail').add({
          createdAt: new Date(),
          bccUids: filteredSubscribedUids,
          from: `${createdByName} - Ensembl Talk <ensembl@mg.ensembl.so>`,
          template: {
            name: 'commentAddedNotification',
            data: {
              spaceName,
              title,
              createdByName,
              htmlBody,
              talkId,
              hash,
              type,
              senderAction: 'says in',
              receiverAction: 'View and respond',
            },
          },
        });
      } catch (error) {
        functions.logger.error('Error adding mail for filteredSubscribedUids: ', {
          error, spaceName, talkId, commentId,
        });
      }
    }
  });
