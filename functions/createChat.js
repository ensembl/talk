const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.createChat = functions.https.onCall(async (data, context) => {
  const { uid } = context.auth;
  const { spaceName } = data;

  functions.logger.info('createChat fired: ', {
    uid, spaceName,
  });

  try {
    const chatRef = db.collection(`spaces/${spaceName}/chats`).doc('0');
    await chatRef.set({
      createdAt: new Date(),
    });
  } catch (error) {
    functions.logger.error('Error setting chat');
  }
});
