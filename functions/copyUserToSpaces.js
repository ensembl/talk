const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

const upsertCopiedUser = async (spaceName, uid, user) => {
  const filteredUser = { ...user };
  if (spaceName === 'Ensembl Community') {
    filteredUser.email = '';
  }
  try {
    await db.collection(`spaces/${spaceName}/copied-users`).doc(uid)
      .set(filteredUser);
  } catch (error) {
    functions.logger.error('Error adding copied user',
      { error, uid, filteredUser });
  }
};

const removeCopiedUser = async (spaceName, uid) => {
  try {
    await db.collection(`spaces/${spaceName}/copied-users`).doc(uid)
      .delete();
  } catch (error) {
    functions.logger.error('Error deleting copied user',
      { error, uid });
  }
};

const getUserWithUid = async (uid) => {
  let userDoc;
  try {
    userDoc = await db.collection('users').doc(uid).get();
  } catch (error) {
    functions.logger.error('Error getting user',
      { error, uid });
  }
  const {
    name,
    email,
    avatarUrl,
  } = userDoc.data();

  // Return a user only if have name and profile image, since user may not have set it yet
  if (name && avatarUrl) {
    return {
      name,
      email,
      avatarUrl,
      copiedAt: new Date(),
    };
  }
  return null;
};

const syncUsersAll = async () => {
  let snapshot;
  try {
    snapshot = await db.collectionGroup('secured').get();
  } catch (error) {
    functions.logger.error('Error getting secured for syncUsersAll', { error });
  }
  for (let i = 0; i < snapshot.docs.length; i += 1) {
    const uid = snapshot.docs[i].ref.parent.parent.id;
    // eslint-disable-next-line no-await-in-loop
    const user = await getUserWithUid(uid);
    if (user) {
      const { allowable: spaceNames = [] } = snapshot.docs[i].data();
      for (let j = 0; j < spaceNames.length; j += 1) {
        const filteredUser = { ...user };
        if (spaceNames[j] === 'Ensembl Community') {
          filteredUser.email = '';
        }
        try {
          // eslint-disable-next-line no-await-in-loop
          await db.collection(`spaces/${spaceNames[j]}/copied-users`).doc(uid).set(filteredUser);
        } catch (error) {
          functions.logger.error('Error writing copied-users: ', { uid, filteredUser });
        }
      }
    }
  }
  try {
    snapshot = await db.collectionGroup('copied-users').get();
  } catch (error) {
    functions.logger.error('Error getting copied-users for syncUsersAll', { error });
  }
  const nowDate = new Date();
  for (let i = 0; i < snapshot.docs.length; i += 1) {
    const { copiedAt: copiedAtFirestore = null } = snapshot.docs[i].data();
    const copiedAt = copiedAtFirestore ? copiedAtFirestore.toDate() : new Date('2021-01-01');
    // If the user wasn't copied within the last hour, delete it
    if ((nowDate - copiedAt) > 3600000) {
      try {
        // eslint-disable-next-line no-await-in-loop
        await snapshot.docs[i].ref.delete();
      } catch (error) {
        functions.logger.error('Error deleting copied-user');
      }
    }
  }
};

exports.copyUserOnUpdate = functions.firestore.document('users/{uid}').onWrite(async (change, context) => {
  const { uid } = context.params;
  functions.logger.info('copyUserOnUpdate fired: ', { uid });

  const {
    name = '',
    email = '',
    avatarUrl = '',
    allowableSpaceNames = [],
  } = change.after.data();
  const {
    name: beforeName = '',
    email: beforeEmail = '',
    avatarUrl: beforeAvatarUrl = '',
  } = change.before.data();

  if (name === beforeName && email === beforeEmail && avatarUrl === beforeAvatarUrl) {
    return;
  }

  const user = await getUserWithUid(uid);
  if (user) {
    for (let i = 0; i < allowableSpaceNames.length; i += 1) {
      // eslint-disable-next-line no-await-in-loop
      await upsertCopiedUser(allowableSpaceNames[i], uid, user);
    }
  }
});

exports.copyUserOnSpaceNamesWrite = functions.firestore.document('users/{uid}/secured/spaceNames').onWrite(async (change, context) => {
  const { uid } = context.params;
  functions.logger.info('copyOnSpaceNamesWrite fired: ', { uid });

  const allowable = change.after.exists ? change.after.data().allowable : [];
  const beforeAllowable = change.before.exists ? change.before.data().allowable : [];

  const addedSpaceNames = allowable.filter((spaceName) => !beforeAllowable.includes(spaceName));
  const removedSpaceNames = beforeAllowable.filter((spaceName) => !allowable.includes(spaceName));

  if (addedSpaceNames.length > 0) {
    const user = await getUserWithUid(uid);
    if (user) {
      for (let i = 0; i < addedSpaceNames.length; i += 1) {
        // eslint-disable-next-line no-await-in-loop
        await upsertCopiedUser(addedSpaceNames[i], uid, user);
      }
    }
  }
  if (removedSpaceNames.length > 0) {
    for (let i = 0; i < removedSpaceNames.length; i += 1) {
      // eslint-disable-next-line no-await-in-loop
      await removeCopiedUser(removedSpaceNames[i], uid);
    }
  }
});

exports.syncCopiedUsersAllNightly = functions.pubsub.schedule('50 23 * * *').onRun(async () => {
  functions.logger.info('syncCopiedUsersAllNightly fired: ');
  await syncUsersAll();
});

// Run on command line for development
// node -e 'require("./functions/copyUserToSpaces").syncCopiedUsersAllDev()'
module.exports.syncCopiedUsersAllDev = async () => {
  await syncUsersAll();
};
