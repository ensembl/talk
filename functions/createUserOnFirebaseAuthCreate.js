const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.createUserOnFirebaseAuthCreate = functions.auth.user().onCreate(async (user) => {
  const {
    uid,
    email,
    displayName,
    photoURL,
  } = user;

  try {
    await db.collection('users').doc(uid).set({
      email,
      name: displayName || '',
      avatarUrl: photoURL || '',
      lastVisitedSpaceName: '',
      createdAt: new Date(),
    });
  } catch (error) {
    functions.logger.error('Error setting user: ', {
      error, email, displayName, photoURL,
    });
  }

  let snapshot;
  try {
    snapshot = await db.collectionGroup('invitees')
      .where('email', '==', email)
      .where('userLoggedIn', '==', false)
      .get();
  } catch (error) {
    functions.logger.error('Error getting invitees: ',
      { error, uid, email });
  }

  const securedSpaceNames = [];
  if (!snapshot.empty) {
    for (let i = 0; i < snapshot.docs.length; i += 1) {
      const inviteeRef = snapshot.docs[i].ref;
      securedSpaceNames.push(
        inviteeRef.parent.parent.id, // Space name is the document id of two parents up
      );
      try {
        // eslint-disable-next-line no-await-in-loop
        await inviteeRef.update({
          userLoggedIn: true,
          userLoggedInAt: new Date(),
        });
      } catch (error) {
        functions.logger.error('Error updating invitee user: ',
          { error });
      }
    }
  }

  try {
    await db.collection('users').doc(uid).collection('secured').doc('spaceNames')
      .set({
        allowable: securedSpaceNames,
      });
  } catch (error) {
    functions.logger.error('Error setting secured spaceNames: ', {
      error, uid, securedSpaceNames,
    });
  }

  // We rely on copySpaceNamesToUsers-copySpaceNamesOnWrite to update
  // allowableSpaceNames and a timestamp to the user doc
  // to let the frontend know that the user is ready
});
