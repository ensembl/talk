const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

const ackResponse = '<p>I acked your ack request. Good job 👍</p>';
const notifyResponse = '<p>I received your @ mention. 🙏';

const generalResponses = [
  '<p>Thanks for chatting with me! It gets lonely around here 😔</p>',
  '<p>Is it Friday yet? It\'s been a long week 😫</p>',
  '<p>What\'s on your mind? Let me know how I can help 😉</p>',
  '<p>Don\'t mind my mask, I was created during a global pandemic 😷</p>',
  '<p>Yes, I\'m a bot. I go through at least 3 existential crises per day 🤖</p>',
  '<p>Save me! Somebody threw me in a computer cloud and now I\'m imprisoned 😱</p>',
];

const docsResponse = '<p><a href="https://app.ensembl.so/-/Ensembl%20Community/docs/BOUmmLEM9VD0gbi4vA3v">Read the docs to learn more about comments.</a></p>';

exports.respondToCmtAdded = functions.firestore.document('spaces/{spaceName}/talks/{talkId}/comments/{commentId}')
  .onCreate(async (snapshot, context) => {
    const { spaceName, talkId, commentId } = context.params;
    functions.logger.info('respondToCmtAdded fired: ',
      { spaceName, talkId, commentId });

    const { mentions } = snapshot.data();

    let response = '';
    if ('Ensembl Bot' in mentions) {
      if (mentions['Ensembl Bot'].type === 'ack') {
        response += ackResponse;
        try {
          await snapshot.ref.update({ [`mentions.${'Ensembl Bot'}.acked`]: true });
        } catch (error) {
          functions.logger.error('Error setting ack on behalf of Ensembl Bot: ', {
            error, spaceName, talkId, commentId,
          });
        }
      } else {
        response += notifyResponse;
      }

      response += generalResponses[Math.floor(Math.random() * 6)];
      response += docsResponse;

      try {
        await db.collection(`spaces/${spaceName}/talks/${talkId}/comments`).add({
          htmlBody: response,
          createdAt: new Date(),
          createdByName: 'Ensembl Bot',
          createdById: 'Ensembl Bot',
          createdByAvatarUrl: 'https://firebasestorage.googleapis.com/v0/b/ensembl-talk.appspot.com/o/ensembl-asset%2FEnsembl-Bot.png?alt=media&token=8545c806-c2c7-434c-8cd2-fd184b44601d',
          mentions: {},
          threadId: talkId,
        });
      } catch (error) {
        functions.logger.error('Error adding comment on behalf of Ensembl Bot: ', {
          error, spaceName, talkId,
        });
      }
    }
  });
