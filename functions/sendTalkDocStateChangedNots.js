const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.sendTalkDocStateChangedNots = functions.firestore.document('spaces/{spaceName}/talks/{talkId}')
  .onUpdate(async (change, context) => {
    const { spaceName, talkId } = context.params;
    functions.logger.info('sendTalkDocStateChangedNots fired: ', {
      spaceName, talkId,
    });

    const { state: beforeState } = change.before.data();
    const {
      isDoc,
      state,
      subscribedUids,
      title,
      changedStateById,
      changedStateByName,
    } = change.after.data();

    if (state === beforeState) {
      functions.logger.info('No change to state: ', { state });
      return;
    }

    const filteredSubscribedUids = subscribedUids.filter((uid) => uid !== changedStateById);
    if (filteredSubscribedUids.length > 0) {
      try {
        await db.collection('mail').add({
          createdAt: new Date(),
          bccUids: filteredSubscribedUids,
          from: `${changedStateByName} - Ensembl Talk <ensembl@mg.ensembl.so>`,
          template: {
            name: isDoc ? 'docStateChangedNotification' : 'talkStateChangedNotification',
            data: {
              spaceName,
              title,
              talkId,
              changedStateByName,
              // Used only in talkStateChangedNotification
              state,
              // Used only in docStateChangedNotification
              docAction: state === 'Archived' ? 'archived' : 'unarchived',
            },
          },
        });
      } catch (error) {
        functions.logger.error('Error adding mail: ', {
          error, spaceName, talkId,
        });
      }
    }
  });
