const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.subscribeToTalkOnCmtMentioned = functions.firestore.document('spaces/{space}/talks/{talkId}/comments/{commentId}')
  .onCreate(async (snapshot, context) => {
    const { space, talkId, commentId } = context.params;
    functions.logger.info('subscribeToTalkOnCmtMentioned fired: ',
      { space, talkId, commentId });

    const mentions = snapshot.get('mentions');

    const mentionUids = Object.keys(mentions).filter(
      (uid) => mentions[uid].type === 'notify' || mentions[uid].type === 'answer' || mentions[uid].type === 'ack',
    );

    if (mentionUids.length > 0) {
      try {
        await db.collection(`spaces/${space}/talks`).doc(talkId).update({
          subscribedUids: admin.firestore.FieldValue.arrayUnion(...mentionUids),
        });
      } catch (error) {
        functions.logger.error('Error subscribing user to Talk', {
          error, space, talkId, commentId, mentionUids,
        });
      }
    }
  });
