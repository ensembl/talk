const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.subscribeToTalkOnCmtAck = functions.firestore.document('spaces/{space}/talks/{talkId}/comments/{commentId}')
  .onUpdate(async (change, context) => {
    const { space, talkId, commentId } = context.params;
    functions.logger.info('subscribeToTalkOnCmtAck fired: ',
      { space, talkId, commentId });

    const beforeMentions = change.before.get('mentions');
    const afterMentions = change.after.get('mentions');

    const ackMentionUids = Object.keys(beforeMentions).filter(
      (id) => !('acked' in beforeMentions[id]) && afterMentions[id].acked === true,
    );

    if (ackMentionUids.length > 0) {
      try {
        await db.collection(`spaces/${space}/talks`).doc(talkId).update({
          subscribedUids: admin.firestore.FieldValue.arrayUnion(...ackMentionUids),
        });
      } catch (error) {
        functions.logger.error('Error subscribing user to Talk', {
          error, space, talkId, commentId, ackMentionUids,
        });
      }
    }
  });
