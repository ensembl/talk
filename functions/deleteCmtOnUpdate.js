const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.deleteCmtOnUpdate = functions.firestore.document('spaces/{space}/talks/{talkId}/comments/{commentId}')
  .onUpdate(async (change, context) => {
    const { space, talkId, commentId } = context.params;
    functions.logger.info('deleteCmtOnUpdate fired: ', {
      space, talkId, commentId,
    });

    if (change.after.get('toDelete')) {
      try {
        await db.collection(`spaces/${space}/talks/${talkId}/comments/`).doc(commentId).delete();
      } catch (error) {
        functions.logger.error('Error deleting Comment: ', { error, talkId, commentId });
      }
    }
  });
