const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.sendCmtHeartedNot = functions.firestore.document('spaces/{spaceName}/talks/{talkId}/comments/{commentId}')
  .onUpdate(async (change, context) => {
    const { spaceName, talkId, commentId } = context.params;
    functions.logger.info('sendCmtHeartedNot fired: ',
      { spaceName, talkId, commentId });

    const { heartedUids: beforeHeartedUids = [] } = change.before.data();
    const {
      createdById,
      heartedUids = [],
    } = change.after.data();

    if (createdById === 'Ensembl Bot') {
      functions.logger.info('Hearted Ensembl Bot comment. Do nothing');
      return;
    }

    const addedUids = heartedUids.filter((uid) => !beforeHeartedUids.includes(uid));
    if (addedUids.length === 0) {
      functions.logger.info('No additions to heartedUids');
      return;
    }

    let talkDoc;
    try {
      talkDoc = await db.collection(`spaces/${spaceName}/talks`).doc(talkId).get();
    } catch (error) {
      functions.logger.error('Error getting talk: ', {
        error, spaceName, talkId,
      });
      return;
    }
    const { title } = talkDoc.data();

    const heartedUid = addedUids[0];
    let userDoc;
    try {
      userDoc = await db.collection(`spaces/${spaceName}/copied-users`).doc(heartedUid).get();
    } catch (error) {
      functions.logger.error('Error getting user: ', {
        error, spaceName, heartedUid,
      });
      return;
    }
    const { name: heartedName } = userDoc.data();

    try {
      await db.collection('mail').add({
        createdAt: new Date(),
        bccUids: [createdById],
        from: `${heartedName} - Ensembl Talk <ensembl@mg.ensembl.so>`,
        template: {
          name: 'commentHeartedNotification',
          data: {
            spaceName,
            title,
            talkId,
            commentId,
            name: heartedName,
          },
        },
      });
    } catch (error) {
      functions.logger.error('Error adding mail for commentHeartedNotification: ', {
        error, createdById, heartedName, spaceName, title, talkId, commentId,
      });
    }
  });
