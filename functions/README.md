# Cloud Functions

## Backup Firestore
backupFirestore.js outline the scheduler to backup the Firestore database.  

### Storage Information:
- Location: GCP Bucket (us-central1)
- Type: Nearline
- Name of Bucket: ensembl-talk-backup
- Collection being backup: 'users', 'talks', 'handlebar', 'mail'
- Frequency: every 24 hours

To look at the backup, navigate to https://console.cloud.google.com/ \
Click on `Storage` > `ensembl-talk-backup`

### Backup Manually
With GUI:
  - Go to https://console.cloud.google.com/
  - Click on `Cloud Scheduler`
  - Click on `RUN NOW` for the backupFirestore job

### Restore from daily / hourly backup
With GUI:
 - Go to https://console.cloud.google.com/firestore/import-export
 - Click `IMPORT`
 - In the Filename field, enter the filename of an .overall_export_metadata file from a completed export operation. You can use the Browse button to help you select the file.
   - Click `Browse`
   - Click `ensembl-talk-backup`
   - Click the date and time you want to restore to
   - Click the file with the `.overall_export_metadata` file
   - Click `SELECT`
 - Click `Import`
   - **Latest comment preview will not be available after index update**

### Refresh custom claims 
Refresh custom claims for emulator:
 - `useEmulator` in config.js and admin.js set to `true`
 - In the functions directory, run `node -e 'require("./refreshCstmClaims").refreshCstmClaims()'` 

Refresh custom claims for production:
 - useEmulator` in admin.js set to `false`
 - Uncomment the following lines in `admin.js`.  This will use the local admin json token to initialize the app to connect to production.

```
// settings.credential = admin.credential.cert(serviceAccount);
```

 - In the functions directory, run `node -e 'require("./refreshCstmClaims").refreshCstmClaims()'` 

 **Notes:** Currently, even after completing the tasks and the functions resolved, firebase doesn't terminate the app gracefully when updating the production custom claims because the app runs as a background task.  You can use `ctrl + c` to terminate the app.  Looking into a solution to resolve this issue.  


