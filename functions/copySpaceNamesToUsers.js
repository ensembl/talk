const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

const updateAllowableSpaceNames = async (uid, allowableSpaceNames) => {
  try {
    await db.collection('users').doc(uid).update({
      allowableSpaceNames,
      allowableSpaceNamesUpdatedAt: new Date(),
    });
  } catch (error) {
    functions.logger.error('Error updating allowableSpaceNames: ', { error });
  }
};

const syncUserSpaceNamesAll = async () => {
  let snapshot;
  try {
    snapshot = await db.collectionGroup('secured').get();
  } catch (error) {
    functions.logger.error('Error getting spaceNames for syncUserSpaceNamesAll', { error });
  }
  for (let i = 0; i < snapshot.docs.length; i += 1) {
    const uid = snapshot.docs[i].ref.parent.parent.id;
    const { allowable: allowableSpaceNames = [] } = snapshot.docs[i].data();

    // eslint-disable-next-line no-await-in-loop
    await updateAllowableSpaceNames(uid, allowableSpaceNames);
  }
};

exports.copySpaceNamesOnWrite = functions.firestore.document('users/{uid}/secured/spaceNames').onWrite(async (change, context) => {
  const { uid } = context.params;
  functions.logger.info('copySpaceNamesOnWrite fired: ', { uid });

  const allowableSpaceNames = change.after.exists ? change.after.data().allowable : [];

  await updateAllowableSpaceNames(uid, allowableSpaceNames);
});

exports.syncUserSpaceNamesAllNightly = functions.pubsub.schedule('50 23 * * *').onRun(async () => {
  functions.logger.info('syncUserSpaceNamesAllNightly fired: ');
  await syncUserSpaceNamesAll();
});

// Run on command line for development
// node -e 'require("./functions/copySpaceNamesToUsers").syncUserSpaceNamesAllDev()'
module.exports.syncUserSpaceNamesAllDev = async () => {
  await syncUserSpaceNamesAll();
};
