const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.cmtActivityOnCmtCreate = functions.firestore.document('spaces/{spaceName}/talks/{talkId}/comments/{commentId}')
  .onCreate(async (snapshot, context) => {
    const { spaceName, talkId, commentId } = context.params;
    functions.logger.info('cmtActivityOnCmtCreate fired: ',
      { spaceName, talkId, commentId });

    const {
      createdAt,
      createdByName,
      createdByAvatarUrl,
      htmlBody: htmlCommentBody,
      threadId,
    } = snapshot.data();

    let talkDoc;
    try {
      talkDoc = await db.collection(`spaces/${spaceName}/talks`).doc(talkId).get();
    } catch (error) {
      functions.logger.error('Error getting talk: ', {
        error, spaceName, talkId,
      });
    }

    const { title, isDoc } = talkDoc.data();

    const type = isDoc ? 'doc' : 'talk';
    const hash = isDoc ? `thread_${threadId}` : `comment_${commentId}`;
    const href = `"https://app.ensembl.so/-/${spaceName}/${type}s/${talkId}"`;
    const deepLinkHref = `"https://app.ensembl.so/-/${spaceName}/${type}s/${talkId}#${hash}"`;

    const data = {
      activityCreatedAt: createdAt,
      avatarUrl: createdByAvatarUrl,
      name: createdByName,
      htmlMessage: `<a href=${deepLinkHref}>Commented</a> on ${type} <a href=${href}>${title}</a>`,
      htmlCommentBody,
    };
    try {
      await db.collection(`spaces/${spaceName}/activities`).add(data);
    } catch (error) {
      functions.logger.error('Error adding activity: ', {
        error, spaceName,
      });
    }

    try {
      await db.collection('spaces').doc(spaceName).set(
        {
          lastActivityCreatedAt: createdAt,
        },
        {
          merge: true,
        },
      );
    } catch (error) {
      functions.logger.error('Error setting space: ', {
        error, spaceName,
      });
    }
  });
