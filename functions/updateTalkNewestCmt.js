const { FieldValue } = require('@google-cloud/firestore');
const functions = require('firebase-functions');

const { admin } = require('./admin');

const db = admin.firestore();

const updateTalkNewestCmt = async (space, talkId) => {
  const talkRef = db.collection(`spaces/${space}/talks`).doc(talkId);
  const cmtsSnapshot = await talkRef.collection('comments').orderBy('createdAt', 'desc').limit(1).get();

  if (cmtsSnapshot.empty) {
    try {
      await talkRef.update({
        newestCommentCreatedByAvatarUrl: FieldValue.delete(),
        newestCommentCreatedByName: FieldValue.delete(),
        newestCommentHtmlBody: FieldValue.delete(),
        newestCommentId: FieldValue.delete(),
      });
    } catch (error) {
      functions.logger.error('Error deleting newest comment copy from talk: ', {
        error, space, talkId,
      });
    }
  } else {
    const {
      createdById: newestCommentCreatedById,
      createdByAvatarUrl: newestCommentCreatedByAvatarUrl,
      createdByName: newestCommentCreatedByName,
      createdAt,
      htmlBody: newestCommentHtmlBody,
    } = cmtsSnapshot.docs[0].data();
    const newestCommentId = cmtsSnapshot.docs[0].id;
    try {
      await talkRef.update({
        newestCommentCreatedById,
        newestCommentCreatedByAvatarUrl,
        newestCommentCreatedByName,
        newestCommentHtmlBody,
        newestCommentId,
        writtenAt: createdAt,
      });
    } catch (error) {
      functions.logger.error('Error updating talk: ', {
        error, space, talkId, newestCommentId,
      });
    }
  }
};

exports.updateTalkNewestCmtOnCmtWrite = functions.firestore
  .document('spaces/{space}/talks/{talkId}/comments/{commentId}')
  .onWrite(async (change, context) => {
    const { space, talkId, commentId } = context.params;
    functions.logger.info('updateTalkNewestCmtOnCmtWrite fired: ', {
      space, talkId, commentId,
    });
    await updateTalkNewestCmt(space, talkId);
  });

exports.updateTalkNewestCmtNightly = functions.pubsub.schedule('55 22 * * *').onRun(async () => {
  functions.logger.info('updateTalkNewestCmtNightly fired');

  const talkDocs = await db.collectionGroup('talks').get();
  talkDocs.forEach(async (talkDoc) => {
    await updateTalkNewestCmt(talkDoc.ref.parent.parent.id, talkDoc.id);
  });
});
