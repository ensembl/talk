const functions = require('firebase-functions');

const { admin } = require('./admin');

const db = admin.firestore();

const slackCredentialsFromSpaceName = async ({ spaceName }) => {
  let slackDoc;
  try {
    slackDoc = await db.collection(`spaces/${spaceName}/integrations`).doc('slack').get();
  } catch (error) {
    functions.logger.error('Error getting slack doc in slackCredentialsFromSpaceName', { error, spaceName });
  }
  if (slackDoc.exists) {
    const { accessToken, incomingWebhook } = slackDoc.data();
    const { channel_id: channelId } = incomingWebhook;
    return { accessToken, channelId };
  }
  return {};
};

const slackCredentialsFromUid = async ({ uid }) => {
  let slackDoc;
  try {
    slackDoc = await db.collection(`users/${uid}/integrations`).doc('slack').get();
  } catch (error) {
    functions.logger.error('Error getting slack doc in slackCredentialsFromUid', { error, uid });
  }
  if (slackDoc.exists) {
    const { accessToken } = slackDoc.data();
    return { accessToken };
  }
  return {};
};

const slackCredentialsFromSlackTeamId = async ({ slackTeamId }) => {
  let slackSnapshots;
  try {
    slackSnapshots = await db.collectionGroup('integrations').where('team.id', '==', slackTeamId).get();
  } catch (error) {
    functions.logger.error('Error getting slack snapshots in slackCredentialsFromSlackTeamId', { error, slackTeamId });
  }
  if (!slackSnapshots.empty) {
    const { docs } = slackSnapshots;
    const [doc] = docs;
    const { accessToken } = doc.data();
    const spaceName = doc.ref.parent.parent.id;
    return { accessToken, spaceName };
  }
  return {};
};

const slackCredentialsFromSlackUserId = async ({ slackUserId }) => {
  let slackSnapshots;
  try {
    slackSnapshots = await db.collectionGroup('integrations').where('slackUserId', '==', slackUserId).get();
  } catch (error) {
    functions.logger.error('Error getting slack snapshots in slackCredentialsFromSlackUserId', { error, slackUserId });
  }
  if (!slackSnapshots.empty) {
    const { docs } = slackSnapshots;
    const [doc] = docs;
    const { accessToken } = doc.data();
    return {
      accessToken,
      uid: doc.ref.parent.parent.id,
    };
  }
  return {};
};

module.exports = {
  slackCredentialsFromSpaceName,
  slackCredentialsFromUid,
  slackCredentialsFromSlackTeamId,
  slackCredentialsFromSlackUserId,
};
