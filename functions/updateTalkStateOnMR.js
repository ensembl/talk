const functions = require('firebase-functions');

exports.updateTalkStateOnMR = functions.firestore.document('spaces/{spaceName}/talks/{talkId}')
  .onUpdate(async (change, context) => {
    const { spaceName, talkId } = context.params;
    functions.logger.info('updateTalkStateOnMR fired: ', {
      spaceName, talkId,
    });
    const beforeTalk = change.before.data();
    const {
      mergeRequestCreatedById: beforeMergeRequestCreatedById,
    } = beforeTalk;

    const talk = change.after.data();
    const {
      mergeRequestCreatedById,
      mergeRequestCreatedByName,
      mergeRequestCreatedByAvatarUrl,
    } = talk;

    if (beforeMergeRequestCreatedById === mergeRequestCreatedById) return;

    await change.after.ref.update({
      state: 'In progress',
      changedStateById: mergeRequestCreatedById,
      changedStateByName: mergeRequestCreatedByName,
      changedStateByAvatarUrl: mergeRequestCreatedByAvatarUrl,
      changedStateAt: new Date(),
    });
  });
