# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.4.348](https://gitlab.com/ensembl/talk/compare/v0.4.347...v0.4.348) (2021-07-19)


### Bug Fixes

* **Conversion:** Converting from talk to doc result in Open state ([d00ed0d](https://gitlab.com/ensembl/talk/commit/d00ed0d517d0320698a345529e63d0d2be273d5e))

### [0.4.347](https://gitlab.com/ensembl/talk/compare/v0.4.346...v0.4.347) (2021-07-07)


### Features

* **Slack unfurl:** Show spaceName ([e4e2790](https://gitlab.com/ensembl/talk/commit/e4e27904769435597930d606d0a62f269df436ca))

### [0.4.346](https://gitlab.com/ensembl/talk/compare/v0.4.345...v0.4.346) (2021-07-07)

### [0.4.345](https://gitlab.com/ensembl/talk/compare/v0.4.344...v0.4.345) (2021-07-07)


### Features

* **Slack integration:** Remove update integrated trigger and use batch ([eb5ecca](https://gitlab.com/ensembl/talk/commit/eb5ecca5561141464a45e15bd7dcbe77cba32f25))
* **Slack unfurl:** Show talk doc state in unfurl ([d3c0541](https://gitlab.com/ensembl/talk/commit/d3c0541568693ed6c1468fee334a9b780987aa68))

### [0.4.344](https://gitlab.com/ensembl/talk/compare/v0.4.343...v0.4.344) (2021-07-07)


### Features

* **Slack loading page:** Resytyle ([39eb83c](https://gitlab.com/ensembl/talk/commit/39eb83cb26556882e06dc3c5219479ce778b0880))
* **Slack loading page:** Show spinner all the time ([84edfc3](https://gitlab.com/ensembl/talk/commit/84edfc3ff9588eb4303106936132fddce939c691))

### [0.4.343](https://gitlab.com/ensembl/talk/compare/v0.4.342...v0.4.343) (2021-07-07)


### Features

* **Slack integration:** Show a banner indicating connection status ([a6152c0](https://gitlab.com/ensembl/talk/commit/a6152c0ac3bbf7194193e7083c42187ae116c2df))

### [0.4.342](https://gitlab.com/ensembl/talk/compare/v0.4.341...v0.4.342) (2021-07-07)


### Features

* **Slack integration:** Tweak instructions ([ea4aed7](https://gitlab.com/ensembl/talk/commit/ea4aed72ebdc8dd8348dea87ea9f35429b63de5a))

### [0.4.341](https://gitlab.com/ensembl/talk/compare/v0.4.340...v0.4.341) (2021-07-07)


### Features

* **Slack:** Restyle Slack integration page ([7b1b9e5](https://gitlab.com/ensembl/talk/commit/7b1b9e515a8e5dbdfeb39ae43391592a88eb48a0))

### [0.4.340](https://gitlab.com/ensembl/talk/compare/v0.4.339...v0.4.340) (2021-07-07)


### Features

* **Slack:** Slack notifications uses space token ([cd570ff](https://gitlab.com/ensembl/talk/commit/cd570ff793d6810d5bdaa8c73bfe3c8dfe6bec5f))

### [0.4.339](https://gitlab.com/ensembl/talk/compare/v0.4.338...v0.4.339) (2021-07-07)


### Features

* **Slack:** Slack notification uses user token ([c0107ab](https://gitlab.com/ensembl/talk/commit/c0107ab71e167b5c848ec4db11fab95c732b224f))

### [0.4.338](https://gitlab.com/ensembl/talk/compare/v0.4.337...v0.4.338) (2021-07-07)


### Bug Fixes

* **Slack:** Fix for loop in slack notif. on create talk ([6bdba92](https://gitlab.com/ensembl/talk/commit/6bdba92f1b82ba73787c51a60bf00865f575b3a7))
* **Slack:** Fix reading user access token ([50597be](https://gitlab.com/ensembl/talk/commit/50597be989e5ff5a81b04274a42910fa4e603bb1))

### [0.4.337](https://gitlab.com/ensembl/talk/compare/v0.4.335...v0.4.337) (2021-07-06)

### [0.4.336](https://gitlab.com/ensembl/talk/compare/v0.4.335...v0.4.336) (2021-07-06)

### [0.4.335](https://gitlab.com/ensembl/talk/compare/v0.4.334...v0.4.335) (2021-07-06)


### Features

* **slack:** Remove channels:history scope ([ab3599e](https://gitlab.com/ensembl/talk/commit/ab3599ed22b4ee23dcc79f012b254c7014d39d7e))

### [0.4.334](https://gitlab.com/ensembl/talk/compare/v0.4.333...v0.4.334) (2021-07-06)

### [0.4.333](https://gitlab.com/ensembl/talk/compare/v0.4.332...v0.4.333) (2021-07-06)

### [0.4.332](https://gitlab.com/ensembl/talk/compare/v0.4.331...v0.4.332) (2021-07-06)


### Features

* **slack:** For unfurl code, use same auth connect blocks as create talk flow ([28cbee3](https://gitlab.com/ensembl/talk/commit/28cbee3a91d1b8ef9980a21e40224fae886b66e2))

### [0.4.331](https://gitlab.com/ensembl/talk/compare/v0.4.330...v0.4.331) (2021-07-06)


### Features

* **Redirect:** /settings redirects to settings page of last space ([7467016](https://gitlab.com/ensembl/talk/commit/74670161ac4b17b9e3092a4d7477757f7cd8c234))

### [0.4.330](https://gitlab.com/ensembl/talk/compare/v0.4.329...v0.4.330) (2021-07-06)


### Features

* **Slack:** Create talk from message using user token ([01ed4c1](https://gitlab.com/ensembl/talk/commit/01ed4c182bf915463fe358abdb727708cd43c2c1))

### [0.4.329](https://gitlab.com/ensembl/talk/compare/v0.4.328...v0.4.329) (2021-07-02)

### [0.4.328](https://gitlab.com/ensembl/talk/compare/v0.4.327...v0.4.328) (2021-07-02)


### Features

* **slack:** New talk notification appears in both channels ([0d362fb](https://gitlab.com/ensembl/talk/commit/0d362fb74a5163b9c80a12d3ee92b7177a049953))

### [0.4.327](https://gitlab.com/ensembl/talk/compare/v0.4.325...v0.4.327) (2021-07-02)


### Features

* **slack:** Refactor new talk / doc notification ([e37e639](https://gitlab.com/ensembl/talk/commit/e37e6395cfbe3e9031a1fe2daf8b3d49d4dbbe77))

### [0.4.326](https://gitlab.com/ensembl/talk/compare/v0.4.325...v0.4.326) (2021-07-02)

### [0.4.325](https://gitlab.com/ensembl/talk/compare/v0.4.324...v0.4.325) (2021-07-02)

### [0.4.324](https://gitlab.com/ensembl/talk/compare/v0.4.323...v0.4.324) (2021-07-02)

### [0.4.323](https://gitlab.com/ensembl/talk/compare/v0.4.322...v0.4.323) (2021-07-02)

### [0.4.322](https://gitlab.com/ensembl/talk/compare/v0.4.321...v0.4.322) (2021-07-02)


### Bug Fixes

* **slack unfurl:** Fix getting spaceNames with spaces ([7fbee6f](https://gitlab.com/ensembl/talk/commit/7fbee6f23c6261b0c61dc87c824a6a7b1e178f72))

### [0.4.321](https://gitlab.com/ensembl/talk/compare/v0.4.319...v0.4.321) (2021-07-02)


### Features

* **slack unfurl:** Check user token when unfurling links in Slack ([e6d9d41](https://gitlab.com/ensembl/talk/commit/e6d9d41f480508407fe1a1d45e0b00feccdaf9a0))

### [0.4.320](https://gitlab.com/ensembl/talk/compare/v0.4.319...v0.4.320) (2021-07-02)

### [0.4.319](https://gitlab.com/ensembl/talk/compare/v0.4.318...v0.4.319) (2021-07-02)


### Features

* **Sidebar ppt files:** Raise limit to 7 files per talk ([3a23d79](https://gitlab.com/ensembl/talk/commit/3a23d79572e013d44bc644ba33462a440195fae7))

### [0.4.318](https://gitlab.com/ensembl/talk/compare/v0.4.317...v0.4.318) (2021-06-29)


### Features

* **Gitlab:** Create MR for 1on1 repo ([122c5ab](https://gitlab.com/ensembl/talk/commit/122c5abf8ce1bce081f36606292de97a825f2519))

### [0.4.317](https://gitlab.com/ensembl/talk/compare/v0.4.316...v0.4.317) (2021-06-29)


### Features

* **Gitlab:** Create a MR dropdown for different repo ([743ea93](https://gitlab.com/ensembl/talk/commit/743ea934549ca2b131f4a1ddc761fa540d75400c))

### [0.4.316](https://gitlab.com/ensembl/talk/compare/v0.4.315...v0.4.316) (2021-06-29)


### Features

* **Slack integration:** Slack integrate with user for all spaces ([81b97c7](https://gitlab.com/ensembl/talk/commit/81b97c7a291e1d2753b8f3ef7fd5fd806fb03c7c))

### [0.4.315](https://gitlab.com/ensembl/talk/compare/v0.4.314...v0.4.315) (2021-06-29)


### Bug Fixes

* **Slack:** Use space token if user is not integrated ([a420295](https://gitlab.com/ensembl/talk/commit/a420295a4e1154a47f55987af96c8e723e222330))

### [0.4.314](https://gitlab.com/ensembl/talk/compare/v0.4.313...v0.4.314) (2021-06-28)

### [0.4.313](https://gitlab.com/ensembl/talk/compare/v0.4.312...v0.4.313) (2021-06-28)


### Features

* **slackOAuth:** Logging and return bad code ([271d22f](https://gitlab.com/ensembl/talk/commit/271d22f6f8030d58ccc2b1295c488c981f20fee5))

### [0.4.312](https://gitlab.com/ensembl/talk/compare/v0.4.311...v0.4.312) (2021-06-25)

### [0.4.311](https://gitlab.com/ensembl/talk/compare/v0.4.310...v0.4.311) (2021-06-25)

### [0.4.310](https://gitlab.com/ensembl/talk/compare/v0.4.309...v0.4.310) (2021-06-25)


### Features

* **SlackConnect:** Commonize space and user ([261c3a8](https://gitlab.com/ensembl/talk/commit/261c3a8fa6033035fe9b2cf733e702e0661bad0d))

### [0.4.309](https://gitlab.com/ensembl/talk/compare/v0.4.308...v0.4.309) (2021-06-25)


### Features

* **slackOAuth:** Commonize space and user logic ([70bff4b](https://gitlab.com/ensembl/talk/commit/70bff4b500176c8a7adefe2350e4dc2f487141af))

### [0.4.308](https://gitlab.com/ensembl/talk/compare/v0.4.307...v0.4.308) (2021-06-25)


### Features

* **slack oauth:** Don't change isIntegratedWithSlack flag on connect ([805ffa3](https://gitlab.com/ensembl/talk/commit/805ffa3d7d7ecc7133ffb1c85ba8e7afef08c897))

### [0.4.307](https://gitlab.com/ensembl/talk/compare/v0.4.304...v0.4.307) (2021-06-25)


### Features

* **slackOAuth:** Cloud trigger to set space slack int status ([1f0af92](https://gitlab.com/ensembl/talk/commit/1f0af92ae50e150452570d80506270d73dff9a6c))


### Bug Fixes

* **Slack:** Handle user cancel ([62de9ce](https://gitlab.com/ensembl/talk/commit/62de9ce5c849215dc8282e59b66a4dc8fa84f009))

### [0.4.306](https://gitlab.com/ensembl/talk/compare/v0.4.304...v0.4.306) (2021-06-24)


### Bug Fixes

* **Slack:** Handle user cancel ([62de9ce](https://gitlab.com/ensembl/talk/commit/62de9ce5c849215dc8282e59b66a4dc8fa84f009))

### [0.4.305](https://gitlab.com/ensembl/talk/compare/v0.4.304...v0.4.305) (2021-06-24)

### [0.4.304](https://gitlab.com/ensembl/talk/compare/v0.4.303...v0.4.304) (2021-06-24)


### Features

* **Slack:** Update component to disconnect user ([bb13695](https://gitlab.com/ensembl/talk/commit/bb13695ed421c95fec95e97c97d41fdb6222f14e))

### [0.4.303](https://gitlab.com/ensembl/talk/compare/v0.4.302...v0.4.303) (2021-06-24)


### Features

* **Slack:** Update credential location ([45d9de2](https://gitlab.com/ensembl/talk/commit/45d9de24398dfab7e42dfd9478ab196b722ec1fe))
* **Slack unfurl:** Show buttons to access Ensembl talk and doc ([251bf9a](https://gitlab.com/ensembl/talk/commit/251bf9a246c5e264c30419c01ef11c557b923b4b))

### [0.4.302](https://gitlab.com/ensembl/talk/compare/v0.4.301...v0.4.302) (2021-06-24)


### Features

* **Slack:** Handle individual Slack integration ([b28c66c](https://gitlab.com/ensembl/talk/commit/b28c66cc1cbc76b0e5b4d8bd348950f6cb44af3b))


### Bug Fixes

* **slack unfurl:** Fix camelcase of timeago ([0597588](https://gitlab.com/ensembl/talk/commit/059758817cf8f13d8c690f26c2c244370de8623a))

### [0.4.301](https://gitlab.com/ensembl/talk/compare/v0.4.299...v0.4.301) (2021-06-24)


### Features

* **Slack unfurl:** createdAt time ago ([552825a](https://gitlab.com/ensembl/talk/commit/552825a049e42d16b5d63bc5d89b2f4177b2e919))

### [0.4.300](https://gitlab.com/ensembl/talk/compare/v0.4.299...v0.4.300) (2021-06-24)

### [0.4.299](https://gitlab.com/ensembl/talk/compare/v0.4.298...v0.4.299) (2021-06-24)


### Features

* **Slack unfurl:** Creator name and avatar ([f95a4ab](https://gitlab.com/ensembl/talk/commit/f95a4ab03f9948512576fc1c168f4a22e0c807b7))

### [0.4.298](https://gitlab.com/ensembl/talk/compare/v0.4.297...v0.4.298) (2021-06-23)


### Features

* **slackEvent:** Get slack user ([1364147](https://gitlab.com/ensembl/talk/commit/1364147b0afacfad8c7b6409504a82088d5264da))

### [0.4.297](https://gitlab.com/ensembl/talk/compare/v0.4.296...v0.4.297) (2021-06-23)


### Features

* **Slack:** Create individual Slack integration UI ([6f24dd0](https://gitlab.com/ensembl/talk/commit/6f24dd02402787e49667be6b2ab3e30bf0b20825))

### [0.4.296](https://gitlab.com/ensembl/talk/compare/v0.4.295...v0.4.296) (2021-06-23)

### [0.4.295](https://gitlab.com/ensembl/talk/compare/v0.4.294...v0.4.295) (2021-06-23)


### Features

* **Slack:** Unfurl code in own file ([8422970](https://gitlab.com/ensembl/talk/commit/8422970592eb8f83c701abaffcfcd27e498bd13a))

### [0.4.294](https://gitlab.com/ensembl/talk/compare/v0.4.293...v0.4.294) (2021-06-23)


### Features

* **Settings page:** Set document.title ([037b822](https://gitlab.com/ensembl/talk/commit/037b822c0f284a0ade760f2c96aa5e395386b57f))

### [0.4.293](https://gitlab.com/ensembl/talk/compare/v0.4.292...v0.4.293) (2021-06-23)


### Bug Fixes

* **Profile page cancel button:** Fix cancel button ([0bf3dc4](https://gitlab.com/ensembl/talk/commit/0bf3dc4fe9175caa37d14e0a8bcbcc072b71745e))

### [0.4.292](https://gitlab.com/ensembl/talk/compare/v0.4.291...v0.4.292) (2021-06-22)

### [0.4.291](https://gitlab.com/ensembl/talk/compare/v0.4.290...v0.4.291) (2021-06-22)


### Features

* **Slack:** Show integration comp if user is not integrated ([c73480d](https://gitlab.com/ensembl/talk/commit/c73480dc6736d4c05f0a8a126b1c007d72347aa6))

### [0.4.290](https://gitlab.com/ensembl/talk/compare/v0.4.289...v0.4.290) (2021-06-22)


### Features

* **Chat:** Available for all non-community spaces ([2e84471](https://gitlab.com/ensembl/talk/commit/2e84471ede65a498a6faea1b3803df49ccf77a71))

### [0.4.289](https://gitlab.com/ensembl/talk/compare/v0.4.288...v0.4.289) (2021-06-22)


### Bug Fixes

* **NotsSentMessage:** Fix bug not showing auto notified ([60a4833](https://gitlab.com/ensembl/talk/commit/60a4833764d09dba805a06bcbb3a808a901dfe71))

### [0.4.288](https://gitlab.com/ensembl/talk/compare/v0.4.287...v0.4.288) (2021-06-22)


### Features

* **slackUtils:** Use postMessage to send notification in same chnl ([3bff821](https://gitlab.com/ensembl/talk/commit/3bff8219186c22ff5a8d672bae5e66c6d1370b69))

### [0.4.287](https://gitlab.com/ensembl/talk/compare/v0.4.286...v0.4.287) (2021-06-22)

### [0.4.286](https://gitlab.com/ensembl/talk/compare/v0.4.285...v0.4.286) (2021-06-21)


### Features

* **Slack:** Convert markdown to plain text ([3004192](https://gitlab.com/ensembl/talk/commit/3004192383f25bb40858d4faea913cbdbf68509a))
* **SlackUtils:** Notify same Slack channel that talk was created in ([e15f019](https://gitlab.com/ensembl/talk/commit/e15f0198531e7b11da366153f04292cf8e6a247c))

### [0.4.285](https://gitlab.com/ensembl/talk/compare/v0.4.284...v0.4.285) (2021-06-21)


### Features

* **Slack:** Create clickable link for preview ([0bd6dc2](https://gitlab.com/ensembl/talk/commit/0bd6dc2ad7faa6f364d91d483d7e9cc2f9b95bd4))

### [0.4.284](https://gitlab.com/ensembl/talk/compare/v0.4.283...v0.4.284) (2021-06-21)


### Features

* **Slack:** Generate preview for comment ([0c8dec9](https://gitlab.com/ensembl/talk/commit/0c8dec9c1f94e6806914244ea6711349a2d8564f))

### [0.4.283](https://gitlab.com/ensembl/talk/compare/v0.4.282...v0.4.283) (2021-06-21)


### Features

* **Slack:** Deploy to event callback to production ([5ac77ae](https://gitlab.com/ensembl/talk/commit/5ac77ae4f0a38eb0b5e9950ba18506a06654ceb7))
* **Slack:** Generate a preview ([3191880](https://gitlab.com/ensembl/talk/commit/3191880795ca174d3e0c7fe2eea20a3e2f43efa7))

### [0.4.282](https://gitlab.com/ensembl/talk/compare/v0.4.280...v0.4.282) (2021-06-21)


### Features

* **Slack:** Post a placeholder message when link is posted ([5bbbe15](https://gitlab.com/ensembl/talk/commit/5bbbe1563da9f66f3f24e7f50c284e4da83842fd))
* **Slack Utils:** Scheduled ping to keep create msg func warm ([a02a06e](https://gitlab.com/ensembl/talk/commit/a02a06e2ace030019e37b0d4bb2e8ecc55aa9d8c))

### [0.4.281](https://gitlab.com/ensembl/talk/compare/v0.4.280...v0.4.281) (2021-06-21)

### [0.4.280](https://gitlab.com/ensembl/talk/compare/v0.4.279...v0.4.280) (2021-06-21)


### Features

* **Slack:** Update Slack app scope ([780ae51](https://gitlab.com/ensembl/talk/commit/780ae51113675bd04b33a368a53d36baa3b552c4))

### [0.4.279](https://gitlab.com/ensembl/talk/compare/v0.4.278...v0.4.279) (2021-06-21)


### Features

* **slackUtils:** More logging ([2490187](https://gitlab.com/ensembl/talk/commit/2490187c26ce62d00734628ecd72b79bb45407bd))

### [0.4.278](https://gitlab.com/ensembl/talk/compare/v0.4.277...v0.4.278) (2021-06-21)


### Features

* **slackUtils:** Send creating new talk confirmation immediately ([fa7a3ea](https://gitlab.com/ensembl/talk/commit/fa7a3eab53beffc8aacc6408af3842dc957fd68e))

### [0.4.277](https://gitlab.com/ensembl/talk/compare/v0.4.276...v0.4.277) (2021-06-21)


### Bug Fixes

* **Slack:** Fix message being edited when visit ([3aadee5](https://gitlab.com/ensembl/talk/commit/3aadee506bf702dcc3538faff3ba1efe85187b85))

### [0.4.276](https://gitlab.com/ensembl/talk/compare/v0.4.275...v0.4.276) (2021-06-21)


### Features

* **slackUtils:** Timestamps for create talk from Slack ([c48fb77](https://gitlab.com/ensembl/talk/commit/c48fb77339373c615e89e8c6b455aae4377d77ab))

### [0.4.275](https://gitlab.com/ensembl/talk/compare/v0.4.273...v0.4.275) (2021-06-21)


### Features

* **Slack integration:** After create msg from Slack let fun notify channel ([73d5780](https://gitlab.com/ensembl/talk/commit/73d5780e6cbd3be23ef9d4a60548a7f7a4f5caaa))

### [0.4.274](https://gitlab.com/ensembl/talk/compare/v0.4.273...v0.4.274) (2021-06-21)

### [0.4.273](https://gitlab.com/ensembl/talk/compare/v0.4.272...v0.4.273) (2021-06-18)


### Features

* **Slack notification for new talk/doc:** Send right away ([85c163e](https://gitlab.com/ensembl/talk/commit/85c163e8b1815360f27c510436be249941a39d60))

### [0.4.272](https://gitlab.com/ensembl/talk/compare/v0.4.271...v0.4.272) (2021-06-18)

### [0.4.271](https://gitlab.com/ensembl/talk/compare/v0.4.270...v0.4.271) (2021-06-18)


### Features

* **NewTalkPage:** Dedicated new talk page ([ed07310](https://gitlab.com/ensembl/talk/commit/ed07310a2dc65998863bdc2062ca4d6487f12137))

### [0.4.270](https://gitlab.com/ensembl/talk/compare/v0.4.269...v0.4.270) (2021-06-18)


### Features

* **Chat:** Chat header disappear after 3 comments ([c587adf](https://gitlab.com/ensembl/talk/commit/c587adf88e57ab4a764bdc4bf76af9dd0e419e11))

### [0.4.269](https://gitlab.com/ensembl/talk/compare/v0.4.268...v0.4.269) (2021-06-18)


### Features

* **Chat:** Delete comments when marked ([658a54d](https://gitlab.com/ensembl/talk/commit/658a54deb5d97e5d793bfc0d4e7187eae772367b))

### [0.4.268](https://gitlab.com/ensembl/talk/compare/v0.4.267...v0.4.268) (2021-06-18)


### Bug Fixes

* **Chat:** Shrink comments ([4c6a18e](https://gitlab.com/ensembl/talk/commit/4c6a18ebaac7592f28934cecf2bd47de0aa1f93e))

### [0.4.267](https://gitlab.com/ensembl/talk/compare/v0.4.266...v0.4.267) (2021-06-18)


### Bug Fixes

* **Chat:** Fix height for mobile ([b94ea44](https://gitlab.com/ensembl/talk/commit/b94ea44b578259fd96ca1c2bb79fd0ac02539035))

### [0.4.266](https://gitlab.com/ensembl/talk/compare/v0.4.265...v0.4.266) (2021-06-18)


### Features

* **Chat:** Hide help message on smaller screen ([c2a83bc](https://gitlab.com/ensembl/talk/commit/c2a83bc7c5bcf6e83d25a3ffbc8757144fb761e4))

### [0.4.265](https://gitlab.com/ensembl/talk/compare/v0.4.264...v0.4.265) (2021-06-18)


### Features

* **Chat:** Message should start near add comment ([bece050](https://gitlab.com/ensembl/talk/commit/bece0504dfb9e0ae2ebc1372f01c03b16238a570))

### [0.4.264](https://gitlab.com/ensembl/talk/compare/v0.4.263...v0.4.264) (2021-06-18)

### [0.4.263](https://gitlab.com/ensembl/talk/compare/v0.4.262...v0.4.263) (2021-06-18)

### [0.4.262](https://gitlab.com/ensembl/talk/compare/v0.4.261...v0.4.262) (2021-06-18)


### Features

* **Chat:** Scroll to hashed comment ([be68bc9](https://gitlab.com/ensembl/talk/commit/be68bc98082151639dfb4ea359e8b08a1aad50e3))

### [0.4.261](https://gitlab.com/ensembl/talk/compare/v0.4.260...v0.4.261) (2021-06-18)


### Features

* **Chat page:** Empty state message ([d8a198d](https://gitlab.com/ensembl/talk/commit/d8a198d0d6425bfb7ce031343387ac4dd1a9204b))

### [0.4.260](https://gitlab.com/ensembl/talk/compare/v0.4.258...v0.4.260) (2021-06-18)


### Features

* **Chat Add comment:** Make it shorter ([49b1e42](https://gitlab.com/ensembl/talk/commit/49b1e42647f7a45dd908c43d358f6db042862ba3))

### [0.4.259](https://gitlab.com/ensembl/talk/compare/v0.4.258...v0.4.259) (2021-06-18)

### [0.4.258](https://gitlab.com/ensembl/talk/compare/v0.4.257...v0.4.258) (2021-06-18)


### Features

* **Ack:** Send ack clicked notification ([1562497](https://gitlab.com/ensembl/talk/commit/1562497481a241fed4c2d4f720a672dbf4d415ee))

### [0.4.257](https://gitlab.com/ensembl/talk/compare/v0.4.256...v0.4.257) (2021-06-18)


### Bug Fixes

* **chat comment:** Fix correct email template for at mention not ([05b9b56](https://gitlab.com/ensembl/talk/commit/05b9b56a40d7be343f50d4a9fc30a93d228781d5))

### [0.4.256](https://gitlab.com/ensembl/talk/compare/v0.4.255...v0.4.256) (2021-06-18)


### Features

* **Chat comment:** Email notifications for at mention ([49eefbc](https://gitlab.com/ensembl/talk/commit/49eefbc54b4c14d2d27e030ea96ba574092af869))

### [0.4.255](https://gitlab.com/ensembl/talk/compare/v0.4.254...v0.4.255) (2021-06-18)


### Features

* **ChatComment Hearting:** Send notification ([d0ef926](https://gitlab.com/ensembl/talk/commit/d0ef92652fd7010f016f0ae3574277c2b43518b0))

### [0.4.254](https://gitlab.com/ensembl/talk/compare/v0.4.253...v0.4.254) (2021-06-17)


### Features

* **ChatPage:** Limit comments to newest 500 ([3f0e114](https://gitlab.com/ensembl/talk/commit/3f0e1140d836bf0c8df3c9cbb5133adb06e6a98a))

### [0.4.253](https://gitlab.com/ensembl/talk/compare/v0.4.252...v0.4.253) (2021-06-16)


### Features

* **Chat:** Scroll to bottom of comments ([44c49c9](https://gitlab.com/ensembl/talk/commit/44c49c93b753c0d32e56a362e7f6a9fcebc7c227))

### [0.4.252](https://gitlab.com/ensembl/talk/compare/v0.4.251...v0.4.252) (2021-06-16)


### Features

* **Chat:** Anchor add comment box to the bottom of page ([4261047](https://gitlab.com/ensembl/talk/commit/42610475ddb1e0834904ef5eb476968461ceb066))

### [0.4.251](https://gitlab.com/ensembl/talk/compare/v0.4.250...v0.4.251) (2021-06-16)


### Features

* **Chat:** Initialize chat doc when space created ([544bd2b](https://gitlab.com/ensembl/talk/commit/544bd2be9ba240fbcba05ef1c752cdcf8e93d238))

### [0.4.250](https://gitlab.com/ensembl/talk/compare/v0.4.249...v0.4.250) (2021-06-15)


### Features

* **LeftNav:** Add divider below space dropdown selector ([c7401c5](https://gitlab.com/ensembl/talk/commit/c7401c5aac89d09fabff8922bf137659b59d0e37))
* **LeftNav:** Make font color same as right sidebar ([99c8d71](https://gitlab.com/ensembl/talk/commit/99c8d71b6766fe1870df720d9b6fedb73584c52f))

### [0.4.249](https://gitlab.com/ensembl/talk/compare/v0.4.248...v0.4.249) (2021-06-15)


### Features

* **Chat Page:** Comments ([6e4a5e8](https://gitlab.com/ensembl/talk/commit/6e4a5e8b526202b9092c364957c43687d0ad51fa))

### [0.4.248](https://gitlab.com/ensembl/talk/compare/v0.4.247...v0.4.248) (2021-06-15)

### [0.4.247](https://gitlab.com/ensembl/talk/compare/v0.4.246...v0.4.247) (2021-06-15)


### Features

* **Chat Page:** Show raw comments ([6d20efe](https://gitlab.com/ensembl/talk/commit/6d20efefb1536147219d46a353f31c9a21749f5a))

### [0.4.246](https://gitlab.com/ensembl/talk/compare/v0.4.245...v0.4.246) (2021-06-15)

### [0.4.245](https://gitlab.com/ensembl/talk/compare/v0.4.244...v0.4.245) (2021-06-14)

### [0.4.244](https://gitlab.com/ensembl/talk/compare/v0.4.243...v0.4.244) (2021-06-14)

### [0.4.243](https://gitlab.com/ensembl/talk/compare/v0.4.242...v0.4.243) (2021-06-14)


### Features

* **Chat Page:** Add comment box ([218d862](https://gitlab.com/ensembl/talk/commit/218d862f270b4d0934431bd18e2ba2142dce0ac5))

### [0.4.242](https://gitlab.com/ensembl/talk/compare/v0.4.241...v0.4.242) (2021-06-14)

### [0.4.241](https://gitlab.com/ensembl/talk/compare/v0.4.240...v0.4.241) (2021-06-14)

### [0.4.240](https://gitlab.com/ensembl/talk/compare/v0.4.239...v0.4.240) (2021-06-11)


### Features

* **Talk Mention:** Update ordering using writtenAt ([9455069](https://gitlab.com/ensembl/talk/commit/9455069700ecd1bc7f6eb6c35299bd9189e80d57))

### [0.4.239](https://gitlab.com/ensembl/talk/compare/v0.4.238...v0.4.239) (2021-06-11)


### Features

* **Mention Talk:** Feed item to talk ([599c047](https://gitlab.com/ensembl/talk/commit/599c047fc185b37930a1fa41178b960cae40daf5))

### [0.4.238](https://gitlab.com/ensembl/talk/compare/v0.4.237...v0.4.238) (2021-06-09)


### Features

* **Slack integration:** Fix wording ([1794f04](https://gitlab.com/ensembl/talk/commit/1794f04bc50d72f1a831dd216bcf726b734e9293))

### [0.4.237](https://gitlab.com/ensembl/talk/compare/v0.4.236...v0.4.237) (2021-06-09)


### Features

* **Slack Integration:** Send notification in all Spaces ([41d35da](https://gitlab.com/ensembl/talk/commit/41d35dae5632814ed6aad832bec750ed50c15de6))

### [0.4.236](https://gitlab.com/ensembl/talk/compare/v0.4.235...v0.4.236) (2021-06-09)


### Features

* **Slack Integration:** Release Slack integration to all spaces ([25ef12e](https://gitlab.com/ensembl/talk/commit/25ef12e2e7f8c560256d7a5df2cd934ecc3892fc))

### [0.4.235](https://gitlab.com/ensembl/talk/compare/v0.4.234...v0.4.235) (2021-06-09)


### Bug Fixes

* **Slack Integration:** Fix single threaded body for comment ([85110f8](https://gitlab.com/ensembl/talk/commit/85110f8c4b0b5291100e2df896c1010f6ee666a3))

### [0.4.234](https://gitlab.com/ensembl/talk/compare/v0.4.232...v0.4.234) (2021-06-09)


### Features

* **NewTalkStrip:** Reduce size of avatars ([ea75cea](https://gitlab.com/ensembl/talk/commit/ea75cea20324f23e0fab430eee8612b97bcdd7e8))

### [0.4.233](https://gitlab.com/ensembl/talk/compare/v0.4.232...v0.4.233) (2021-06-09)

### [0.4.232](https://gitlab.com/ensembl/talk/compare/v0.4.231...v0.4.232) (2021-06-09)


### Features

* **LeftNav:** Shorter items ([cecb7eb](https://gitlab.com/ensembl/talk/commit/cecb7eb49f5ab31eb651b35def5f7f8f466e2978))

### [0.4.231](https://gitlab.com/ensembl/talk/compare/v0.4.230...v0.4.231) (2021-06-09)

### [0.4.230](https://gitlab.com/ensembl/talk/compare/v0.4.229...v0.4.230) (2021-06-09)


### Features

* **Slack integration:** Rename settings to Slack integration ([bad4bbd](https://gitlab.com/ensembl/talk/commit/bad4bbd80d1023a9c99c14567d870bcaa2ad5077))

### [0.4.229](https://gitlab.com/ensembl/talk/compare/v0.4.228...v0.4.229) (2021-06-08)


### Features

* **Slack Integration:** Disconnect Slack from Ensembl Talk ([a2a99c3](https://gitlab.com/ensembl/talk/commit/a2a99c3b7ab5d528835a1b0d21b0dbb43dd801e2))

### [0.4.228](https://gitlab.com/ensembl/talk/compare/v0.4.227...v0.4.228) (2021-06-08)


### Features

* **Link talk:** Allow # for beta space ([c5f3f50](https://gitlab.com/ensembl/talk/commit/c5f3f50a1b1869d9f66d752efc738985fff26103))

### [0.4.227](https://gitlab.com/ensembl/talk/compare/v0.4.226...v0.4.227) (2021-06-07)


### Features

* **Chat Page:** Chat Page for beta spaces only ([75fcc31](https://gitlab.com/ensembl/talk/commit/75fcc3110555b7f13b58f2dfc8d7024bf946a980))

### [0.4.226](https://gitlab.com/ensembl/talk/compare/v0.4.225...v0.4.226) (2021-06-05)


### Features

* **Slack Integration:** Distinquish talk / doc notification ([3579f05](https://gitlab.com/ensembl/talk/commit/3579f05585742b7038cb74935ffa1309e69dba42))

### [0.4.225](https://gitlab.com/ensembl/talk/compare/v0.4.224...v0.4.225) (2021-06-05)


### Bug Fixes

* **Firestore:** Fix bot rule syntax ([bc8acd2](https://gitlab.com/ensembl/talk/commit/bc8acd2a3b46d31bbf9e8dadd4f9581f22257a4a))

### [0.4.224](https://gitlab.com/ensembl/talk/compare/v0.4.223...v0.4.224) (2021-06-05)


### Features

* **Talk:** Allow deletion for everyone if created by bot ([37dc99c](https://gitlab.com/ensembl/talk/commit/37dc99c637b3a3dfc477493113489acd15b17cdb))

### [0.4.223](https://gitlab.com/ensembl/talk/compare/v0.4.222...v0.4.223) (2021-06-05)


### Bug Fixes

* **Slack Integration:** Fix message for error ([5557a3b](https://gitlab.com/ensembl/talk/commit/5557a3b0e9448b04ca24b8e4039bfaf117927300))

### [0.4.222](https://gitlab.com/ensembl/talk/compare/v0.4.221...v0.4.222) (2021-06-04)


### Features

* **Slack Integration:** Get Slack thread content for talk proposal ([1936e8d](https://gitlab.com/ensembl/talk/commit/1936e8d2f38279b6006c6a9e1d122846c72564e2))

### [0.4.221](https://gitlab.com/ensembl/talk/compare/v0.4.220...v0.4.221) (2021-06-04)


### Features

* **Slack Integration:** Add user channel history scope ([0acc945](https://gitlab.com/ensembl/talk/commit/0acc945c84cdad352059a12d1f9fe8d8926fcea7))

### [0.4.220](https://gitlab.com/ensembl/talk/compare/v0.4.219...v0.4.220) (2021-06-04)


### Features

* **Slack Integration:** Add channels history to Slack access scope ([e74d3c0](https://gitlab.com/ensembl/talk/commit/e74d3c0c0d01a37ee73695937a38316a68974fa6))

### [0.4.219](https://gitlab.com/ensembl/talk/compare/v0.4.218...v0.4.219) (2021-06-04)


### Features

* **Slack Integration:** Add error message for Slack integration ([d096233](https://gitlab.com/ensembl/talk/commit/d09623301374e03809269fd9354153bef2439753))

### [0.4.218](https://gitlab.com/ensembl/talk/compare/v0.4.217...v0.4.218) (2021-06-04)


### Features

* **Slack Integration:** Only add Slack notification status if integrated ([6dbafc9](https://gitlab.com/ensembl/talk/commit/6dbafc90fd4d9c6af0bc35737bc8416b5d6eb0d1))

### [0.4.217](https://gitlab.com/ensembl/talk/compare/v0.4.216...v0.4.217) (2021-06-04)


### Features

* **MentionAction:** Make request ack default ([a2038a5](https://gitlab.com/ensembl/talk/commit/a2038a50f439984438908182e80e3ce98e1f459a))
* **Settings:** Change settings to be part of space ([4dd413a](https://gitlab.com/ensembl/talk/commit/4dd413abd8a5a3ea0c657b385b9951ab1587b7ab))

### [0.4.216](https://gitlab.com/ensembl/talk/compare/v0.4.215...v0.4.216) (2021-06-04)


### Features

* **Slack Integration:** Add message to show space is already integrated ([cd4c161](https://gitlab.com/ensembl/talk/commit/cd4c1614cdca42cec5749ef541168e8700fa4875))

### [0.4.215](https://gitlab.com/ensembl/talk/compare/v0.4.214...v0.4.215) (2021-06-04)


### Features

* **Slack Integration:** Update function to read credential from space ([8b8ae8b](https://gitlab.com/ensembl/talk/commit/8b8ae8bf91f7b75117a231127b69b5d00439bf6b))

### [0.4.214](https://gitlab.com/ensembl/talk/compare/v0.4.213...v0.4.214) (2021-06-04)


### Bug Fixes

* **CommentThreads in doc:** Show first thread if not resolved ([f0932b2](https://gitlab.com/ensembl/talk/commit/f0932b2e760727ff64b3c2fd40523c4853da5086))

### [0.4.213](https://gitlab.com/ensembl/talk/compare/v0.4.212...v0.4.213) (2021-06-04)


### Features

* **Slack Integration:** Store slack credential under space ([c2da149](https://gitlab.com/ensembl/talk/commit/c2da14952ed32e29c74750621d0bcc2e889c1a8c))

### [0.4.212](https://gitlab.com/ensembl/talk/compare/v0.4.211...v0.4.212) (2021-06-04)


### Features

* **Heart button:** Do not show for doc comments ([8e55e81](https://gitlab.com/ensembl/talk/commit/8e55e816aade2d7bdf289920b7074fcd6ddb47ca))

### [0.4.211](https://gitlab.com/ensembl/talk/compare/v0.4.210...v0.4.211) (2021-06-04)

### [0.4.210](https://gitlab.com/ensembl/talk/compare/v0.4.209...v0.4.210) (2021-06-03)


### Features

* **Settings:** Use Pagelayout component for SettingsPage ([bcc936b](https://gitlab.com/ensembl/talk/commit/bcc936b54c4d788d586027cf5a5e0901cf655821))

### [0.4.209](https://gitlab.com/ensembl/talk/compare/v0.4.208...v0.4.209) (2021-06-03)


### Features

* **Slack Integration:** Reword pong message to be more specific ([10f8e7f](https://gitlab.com/ensembl/talk/commit/10f8e7f26fdc927e577db63639724f9c3d5db722))

### [0.4.208](https://gitlab.com/ensembl/talk/compare/v0.4.207...v0.4.208) (2021-06-03)

### [0.4.207](https://gitlab.com/ensembl/talk/compare/v0.4.206...v0.4.207) (2021-06-03)


### Features

* **SpaceName in nav:** Larger ([d33865b](https://gitlab.com/ensembl/talk/commit/d33865b90aa9ba3848f566f4c0f4ba78de8d11cb))

### [0.4.206](https://gitlab.com/ensembl/talk/compare/v0.4.205...v0.4.206) (2021-06-03)


### Features

* **Hearting:** For all spaces ([e8e5c39](https://gitlab.com/ensembl/talk/commit/e8e5c3968aa7d351e517b40dc68af953802954a4))

### [0.4.205](https://gitlab.com/ensembl/talk/compare/v0.4.204...v0.4.205) (2021-06-03)


### Features

* **Hearts:** Show them all the time ([276488d](https://gitlab.com/ensembl/talk/commit/276488d339528798453c0b4a15977d383f700470))

### [0.4.204](https://gitlab.com/ensembl/talk/compare/v0.4.203...v0.4.204) (2021-06-03)


### Features

* **Hearted:** Send notification to commentor ([6f5bbcb](https://gitlab.com/ensembl/talk/commit/6f5bbcb5e9df726aa7c1fe845584c1401b659c5e))

### [0.4.203](https://gitlab.com/ensembl/talk/compare/v0.4.202...v0.4.203) (2021-06-02)


### Bug Fixes

* **Slack Integration:** Fix spaceName in route for Slack ([c266738](https://gitlab.com/ensembl/talk/commit/c2667384487c7a1c7a1fd057af40a642ef2376e4))

### [0.4.202](https://gitlab.com/ensembl/talk/compare/v0.4.201...v0.4.202) (2021-06-02)


### Features

* **Slack Integration:** Integrate Slack with one space ([a2382d5](https://gitlab.com/ensembl/talk/commit/a2382d5cc74d217c45fca58427de55c18805995f))

### [0.4.201](https://gitlab.com/ensembl/talk/compare/v0.4.200...v0.4.201) (2021-06-02)


### Features

* **Slack Integration:** Update callback to read from secured user ([06825b2](https://gitlab.com/ensembl/talk/commit/06825b2cedba46b49b8d223674313a57ecdcdf97))

### [0.4.200](https://gitlab.com/ensembl/talk/compare/v0.4.199...v0.4.200) (2021-06-02)


### Features

* **Slack Integration:** Create talk with message from Slack ([6d5e0cf](https://gitlab.com/ensembl/talk/commit/6d5e0cfebca61683eb1403727ad312b6b2376e4a))

### [0.4.199](https://gitlab.com/ensembl/talk/compare/v0.4.198...v0.4.199) (2021-06-02)


### Bug Fixes

* **Firestore rules:** All users can change heartedUids ([755046e](https://gitlab.com/ensembl/talk/commit/755046e99e3676a7bc2fe5e17e79f0f2f3e90a98))

### [0.4.198](https://gitlab.com/ensembl/talk/compare/v0.4.197...v0.4.198) (2021-06-02)


### Features

* **Heart button:** Store hearters uids ([df3fca1](https://gitlab.com/ensembl/talk/commit/df3fca1a6e9bb8ef5c5a691720322e285f10d8e0))

### [0.4.197](https://gitlab.com/ensembl/talk/compare/v0.4.196...v0.4.197) (2021-06-02)


### Features

* **Slack Integration:** Response to user with a confirmation text ([2a387d1](https://gitlab.com/ensembl/talk/commit/2a387d1922ee37a4723b7cd01ad1694b426735e9))

### [0.4.196](https://gitlab.com/ensembl/talk/compare/v0.4.195...v0.4.196) (2021-06-02)

### [0.4.195](https://gitlab.com/ensembl/talk/compare/v0.4.194...v0.4.195) (2021-06-02)


### Features

* **Slack Integration:** Add function to handle callback for shortcut ([859a008](https://gitlab.com/ensembl/talk/commit/859a008b88078802883d4119272437d7f544c656))

### [0.4.194](https://gitlab.com/ensembl/talk/compare/v0.4.193...v0.4.194) (2021-06-02)


### Bug Fixes

* **copyUserToSpaces:** Catch condition when user has no spaces ([6ee4124](https://gitlab.com/ensembl/talk/commit/6ee4124b3122950b04edacbe8c147a8f0b298d49))

### [0.4.193](https://gitlab.com/ensembl/talk/compare/v0.4.192...v0.4.193) (2021-06-02)


### Features

* **Slack Integration:** Add command to Slack access scope ([613c9eb](https://gitlab.com/ensembl/talk/commit/613c9ebee619a48e3c0007e037afa7e00b88cc44))

### [0.4.192](https://gitlab.com/ensembl/talk/compare/v0.4.191...v0.4.192) (2021-06-01)


### Features

* **Slack Integration:** Redirect to Slack auth page ([41a43b5](https://gitlab.com/ensembl/talk/commit/41a43b55a33de10088d54f131f72d63079ebe728))

### [0.4.191](https://gitlab.com/ensembl/talk/compare/v0.4.190...v0.4.191) (2021-06-01)


### Features

* **Slack Integration:** Use state checking on redirect for security ([34bae28](https://gitlab.com/ensembl/talk/commit/34bae28b52b7a6f53ee4a58779f08988cd54787f))

### [0.4.190](https://gitlab.com/ensembl/talk/compare/v0.4.189...v0.4.190) (2021-06-01)


### Features

* **Slack Integration:** Update function to read credential from db ([a6b1f89](https://gitlab.com/ensembl/talk/commit/a6b1f8925a38a027a441a257d0df0aa7869f188f))

### [0.4.189](https://gitlab.com/ensembl/talk/compare/v0.4.188...v0.4.189) (2021-06-01)


### Features

* **Slack Integration:** Handle redirect from Slack ([970fe5a](https://gitlab.com/ensembl/talk/commit/970fe5ac562687ee8f5331394c84c701c8413e20))

### [0.4.188](https://gitlab.com/ensembl/talk/compare/v0.4.187...v0.4.188) (2021-06-01)


### Features

* **Convert talk to doc:** Update modal message to say Comment threads ([83d9317](https://gitlab.com/ensembl/talk/commit/83d9317507cbbd66764dbfaf453bdd78187c7a74))

### [0.4.187](https://gitlab.com/ensembl/talk/compare/v0.4.186...v0.4.187) (2021-06-01)

### [0.4.186](https://gitlab.com/ensembl/talk/compare/v0.4.185...v0.4.186) (2021-06-01)

### [0.4.185](https://gitlab.com/ensembl/talk/compare/v0.4.184...v0.4.185) (2021-06-01)

### [0.4.184](https://gitlab.com/ensembl/talk/compare/v0.4.183...v0.4.184) (2021-06-01)


### Bug Fixes

* **Answer request:** Fix answer request not being default ([fe4722b](https://gitlab.com/ensembl/talk/commit/fe4722b70e07221b88e80ced60e51ad67fdd6a95))

### [0.4.183](https://gitlab.com/ensembl/talk/compare/v0.4.182...v0.4.183) (2021-06-01)


### Features

* **Request Answer:** Available for all spaces and default ([8eae6ef](https://gitlab.com/ensembl/talk/commit/8eae6ef806061bc6fb466a379084af7bcb3a0472))

### [0.4.182](https://gitlab.com/ensembl/talk/compare/v0.4.181...v0.4.182) (2021-06-01)

### [0.4.181](https://gitlab.com/ensembl/talk/compare/v0.4.180...v0.4.181) (2021-05-29)


### Features

* **CommentAddedNotification:** For answer requests ([400c8d9](https://gitlab.com/ensembl/talk/commit/400c8d9d64e07564c810c68614fbdfd41c3bfa44))

### [0.4.180](https://gitlab.com/ensembl/talk/compare/v0.4.179...v0.4.180) (2021-05-28)


### Features

* **ResponseWaiting:** For ack and answer requests ([e656762](https://gitlab.com/ensembl/talk/commit/e656762b62f015ddffc1595bb0587f12ffe636c5))

### [0.4.179](https://gitlab.com/ensembl/talk/compare/v0.4.178...v0.4.179) (2021-05-28)


### Features

* **Comment response counts:** Ack request and Answer request ([44f5fc2](https://gitlab.com/ensembl/talk/commit/44f5fc2997f818aebfe534386d331937f5840ac2))

### [0.4.178](https://gitlab.com/ensembl/talk/compare/v0.4.177...v0.4.178) (2021-05-28)


### Features

* **Comment response button:** Request answer and request ack ([61cfafe](https://gitlab.com/ensembl/talk/commit/61cfafe99a1845424210716756e2cc8c4a76ee39))

### [0.4.177](https://gitlab.com/ensembl/talk/compare/v0.4.176...v0.4.177) (2021-05-28)


### Features

* **NotsSentMessage:** Account for request answer ([eaa0431](https://gitlab.com/ensembl/talk/commit/eaa0431d970e94648c77a4a1cbc71c0ecb84ebd4))

### [0.4.176](https://gitlab.com/ensembl/talk/compare/v0.4.175...v0.4.176) (2021-05-28)


### Features

* **MentionAction:** New type for request answer ([e2a1cd9](https://gitlab.com/ensembl/talk/commit/e2a1cd949740a190c0daded65a804f52d35a0911))

### [0.4.175](https://gitlab.com/ensembl/talk/compare/v0.4.174...v0.4.175) (2021-05-28)

### [0.4.174](https://gitlab.com/ensembl/talk/compare/v0.4.173...v0.4.174) (2021-05-28)


### Features

* **Slack Integration:** Set up redirect for cancel and completion ([99f73b1](https://gitlab.com/ensembl/talk/commit/99f73b17fbab4f1c52e20cc39cae1cc971693527))

### [0.4.173](https://gitlab.com/ensembl/talk/compare/v0.4.172...v0.4.173) (2021-05-28)


### Features

* **Slack Integration:** Create function to recieve OAuth redirect f. Slack ([03d70a4](https://gitlab.com/ensembl/talk/commit/03d70a48c82639a33563392e64b166c05af34568))

### [0.4.172](https://gitlab.com/ensembl/talk/compare/v0.4.171...v0.4.172) (2021-05-28)


### Features

* **Settings:** Create a settings page to host Slack button ([92e6682](https://gitlab.com/ensembl/talk/commit/92e6682559ec5e96dc31caf9786ab4d34fc0613c))

### [0.4.171](https://gitlab.com/ensembl/talk/compare/v0.4.170...v0.4.171) (2021-05-28)


### Bug Fixes

* **NewTalk:** Fix typo in notification field ([95f0876](https://gitlab.com/ensembl/talk/commit/95f0876df16bdfca445900e8cdeb26f63973f9c2))

### [0.4.170](https://gitlab.com/ensembl/talk/compare/v0.4.169...v0.4.170) (2021-05-28)


### Features

* **SlackNots:** Send notification every 3 minutes ([d7a557d](https://gitlab.com/ensembl/talk/commit/d7a557d4d3a97725b50c377be1ef416688c4bd37))

### [0.4.169](https://gitlab.com/ensembl/talk/compare/v0.4.168...v0.4.169) (2021-05-28)

### [0.4.168](https://gitlab.com/ensembl/talk/compare/v0.4.166...v0.4.168) (2021-05-28)


### Features

* **Doc comments view:** Take threadids only in comments ([64d298d](https://gitlab.com/ensembl/talk/commit/64d298d6cffca388a07351a20302b71fdf963d07))

### [0.4.167](https://gitlab.com/ensembl/talk/compare/v0.4.166...v0.4.167) (2021-05-28)

### [0.4.166](https://gitlab.com/ensembl/talk/compare/v0.4.165...v0.4.166) (2021-05-28)

### [0.4.165](https://gitlab.com/ensembl/talk/compare/v0.4.164...v0.4.165) (2021-05-27)


### Features

* **Comment threads:** Show all of them ([3259cdc](https://gitlab.com/ensembl/talk/commit/3259cdca25292afbd82c6b26bb2bd7bcd2974648))
* **SlackNots:** Send notification to slack on talk creation ([8be0b5f](https://gitlab.com/ensembl/talk/commit/8be0b5f593f626c867b0c864c14efa1ff2b9bfd1))

### [0.4.164](https://gitlab.com/ensembl/talk/compare/v0.4.163...v0.4.164) (2021-05-27)

### [0.4.163](https://gitlab.com/ensembl/talk/compare/v0.4.162...v0.4.163) (2021-05-27)


### Features

* **talkToDoc:** Release talk to doc to all spaces ([56a62fe](https://gitlab.com/ensembl/talk/commit/56a62fe7c87787e2a2c97e633fe727b3a1e15245))

### [0.4.162](https://gitlab.com/ensembl/talk/compare/v0.4.161...v0.4.162) (2021-05-27)


### Features

* **ckeditor token:** Use victor's uid for local dev ([eca27ce](https://gitlab.com/ensembl/talk/commit/eca27cea53582dcdf8dc70b0d3e4528407288119))

### [0.4.161](https://gitlab.com/ensembl/talk/compare/v0.4.160...v0.4.161) (2021-05-27)


### Features

* **ckeditorThreadId:** Rename to threadId ([26a88b5](https://gitlab.com/ensembl/talk/commit/26a88b568ee3ba65e0975aa5190092277c0b39c1))

### [0.4.160](https://gitlab.com/ensembl/talk/compare/v0.4.159...v0.4.160) (2021-05-27)


### Bug Fixes

* **Comment Added Notification:** Fix it so it is the right type ([9d07f9a](https://gitlab.com/ensembl/talk/commit/9d07f9a6dea20356d27fee713a51031eb4d80a84))

### [0.4.159](https://gitlab.com/ensembl/talk/compare/v0.4.158...v0.4.159) (2021-05-27)


### Features

* **threadId:** Write threadId to Firestore ([0ab1249](https://gitlab.com/ensembl/talk/commit/0ab1249e5784ab46fb9e55103f79bf17586f838e))

### [0.4.158](https://gitlab.com/ensembl/talk/compare/v0.4.157...v0.4.158) (2021-05-27)


### Features

* **ThreadResolvedMessage:** HIde it after user clicks resolved comments view ([61b7999](https://gitlab.com/ensembl/talk/commit/61b799906e8b9eba65ba4f989552379d06e9168d))

### [0.4.157](https://gitlab.com/ensembl/talk/compare/v0.4.156...v0.4.157) (2021-05-26)


### Features

* **Convert to doc:** Move button to lower in sidebar ([c6098ec](https://gitlab.com/ensembl/talk/commit/c6098ecab11a8bccbc7d5445257e4cbc848e9d36))

### [0.4.156](https://gitlab.com/ensembl/talk/compare/v0.4.155...v0.4.156) (2021-05-26)


### Features

* **Comment message:** Set default message for converted comments ([6603d6a](https://gitlab.com/ensembl/talk/commit/6603d6a8f0602869cd8a9974ecdf2e269983c381))

### [0.4.155](https://gitlab.com/ensembl/talk/compare/v0.4.154...v0.4.155) (2021-05-26)


### Features

* **CommentThreads:** Restyle comment thread for conversion ([466edff](https://gitlab.com/ensembl/talk/commit/466edff7a271d8f10ba95885805b10255e83bc6f))

### [0.4.154](https://gitlab.com/ensembl/talk/compare/v0.4.153...v0.4.154) (2021-05-26)


### Features

* **DocViewToggle:** Show for all spaces ([89bb6ce](https://gitlab.com/ensembl/talk/commit/89bb6ce709154b6b01f578e39b0b673a6a421d8a))

### [0.4.153](https://gitlab.com/ensembl/talk/compare/v0.4.152...v0.4.153) (2021-05-26)


### Features

* **Modal:** Restyle confirm button ([036592a](https://gitlab.com/ensembl/talk/commit/036592ab1ccc2b0f32f21c2f240ba9489a026ea7))

### [0.4.152](https://gitlab.com/ensembl/talk/compare/v0.4.151...v0.4.152) (2021-05-26)


### Bug Fixes

* **ResolvedCommentThread message:** Fix spacing ([32ebfee](https://gitlab.com/ensembl/talk/commit/32ebfee777a3cdfcec19692ea360e071dac5b076))

### [0.4.151](https://gitlab.com/ensembl/talk/compare/v0.4.150...v0.4.151) (2021-05-26)


### Features

* **AddCommnet:** Add markerContent when comment is added ([564724a](https://gitlab.com/ensembl/talk/commit/564724aac75e0fce042ddf67a25301a8c32cb19a))

### [0.4.150](https://gitlab.com/ensembl/talk/compare/v0.4.149...v0.4.150) (2021-05-26)


### Features

* **ConvertTalkToDoc:** Use modal workflow for talk conversion ([1be4a75](https://gitlab.com/ensembl/talk/commit/1be4a75eff808b409ff03c5e03276f03c3f53d82))

### [0.4.149](https://gitlab.com/ensembl/talk/compare/v0.4.148...v0.4.149) (2021-05-26)


### Features

* **Comments:** Join Ensembl Community link ([1a628b1](https://gitlab.com/ensembl/talk/commit/1a628b1a31e90e100a6759746614553277b96912))

### [0.4.148](https://gitlab.com/ensembl/talk/compare/v0.4.147...v0.4.148) (2021-05-26)


### Features

* **createSpace:** Ensembl bot comment to include ckeditorThreadId ([88712f1](https://gitlab.com/ensembl/talk/commit/88712f100b880e76ac5337dd31468be2ca4567dc))

### [0.4.147](https://gitlab.com/ensembl/talk/compare/v0.4.146...v0.4.147) (2021-05-26)

### [0.4.146](https://gitlab.com/ensembl/talk/compare/v0.4.145...v0.4.146) (2021-05-26)


### Features

* **DeleteDoc:** Tell people to prefer archiving a doc over delete ([95674f9](https://gitlab.com/ensembl/talk/commit/95674f94f67e9a8c1ad9cdc1ce92cee40edc9425))

### [0.4.145](https://gitlab.com/ensembl/talk/compare/v0.4.144...v0.4.145) (2021-05-26)


### Features

* **Doc state:** Archived doc state ([10d2393](https://gitlab.com/ensembl/talk/commit/10d23930f256ffc04ef69f37df9e8a14ff8a9660))

### [0.4.144](https://gitlab.com/ensembl/talk/compare/v0.4.143...v0.4.144) (2021-05-26)


### Features

* **ckeditorThreadId:** Add ckeditorThreadId on cmt added ([573d05b](https://gitlab.com/ensembl/talk/commit/573d05bf98151718bb59c4d78358b9d0d1256783))
* **Doc notifications:** Archive / unarchive notifications ([c4108aa](https://gitlab.com/ensembl/talk/commit/c4108aab215f90fc9d342e132baae97c269c1027))
* **updateTalkTabs:** archivedDocs ([1041ca4](https://gitlab.com/ensembl/talk/commit/1041ca49b33acb3c35bc1a1d024598ca37981e02))

### [0.4.143](https://gitlab.com/ensembl/talk/compare/v0.4.142...v0.4.143) (2021-05-26)

### [0.4.142](https://gitlab.com/ensembl/talk/compare/v0.4.141...v0.4.142) (2021-05-26)


### Bug Fixes

* **convertTalkCmts:** Fix escape condition for trigger ([dc3328e](https://gitlab.com/ensembl/talk/commit/dc3328e670286dd82fecc7120e5885eb066bee23))

### [0.4.141](https://gitlab.com/ensembl/talk/compare/v0.4.140...v0.4.141) (2021-05-26)


### Features

* **Doc:** Archive doc for Ensembl space only ([00ccc77](https://gitlab.com/ensembl/talk/commit/00ccc77e8414dab6c7a0dacae5769befad24bf54))

### [0.4.140](https://gitlab.com/ensembl/talk/compare/v0.4.139...v0.4.140) (2021-05-25)

### [0.4.139](https://gitlab.com/ensembl/talk/compare/v0.4.138...v0.4.139) (2021-05-25)

### [0.4.138](https://gitlab.com/ensembl/talk/compare/v0.4.137...v0.4.138) (2021-05-25)


### Features

* **CommentThread:** Rename resolve comment thread ([e96aeec](https://gitlab.com/ensembl/talk/commit/e96aeec4eb84c1a5d2148dc069eab263f715f3ff))

### [0.4.137](https://gitlab.com/ensembl/talk/compare/v0.4.136...v0.4.137) (2021-05-25)

### [0.4.136](https://gitlab.com/ensembl/talk/compare/v0.4.135...v0.4.136) (2021-05-25)


### Features

* **ConvertTalkCmts:** Convert talk comments to display in resolved view ([f64974d](https://gitlab.com/ensembl/talk/commit/f64974d5bcbb61b385b3d158ad9a7fa31194d44b))

### [0.4.135](https://gitlab.com/ensembl/talk/compare/v0.4.134...v0.4.135) (2021-05-25)


### Features

* **Talk State Changer:** Style it to match state banner ([b55cb19](https://gitlab.com/ensembl/talk/commit/b55cb198faec08f3816f035ca9123e30f1c006b1))

### [0.4.134](https://gitlab.com/ensembl/talk/compare/v0.4.133...v0.4.134) (2021-05-25)


### Features

* **ConvertTalkToDoc:** Update database record to talk for conversion ([adda78b](https://gitlab.com/ensembl/talk/commit/adda78bc675985d2db1d0d7e5cac0117808090c6))

### [0.4.133](https://gitlab.com/ensembl/talk/compare/v0.4.132...v0.4.133) (2021-05-25)


### Features

* **ConvertTalkToDoc:** Create button for convert talk to doc ([ceb0947](https://gitlab.com/ensembl/talk/commit/ceb09475b5761b2d23dd7323e5c518f3ef32b94c))

### [0.4.132](https://gitlab.com/ensembl/talk/compare/v0.4.131...v0.4.132) (2021-05-25)

### [0.4.131](https://gitlab.com/ensembl/talk/compare/v0.4.130...v0.4.131) (2021-05-25)


### Features

* **DocPage:** Unlock doc comments alert ([18228e8](https://gitlab.com/ensembl/talk/commit/18228e8d2c7009e6842490ee4aaa0a871a5a8372))

### [0.4.130](https://gitlab.com/ensembl/talk/compare/v0.4.129...v0.4.130) (2021-05-24)


### Features

* **Collaborative Editor:** Disable if using localhost ([4809dba](https://gitlab.com/ensembl/talk/commit/4809dbaa2a483d86cca4e71b5cadd1c82ce79b6a))

### [0.4.129](https://gitlab.com/ensembl/talk/compare/v0.4.128...v0.4.129) (2021-05-24)


### Features

* **Collaborative Editor:** Disable if only one user in space ([ccc09b0](https://gitlab.com/ensembl/talk/commit/ccc09b09cca432bf28b66b1b60a1a7dd50789b58))

### [0.4.128](https://gitlab.com/ensembl/talk/compare/v0.4.127...v0.4.128) (2021-05-24)


### Features

* **ResolvedNots:** Send doc comment resolved nots to subscribers ([f9a4987](https://gitlab.com/ensembl/talk/commit/f9a498774eda0925c8c5dcc844197b6af83d33cc))

### [0.4.127](https://gitlab.com/ensembl/talk/compare/v0.4.126...v0.4.127) (2021-05-24)


### Features

* **Doc page:** Alert message after resolving comment thread ([34e9dd5](https://gitlab.com/ensembl/talk/commit/34e9dd5e73326fc46d3500f21512be4470fb4058))

### [0.4.126](https://gitlab.com/ensembl/talk/compare/v0.4.125...v0.4.126) (2021-05-24)


### Bug Fixes

* **updateTalkStateOnMR:** Fix escape condition ([b341a28](https://gitlab.com/ensembl/talk/commit/b341a28784916f48ae054dff3ea7c831cb5d8486))

### [0.4.125](https://gitlab.com/ensembl/talk/compare/v0.4.124...v0.4.125) (2021-05-24)


### Features

* **DocViewToggle:** Show number of comment threads ([7df0900](https://gitlab.com/ensembl/talk/commit/7df090014003d631faa37a9a56a2f8892f1e73e0))

### [0.4.124](https://gitlab.com/ensembl/talk/compare/v0.4.123...v0.4.124) (2021-05-24)

### [0.4.123](https://gitlab.com/ensembl/talk/compare/v0.4.122...v0.4.123) (2021-05-24)


### Features

* **Resolved comment threads:** Sort them by createdAt ([b7251a1](https://gitlab.com/ensembl/talk/commit/b7251a1a0cebe520967803c3f49025cb88ae3e8f))

### [0.4.122](https://gitlab.com/ensembl/talk/compare/v0.4.121...v0.4.122) (2021-05-24)


### Bug Fixes

* **TalkPageBody:** Fix talk subject not showing up in talk with hash ([3b1b051](https://gitlab.com/ensembl/talk/commit/3b1b051cf685f6de28db8f5331b5a328a51172e5))

### [0.4.121](https://gitlab.com/ensembl/talk/compare/v0.4.120...v0.4.121) (2021-05-24)


### Bug Fixes

* **TalkPage:** Fix comment hash not showing in talk ([373f8b0](https://gitlab.com/ensembl/talk/commit/373f8b03f3ff88d8a9a08dfe09fc9fb7b228e433))

### [0.4.120](https://gitlab.com/ensembl/talk/compare/v0.4.119...v0.4.120) (2021-05-24)


### Bug Fixes

* **DocViewToggle:** Only show on doc page ([9c3721a](https://gitlab.com/ensembl/talk/commit/9c3721a03eee31739cfc77e26dc6ec0158a9399e))
* **New inline comment features:** Only for Ensembl and Ensembl Two ([28af6fb](https://gitlab.com/ensembl/talk/commit/28af6fb3f60ab614a52db8ff9b700bd9c41f986d))

### [0.4.119](https://gitlab.com/ensembl/talk/compare/v0.4.118...v0.4.119) (2021-05-24)

### [0.4.118](https://gitlab.com/ensembl/talk/compare/v0.4.117...v0.4.118) (2021-05-24)


### Features

* **ResolvedCommentThreads:** Show all the time with an empty state message ([45651c8](https://gitlab.com/ensembl/talk/commit/45651c898965f508c94cabd1370de5b25730e79e))

### [0.4.117](https://gitlab.com/ensembl/talk/compare/v0.4.116...v0.4.117) (2021-05-24)


### Features

* **StateBanner:** Refactor and specific message for Talk vs Cmt Thrd ([8be279c](https://gitlab.com/ensembl/talk/commit/8be279cdcd98cf8157e92ddcc9bd74a46689d142))

### [0.4.116](https://gitlab.com/ensembl/talk/compare/v0.4.115...v0.4.116) (2021-05-24)


### Features

* **DocComment:** Deep link into inline comment and resolved comment ([fe626ed](https://gitlab.com/ensembl/talk/commit/fe626ed8ee92cba5fed18dfe787f50ab37c1ef60))

### [0.4.115](https://gitlab.com/ensembl/talk/compare/v0.4.113...v0.4.115) (2021-05-24)


### Features

* **DocViewToggle:** Call it Resolved comment threads ([08e429a](https://gitlab.com/ensembl/talk/commit/08e429a9a903406138f537201517dcdfb42b4136))

### [0.4.114](https://gitlab.com/ensembl/talk/compare/v0.4.113...v0.4.114) (2021-05-24)

### [0.4.113](https://gitlab.com/ensembl/talk/compare/v0.4.112...v0.4.113) (2021-05-24)


### Features

* **MergeRequest:** Move talk to InProgress when MR is created ([c41bb5f](https://gitlab.com/ensembl/talk/commit/c41bb5f8cf49081ef173ff242627d875a180591b))

### [0.4.112](https://gitlab.com/ensembl/talk/compare/v0.4.111...v0.4.112) (2021-05-23)


### Features

* **SpaceDropdown:** Shows complete space name is dropdown ([e9cb809](https://gitlab.com/ensembl/talk/commit/e9cb80912a4776b68023ac71b0a71e364a7ba13f))

### [0.4.111](https://gitlab.com/ensembl/talk/compare/v0.4.110...v0.4.111) (2021-05-23)


### Features

* **LabelsPage:** Empty state message ([08168bb](https://gitlab.com/ensembl/talk/commit/08168bb9603a70f261bd3fe75226281981ffe1cb))

### [0.4.110](https://gitlab.com/ensembl/talk/compare/v0.4.109...v0.4.110) (2021-05-23)


### Features

* **TalksPageTalks:** Handle Firestore loading and error states ([a47b6df](https://gitlab.com/ensembl/talk/commit/a47b6df0329fdc2d5dba935bc53eb2fca79fde62))

### [0.4.109](https://gitlab.com/ensembl/talk/compare/v0.4.108...v0.4.109) (2021-05-23)


### Features

* **Comments:** Show older comment if hidden ([fa73798](https://gitlab.com/ensembl/talk/commit/fa7379857c8d239d696690a6bf4462dfa2dffa8e))

### [0.4.108](https://gitlab.com/ensembl/talk/compare/v0.4.107...v0.4.108) (2021-05-23)


### Features

* **TalksPage and TalkPage:** Use Firestore error state ([5d5b9e9](https://gitlab.com/ensembl/talk/commit/5d5b9e95b1fce4c246ce97821e0353396962b11c))

### [0.4.107](https://gitlab.com/ensembl/talk/compare/v0.4.106...v0.4.107) (2021-05-23)


### Features

* **TalkPage:** Firestore error states ([2ee4673](https://gitlab.com/ensembl/talk/commit/2ee467345cb5e21c92d55a04845ba4401bfbc6c3))

### [0.4.106](https://gitlab.com/ensembl/talk/compare/v0.4.105...v0.4.106) (2021-05-23)


### Features

* **ActivityPage and LabelsPage:** Use firestore error state ([baf9602](https://gitlab.com/ensembl/talk/commit/baf9602b1033f389d9927f93ab810b8024542cfb))

### [0.4.105](https://gitlab.com/ensembl/talk/compare/v0.4.104...v0.4.105) (2021-05-23)


### Features

* **PeoplePage:** Use error state for useDocumentData ([3b46c38](https://gitlab.com/ensembl/talk/commit/3b46c3845c600753ed1cd8c229571cf42917b477))

### [0.4.104](https://gitlab.com/ensembl/talk/compare/v0.4.103...v0.4.104) (2021-05-23)


### Features

* **PeoplePage:** Use error state in useDocumentData ([a103d5a](https://gitlab.com/ensembl/talk/commit/a103d5a20dc70afb9f0bd1806cdc40abc8236078))
* **ResolvedComments:** Show resolved doc comments in separate view ([904e33c](https://gitlab.com/ensembl/talk/commit/904e33c1961c7974bfb9994c5985bf7e4fe9985a))

### [0.4.103](https://gitlab.com/ensembl/talk/compare/v0.4.102...v0.4.103) (2021-05-22)

### [0.4.102](https://gitlab.com/ensembl/talk/compare/v0.4.101...v0.4.102) (2021-05-22)


### Bug Fixes

* **Community Space:** Allow subscribing and approving as members ([02a40a6](https://gitlab.com/ensembl/talk/commit/02a40a6bdf3f7534d76aaa5f26c96b57ed63ad04))

### [0.4.101](https://gitlab.com/ensembl/talk/compare/v0.4.100...v0.4.101) (2021-05-22)


### Bug Fixes

* **TalkPageLabels:** Allow remove on community space only if admin ([c523241](https://gitlab.com/ensembl/talk/commit/c523241ceb902eb723fd9e53fdafdd91f59f0392))

### [0.4.100](https://gitlab.com/ensembl/talk/compare/v0.4.99...v0.4.100) (2021-05-22)


### Features

* **TalksPage Pills:** Styling ([556f646](https://gitlab.com/ensembl/talk/commit/556f64616f8836e379c2ee5d5f2a53fcf4a411f5))

### [0.4.99](https://gitlab.com/ensembl/talk/compare/v0.4.98...v0.4.99) (2021-05-22)


### Features

* **Talk State:** In progress state ([f875c7b](https://gitlab.com/ensembl/talk/commit/f875c7b3bd8ceb77fd94c7f3d71409c67059d098))
* **talkActivityOnTalkUpdate:** Based on talk state changed ([7b596ff](https://gitlab.com/ensembl/talk/commit/7b596ff8adca6a72deeee15ee9631dd5cdd4601a))

### [0.4.98](https://gitlab.com/ensembl/talk/compare/v0.4.97...v0.4.98) (2021-05-22)


### Features

* **updateTalkTabs:** Use state ([31c88e4](https://gitlab.com/ensembl/talk/commit/31c88e4587d723054b008ee110922183b366d029))

### [0.4.97](https://gitlab.com/ensembl/talk/compare/v0.4.96...v0.4.97) (2021-05-22)


### Features

* **TalkStateChangedNots:** State changed notifications ([2be92b0](https://gitlab.com/ensembl/talk/commit/2be92b066f01884b136b8c7509070fddacd278fa))

### [0.4.96](https://gitlab.com/ensembl/talk/compare/v0.4.95...v0.4.96) (2021-05-22)


### Features

* **TalkStateBanner:** Banner messages for In progress and Resolved ([460c320](https://gitlab.com/ensembl/talk/commit/460c3204969bd5df0da3398dffc7292ebd15d49f))

### [0.4.95](https://gitlab.com/ensembl/talk/compare/v0.4.94...v0.4.95) (2021-05-21)


### Features

* **At mention Everyone:** Remove when you are only user in space ([f8687cc](https://gitlab.com/ensembl/talk/commit/f8687cc3a2c00064aadeebcb0d0ba7dac545ff93))

### [0.4.94](https://gitlab.com/ensembl/talk/compare/v0.4.93...v0.4.94) (2021-05-21)


### Features

* **SpaceSwitcher:** Show current space name above switcher ([acd3796](https://gitlab.com/ensembl/talk/commit/acd3796fa5e636a2d986eaabc9f2180d9ba396ea))

### [0.4.93](https://gitlab.com/ensembl/talk/compare/v0.4.92...v0.4.93) (2021-05-21)


### Features

* **createSpace:** Change starter talk text to say change state ([5137b21](https://gitlab.com/ensembl/talk/commit/5137b216266936b6d34a34be2c4852a2f9c61d82))

### [0.4.92](https://gitlab.com/ensembl/talk/compare/v0.4.91...v0.4.92) (2021-05-21)


### Features

* **InvitePerson:** Update invite button text ([9790897](https://gitlab.com/ensembl/talk/commit/9790897300e34fbf3cdb2ea3c3c0ac5f7b1b84bc))

### [0.4.91](https://gitlab.com/ensembl/talk/compare/v0.4.90...v0.4.91) (2021-05-21)


### Features

* **DocSubject:** Store resolved comment metadata ([9fa7c9b](https://gitlab.com/ensembl/talk/commit/9fa7c9b7aad13a19c88a2d59e5f499f7c63dae6c))

### [0.4.90](https://gitlab.com/ensembl/talk/compare/v0.4.89...v0.4.90) (2021-05-21)


### Features

* **TalksPageTabs:** Show talk state as disabled if on docs ([29ee50a](https://gitlab.com/ensembl/talk/commit/29ee50aadc61ec7244b1c36f7d180fd01e6e809b))

### [0.4.89](https://gitlab.com/ensembl/talk/compare/v0.4.88...v0.4.89) (2021-05-21)


### Bug Fixes

* **TalkStateChanger:** Fix old Ensembl talks not displaying state ([a394287](https://gitlab.com/ensembl/talk/commit/a394287539c4ccfba479785bf9170b90a70b2846))

### [0.4.88](https://gitlab.com/ensembl/talk/compare/v0.4.87...v0.4.88) (2021-05-21)


### Features

* **DocSubject:** Store commented on text each time cmt is added ([0579c21](https://gitlab.com/ensembl/talk/commit/0579c2138375bb7f5f6e17d49b93e939277d515f))

### [0.4.87](https://gitlab.com/ensembl/talk/compare/v0.4.85...v0.4.87) (2021-05-21)


### Features

* **TalkStateChanger:** In progress state for Ensembl space ([b6ec0e5](https://gitlab.com/ensembl/talk/commit/b6ec0e5d2dabbe4130810f8972216abee9418423))

### [0.4.86](https://gitlab.com/ensembl/talk/compare/v0.4.85...v0.4.86) (2021-05-21)

### [0.4.85](https://gitlab.com/ensembl/talk/compare/v0.4.84...v0.4.85) (2021-05-21)


### Features

* **TalkStateChanger:** Show all options with selected option disabled ([9dc13a9](https://gitlab.com/ensembl/talk/commit/9dc13a9eba81c47b82a85d3ec5cc2b40871e8c58))

### [0.4.84](https://gitlab.com/ensembl/talk/compare/v0.4.83...v0.4.84) (2021-05-21)


### Features

* **TalkStateChanger:** Replace button with a dropdown select ([8b6a601](https://gitlab.com/ensembl/talk/commit/8b6a60131d1b6bc97fb770e9fee48d60438cab88))

### [0.4.83](https://gitlab.com/ensembl/talk/compare/v0.4.82...v0.4.83) (2021-05-21)


### Features

* **TalksPageTabs:** Re-design pills to separate type vs state ([2953fea](https://gitlab.com/ensembl/talk/commit/2953feab2b476ec9d0dddad7d5edc71ec1e35f0f))

### [0.4.82](https://gitlab.com/ensembl/talk/compare/v0.4.81...v0.4.82) (2021-05-21)


### Bug Fixes

* **CommentHeader, ShowOlderComments, MR list:** Fix bugs ([1620e98](https://gitlab.com/ensembl/talk/commit/1620e98d06892855a54de9f431e26d2f0c55a9b8))

### [0.4.81](https://gitlab.com/ensembl/talk/compare/v0.4.80...v0.4.81) (2021-05-20)


### Features

* **ShowOlderComment:** Make button more standout ([383c48e](https://gitlab.com/ensembl/talk/commit/383c48eb79952138876ba1598f23a1d771918b8a))

### [0.4.80](https://gitlab.com/ensembl/talk/compare/v0.4.79...v0.4.80) (2021-05-20)


### Features

* **Comments:** Show only newest comments ([d113124](https://gitlab.com/ensembl/talk/commit/d1131249d4b94396dfaa31feb61cabf8ffd20529))

### [0.4.79](https://gitlab.com/ensembl/talk/compare/v0.4.78...v0.4.79) (2021-05-20)

### [0.4.78](https://gitlab.com/ensembl/talk/compare/v0.4.77...v0.4.78) (2021-05-20)


### Features

* **TalkRow:** Dynamic names and avatars ([2b04f96](https://gitlab.com/ensembl/talk/commit/2b04f964ebf1a502a3f22b375028ec30a833927f))

### [0.4.77](https://gitlab.com/ensembl/talk/compare/v0.4.76...v0.4.77) (2021-05-20)


### Features

* **Comment name and avatar:** Dynamically changing ([79406d9](https://gitlab.com/ensembl/talk/commit/79406d9c19e7e94dfccd61baeb2af0744ffdc37a))

### [0.4.76](https://gitlab.com/ensembl/talk/compare/v0.4.75...v0.4.76) (2021-05-20)

### [0.4.75](https://gitlab.com/ensembl/talk/compare/v0.4.74...v0.4.75) (2021-05-20)


### Features

* **IsClosedBanner:** Show closer name and avatar dynamically ([feab15e](https://gitlab.com/ensembl/talk/commit/feab15efde0a67c8f91bcf2d2d0b4d938648be74))

### [0.4.74](https://gitlab.com/ensembl/talk/compare/v0.4.73...v0.4.74) (2021-05-20)


### Features

* **Talk/Doc Subject:** Creator/Updater name and avatar is dynamic ([3134bee](https://gitlab.com/ensembl/talk/commit/3134beed642eb12feef61d3012b82fcd855585bb))

### [0.4.73](https://gitlab.com/ensembl/talk/compare/v0.4.71...v0.4.73) (2021-05-20)

### [0.4.72](https://gitlab.com/ensembl/talk/compare/v0.4.71...v0.4.72) (2021-05-20)

### [0.4.71](https://gitlab.com/ensembl/talk/compare/v0.4.70...v0.4.71) (2021-05-19)


### Features

* **Ensembl Bot:** More interesting and random responses ([8d8385f](https://gitlab.com/ensembl/talk/commit/8d8385f589aefd104f22d0bbb96930beca058479))

### [0.4.70](https://gitlab.com/ensembl/talk/compare/v0.4.69...v0.4.70) (2021-05-19)


### Features

* **NewSpacePage:** Give more feedback to user when creating space ([2703069](https://gitlab.com/ensembl/talk/commit/2703069dab0f95380b53c7cd49c64bce241e6839))

### [0.4.69](https://gitlab.com/ensembl/talk/compare/v0.4.68...v0.4.69) (2021-05-19)


### Bug Fixes

* **Subscribe func:** Fix error with filtering null ([8a9e7b3](https://gitlab.com/ensembl/talk/commit/8a9e7b3a00e603c1770174b2a2e38153dad6de8f))

### [0.4.68](https://gitlab.com/ensembl/talk/compare/v0.4.67...v0.4.68) (2021-05-19)


### Bug Fixes

* **Starter Talk:** Avoid re-saving upon opening page ([2d3e038](https://gitlab.com/ensembl/talk/commit/2d3e03822f36a385b6e44190474ca9f3c8afb94c))

### [0.4.67](https://gitlab.com/ensembl/talk/compare/v0.4.66...v0.4.67) (2021-05-19)


### Features

* **Ensembl Bot:** Update text for starter talk, comment, response ([4a4bfd1](https://gitlab.com/ensembl/talk/commit/4a4bfd13fb5c804e2def885b8608b7d83dd83a08))

### [0.4.66](https://gitlab.com/ensembl/talk/compare/v0.4.65...v0.4.66) (2021-05-19)


### Bug Fixes

* **PageLayout:** Fix warning with undefined isSpaceMember prop ([50b36e4](https://gitlab.com/ensembl/talk/commit/50b36e456b5540301bcd5d2b53351f767122e2fb))

### [0.4.65](https://gitlab.com/ensembl/talk/compare/v0.4.64...v0.4.65) (2021-05-19)


### Bug Fixes

* **EnsemblBot:** Move Ensembl Bot to the bottom of mention list ([d586cc1](https://gitlab.com/ensembl/talk/commit/d586cc14f13be2ae09e60301d4a56105f71a9b97))

### [0.4.64](https://gitlab.com/ensembl/talk/compare/v0.4.63...v0.4.64) (2021-05-19)

### [0.4.63](https://gitlab.com/ensembl/talk/compare/v0.4.62...v0.4.63) (2021-05-19)


### Features

* **Notification:** Don't send notifications if added by Bot ([c81dd68](https://gitlab.com/ensembl/talk/commit/c81dd68f5fa18135a1bdb43fab9f706b5d3408d2))

### [0.4.62](https://gitlab.com/ensembl/talk/compare/v0.4.61...v0.4.62) (2021-05-19)


### Features

* **NewSpacePage:** Show join community link on New Space Page ([78d45b2](https://gitlab.com/ensembl/talk/commit/78d45b2c6038f01001f7772ec4c56025758918fa))

### [0.4.61](https://gitlab.com/ensembl/talk/compare/v0.4.60...v0.4.61) (2021-05-19)


### Features

* **Help Link:** Show to all spaces ([5859472](https://gitlab.com/ensembl/talk/commit/58594723641cf355e91dc2a3650a5c70a9f4c49a))

### [0.4.60](https://gitlab.com/ensembl/talk/compare/v0.4.59...v0.4.60) (2021-05-19)


### Features

* **Join Page:** Styling and redirect to talks page and doc ([59bbc4f](https://gitlab.com/ensembl/talk/commit/59bbc4f3299d4ba022bc1a756e0fcb5c73557fbb))

### [0.4.59](https://gitlab.com/ensembl/talk/compare/v0.4.58...v0.4.59) (2021-05-19)


### Bug Fixes

* **AuthProvider:** If logged out, should redirect ([78842ca](https://gitlab.com/ensembl/talk/commit/78842ca0a5deb006ba590e7846dcdc76b5adb245))

### [0.4.58](https://gitlab.com/ensembl/talk/compare/v0.4.57...v0.4.58) (2021-05-19)

### [0.4.57](https://gitlab.com/ensembl/talk/compare/v0.4.56...v0.4.57) (2021-05-19)


### Features

* **Support:** Allow mention Ensembl Bot ([b2417b6](https://gitlab.com/ensembl/talk/commit/b2417b6ed6a1218a1b259ab9bb8c7e9516b9805e))

### [0.4.56](https://gitlab.com/ensembl/talk/compare/v0.4.55...v0.4.56) (2021-05-19)


### Features

* **NewSpace:** Add start comment with ack to starter talk ([b3691a3](https://gitlab.com/ensembl/talk/commit/b3691a334068940f223818bbb856fdbace8b3b08))

### [0.4.55](https://gitlab.com/ensembl/talk/compare/v0.4.54...v0.4.55) (2021-05-19)


### Bug Fixes

* **Space switcher:** Don't show if logged out ([d7e4e0d](https://gitlab.com/ensembl/talk/commit/d7e4e0dd618d080f6d838f8f4a73bd0dd0e7e6ef))

### [0.4.54](https://gitlab.com/ensembl/talk/compare/v0.4.52...v0.4.54) (2021-05-19)


### Features

* **Space switcher:** Space switcher shows Go to space... if not member ([d3d0ad0](https://gitlab.com/ensembl/talk/commit/d3d0ad02feec65cf76c3c20391bd783e0ca0b311))

### [0.4.53](https://gitlab.com/ensembl/talk/compare/v0.4.52...v0.4.53) (2021-05-19)

### [0.4.52](https://gitlab.com/ensembl/talk/compare/v0.4.51...v0.4.52) (2021-05-18)


### Features

* **NewSpace:** Create a starter talk when creating a new space ([d2abc16](https://gitlab.com/ensembl/talk/commit/d2abc16236f723e38682839ea6299ea815d8f072))

### [0.4.51](https://gitlab.com/ensembl/talk/compare/v0.4.50...v0.4.51) (2021-05-18)

### [0.4.50](https://gitlab.com/ensembl/talk/compare/v0.4.49...v0.4.50) (2021-05-18)


### Bug Fixes

* **JoinEnsemblCommunity:** Fix console error for prop requirement ([addc770](https://gitlab.com/ensembl/talk/commit/addc770988ff4d78af04baf3ee1fdf469555293d))

### [0.4.49](https://gitlab.com/ensembl/talk/compare/v0.4.48...v0.4.49) (2021-05-18)


### Features

* **Login:** Link to tos and privacy policy in login / sign up ([021a74b](https://gitlab.com/ensembl/talk/commit/021a74b0b78c145d73ebf005392270a2ffd9d6da))

### [0.4.48](https://gitlab.com/ensembl/talk/compare/v0.4.47...v0.4.48) (2021-05-18)


### Features

* **Logo:** Favicon and login page ([4888a3e](https://gitlab.com/ensembl/talk/commit/4888a3e80c7acdfe11186e95936e0a244039e1dc))


### Bug Fixes

* **Mention:** Fix everyone not being filtered ([193f426](https://gitlab.com/ensembl/talk/commit/193f4262487352bf4b04ee1922970514b15a99a8))

### [0.4.47](https://gitlab.com/ensembl/talk/compare/v0.4.46...v0.4.47) (2021-05-17)


### Features

* **Nav:** Allow Joining Ensembl Comm. in LeftNav ([c9bc94b](https://gitlab.com/ensembl/talk/commit/c9bc94bd7cf28b83bf049ae1cf1654643f1947c5))

### [0.4.46](https://gitlab.com/ensembl/talk/compare/v0.4.45...v0.4.46) (2021-05-17)


### Bug Fixes

* **SubjectTimestamp:** Fix long text no wrapping ([34f64f9](https://gitlab.com/ensembl/talk/commit/34f64f9910034685e71c02d68677d45f011448c3))

### [0.4.45](https://gitlab.com/ensembl/talk/compare/v0.4.44...v0.4.45) (2021-05-17)

### [0.4.44](https://gitlab.com/ensembl/talk/compare/v0.4.43...v0.4.44) (2021-05-17)

### [0.4.43](https://gitlab.com/ensembl/talk/compare/v0.4.42...v0.4.43) (2021-05-17)


### Features

* **Nav:** Add Help to LeftNav ([c9a807a](https://gitlab.com/ensembl/talk/commit/c9a807ab0237e2a3949a7113bc4ef09e58d7af58))

### [0.4.42](https://gitlab.com/ensembl/talk/compare/v0.4.41...v0.4.42) (2021-05-17)


### Features

* **NewSpace:** Provide link to join Ensembl Comm ([190c9b0](https://gitlab.com/ensembl/talk/commit/190c9b030ee677090d4c4ad8092a9ed45bf50a6a))

### [0.4.41](https://gitlab.com/ensembl/talk/compare/v0.4.40...v0.4.41) (2021-05-16)


### Bug Fixes

* **JoinPage:** Wait for function to copy user to space ([19cacf2](https://gitlab.com/ensembl/talk/commit/19cacf257fc54c30b42d1d5063686f5e17530a88))

### [0.4.40](https://gitlab.com/ensembl/talk/compare/v0.4.39...v0.4.40) (2021-05-16)


### Bug Fixes

* **JoinPage:** Fix redirection loccation ([851c7f3](https://gitlab.com/ensembl/talk/commit/851c7f376cb1547359d9dc7c9fbb5af2c7063a4a))

### [0.4.39](https://gitlab.com/ensembl/talk/compare/v0.4.38...v0.4.39) (2021-05-16)


### Features

* **JoinPage:** Create JoinPage for Ensembl Community ([fd7c099](https://gitlab.com/ensembl/talk/commit/fd7c09943e4bd906c6cb71f6b980f2b3fbc29ec7))

### [0.4.38](https://gitlab.com/ensembl/talk/compare/v0.4.37...v0.4.38) (2021-05-16)


### Bug Fixes

* **ProfilePage:** Clicking on cancel will go back to previous page ([d72a31c](https://gitlab.com/ensembl/talk/commit/d72a31c7a90b59cad03cfa773dcc108615a80fcc))

### [0.4.37](https://gitlab.com/ensembl/talk/compare/v0.4.36...v0.4.37) (2021-05-15)


### Bug Fixes

* **GitLab:** Fix ET url in Gitlab ([acce262](https://gitlab.com/ensembl/talk/commit/acce262d502b7be0868b69f6ddf831c3ac445d75))

### [0.4.36](https://gitlab.com/ensembl/talk/compare/v0.4.35...v0.4.36) (2021-05-15)


### Features

* **Community:** Message explaining Community ([95435ff](https://gitlab.com/ensembl/talk/commit/95435ff032a9151efaa09712d5a047f4a48930d6))

### [0.4.35](https://gitlab.com/ensembl/talk/compare/v0.4.34...v0.4.35) (2021-05-15)


### Features

* **Talk Sidebar:** For community space ([6353535](https://gitlab.com/ensembl/talk/commit/6353535f5add3650bd9bf87f9232450491ddfa17))

### [0.4.34](https://gitlab.com/ensembl/talk/compare/v0.4.33...v0.4.34) (2021-05-14)


### Features

* **Add Comment:** Show it only if you are a space member ([491e1d3](https://gitlab.com/ensembl/talk/commit/491e1d35775302474781dda34831df4577b957fa))

### [0.4.33](https://gitlab.com/ensembl/talk/compare/v0.4.32...v0.4.33) (2021-05-14)


### Features

* **IsSaving:** Show it only when user can edit ([18163a4](https://gitlab.com/ensembl/talk/commit/18163a46cf525e4ec5de9bb5f5b322341e33fd39))

### [0.4.32](https://gitlab.com/ensembl/talk/compare/v0.4.31...v0.4.32) (2021-05-14)


### Bug Fixes

* **CombinedUser :** Initial values for a logged out user ([a34aab0](https://gitlab.com/ensembl/talk/commit/a34aab05210f100f4a6d857c4cf64c74a4352657))

### [0.4.31](https://gitlab.com/ensembl/talk/compare/v0.4.30...v0.4.31) (2021-05-14)


### Bug Fixes

* **Admin for CKeditors:** Use email to determine admin ([01ca701](https://gitlab.com/ensembl/talk/commit/01ca701a6660e4a58f47871600e54ec202b5d496))

### [0.4.30](https://gitlab.com/ensembl/talk/compare/v0.4.29...v0.4.30) (2021-05-14)


### Features

* **isAdmin:** Return isAdmin via a cloud function ([097f5db](https://gitlab.com/ensembl/talk/commit/097f5db54858faa60d0fafdabdfe9970009f8e02))

### [0.4.29](https://gitlab.com/ensembl/talk/compare/v0.4.28...v0.4.29) (2021-05-14)


### Features

* **CKEditors:** Use non-collaborative ckeditors for community space ([823c1e0](https://gitlab.com/ensembl/talk/commit/823c1e04e2a51ef6409868f0535ec974e23e9472))

### [0.4.28](https://gitlab.com/ensembl/talk/compare/v0.4.27...v0.4.28) (2021-05-14)


### Bug Fixes

* **IsSaving:** Fix saving message wrapping ([568c7f5](https://gitlab.com/ensembl/talk/commit/568c7f5ca437792d5ea22bec9e57c22d35b543da))

### [0.4.27](https://gitlab.com/ensembl/talk/compare/v0.4.26...v0.4.27) (2021-05-14)


### Features

* **CKEditor for subject area:** Show different ckeditors depending on conditions ([2642aa3](https://gitlab.com/ensembl/talk/commit/2642aa33ae8c76a2c222fc044214a7f5453caffc))

### [0.4.26](https://gitlab.com/ensembl/talk/compare/v0.4.25...v0.4.26) (2021-05-14)


### Bug Fixes

* **Labels:** Fix color picker spacing ([f00f53c](https://gitlab.com/ensembl/talk/commit/f00f53cc4ccf2caee975d61493e7ffef8d315bc9))

### [0.4.25](https://gitlab.com/ensembl/talk/compare/v0.4.24...v0.4.25) (2021-05-14)


### Bug Fixes

* **Approval:** Fix approval should subscribe to talk ([b4f1cb7](https://gitlab.com/ensembl/talk/commit/b4f1cb7371019efd65207c3e46c4de86b70b7e71))

### [0.4.24](https://gitlab.com/ensembl/talk/compare/v0.4.22...v0.4.24) (2021-05-14)


### Features

* **Ensembl Community:** Talk page ([e686311](https://gitlab.com/ensembl/talk/commit/e686311c91f2c40bf4aaef17c3368b7a3d62fc92))

### [0.4.23](https://gitlab.com/ensembl/talk/compare/v0.4.22...v0.4.23) (2021-05-14)

### [0.4.22](https://gitlab.com/ensembl/talk/compare/v0.4.21...v0.4.22) (2021-05-14)


### Features

* **Ensembl Community:** Show talks page of Ensembl Community if logged out or logged in ([37dcf69](https://gitlab.com/ensembl/talk/commit/37dcf695f757e84d51825566dc18ddf5dd983e63))

### [0.4.21](https://gitlab.com/ensembl/talk/compare/v0.4.20...v0.4.21) (2021-05-12)


### Bug Fixes

* **Mention:** Fix mention filtering ([0a3d9c9](https://gitlab.com/ensembl/talk/commit/0a3d9c9bedad960d7fc4201776af9eb471a93139))

### [0.4.20](https://gitlab.com/ensembl/talk/compare/v0.4.19...v0.4.20) (2021-05-12)


### Features

* **DocSubject:** Comment thread button visibility ([f030001](https://gitlab.com/ensembl/talk/commit/f030001960d12933038b0d5ebc1c226510fbb010))

### [0.4.19](https://gitlab.com/ensembl/talk/compare/v0.4.18...v0.4.19) (2021-05-12)

### [0.4.18](https://gitlab.com/ensembl/talk/compare/v0.4.17...v0.4.18) (2021-05-12)


### Features

* **DocSubject:** Move comment toolbar item to the left ([68e8ad9](https://gitlab.com/ensembl/talk/commit/68e8ad9ac117b42fd5705250dc855d20682b391f))
* **ReplaceUntitledAlert:** Update Done message to be Title changed ([64bc00c](https://gitlab.com/ensembl/talk/commit/64bc00cab4ab9d839a7f09b40318d6fb0445066d))

### [0.4.17](https://gitlab.com/ensembl/talk/compare/v0.4.16...v0.4.17) (2021-05-12)


### Features

* **Resolve doc comment:** Style pointer ([a5db61e](https://gitlab.com/ensembl/talk/commit/a5db61e2f6378bbbf4b0cbb27edaa024ae167aa0))

### [0.4.16](https://gitlab.com/ensembl/talk/compare/v0.4.15...v0.4.16) (2021-05-12)


### Features

* **Doc comment thread:** Styling ([9316576](https://gitlab.com/ensembl/talk/commit/9316576dfb1923530a49b40e67e17e6f0da94cfa))

### [0.4.15](https://gitlab.com/ensembl/talk/compare/v0.4.14...v0.4.15) (2021-05-12)


### Bug Fixes

* **StarterMention:** Should include avatar url ([70e3e0f](https://gitlab.com/ensembl/talk/commit/70e3e0f4c0f0b65b1de567925dfd56aae099efc1))

### [0.4.14](https://gitlab.com/ensembl/talk/compare/v0.4.13...v0.4.14) (2021-05-12)


### Features

* **ReplaceUntitledAlert:** Allow user to dismiss the message ([136756e](https://gitlab.com/ensembl/talk/commit/136756eed4c84addbb143162f8a33e34010a331c))

### [0.4.13](https://gitlab.com/ensembl/talk/compare/v0.4.12...v0.4.13) (2021-05-12)


### Features

* **TalkSubjectCKEditor/DocSubjectCKEditor:** Highlight untitled talk/doc for new talk/doc ([3d348c1](https://gitlab.com/ensembl/talk/commit/3d348c1fc36bb5631685b3cb227cb08210fa303e))

### [0.4.12](https://gitlab.com/ensembl/talk/compare/v0.4.11...v0.4.12) (2021-05-12)

### [0.4.11](https://gitlab.com/ensembl/talk/compare/v0.4.10...v0.4.11) (2021-05-11)


### Features

* **DocComment:** Resolve and removing comment thread ([8e27c9a](https://gitlab.com/ensembl/talk/commit/8e27c9a98a52feced1a9d5abef931e67b670e2d5))

### [0.4.10](https://gitlab.com/ensembl/talk/compare/v0.4.9...v0.4.10) (2021-05-11)


### Features

* **ReadOnlyCKEditor:** Show ckeditor in grey indicating not editable yet ([9c135d5](https://gitlab.com/ensembl/talk/commit/9c135d537ac2511707ff07fdb3e79b045a0b8247))

### [0.4.9](https://gitlab.com/ensembl/talk/compare/v0.4.8...v0.4.9) (2021-05-11)


### Features

* **DocSubjectCKEditor / TalkSubjectCKEditor:** Auto focus on ckeditor if untitled talk/doc ([7e0a530](https://gitlab.com/ensembl/talk/commit/7e0a530a6d5a1e1f7b12b365bf427e77d5e713d6))

### [0.4.8](https://gitlab.com/ensembl/talk/compare/v0.4.7...v0.4.8) (2021-05-11)


### Bug Fixes

* **Mentions autocomplete:** Should show all users ([be834ee](https://gitlab.com/ensembl/talk/commit/be834ee7aaf75e0e26c32c17ef6e9c08eccb30d3))

### [0.4.7](https://gitlab.com/ensembl/talk/compare/v0.4.6...v0.4.7) (2021-05-11)


### Bug Fixes

* **DocSubjectCKEditor:** If comment deleted, just load doc page as normal ([5ce7cd8](https://gitlab.com/ensembl/talk/commit/5ce7cd8bad3abec68389f85ae9745303decb8a2f))
* **DocSubjectCKEditor:** If comment is deleted, should not try to find it ([f6273d9](https://gitlab.com/ensembl/talk/commit/f6273d9db45d09020f7f84edae2b80e3d50aa039))

### [0.4.6](https://gitlab.com/ensembl/talk/compare/v0.4.5...v0.4.6) (2021-05-11)


### Bug Fixes

* **ACK:** Consistent ACK styling ([e4d9896](https://gitlab.com/ensembl/talk/commit/e4d9896cb832f77cdc57b5229d18bbeb04cd3ddb))

### [0.4.5](https://gitlab.com/ensembl/talk/compare/v0.4.4...v0.4.5) (2021-05-11)


### Features

* **CacheBuster:** Wait one more page route chng prior busting cache ([856af56](https://gitlab.com/ensembl/talk/commit/856af565c81e63c62eccd5209e4f1f3a84b92a96))

### [0.4.4](https://gitlab.com/ensembl/talk/compare/v0.4.3...v0.4.4) (2021-05-11)


### Features

* **Owners:** Subscribed owners to notification when added ([dedd74e](https://gitlab.com/ensembl/talk/commit/dedd74e3824fba8071dc6b5fda4a98c88c46d2bc))

### [0.4.3](https://gitlab.com/ensembl/talk/compare/v0.4.2...v0.4.3) (2021-05-10)


### Bug Fixes

* **NotsSentMessage:** Fix margins when only notified ([9a6ec13](https://gitlab.com/ensembl/talk/commit/9a6ec139e0d83cbe24444b40dbf1a95237c7eb0b))

### [0.4.2](https://gitlab.com/ensembl/talk/compare/v0.4.1...v0.4.2) (2021-05-10)

### [0.4.1](https://gitlab.com/ensembl/talk/compare/v0.4.0...v0.4.1) (2021-05-10)

## 0.4.0 (2021-05-10)


### ⚠ BREAKING CHANGES

* **ActivityPage:** #802

### Features

* **Activity:** Update on comment create to handle doc ([4484049](https://gitlab.com/ensembl/talk/commit/4484049d612827dd642e674a86a49c933ba76ce6))
* **ActivityPage NewTalkLoadingPage:** URL with space slug ([e1c6c6f](https://gitlab.com/ensembl/talk/commit/e1c6c6f642d80b846c15390cdb55233968c2ca06))
* **Add comment:** Show subscribers before adding comment ([9299a9f](https://gitlab.com/ensembl/talk/commit/9299a9ffa6ba278d3ee9ad79703887f58525216c))
* **Algolia backend code:** Remove ([d099165](https://gitlab.com/ensembl/talk/commit/d099165ba113e91c81c4da0fcb74a073d0399357))
* **Analytic:** Add Heap tracking snippet ([7c74883](https://gitlab.com/ensembl/talk/commit/7c74883caa1e435a79f51d9e9f34dedeb2bf3abb))
* **App:** Behind the scenes reload page 30 mins ([8118cec](https://gitlab.com/ensembl/talk/commit/8118cec0d64b6e1814cde335fccc9e2cb357b19a)), closes [#682](https://gitlab.com/ensembl/talk/issues/682)
* **Approvals:** Refactor Approvals to use ids only ([cee46e2](https://gitlab.com/ensembl/talk/commit/cee46e2cc93e765e15d7c8aa1306e019ef8813ca))
* **Autosave:** Show warning message of other editor ([b91144a](https://gitlab.com/ensembl/talk/commit/b91144a515a790a18657d0aace4d915a9c78f7db)), closes [#653](https://gitlab.com/ensembl/talk/issues/653)
* **Autosave Talk Subject:** Remove beta, full release ([fdfd2ba](https://gitlab.com/ensembl/talk/commit/fdfd2ba5999ed82657801ac7606dedd92cc6dfb9)), closes [#675](https://gitlab.com/ensembl/talk/issues/675)
* **Avatar:** In PeoplePage, SubjectTimestamp, NewTalkAvatar ([cbcb202](https://gitlab.com/ensembl/talk/commit/cbcb2023c15450623f69c986d15836a0e8cb894f))
* **AvatarsDropdown:** Row styling ([c9f4632](https://gitlab.com/ensembl/talk/commit/c9f4632c52f66ce0855125159462151ea964267f))
* **Browser tab:** Title in browser tab ([e3360e2](https://gitlab.com/ensembl/talk/commit/e3360e284205b3e309b137ca1792ca0ab66dc953)), closes [#570](https://gitlab.com/ensembl/talk/issues/570)
* **CacheBuster:** Add cache buster by checking meta version ([51d13a0](https://gitlab.com/ensembl/talk/commit/51d13a00b4c63e5650b82ed664d116b87cedacce))
* **ckeditor:** Update to pay subscription ([4a98de1](https://gitlab.com/ensembl/talk/commit/4a98de18356f67062e242c8bb52fb9d06003a8e5))
* **CKeditor:** Use CKeditor styles for talk subject and comment ([0ed28a0](https://gitlab.com/ensembl/talk/commit/0ed28a02f1ab133e35f22a30b4dd70c0529ed151)), closes [#534](https://gitlab.com/ensembl/talk/issues/534)
* **CKEditor:** More powerful table functions ([64dd509](https://gitlab.com/ensembl/talk/commit/64dd5093090466534b6b45654bc2a9566fb1806b)), closes [#568](https://gitlab.com/ensembl/talk/issues/568)
* **CKeditor collaborative comments:** Add avatar ([6dc6a50](https://gitlab.com/ensembl/talk/commit/6dc6a506b6bf0836019aeb2217bf5b65a1524b51))
* **CKEditor comments:** Write to Firestore ([7720582](https://gitlab.com/ensembl/talk/commit/7720582e9b9022548653dbe293014625794f45f3))
* **CKeditor image toolbar:** Add it because it was missing ([fa8803b](https://gitlab.com/ensembl/talk/commit/fa8803b3ad95d86f0f67d9fe60560833208f9b8e))
* **CKeditor placeholder:** Inform about drag and drop images ([a7a8f11](https://gitlab.com/ensembl/talk/commit/a7a8f116f6b41d9b999154a256aed1d26f6d6374)), closes [#710](https://gitlab.com/ensembl/talk/issues/710)
* **ckeditortoken:** Get information from firestore ([429d6fe](https://gitlab.com/ensembl/talk/commit/429d6fe9cf2dc6a4fdce1c2e34b396cbbaaa437c))
* **Cloud functions:** Copy users to spaces ([209a1ff](https://gitlab.com/ensembl/talk/commit/209a1ffecb5cf7eb01aeb373723ce2c23a1cee52))
* **Cloud functions:** UpdateTalkTabs/Matches only when necessary ([f6ce030](https://gitlab.com/ensembl/talk/commit/f6ce03027e59f58e2b10904e2a150fe1249713c6))
* **Comment:** CKeditor for comments ([7432ad6](https://gitlab.com/ensembl/talk/commit/7432ad664a6b6a9358470d07c0691cd02d6e0f9f)), closes [#531](https://gitlab.com/ensembl/talk/issues/531)
* **Comment:** LocalStorage for Comment with CKeditor ([fd77690](https://gitlab.com/ensembl/talk/commit/fd7769024649819f653299f1238c0364ee03e207)), closes [#546](https://gitlab.com/ensembl/talk/issues/546)
* **Comment sent message:** Harmonized subscribed and notified message ([ac6eb1f](https://gitlab.com/ensembl/talk/commit/ac6eb1f295a76d207c11551f76a8f8842bb62f13))
* **CommentNotify:** Harmonized box for everyone, subscribers, and none ([917de8c](https://gitlab.com/ensembl/talk/commit/917de8c1dd6a0426551c0fea74d3a57e25fed7d8))
* **Comments:** @ mention users in ckeditor comments ([cea119b](https://gitlab.com/ensembl/talk/commit/cea119be57f45f0eb3b63a270adefb835796ff7d)), closes [#563](https://gitlab.com/ensembl/talk/issues/563)
* **ContentEditor:** Allow mention user in space ([be1f1b6](https://gitlab.com/ensembl/talk/commit/be1f1b68bf0e150c8896b6edf1a9d0da17e761cb))
* **ContentEditor:** Collaborative Editing available for all ([c3d5b06](https://gitlab.com/ensembl/talk/commit/c3d5b06546519463fae10625d590e7ee869f6d59))
* **ContentEditor:** Placeholder for ContentEditor ([1fb522e](https://gitlab.com/ensembl/talk/commit/1fb522e6e864ccad8e79c555718842a56c1784ab)), closes [#543](https://gitlab.com/ensembl/talk/issues/543)
* **ContentEditorBeta:** Add cloud function to get access token ([fb99a0a](https://gitlab.com/ensembl/talk/commit/fb99a0a758319f03b3f2522dfc9d71d5bc2dd5ff))
* **ContentEditorBeta:** Show it to Ensembl by default ([fdc42b0](https://gitlab.com/ensembl/talk/commit/fdc42b03f08ad603c72ab902166e3a1c062a8110))
* **copied-users:** Copy user when name or email or avatarUrl changes ([cbb92af](https://gitlab.com/ensembl/talk/commit/cbb92af17e40efe0c0d534df592733caa562e3e0))
* **Create space:** Loading message tells users to wait ([2b5d31d](https://gitlab.com/ensembl/talk/commit/2b5d31d92b2561e91ab80c3ba098c321f70a7685))
* **createUserOnFirebaseAuth:** Update FIrebase auth user with name and photoURL ([718586e](https://gitlab.com/ensembl/talk/commit/718586ea3ff42eaa860acecc39ad83cfde3b5913)), closes [#513](https://gitlab.com/ensembl/talk/issues/513)
* **createUserOnFirebaseAuthCreate:** Get secured names of invitees and write to user ([cbd7d7f](https://gitlab.com/ensembl/talk/commit/cbd7d7f56cca54dd9efe7ebfa8fad2ff4d843ead))
* **Doc:** Inline comment threads ([935d9b3](https://gitlab.com/ensembl/talk/commit/935d9b3c4dfdd296a0d2961662b86239a0a11552))
* **Doc:** Shows inline comment when specify hash ([cd731aa](https://gitlab.com/ensembl/talk/commit/cd731aad9abc2f9554a024f5dc7e8948069075a8))
* **Doc:** Subscribed user to doc when mentioned in comment ([f5692a0](https://gitlab.com/ensembl/talk/commit/f5692a04a771f5d1ec8d54f9cae832a1b36f0780))
* **DocEditor:** Focus on inline comment when hash present ([f1994dc](https://gitlab.com/ensembl/talk/commit/f1994dc50362bcad32c506119e10bfeea9b47208))
* **DocEditor:** Scroll doc comment into view ([d583fa8](https://gitlab.com/ensembl/talk/commit/d583fa86228842b800d3caef6a015ae05081aa48))
* **Dropdown:** Set scrollbar always appear in dropdown ([2dcd9f9](https://gitlab.com/ensembl/talk/commit/2dcd9f9c684b55efe13d907de6590ec3a4c42db9))
* **EditComment:** Allow image attachment on newTalk ([7967c27](https://gitlab.com/ensembl/talk/commit/7967c27517a328179f6cb612e545f3238c56e09b))
* **EditSubjectBeta:** Autosave in beta for talk subject ([72e7e0b](https://gitlab.com/ensembl/talk/commit/72e7e0b6c4b3ca637e5b12c3ba0953595bfd88c0)), closes [#578](https://gitlab.com/ensembl/talk/issues/578)
* **EditSubjectButtons:** Display  to be explicit ([6d4cb72](https://gitlab.com/ensembl/talk/commit/6d4cb7291d2e78d7a227415ac70873946886d96f)), closes [#490](https://gitlab.com/ensembl/talk/issues/490)
* **Email:** Include threadId in doc comment notification ([0eb1a26](https://gitlab.com/ensembl/talk/commit/0eb1a2682ec84ca5fec4cfc28a77f68a6b363d90))
* **EmailNot:** Send email notification when doc comment added ([3fba0bf](https://gitlab.com/ensembl/talk/commit/3fba0bf0066033ab098c6d07436e88d0f625802f))
* **firebaseui-web:** Install for Google sign in ([241c906](https://gitlab.com/ensembl/talk/commit/241c906dfd8178e7ded9ae1d1d449216806ff9d3))
* **functions:** Add util functions generate JWT for ckeditor ([7f6ea3f](https://gitlab.com/ensembl/talk/commit/7f6ea3f686c0b1bc4726e30612c2e6f4cfd84b45))
* **functions:** Remove Talk subject notification ([9d9b50f](https://gitlab.com/ensembl/talk/commit/9d9b50ffdd83ea11d1f182d1d1bd2a8842ca71fb))
* **Header:** Create responsive navigation for TalksPage ([ce26490](https://gitlab.com/ensembl/talk/commit/ce26490cb90838554425ce5f905b8c4f35a8df59))
* **Heap:** Add userId to heap user identifier ([fdc16be](https://gitlab.com/ensembl/talk/commit/fdc16be8898b6db8c61a8d13e63fcba843060895))
* **Heap:** Reset Heap identity when signed out ([e5525ce](https://gitlab.com/ensembl/talk/commit/e5525ce26084362ed3362cb3e9fa7042d3f10326))
* **Heap:** Track spaceName and email ([ff94ae8](https://gitlab.com/ensembl/talk/commit/ff94ae813e815f4e4c4d730e6635c10ecd34e6e9))
* **ImageUpload:** Allow custom image upload on profile page ([7567bf9](https://gitlab.com/ensembl/talk/commit/7567bf9769c36884a600ffae497a6a46a7b1f763))
* **Invite:** Focus on invite input when clicked on invite ([10d3c71](https://gitlab.com/ensembl/talk/commit/10d3c7192d42e28aded3c62105438e8e1860e0f7))
* **Invitees:** Invite existing user to a new space ([0e02d99](https://gitlab.com/ensembl/talk/commit/0e02d991db70e7f08d1342cb8bc26d74796e561d))
* **IsClosingToggle:** Remove spinner since not using Algolia ([ca55871](https://gitlab.com/ensembl/talk/commit/ca55871e1faf73c6c7498656c4b22616ba74c489))
* **IsSaving:** Move Autosaves/Saving/Saved UI to bottom left ([7b2a30d](https://gitlab.com/ensembl/talk/commit/7b2a30d0f23a8f2b7a8f50207162cd8ec7107e17))
* **IsSaving:** Move back to top right and include loading message ([a7ec77e](https://gitlab.com/ensembl/talk/commit/a7ec77e0671d9d50bda97d000dba1e3d8dd3cb61))
* **IsSaving:** Remove other person saving name ([09219e1](https://gitlab.com/ensembl/talk/commit/09219e1ca3e4267043fa254cd4a2733ab76d3dab))
* **Label dropdown:** Use default styling ([fd89772](https://gitlab.com/ensembl/talk/commit/fd897729dddff0734abc8e852616220bcc37cc26))
* **Labels:** Clicks go to docs or talks depending on context ([1d14d0b](https://gitlab.com/ensembl/talk/commit/1d14d0be7d1727d7145079d770be9d0bf189a92c))
* **Labels:** Filter by labels on search page ([a86f5fa](https://gitlab.com/ensembl/talk/commit/a86f5fa029c6d04d6f59e5ef0aa58757ff161e2c))
* **Labels Page and People Page:** Use space name in url ([beed82f](https://gitlab.com/ensembl/talk/commit/beed82f2dfd35c20d423eb38f16f445c55da2126))
* **LabelsDropdown:** Remove space name to save horizontal space ([a872945](https://gitlab.com/ensembl/talk/commit/a872945abef66a6e98c090743aee5b4740013147))
* **LabelsDropdown:** Scrollabel LabelsDropdown ([7a5a48a](https://gitlab.com/ensembl/talk/commit/7a5a48a041525874c978434807352da4b45776e9))
* **lastVisitedSpaceName:** Load non talk page based on lastVisitedSpaceName ([c0b270f](https://gitlab.com/ensembl/talk/commit/c0b270fd453cdcc0254904a9fdd5f0ffead64eaf))
* **lastVisitedSpaceName:** Update spaceName in user doc when user visit page ([0289294](https://gitlab.com/ensembl/talk/commit/028929486178ec962ee110a08623c5fa256a89b7))
* **Layout:** Move TalksPage navigation to left side ([237dd88](https://gitlab.com/ensembl/talk/commit/237dd882deec903e46e3caaad77b9726b6adb950))
* **Left nav:** Add other links to left nav ([eeffee4](https://gitlab.com/ensembl/talk/commit/eeffee43b19c505546eaa4d9b4ed5c948ed08e50)), closes [#723](https://gitlab.com/ensembl/talk/issues/723)
* **Left nav:** Left nav for new talk, people, labels, workflow ([9266d1c](https://gitlab.com/ensembl/talk/commit/9266d1c1d19cc723acfa300eec70c57e5ae303b4)), closes [#726](https://gitlab.com/ensembl/talk/issues/726)
* **Left nav:** Remove docs from left nav ([650242f](https://gitlab.com/ensembl/talk/commit/650242fe3ee410cce9b7daec03ea91ed1faef025))
* **Left Nav:** More prominent new talk and new doc ([70e404f](https://gitlab.com/ensembl/talk/commit/70e404f33e26149d4d7c4b76ee0a14d6374aa893))
* **Left Nav:** Remove Home nav ([b03d10f](https://gitlab.com/ensembl/talk/commit/b03d10f63838f838b91d250095f4488fbd4a81fd))
* **Left sidebar:** Always show space ([6c8be5b](https://gitlab.com/ensembl/talk/commit/6c8be5bca1780d84227ec70f4852c5b843a7afbd))
* **Left Sidebar:** Don't wait for left sidebar to render ([f66b1f9](https://gitlab.com/ensembl/talk/commit/f66b1f91c5be9e7af87602a0f8321a7c86d169a5)), closes [#728](https://gitlab.com/ensembl/talk/issues/728)
* **LeftNav:** Improved nav for talks and docs ([2e5de52](https://gitlab.com/ensembl/talk/commit/2e5de526b90c95c53b784d6304d74db165962a25))
* **LeftNav:** Invite user ([77c0445](https://gitlab.com/ensembl/talk/commit/77c0445e85ff2130a679b81b451576ac8a70dbae))
* **LoadingPage:** Loading page as soon as possible ([2802864](https://gitlab.com/ensembl/talk/commit/28028645e058b6064db16003e848a33ea3a055c6))
* **Login:** Initial work on Login with Microsoft ([72dbe8e](https://gitlab.com/ensembl/talk/commit/72dbe8ebdb9bca758b027586d32720e4b1150d76))
* **Login:** Login with email and password ([27fd91a](https://gitlab.com/ensembl/talk/commit/27fd91a317330e5fb94ba78ba2555eb884dc94bd)), closes [#514](https://gitlab.com/ensembl/talk/issues/514)
* **Login:** Reset password ([eb5f54d](https://gitlab.com/ensembl/talk/commit/eb5f54dc23df8ce6f6cb5c90f38866a730eaa62b)), closes [#521](https://gitlab.com/ensembl/talk/issues/521)
* **Login and Sign Up:** Separate pages for login and signup ([28a85a4](https://gitlab.com/ensembl/talk/commit/28a85a4865895f4a374c83e1c951a8091218200b))
* **maintenance:** Set up a maintenance html page ([d89225d](https://gitlab.com/ensembl/talk/commit/d89225d53d1d4af5cf94f44bf9d8f16d91d3d69d))
* **Mention:** Add basic mention for Talk subject ([50c0f42](https://gitlab.com/ensembl/talk/commit/50c0f422055be71be61dbf7fc8dd40865f6d07c4))
* **Mention:** Add custom attributes to mention ([fba4388](https://gitlab.com/ensembl/talk/commit/fba43882395a97eb2dca14f487c7d4f33aa07aad))
* **Mention:** Add mention with newTalk for ckeditor ([7734484](https://gitlab.com/ensembl/talk/commit/773448486f99d4f875f78ba2a276fc325c4050af)), closes [#545](https://gitlab.com/ensembl/talk/issues/545)
* **Mention:** Connect MentionActions with ckeditor ([2bcb170](https://gitlab.com/ensembl/talk/commit/2bcb170f205532bd38546a75dcdaccae71203ed5))
* **Mention:** Release mention everyone to all spaces ([d49647f](https://gitlab.com/ensembl/talk/commit/d49647fe82996a64b20386807b1db27c75e7cd0b))
* **Mention:** Upcast HTML elem to ckeditor model ([4491550](https://gitlab.com/ensembl/talk/commit/449155011da42ffab6c66c97869ae0e26f58c95c))
* **MentionAction:** Avatar in MentionAction ([8e8564b](https://gitlab.com/ensembl/talk/commit/8e8564bf5b92f674df164b4f771c3e675765bcff))
* **mentionEveryone:** Construct mention object if everyone is mentioned ([ad1de2a](https://gitlab.com/ensembl/talk/commit/ad1de2afd930e1cacc8f0226875dcf417c6752ee))
* **MentionEveryone:** Notify everyone in comment ([98bfac9](https://gitlab.com/ensembl/talk/commit/98bfac97f2e5c4f4681459a629198b6da39d4102))
* **Mentions:** Create and pass mentions object for doc cmt ([93050fd](https://gitlab.com/ensembl/talk/commit/93050fd72bc86c25808b4bdd7f4d305de394ec10))
* **Nav and top bar:** Show top bar and left sidebar when page is loading ([20448aa](https://gitlab.com/ensembl/talk/commit/20448aa84d7024ce036ad6ad5fbc0abb5e07689f)), closes [#737](https://gitlab.com/ensembl/talk/issues/737)
* **New Space Page:** Accesible in space dropdown ([c668368](https://gitlab.com/ensembl/talk/commit/c668368e60f87f5d9a186fb5430367c06f696934))
* **Newest Comment:** Newest comment updates writtenAt ([eb19778](https://gitlab.com/ensembl/talk/commit/eb19778e67226438bbd9f6bc50994e4d19a9998f))
* **NewSpacePage:** Explain to user what is a space ([db63bac](https://gitlab.com/ensembl/talk/commit/db63bac3726ec0de215ac7d2164c4f05080b6818))
* **NewTalkStrip:** Encourage to invite users if only user ([215ecd3](https://gitlab.com/ensembl/talk/commit/215ecd357282ae85bca2fceabe1d329b30aefb5a))
* **Notifications:** Account for spaceName ([1730a67](https://gitlab.com/ensembl/talk/commit/1730a672a65f6a560c83010bdf16bbaa369721ea))
* **Nots:** Update template for document comment ([d0cd2b5](https://gitlab.com/ensembl/talk/commit/d0cd2b55dc31eac29e48888741049f0e07d3068b))
* **Owner:** Assign Talk Owner ([1eea73e](https://gitlab.com/ensembl/talk/commit/1eea73e12c69dd17762081b4d1522c5723deddda))
* **Owner:** One click to add yourself as owner ([63681f2](https://gitlab.com/ensembl/talk/commit/63681f2871a6fceff4ee15e07602c2f3b1ff1d1f))
* **Owners:** Consistent styling with approvals ([876d505](https://gitlab.com/ensembl/talk/commit/876d505e56fc217f823d950715eb00f6e7065a7f))
* **Owners:** Refactor Avatar and AvatarSet for Owners ([22093a4](https://gitlab.com/ensembl/talk/commit/22093a4c420dca37618e2d49c2715e1b524ed965))
* **Owners:** Style owners and refactor to be same as approvals ([41ab15d](https://gitlab.com/ensembl/talk/commit/41ab15d6f5b6c6c5327629f908c8ccec7d643d23))
* **PageLoading:** Keep showing loading until get firestore user ([d5df33a](https://gitlab.com/ensembl/talk/commit/d5df33ad1e261e07fc595a2cb2536e4e69ef0ee0))
* **Pagination:** Style pagination ([b7cbfae](https://gitlab.com/ensembl/talk/commit/b7cbfaeb2187a82d8919c7b3757960128ee73fc7))
* **Pagination Controls:** More space ([dd3b12f](https://gitlab.com/ensembl/talk/commit/dd3b12f4381809178ff9b9bd8bea38e456ee411f))
* **ProfileImage:** Download initials as profile image ([bd8674c](https://gitlab.com/ensembl/talk/commit/bd8674c93092d39fb1e761af17dd098ac049e0c6))
* **ProfilePage:** In left nav ([d2aba5b](https://gitlab.com/ensembl/talk/commit/d2aba5b6237f5b4862b5d9afc9d864fa04a80423))
* **ProfilePage:** Redirect to profile page ([c6067cc](https://gitlab.com/ensembl/talk/commit/c6067cc050dd4d05ae0634cd2cbbc6da2effc4ed))
* **ProfilePage:** Style and flows ([c23fab3](https://gitlab.com/ensembl/talk/commit/c23fab33c906bd281554ff578e9b5dd0743a2fa7))
* **ProfilePage and NewSpacePage:** Sign out links if stuck on these pages ([a9b191d](https://gitlab.com/ensembl/talk/commit/a9b191d905bc9d77f34eb8b019e74dd25eb21c91))
* **Release:** Use standard-version for release ([42adba0](https://gitlab.com/ensembl/talk/commit/42adba0308659766b4e50e0d5b169a2cb128cacb))
* **sendCmtAddedNots:** Tweak doc comment wording ([e3187ff](https://gitlab.com/ensembl/talk/commit/e3187fff6c5b7b16f83cd5f8391f96f3d8bd5c0e))
* **sendTalkSubjectUpdatedNots:** Send max every 7 mins ([a28aacf](https://gitlab.com/ensembl/talk/commit/a28aacf0b03a4af9829bb0c180ed74583f7c76b9)), closes [#640](https://gitlab.com/ensembl/talk/issues/640)
* **sendTalkSubjectUpdatedNots:** Send nots every 5 minutes ([f428e55](https://gitlab.com/ensembl/talk/commit/f428e5538b3a7254ecabc15fbc892e69e954d027)), closes [#642](https://gitlab.com/ensembl/talk/issues/642)
* **sidebar:** Right sidebar hide before left sidebar on resizing ([3332883](https://gitlab.com/ensembl/talk/commit/33328837f4d583f61224543e7c42728ae73677fb))
* **Sidebar:** Raise upload file limit to 500 MB ([3ee3e51](https://gitlab.com/ensembl/talk/commit/3ee3e510e8a62c8325496a74a52db80b2df3b451))
* **Sidebar Files:** Prevent overwriting files ([b652e9d](https://gitlab.com/ensembl/talk/commit/b652e9d5356481ab00744c6b30597d0a1aceb5cc))
* **Sidebar Files:** Turn on for CBCR Worship ([e4bdfaf](https://gitlab.com/ensembl/talk/commit/e4bdfaf32e43152430b4466d36dba3d9ab7280e9))
* **Sidebar upload:** Show percentage ([839b6d5](https://gitlab.com/ensembl/talk/commit/839b6d5dcecfea0c00409733b12d9c7ef47167ae))
* **SidebarFiles:** Delete file ([9c5cdc9](https://gitlab.com/ensembl/talk/commit/9c5cdc9149cf5265e90d1e23f5bebf4fcb75610f))
* **SidebarFiles:** Show list of links to sidebar files ([6bc78f0](https://gitlab.com/ensembl/talk/commit/6bc78f019c9f5d1492e69c17d3935da5447c6e13))
* **SidebarFiles:** Upload ([1f8048c](https://gitlab.com/ensembl/talk/commit/1f8048c92de582d85961e3bb544fa55c58ed93e0))
* **Sign in page:** Sign in and sign up with email and password ([26798c7](https://gitlab.com/ensembl/talk/commit/26798c79f12b820f31cd1398c9082a416be40fdd))
* **Sign In page:** Passwordless sign in ([1648387](https://gitlab.com/ensembl/talk/commit/16483874801c6f6d317b5306e6fd849c533d5685))
* **Sign In Page:** Should redirect to talks page if logged in ([680b7ca](https://gitlab.com/ensembl/talk/commit/680b7ca30d9d70a875343877974c942c7171980e))
* **Sign out link:** Style sign out link as a link ([27b4595](https://gitlab.com/ensembl/talk/commit/27b4595044cebd95ae092d84a3c9c6637267dbec)), closes [#738](https://gitlab.com/ensembl/talk/issues/738)
* **SignIn Page:** Sign In Page with Google, Microsoft, & Email Link ([3a98ae5](https://gitlab.com/ensembl/talk/commit/3a98ae5d0fcb6828d505d27d915c64f19d09b870))
* **SingIn:** Display tos and privacy policy Url ([7aa7a16](https://gitlab.com/ensembl/talk/commit/7aa7a163c5323bd2e4e8923eedbf0e5e5512ee90))
* **Space:** Create space ([34e768e](https://gitlab.com/ensembl/talk/commit/34e768e093bd960a58354a938d0c05205f685b4f))
* **Space:** Track when created at ([9fa1eef](https://gitlab.com/ensembl/talk/commit/9fa1eefe4e56feae4bd992b334880f22f86d26a8))
* **spaceNames:** Handle multiple spaces ([235be6f](https://gitlab.com/ensembl/talk/commit/235be6f5d17287b067862729071096d11625c382))
* **Spaces:** Switch spaces ([1f6f5ca](https://gitlab.com/ensembl/talk/commit/1f6f5ca442bfe00954937eade44a0af3919d7587))
* **SpaceSelectDropdown:** Always show dropdown since need to start new space ([88b6f92](https://gitlab.com/ensembl/talk/commit/88b6f926cf7e875dff05b8bd731b19f0e2de06b9))
* **StorageRule:** Update storage rule ([7526c62](https://gitlab.com/ensembl/talk/commit/7526c625a86d08ab724430747849de93d0e7ea8a))
* **style:** Collapse right sidebar at 1100px ([87934da](https://gitlab.com/ensembl/talk/commit/87934da1905c996ee5b7f0db4b896d3cab84206d))
* **Subject Autosave:** Show saving / saved feedback ([6a286a6](https://gitlab.com/ensembl/talk/commit/6a286a6796f9f6b573c767e62453d6ea8e9539f1)), closes [#654](https://gitlab.com/ensembl/talk/issues/654) [#656](https://gitlab.com/ensembl/talk/issues/656)
* **SubjectHeader:** Make subject header responsive ([6b14493](https://gitlab.com/ensembl/talk/commit/6b14493fb6717e1032a7cea7690222510eeea43f))
* **SubjectHeader:** Style elements, centering them vertically ([7492f37](https://gitlab.com/ensembl/talk/commit/7492f37aab836e006ba0d3e20a6f30d47be38cec)), closes [#689](https://gitlab.com/ensembl/talk/issues/689)
* **Tabs:** Reorder tabs to have both talks and docs ([bcdf5a2](https://gitlab.com/ensembl/talk/commit/bcdf5a216df54364f49aeae3934e791bd25a9a52))
* **Tabs:** Style inactive tabs as links ([92b4abd](https://gitlab.com/ensembl/talk/commit/92b4abdb884f2bb937dd46c45b8a80298c77b87a))
* **Talk:** Add left nav drawer to TalkPage ([612b0de](https://gitlab.com/ensembl/talk/commit/612b0deb4e042e3ba46714556e30f0f21a8bb857))
* **Talk:** Is Closed Banner ([e90ad83](https://gitlab.com/ensembl/talk/commit/e90ad8354108ee6f83510803a88aa9c1e86e9173)), closes [#721](https://gitlab.com/ensembl/talk/issues/721)
* **Talk:** Put talk controls to sidebar ([af11ea1](https://gitlab.com/ensembl/talk/commit/af11ea1ac584217b19343906934fb16d01eb6252))
* **Talk Subject:** Notify subscribers of changes button ([071ed2e](https://gitlab.com/ensembl/talk/commit/071ed2e9966053e6c824d33b4a2c862251e2318d))
* **TalkPage:** Add right side bar ([ca31a8a](https://gitlab.com/ensembl/talk/commit/ca31a8a2dc716234409946b8f3bdafad7fa8d803))
* **TalkPage:** Fix offset to autoscroll to correct top of talk ([4bb0ac4](https://gitlab.com/ensembl/talk/commit/4bb0ac43d509947f4cfe539524e83b6de2d368b0)), closes [#720](https://gitlab.com/ensembl/talk/issues/720)
* **TalkPage:** Message to encourage user to change title ([02b0e21](https://gitlab.com/ensembl/talk/commit/02b0e2162cd8f7e9387d81aa6649787533348bfc))
* **TalkPage:** No talk found wrapped with nav and topbar ([afada15](https://gitlab.com/ensembl/talk/commit/afada15993a40d986d6d7bfb8f2a05c87051e7a0)), closes [#735](https://gitlab.com/ensembl/talk/issues/735)
* **TalkPageSidebar:** Refactor elements and spacing ([3356c79](https://gitlab.com/ensembl/talk/commit/3356c79946eb4ea68db617a0e5adf17bc758a963))
* **Talks page:** Labels for URL params ([9fd12f9](https://gitlab.com/ensembl/talk/commit/9fd12f96d14f915ce7931aa16bcb37836e3c0da2))
* **Talks Page:** Order by writtenAt ([4ff2354](https://gitlab.com/ensembl/talk/commit/4ff23545d0626823fc78da959514bce7dccaf032))
* **Talks Page:** URL Params for tabs ([3d131ca](https://gitlab.com/ensembl/talk/commit/3d131caa77172508f78580e6dea93ddf7a69cf62))
* **Talks Page and Talk Page:** Space name in URL ([f420cab](https://gitlab.com/ensembl/talk/commit/f420cab3108f226aee84d6a6d797e8160a1900ca))
* **TalkSelect:** Adjust padding for TalkSelect ([5a754be](https://gitlab.com/ensembl/talk/commit/5a754be1f5ed8061b106895c93cdea5224f76b94))
* **TalkSelect Preview Comment:** CKEditor styles in TalkSelect Preview Comment ([f20b5db](https://gitlab.com/ensembl/talk/commit/f20b5dbdbfa3f15291f7524860df94949d29a600)), closes [#596](https://gitlab.com/ensembl/talk/issues/596)
* **TalksPage:** Consistent vertical spacing between controls ([8f51f18](https://gitlab.com/ensembl/talk/commit/8f51f184e3e7e1f81f2d22e35491ff6fde5deee6))
* **TalksPage:** ContentEditorBeta should not flash into view ([92f1039](https://gitlab.com/ensembl/talk/commit/92f103903f6831c9acfe266e1adaeebe976fae47))
* **TalksPage:** Don't show toDelete talks ([32d3670](https://gitlab.com/ensembl/talk/commit/32d36706632af22f7b6e7a043bf7c32f671c67fc))
* **TalksPage:** Firestore talks - Beta ([69ce1cb](https://gitlab.com/ensembl/talk/commit/69ce1cb02bae6763e4e85442b5096061ef8568f1))
* **TalksPage:** Pagination ([7ab50e2](https://gitlab.com/ensembl/talk/commit/7ab50e246b6376e1a3775bfa01267667cc28334d))
* **TalksPage:** Remove beta flag to use Algolia all the time ([c1bcbeb](https://gitlab.com/ensembl/talk/commit/c1bcbeb6f20ded9f05745983c4917a8668f0bfbf))
* **TalksPage:** TalkRow (replacing TalkSelect) ([ec350e4](https://gitlab.com/ensembl/talk/commit/ec350e4d86a3a6d9e3984d7aeee4296ac149d549))
* **TalksPage beta:** Search titles ([2699798](https://gitlab.com/ensembl/talk/commit/2699798acf84c45c57349699de4902ac176536e8))
* **TalksPageSearch:** Autofocus on component mount ([d0570a1](https://gitlab.com/ensembl/talk/commit/d0570a11905af03f54072cb745f26ffafec119c6))
* **TalkSubject:** Delete talk link for autosave UI ([03434a4](https://gitlab.com/ensembl/talk/commit/03434a4b306d3b0557e90eff6ff19f9e0e1c9812)), closes [#657](https://gitlab.com/ensembl/talk/issues/657)
* **Top bar:** Show space name when talk page loading ([c048286](https://gitlab.com/ensembl/talk/commit/c04828664cbeca55a8e5c86ed3a42e4371e40dd8)), closes [#736](https://gitlab.com/ensembl/talk/issues/736)
* **Update Profile Page:** Update name ([dfec6ab](https://gitlab.com/ensembl/talk/commit/dfec6abd7237587b7cbd34c33cce4d909dd4676f))
* **UpdateProfilePage and NewSpacePage:** Loading UI ([c8155c4](https://gitlab.com/ensembl/talk/commit/c8155c4e1c7d9bba3d0e578f4e8dd82c374afad0))
* **UploadFile:** Allow delete uploaded file ([f4e1d2c](https://gitlab.com/ensembl/talk/commit/f4e1d2c3defcb61ba41b032739529d387ebf6c96))
* **UploadFile:** Create frontend component for fileupload ([1a5331d](https://gitlab.com/ensembl/talk/commit/1a5331dfc76ae7ab52a2c75135e01473bf0ba090))
* **UploadFile:** Style upload button ([244d285](https://gitlab.com/ensembl/talk/commit/244d28547567e42ada2c57d6145dc010335e20d1))
* **UploadFile:** Upload file to firebase storage ([d8f46f2](https://gitlab.com/ensembl/talk/commit/d8f46f2e161a8829d41bc59b29556e8c4c61f670))
* **Workflow:** Drag handle for stages ([351cde5](https://gitlab.com/ensembl/talk/commit/351cde518cf60c9f1b1ffd6b2f280513fe5e9ec2)), closes [#709](https://gitlab.com/ensembl/talk/issues/709)
* **Workflow:** Persist reordered order to database ([b91745f](https://gitlab.com/ensembl/talk/commit/b91745f739be3ec15bece1d3a9d298597e139a6c)), closes [#699](https://gitlab.com/ensembl/talk/issues/699)
* **Workflow:** Reorder existing workflow stages w/o persiting ([c3308bb](https://gitlab.com/ensembl/talk/commit/c3308bbdd9f21e89e3358a706d72a28ca9e8cd94)), closes [#706](https://gitlab.com/ensembl/talk/issues/706)
* **Workflow Page:** Better looking arrow with SVG ([69a5fc0](https://gitlab.com/ensembl/talk/commit/69a5fc00cb2c4ffb60a93c61c90c8cc788f2d8c3)), closes [#700](https://gitlab.com/ensembl/talk/issues/700)
* **Workflow Page:** Restyle workflow page so arrow sits below title ([be5cf49](https://gitlab.com/ensembl/talk/commit/be5cf49f9a3c9ac8310b78223a0c6e902056298a)), closes [#729](https://gitlab.com/ensembl/talk/issues/729)
* **Workflow Stage:** Add new workflow stage ([186426e](https://gitlab.com/ensembl/talk/commit/186426ead5ca8e4bbfe9300c3fe364a309a14af3)), closes [#695](https://gitlab.com/ensembl/talk/issues/695)
* **Workflow Stage:** Edit existing stage ([cdca179](https://gitlab.com/ensembl/talk/commit/cdca1794cfdd18db01260a3f3131181c7dafd8eb)), closes [#696](https://gitlab.com/ensembl/talk/issues/696)
* **Workflow stages:** Style workflow stages for dnd ([4e72ed7](https://gitlab.com/ensembl/talk/commit/4e72ed775df8d5681926cb958b261e0d45a0bd5a)), closes [#708](https://gitlab.com/ensembl/talk/issues/708)
* **WorkflowPage:** Don't show arrow for mobile ([bd80646](https://gitlab.com/ensembl/talk/commit/bd80646aa68d5dfaae60842d3c2073ee567f734f)), closes [#694](https://gitlab.com/ensembl/talk/issues/694)
* **writtenAt:** Add date field to when creating/updating talk/doc ([e746bbb](https://gitlab.com/ensembl/talk/commit/e746bbb6a44d6d6884dccfaa3ba3dfdb2d051179))
* CKEditor real-time collaboration hide behind feature flag (Initial Work) ([44bd8bb](https://gitlab.com/ensembl/talk/commit/44bd8bb1f1fec19f9c0ba4c20576feb9f79a1de0))
* **Activity:** Activity on new comment ([efcafd8](https://gitlab.com/ensembl/talk/commit/efcafd8461b35e0c680e4e64008c8816ee5f77b5)), closes [#804](https://gitlab.com/ensembl/talk/issues/804)
* **Activity:** Activity Page is default page for mobile screens ([831894f](https://gitlab.com/ensembl/talk/commit/831894f33d5fa5e1c39a8b65977c7f9a7a27d7e5)), closes [#806](https://gitlab.com/ensembl/talk/issues/806)
* **Activity:** Style activity page and show it in nav ([4155a42](https://gitlab.com/ensembl/talk/commit/4155a42f7738fd7f8c6f66982dc8ee807a9789b4)), closes [#805](https://gitlab.com/ensembl/talk/issues/805)
* **Activity feed:** Talk resolve and re-open in activity feed ([c8ed415](https://gitlab.com/ensembl/talk/commit/c8ed4150c0c51f166f2212c25167bf76588f3f80)), closes [#803](https://gitlab.com/ensembl/talk/issues/803)
* **Activity Page:** Empty state ([d74601f](https://gitlab.com/ensembl/talk/commit/d74601fff99029ee90e944438a0f021b212facd5)), closes [#807](https://gitlab.com/ensembl/talk/issues/807)
* **ActivityPage:** New page ([bc562f6](https://gitlab.com/ensembl/talk/commit/bc562f63ed814a7eb98b7ce600f2a64bddc218be)), closes [#802](https://gitlab.com/ensembl/talk/issues/802)
* **CKEditor:** Different default heights for comment/subject/doc ([32a9e29](https://gitlab.com/ensembl/talk/commit/32a9e2998cb8087380ad387b37039338cc044c94))
* **Comment notifications:** Try sender name format with a dash ([654a48a](https://gitlab.com/ensembl/talk/commit/654a48a9aab2343121ae34dcc514660a6eacbd82)), closes [#787](https://gitlab.com/ensembl/talk/issues/787)
* **Delete Talk:** Move to talk sidebar ([51bcd25](https://gitlab.com/ensembl/talk/commit/51bcd25333b6ce06fac63ec25df365fed226e60a)), closes [#732](https://gitlab.com/ensembl/talk/issues/732)
* **Doc:** A doc is a talk with a flag ([8675ddd](https://gitlab.com/ensembl/talk/commit/8675ddd2f6302756a22323c742faf22b0d903ca9))
* **Email:** Make email notification sender more obvious ([2a2f46e](https://gitlab.com/ensembl/talk/commit/2a2f46e1c1155817a57318cd40bd4f317950bba3))
* **Email notification:** Sender format ([80f3330](https://gitlab.com/ensembl/talk/commit/80f333013c7744b456f513ed6e5f3377cf5631bf)), closes [#788](https://gitlab.com/ensembl/talk/issues/788)
* **gitlab:** Add function to create a branch from master ([059d27a](https://gitlab.com/ensembl/talk/commit/059d27a2118d11dca63bb76a7fa590b6a56c57d7))
* **gitlab:** Create function to create MR in cloud function ([af1ee2c](https://gitlab.com/ensembl/talk/commit/af1ee2cdcda512988374a83904d0102d70fc7b76))
* **Header bar:** Show header bar only on mobile ([94988a3](https://gitlab.com/ensembl/talk/commit/94988a378f0cbbcfae656214898c07b7a4ff2005)), closes [#648](https://gitlab.com/ensembl/talk/issues/648)
* **Header bar:** Show name of page on header bar ([940d2d5](https://gitlab.com/ensembl/talk/commit/940d2d56ceb9dd15ac6c1fa2020f5b4d0416226a)), closes [#758](https://gitlab.com/ensembl/talk/issues/758)
* **Header bars:** More styling fixes for mobile and desktop ([9f9ed7f](https://gitlab.com/ensembl/talk/commit/9f9ed7fe0ae8d74ed8820c948db8be76230d3473))
* **Home screen and nav:** Incorporate docs ([9987ec4](https://gitlab.com/ensembl/talk/commit/9987ec44be1c4e6c7c0f116e09cc059cde6cd926))
* **InvitePerson:** Lower case email address that comes in ([2a43550](https://gitlab.com/ensembl/talk/commit/2a43550a46d6db6edc754df13326ce9a5ec6efa5)), closes [#823](https://gitlab.com/ensembl/talk/issues/823)
* **Label:** Clickable labels that go to Talks page ([b89e0b8](https://gitlab.com/ensembl/talk/commit/b89e0b86624942c0df6b50eb7dfb5885eb5324ae))
* **LabelsPage:** Hover background on rows to make more readable ([5c72d64](https://gitlab.com/ensembl/talk/commit/5c72d642c71e024f2f256e10e1d5fba35d1b2f72))
* **Left sidebar:** Move sign out link back up ([ef3ef4b](https://gitlab.com/ensembl/talk/commit/ef3ef4b5b54941b3a9c147d488896648cd2de917)), closes [#777](https://gitlab.com/ensembl/talk/issues/777)
* **Left sidebar:** Styling fixes ([c635cf2](https://gitlab.com/ensembl/talk/commit/c635cf274c1b31e1486e87652b80bf673f95a847)), closes [#775](https://gitlab.com/ensembl/talk/issues/775)
* **Main content:** Bottom margin so user can scroll pass content ([289a488](https://gitlab.com/ensembl/talk/commit/289a488fe0a3711447a39ebc5eda584fa926b2b5)), closes [#782](https://gitlab.com/ensembl/talk/issues/782)
* **MergeRequest:** Create callable function to create merge request ([0eb2c1b](https://gitlab.com/ensembl/talk/commit/0eb2c1b538f37f0be2076f2bc6667a0372692437))
* **MergeRequest:** Create MR button only visible to Ensembl ([247eed5](https://gitlab.com/ensembl/talk/commit/247eed5ffbbc7560fb2870f67a388cc29fa84484))
* **Nav and URL Params:** Click Talks in Nav should reset controls ([0931c76](https://gitlab.com/ensembl/talk/commit/0931c7671d2b959837d57ad0775778f39f182fba))
* **PageLoadingSpinner:** Remove Container wrapper ([7a5bd38](https://gitlab.com/ensembl/talk/commit/7a5bd380fa640ec5fb4df7fe852c7cfdbd99c8b7)), closes [#749](https://gitlab.com/ensembl/talk/issues/749)
* **ReloadPage:** Reduce frequency to 3 hours ([c1e7b53](https://gitlab.com/ensembl/talk/commit/c1e7b53054566ed2c0fa5b29f37ac7f3a5845202))
* **Resolve and reopen button:** Resolve and reopen button in sidebar ([5a628a1](https://gitlab.com/ensembl/talk/commit/5a628a16be8d0c854f1fa27f9a1619b8c7d58cda)), closes [#748](https://gitlab.com/ensembl/talk/issues/748)
* **Saving:** Add IsSaving component to desktop header ([d70eab3](https://gitlab.com/ensembl/talk/commit/d70eab37d8dab4933aab350910d8f3e64ff3bb46))
* **Saving queries/tabs/labels:** Remove this feature ([1d921b2](https://gitlab.com/ensembl/talk/commit/1d921b2e99f762c51fbf1906d4864d65112fba94))
* **sendTalkSubjectUpdatedNots:** Sender format ([bb2c493](https://gitlab.com/ensembl/talk/commit/bb2c4939f4e57c1c3a6fe04ac6b4349538c0362b)), closes [#789](https://gitlab.com/ensembl/talk/issues/789)
* **Sidebars:** Styling ([9e4f1b8](https://gitlab.com/ensembl/talk/commit/9e4f1b8c7be3fce070fafdec2c22b76c5b547594)), closes [#755](https://gitlab.com/ensembl/talk/issues/755)
* **Talk Page:** Sticky title bar in desktop ([4a801e9](https://gitlab.com/ensembl/talk/commit/4a801e9ce2af18fda202df24024c655abd0ab3a7)), closes [#757](https://gitlab.com/ensembl/talk/issues/757)
* **Talk sidebar:** Spacing ([d4f5724](https://gitlab.com/ensembl/talk/commit/d4f5724762fb4b66a3d162b7172a0528ec362312))
* **Talk sidebars:** Divider lines and padding ([953ca05](https://gitlab.com/ensembl/talk/commit/953ca053a0aed5a980a34d55edfc784cfc20008e)), closes [#770](https://gitlab.com/ensembl/talk/issues/770)
* **Talk title:** Empty subject talk title and doc title ([43cadcd](https://gitlab.com/ensembl/talk/commit/43cadcd7cffb3d81beafff6b73e5fb992734585f))
* **Talk title:** Remove html whitepsace when getting title ([7c398b6](https://gitlab.com/ensembl/talk/commit/7c398b6b6e7851a888f093866c36bdd52beccf19))
* **Talks Page:** Docs tab ([c4170fe](https://gitlab.com/ensembl/talk/commit/c4170fef41e670c13a982f9ae6f372d1a50bcc5d))
* **Talks Page:** Docs tab ([e31d316](https://gitlab.com/ensembl/talk/commit/e31d316616f32700a5499f39f4bb9e1837a5abf4))
* **Talks paths:** Use docs for doc paths ([e1dcc4b](https://gitlab.com/ensembl/talk/commit/e1dcc4be12af9e8c41bc561eb1e232136768831d))
* **TalksPage Tabs:** Tabs for new Talks Page tabs ([a1b4abd](https://gitlab.com/ensembl/talk/commit/a1b4abdb601b6ea106b63fba46f3d200414f5582))
* **updateAlgoliaTalks:** Send isDoc to Algolia ([9bf6d68](https://gitlab.com/ensembl/talk/commit/9bf6d68b47c9ae9705ed68f42881aabbc3e84c8d))
* **Usage:** Get usage from prod db ([05660b9](https://gitlab.com/ensembl/talk/commit/05660b954e598ae6cb4ec6685dd8076ad36d13ed)), closes [#818](https://gitlab.com/ensembl/talk/issues/818)
* **User spaceNames:** Sync from secured collection to user document ([a75eed7](https://gitlab.com/ensembl/talk/commit/a75eed749e9f0ae74b9f9bb0fa2c2abcb67048a3))
* Style uploading file button ([03621ef](https://gitlab.com/ensembl/talk/commit/03621ef8000cfb3d69d890b4690d46699c4f0b90))
* Update image with Firebase with CKeditor ([76ea15a](https://gitlab.com/ensembl/talk/commit/76ea15acf744bd14f85efb68edb838ce417f0192))
* **ContentEditor:** Remove image and media from content editor ([cca7833](https://gitlab.com/ensembl/talk/commit/cca783315b39d44f85c55c09bdf2082378d79bf0)), closes [#529](https://gitlab.com/ensembl/talk/issues/529)
* **Delete buttons:** Make delete buttons look like links ([1244a73](https://gitlab.com/ensembl/talk/commit/1244a736f7184eeb9526f90fb286f2afa67cb370)), closes [#477](https://gitlab.com/ensembl/talk/issues/477)
* **Labels:** Delete label ([3ff9bec](https://gitlab.com/ensembl/talk/commit/3ff9bec561817e058d1c5014598a553b4e17376b)), closes [#464](https://gitlab.com/ensembl/talk/issues/464)
* **NewTalk:** Support CKeditor for new talk ([4fec57a](https://gitlab.com/ensembl/talk/commit/4fec57abad0e5bc224452cd269c96064aa3596a9)), closes [#544](https://gitlab.com/ensembl/talk/issues/544)
* **Paragraphs:** More space between paragraphs ([2c6eeba](https://gitlab.com/ensembl/talk/commit/2c6eeba00f7d43bc88bdf5c19909a460bfd47003)), closes [#474](https://gitlab.com/ensembl/talk/issues/474)
* **People:** Change my password ([2aed6d0](https://gitlab.com/ensembl/talk/commit/2aed6d0443408fe39972bfd7059997f161b7c181)), closes [#515](https://gitlab.com/ensembl/talk/issues/515) [#516](https://gitlab.com/ensembl/talk/issues/516)
* **People:** Re-invite invitees ([5635017](https://gitlab.com/ensembl/talk/commit/563501745ac08da541208e3cbb12a14d01719abb)), closes [#479](https://gitlab.com/ensembl/talk/issues/479)
* **PeoplePage:** Refactor PeoplePage into subcomponents ([1222119](https://gitlab.com/ensembl/talk/commit/1222119ee394b94540fab2a62c8d9d83d1854fb8)), closes [#518](https://gitlab.com/ensembl/talk/issues/518)
* **Subject:** Support for file attachment for talk description ([78fbb06](https://gitlab.com/ensembl/talk/commit/78fbb066d4baf3de5a517831cad98034f9692066))
* **Talk:** Display message if can't load talk ([c0da2a0](https://gitlab.com/ensembl/talk/commit/c0da2a049055fc01d43ce72758de13e12cf24361)), closes [#147](https://gitlab.com/ensembl/talk/issues/147) [#447](https://gitlab.com/ensembl/talk/issues/447)
* **Talk:** New talk from talk page ([a2e0693](https://gitlab.com/ensembl/talk/commit/a2e06932fa103561953ee9d2cba864d180f04d51)), closes [#494](https://gitlab.com/ensembl/talk/issues/494)
* **Talk Labels:** Link to labels manage page from talk ([44374d1](https://gitlab.com/ensembl/talk/commit/44374d1cf26cbd749cf70e4f10dd3b28df418afd)), closes [#463](https://gitlab.com/ensembl/talk/issues/463) [#456](https://gitlab.com/ensembl/talk/issues/456) [#431](https://gitlab.com/ensembl/talk/issues/431) [#430](https://gitlab.com/ensembl/talk/issues/430)
* **Talk Subject:** CKEditor for talk subject ([d71e9f4](https://gitlab.com/ensembl/talk/commit/d71e9f4137d484e23d8feae52c245538db539ae8)), closes [#538](https://gitlab.com/ensembl/talk/issues/538)
* **Talk Subject:** CKEditor for Talk Subject ([04917a4](https://gitlab.com/ensembl/talk/commit/04917a4bc6d74bbc2db00632124bac7f75948029)), closes [#508](https://gitlab.com/ensembl/talk/issues/508)
* **Talks:** Reset pagination to first page if changing labels ([da9fa0e](https://gitlab.com/ensembl/talk/commit/da9fa0e608382b635c5176b67a67f3551aff38d0)), closes [#458](https://gitlab.com/ensembl/talk/issues/458)
* **Talks:** Retitle text to be Filter by label on talks page ([1cc27dc](https://gitlab.com/ensembl/talk/commit/1cc27dcfce28fc2e17c8f4e675592aca23e530f0)), closes [#455](https://gitlab.com/ensembl/talk/issues/455)
* **Talks:** Save user selected label and search query ([969485c](https://gitlab.com/ensembl/talk/commit/969485ccbd0c18872ed8d4683d8b84bafca720a6)), closes [#459](https://gitlab.com/ensembl/talk/issues/459)
* **Talks:** Search by label name ([f427ea3](https://gitlab.com/ensembl/talk/commit/f427ea34170fd4c18e359183580eecce4d194804)), closes [#442](https://gitlab.com/ensembl/talk/issues/442)
* **Talks:** Tweak text copy of searchbox ([f573795](https://gitlab.com/ensembl/talk/commit/f57379541bf8bc8a41aeb539203184524b3e03ad)), closes [#461](https://gitlab.com/ensembl/talk/issues/461)
* **Talks:** Update Algolia label names ([c5ad8a5](https://gitlab.com/ensembl/talk/commit/c5ad8a5e5fe69f8d58b700a9bec409a958edce3f)), closes [#467](https://gitlab.com/ensembl/talk/issues/467)
* **Talks Page:** Filter by labels on talks page ([c041c48](https://gitlab.com/ensembl/talk/commit/c041c48005ec4405e3ea04561716a42aab0d3aec)), closes [#441](https://gitlab.com/ensembl/talk/issues/441)
* **upload:** Create unique file name for uploaded image ([a2cdd23](https://gitlab.com/ensembl/talk/commit/a2cdd23cc3201b5dcee24e2b0ec5f3d32fe511f4))
* **Upload:** Focus on mention element after inserting markdown ([e7e5f68](https://gitlab.com/ensembl/talk/commit/e7e5f680a349a53672c196471ae12d5b6fbbb5d1))
* **Upload:** Insert image at the end of comment with new lines ([1686fa2](https://gitlab.com/ensembl/talk/commit/1686fa2d7f7a0be48cc163c8ad2abf2289dd019d))
* **UploadFile:** Handle unsucessful image upload ([1696893](https://gitlab.com/ensembl/talk/commit/1696893ff517d2e911fcefa53dd066b39efd9de8))
* **UploadFile:** Refactored UploadFile ([c50c52d](https://gitlab.com/ensembl/talk/commit/c50c52de64352bd2479516fc6111ef990260b455)), closes [#450](https://gitlab.com/ensembl/talk/issues/450) [#440](https://gitlab.com/ensembl/talk/issues/440) [#448](https://gitlab.com/ensembl/talk/issues/448) [#462](https://gitlab.com/ensembl/talk/issues/462)
* **UploadFile:** Round upload progress percent to integer ([b258407](https://gitlab.com/ensembl/talk/commit/b2584072b986ec5718a9c1511ce088ea0c379680)), closes [#489](https://gitlab.com/ensembl/talk/issues/489)
* **Users:** Don't allow inviting invitees or users ([82cc3f2](https://gitlab.com/ensembl/talk/commit/82cc3f20c8e3b9e81ee601fdc683261185e9b442)), closes [#504](https://gitlab.com/ensembl/talk/issues/504)
* Add upload file to comment ([45fab55](https://gitlab.com/ensembl/talk/commit/45fab557aff045b9f64c0a7b9fc32f3ab24f549c))
* **AlgoliaPagination:** Use react bootstrap pagination component ([6731a8a](https://gitlab.com/ensembl/talk/commit/6731a8adbb020ed35de5a60c199ab112ebbde614))
* **AlgoliaSearch:** Style search with react-bootstrap ([a89e91a](https://gitlab.com/ensembl/talk/commit/a89e91a6e61b071f2f0e7bcee0e5880ee5b16793)), closes [#358](https://gitlab.com/ensembl/talk/issues/358)
* **Approvals:** Approvals in alphabetical order ([99a2f42](https://gitlab.com/ensembl/talk/commit/99a2f4260dae66a41ed6d650921d0e99e6feca1e)), closes [#386](https://gitlab.com/ensembl/talk/issues/386)
* **Approvals:** Approve a talk proposal ([9206461](https://gitlab.com/ensembl/talk/commit/92064615e496e8baa8514259327a818106d7bf40)), closes [#301](https://gitlab.com/ensembl/talk/issues/301)
* **Approvals:** Display approvers as avatars ([eb9a731](https://gitlab.com/ensembl/talk/commit/eb9a731c7359924b47802693b4c3171bfa49daae)), closes [#385](https://gitlab.com/ensembl/talk/issues/385)
* **cstmClaims:** Add functions to refresh custom claims ([31a7504](https://gitlab.com/ensembl/talk/commit/31a7504036d19281f9e687ace8e8da17da88c8e3))
* **EditSubject:** Remove calling callable Algolia update ([20f9283](https://gitlab.com/ensembl/talk/commit/20f9283bd486074b38f07635d9c1823d42223e67)), closes [#444](https://gitlab.com/ensembl/talk/issues/444)
* **EditSubject:** Send subject updated notifications ([7f2fde4](https://gitlab.com/ensembl/talk/commit/7f2fde40e27bf72d899b87a593111fb71d9a86a2)), closes [#268](https://gitlab.com/ensembl/talk/issues/268)
* **Inbox:** Remove Inbox feature ([ea37ed3](https://gitlab.com/ensembl/talk/commit/ea37ed36b543985fb8b8890c7a8b7ad1caee163e)), closes [#413](https://gitlab.com/ensembl/talk/issues/413)
* **index:** Add replica index on search ([37e61b9](https://gitlab.com/ensembl/talk/commit/37e61b9dffd8dd8062d8957de089624a1e857689)), closes [#352](https://gitlab.com/ensembl/talk/issues/352)
* **Labels:** Add colors to labels ([78dd090](https://gitlab.com/ensembl/talk/commit/78dd0905885cc841e3cf80498a652e16dc0ab34f)), closes [#423](https://gitlab.com/ensembl/talk/issues/423) [#435](https://gitlab.com/ensembl/talk/issues/435)
* **Labels:** Add labels and edit labels ([0a8a57f](https://gitlab.com/ensembl/talk/commit/0a8a57f9e01eed3189ab31a541f41de664eeda5c)), closes [#427](https://gitlab.com/ensembl/talk/issues/427)
* **Labels:** Hidden Labels starter page ([bbf5a26](https://gitlab.com/ensembl/talk/commit/bbf5a26c2574778a47af450b1d64796142633a84)), closes [#400](https://gitlab.com/ensembl/talk/issues/400)
* **Labels:** Navigate to Labels manage page ([23615c6](https://gitlab.com/ensembl/talk/commit/23615c6afd5b9f2d3f523f3594ca5b90baa44c76)), closes [#438](https://gitlab.com/ensembl/talk/issues/438)
* **Labels:** When cancel edit mode, throw away changes ([525b221](https://gitlab.com/ensembl/talk/commit/525b221791126d6f260ea1013f73a27d03e2ec2b)), closes [#427](https://gitlab.com/ensembl/talk/issues/427)
* **Login:** Explain Ensembl Talk on login page ([18a7557](https://gitlab.com/ensembl/talk/commit/18a7557f7b4c8834fee84491b91ef6b2b8745efa)), closes [#397](https://gitlab.com/ensembl/talk/issues/397)
* **Login:** Revise marketing message to target investors ([6355937](https://gitlab.com/ensembl/talk/commit/635593741e0e961104f047ac2c0f524f56abea99)), closes [#409](https://gitlab.com/ensembl/talk/issues/409)
* **newestComment:** Add latest comment to TalkSelect ([dfff495](https://gitlab.com/ensembl/talk/commit/dfff49582c3e2baeef80c16775e2129d76557c3d))
* **NewTalk:** Send new Algolia talk ([9b9694e](https://gitlab.com/ensembl/talk/commit/9b9694e273a03a4e22933c873cbf70ff3af6f05a)), closes [#345](https://gitlab.com/ensembl/talk/issues/345)
* **Pagination:** Don't show pagination if no results ([640d241](https://gitlab.com/ensembl/talk/commit/640d241feae5d65ab96e1fdabcda4572eabd320f)), closes [#394](https://gitlab.com/ensembl/talk/issues/394)
* **Searchbox:** Font size for one line inputs ([7883567](https://gitlab.com/ensembl/talk/commit/78835678c6d28ff893955a959cfeae680ac42bec)), closes [#405](https://gitlab.com/ensembl/talk/issues/405)
* **sendTalkApprovalNots:** Send email notification for talk approval toggling ([95c5fb7](https://gitlab.com/ensembl/talk/commit/95c5fb7a41fed554a9247a9ca702d706f8f92c3f)), closes [#383](https://gitlab.com/ensembl/talk/issues/383)
* **Subject and EditComment:** Disable save button if no change ([1e5a43e](https://gitlab.com/ensembl/talk/commit/1e5a43eff44621982b57807107d220344370dcd1)), closes [#371](https://gitlab.com/ensembl/talk/issues/371)
* **SubjectHeader:** Remove isSaving UI ([21f048a](https://gitlab.com/ensembl/talk/commit/21f048a189dd2b0e938340ec842926a77a8b1271)), closes [#437](https://gitlab.com/ensembl/talk/issues/437)
* **Talk:** Add label to a talk ([89df5c9](https://gitlab.com/ensembl/talk/commit/89df5c9acd8e6dd6ca74ede6af0278d9ae27b5e4)), closes [#436](https://gitlab.com/ensembl/talk/issues/436)
* **Talk:** Subscribe to talk on approval change ([4e36c19](https://gitlab.com/ensembl/talk/commit/4e36c19de7926248605545c8f912150a771f0be1)), closes [#423](https://gitlab.com/ensembl/talk/issues/423)
* **Talk:** Subscribe to talk on subject update ([4fcb90e](https://gitlab.com/ensembl/talk/commit/4fcb90eec04d62d8505a99bed17eb22e53453ba1)), closes [#424](https://gitlab.com/ensembl/talk/issues/424)
* **TalkSelect:** Labels on TalkSelect ([ea82cfc](https://gitlab.com/ensembl/talk/commit/ea82cfcaeb417ab8f604670a91c5959066d4beb9)), closes [#439](https://gitlab.com/ensembl/talk/issues/439)
* **TalkSelect:** Style TalkSelect ([432c673](https://gitlab.com/ensembl/talk/commit/432c673aac80b552905e2dd6390364df598e2e94)), closes [#361](https://gitlab.com/ensembl/talk/issues/361)
* **updateTalkCmterUrlsOnCmt:** Remove updateTalkCmterUrlsOnCmt ([9eeca2f](https://gitlab.com/ensembl/talk/commit/9eeca2fe0bce176e39574babe202e9b1dc6b1656)), closes [#410](https://gitlab.com/ensembl/talk/issues/410)
* **updateTalkNewestCmt:** Refresh newest comment on each talk nightly ([87ac1bc](https://gitlab.com/ensembl/talk/commit/87ac1bc0d2a833fd48c4955d905e9be93020c98f)), closes [#388](https://gitlab.com/ensembl/talk/issues/388)
* **Users:** one-line-input class to email field ([1f11ddd](https://gitlab.com/ensembl/talk/commit/1f11ddd94ab4fd84459ace51088e27174ff86eeb)), closes [#406](https://gitlab.com/ensembl/talk/issues/406)
* **Users:** View user avatar in users page ([757b4b3](https://gitlab.com/ensembl/talk/commit/757b4b3149ebc2ad501715ffe7dd9d0b991ef464)), closes [#387](https://gitlab.com/ensembl/talk/issues/387)
* Add Algolia css ([4a170e7](https://gitlab.com/ensembl/talk/commit/4a170e776aad7c91b422f2137dd55e982ee67a1d))
* Add empty message ([fab2816](https://gitlab.com/ensembl/talk/commit/fab2816b8708b579bd983276f46cb069fd851b5b))
* Add index name in custom claims ([75aad82](https://gitlab.com/ensembl/talk/commit/75aad82daf58f2ce071cfe7d4f55c34c93fca43f))
* Add refresh prop to InstantSearch ([ba0e723](https://gitlab.com/ensembl/talk/commit/ba0e723f1cf8954d1fa49b0b514faa6ba482e9cc))
* Add Saving / Saved message in UI ([62e3698](https://gitlab.com/ensembl/talk/commit/62e3698f85069423de9f274d96b7179b6c18f1bf))
* Add searchable attributes ([a784a03](https://gitlab.com/ensembl/talk/commit/a784a033fbae6d48801ecf0c3ee0dcef42009336))
* Add space for fauceting ([60e9319](https://gitlab.com/ensembl/talk/commit/60e9319761146d90056fd0bdab8329d63ee17884))
* Add talkId as id ([59f2262](https://gitlab.com/ensembl/talk/commit/59f2262bc757af64196497f5452b693f63a74909))
* Apply faucet before searching ([c0ddab5](https://gitlab.com/ensembl/talk/commit/c0ddab53bf52f0370fea1b6d1aebc8d13706ace3))
* Apply faucet before searching ([4384377](https://gitlab.com/ensembl/talk/commit/4384377373b3ccb96ac90e153a48573f163bd305))
* Cover all scenarios ([0df0463](https://gitlab.com/ensembl/talk/commit/0df046321270adb7fbd9e2346bd78fc8bd24f54b))
* Create setting for dev vs. prod ([e759dc9](https://gitlab.com/ensembl/talk/commit/e759dc9b3cddba6fd0f489b1cd40d8f00a9e476d))
* Initial work on creating dev index ([3708cb5](https://gitlab.com/ensembl/talk/commit/3708cb5e94a71e6cb116b360f0d204452b610a80))
* Initial work on getting pagination ([c86d7ee](https://gitlab.com/ensembl/talk/commit/c86d7eeee7300e10ad8e0d1873b8318d7e4e6a76))
* Initial work on rendering participant avatar ([0ac2664](https://gitlab.com/ensembl/talk/commit/0ac266449a585f8dcd9c633578b83576cbf3cf11)), closes [#307](https://gitlab.com/ensembl/talk/issues/307)
* Initial work on rendering search result ([1c8e6d5](https://gitlab.com/ensembl/talk/commit/1c8e6d5088cc93689d3b27ae08d470a30034d33f))
* Initial work on rendering search result ([c12e0ac](https://gitlab.com/ensembl/talk/commit/c12e0ac91e52b3b65672fbeb4c2f99daeb65b111))
* Initial work on rendering search result ([97d93c0](https://gitlab.com/ensembl/talk/commit/97d93c0c153a539d51dd08c9e79c98c1f6a17a9b))
* Initial work on sort by lastCommentedAt field ([46e32ec](https://gitlab.com/ensembl/talk/commit/46e32ec61a1a007ab813f4c6811a9d44c5634edc))
* Initial work to convert to React ([48d8a90](https://gitlab.com/ensembl/talk/commit/48d8a90e06134ced7799949000e16d9b02f49df9))
* Only update Talk if indexed content has changed ([a5e8ef7](https://gitlab.com/ensembl/talk/commit/a5e8ef70ce967db02a79f22d031ee1a99c2082cc))
* Open non ensembl domain url in new tab ([4c019f6](https://gitlab.com/ensembl/talk/commit/4c019f64b8b8c3bb27b3164b5b216effa7e44d06))
* Preview newest comment from TalkSelect ([dca1f1f](https://gitlab.com/ensembl/talk/commit/dca1f1f1abdd83880c8ea9c9d8be9479944c370b))
* Reindex algolia nightly ([1774b9f](https://gitlab.com/ensembl/talk/commit/1774b9fa4c609d16b2f7ab0ac425f1276ba45207))
* Scroll windows to top when search state changes ([14f9e3f](https://gitlab.com/ensembl/talk/commit/14f9e3ff9fc9e20a51bcb749efbaa0895ca79ad4))
* Set Algolia key ([249d973](https://gitlab.com/ensembl/talk/commit/249d973f183308ba9ad0ea47a1873cd443e6f8cd))
* Sort result by preview time ([95d60d4](https://gitlab.com/ensembl/talk/commit/95d60d46dbdf4e47b9c9d630e2e706ddee3cf3c6))
* Style components ([dca1b6c](https://gitlab.com/ensembl/talk/commit/dca1b6c327a3357c23a503aab8095465256585e7))
* **functions:** Add function to move user between spaces ([0e97865](https://gitlab.com/ensembl/talk/commit/0e978658a1520ceb4315187ce4e1c9ee97289fad))
* **functions:** Add hourly backup ([4fdd03b](https://gitlab.com/ensembl/talk/commit/4fdd03b460de4791fdd1f62cb2d75247b97e2167))
* **functions:** Add subscribe to Talk on comment create ([a35f244](https://gitlab.com/ensembl/talk/commit/a35f244173aa5028ff8065d5ea7ff929da0ec1ed))
* **functions:** Automatically subscribe if ack a comment in Talk ([7403a3f](https://gitlab.com/ensembl/talk/commit/7403a3f52d10b2add8e269686acbb8381709e422))
* **functions:** Backup Firestore every 24 hours ([798308d](https://gitlab.com/ensembl/talk/commit/798308dc82c92fa979451ca6d689fcd25e8c54fd))
* **functions:** Delete comment on update ([52780fa](https://gitlab.com/ensembl/talk/commit/52780fa277bc2b7a737dd78b2c314834c008d3ea))
* **functions:** Delete Talk and set flag to nested comments ([810d2f3](https://gitlab.com/ensembl/talk/commit/810d2f3092ff8d6a65e455575547640415d96f4d))
* **functions:** Send subscribed notification email ([c7dc450](https://gitlab.com/ensembl/talk/commit/c7dc45003239a45a7b2280b3b5bf4dc4c5b4cf3e))
* **functions:** Update email notification with template ([6ae4764](https://gitlab.com/ensembl/talk/commit/6ae4764eb9d643fbb46955d44e76e13b0bd31d17))
* **functions:** Update name and avatar if changes ([3c047ec](https://gitlab.com/ensembl/talk/commit/3c047ec07e98e90f9c2c07bb6e770103e63b9af4))
* Set up front end to read from new db structure ([3368a3f](https://gitlab.com/ensembl/talk/commit/3368a3f3df86f3001af5f78098ab128162c984fb))
* **functions:** Use createByUser as email sender ([c15b99a](https://gitlab.com/ensembl/talk/commit/c15b99adf3ec1eb9873cccde552fb9e73536c2eb))
* **nav:** Add main structure of main nav ([d626280](https://gitlab.com/ensembl/talk/commit/d626280674f6e9ae293ed58976f671c4305bec91))
* **Nav:** Add handler for page change. ([b69bce5](https://gitlab.com/ensembl/talk/commit/b69bce5b49f3dbbf83264905358390e3b47b495c))
* **NewTalk:** Add new talk with option first message ([e2ea21e](https://gitlab.com/ensembl/talk/commit/e2ea21ea5549fa313ac0ad80063ec78c7fc421c1)), closes [#75](https://gitlab.com/ensembl/talk/issues/75)
* **NewTalk:** Subscribe to Talk when starting a new Talk ([82926b1](https://gitlab.com/ensembl/talk/commit/82926b17c0fe6ff50ad54ec512f4a139b5da815a))
* **Talk:** Add preview edit subject ([3ba0b86](https://gitlab.com/ensembl/talk/commit/3ba0b8635e5f4e7f7a305f11b17e226e3be70270))
* Set up cloud functions and emulators ([be929a6](https://gitlab.com/ensembl/talk/commit/be929a643405a53645e1052a37d58819e3b3a72f)), closes [#70](https://gitlab.com/ensembl/talk/issues/70) [#69](https://gitlab.com/ensembl/talk/issues/69)
* Use index in custom claims ([7cca86c](https://gitlab.com/ensembl/talk/commit/7cca86c71a988b74f386df3c3b00d87c9ad08a7a))
* **Talk:** Add simple UI for adding comment. ([a789251](https://gitlab.com/ensembl/talk/commit/a78925119cf886a860c71c224aa5c9f862aeba07)), closes [#27](https://gitlab.com/ensembl/talk/issues/27)
* **Talk:** Render comments and add new comment button ([076b8d0](https://gitlab.com/ensembl/talk/commit/076b8d07f35b712cc1f571b4fe5ce0f384976cb4)), closes [#26](https://gitlab.com/ensembl/talk/issues/26)
* **Talks:** Add click handler for pressing the talk ([5ab32af](https://gitlab.com/ensembl/talk/commit/5ab32af7ea390aa685ca2a3f23061cb22e5647f1))


### Bug Fixes

* **Ack:** Ack button not showing up ([bea67c9](https://gitlab.com/ensembl/talk/commit/bea67c9bd1bb09d25af99d61ea58b4499e4ed66f)), closes [#611](https://gitlab.com/ensembl/talk/issues/611)
* **ACK:** Fix ACK not appearing for user ([2ed08de](https://gitlab.com/ensembl/talk/commit/2ed08de7ca694d008fc38b58dbfb17d6ca91ab71)), closes [#575](https://gitlab.com/ensembl/talk/issues/575)
* **Ack request line:** Ack request line should be one line, not multiple ([6c2e095](https://gitlab.com/ensembl/talk/commit/6c2e095a76b28f71e10c0c8e23a99ecd789e1111)), closes [#744](https://gitlab.com/ensembl/talk/issues/744)
* **Ack.spec:** Fix broken test by passing in comment id ([82f6808](https://gitlab.com/ensembl/talk/commit/82f6808168b08f2fcce887a01069446ae942aef7))
* **AcknowledgeUI:** Support legacy talks ([50d4789](https://gitlab.com/ensembl/talk/commit/50d47895d751fdf645ace91d502c53cbb68a4439)), closes [#118](https://gitlab.com/ensembl/talk/issues/118)
* **Activity feed items:** Fix routing ([bf999e8](https://gitlab.com/ensembl/talk/commit/bf999e849715f28e38123cb052414951fba35bb9))
* **Activity feed ites:** Account for space slug ([82a4a4d](https://gitlab.com/ensembl/talk/commit/82a4a4d8b2f672be69ebdfbb31916e7072d2c16e))
* **Add comment box:** Make it normal full size ([b9bf1c7](https://gitlab.com/ensembl/talk/commit/b9bf1c7e7eb3c1df613de89e0cc67ce440a28abf))
* **AddComment:** Fix add comment not showing in a talk ([afa39ba](https://gitlab.com/ensembl/talk/commit/afa39bab65d1449f85862d2214b12b1c5daf8f29)), closes [#615](https://gitlab.com/ensembl/talk/issues/615)
* **AlgoliaCallables:** Fix file name casing ([12e18c2](https://gitlab.com/ensembl/talk/commit/12e18c2c3a3c6e0f6c2b49096c6b080db0cb5d92)), closes [#585](https://gitlab.com/ensembl/talk/issues/585)
* **AlgoliaInstantSearch:** Fix new users don't have a default tab, talks page ([e5d8d21](https://gitlab.com/ensembl/talk/commit/e5d8d21f6d063c5a455732a21f4a58428c2db4c1)), closes [#414](https://gitlab.com/ensembl/talk/issues/414)
* **AlgoliaSearch:** Fix pagination first click not working ([112721b](https://gitlab.com/ensembl/talk/commit/112721ba9198a21e357f13c4146d9ee27c5c26c6))
* **Approvals:** Fix margin below avatar set ([c28fec4](https://gitlab.com/ensembl/talk/commit/c28fec4c2623f32cd2bfb44138ede8573cc849c2))
* **Auth domain:** Set to default firebaseapp.com one ([3e91ebe](https://gitlab.com/ensembl/talk/commit/3e91ebe3d9b5fecf902167c9711717d9adce284a))
* **Auto reload:** Only on page load, try reloading ([1bc4ffd](https://gitlab.com/ensembl/talk/commit/1bc4ffde3ca64ec54de7f0170d884e6788ae64c6))
* **Autosave:** Prevent autosaving on page load of a talk ([7dc1f80](https://gitlab.com/ensembl/talk/commit/7dc1f808ee6c256a1d27cf569893ee1e27ac87a3)), closes [#667](https://gitlab.com/ensembl/talk/issues/667)
* **Avatar:** Fix long name not wrapping ([fcd342d](https://gitlab.com/ensembl/talk/commit/fcd342d2031b4d474c66f7a8b4fb4f0afeeadafc))
* **backup:** Backup comments collection ([968e86f](https://gitlab.com/ensembl/talk/commit/968e86f6194903c91dc1d39b995ae52081210854))
* **backup:** Fix script not backing up subcollections ([e091875](https://gitlab.com/ensembl/talk/commit/e091875456eb7f74f87f75ab9b65b7d07e560ad5))
* **CI:** Fix build not pass through stages ([458fa55](https://gitlab.com/ensembl/talk/commit/458fa5552cf7360d75d2d5359d1e5b3ebe82eeb4)), closes [#34](https://gitlab.com/ensembl/talk/issues/34)
* **CI/CD:** Build CKeditor asset ([67a55d3](https://gitlab.com/ensembl/talk/commit/67a55d37d12cbcf94a35ce4d383cbf3d4319f853))
* **ckeditor:** Remove nbsp from comment and subject ([6b8ff50](https://gitlab.com/ensembl/talk/commit/6b8ff5032292a241cf46e2e895b25aa00f0b5591))
* **CKEditor:** Floating toolbar positioned below header ([f66b786](https://gitlab.com/ensembl/talk/commit/f66b786229fe4db62e936bd626e37dda09be2e4f)), closes [#533](https://gitlab.com/ensembl/talk/issues/533)
* **Ckeditor styles files:** fix location of import ([e32b858](https://gitlab.com/ensembl/talk/commit/e32b8582d3e248d7c1a15e4ec065b67dd9c419e7))
* **ckeditorJwt:** Set to be blank string intially so no req errs ([2c93fab](https://gitlab.com/ensembl/talk/commit/2c93fabb8ff5f8afa5e43c3d73bfeb877e810990))
* **cmtActivityOnCmtCreate:** Fix not setting docs correctly ([c7dbbab](https://gitlab.com/ensembl/talk/commit/c7dbbab451a204930a0a64c0ca611d9b9de76857))
* **Color selection labels page:** Fix broken colors ([a94b981](https://gitlab.com/ensembl/talk/commit/a94b98117ad3768adc2c896075bc3ec840bbe8e2))
* **CombinedUserRoute:** Allow combinedUser to created even if redirecting ([c12fb85](https://gitlab.com/ensembl/talk/commit/c12fb8595b1a1e62377f97648035119d48735b33))
* **CombinedUserRoute:** Keep waiting until allowableSpaceNamesUpdatedAt ([8856e19](https://gitlab.com/ensembl/talk/commit/8856e19c208cd8382e0e63894c72404c4e217982))
* **Comment:** Allow edit only own comment ([7a33d17](https://gitlab.com/ensembl/talk/commit/7a33d17b2b8ed3991bc2214b9cb83902d30a91da)), closes [#628](https://gitlab.com/ensembl/talk/issues/628)
* **Comment:** Copy comment link to include space name ([c3e1c72](https://gitlab.com/ensembl/talk/commit/c3e1c72bf6a037d211f0416f4a063b1bce12eb4e))
* **Comment:** Fix cmt anchor link scrolling to wrong position ([21cd974](https://gitlab.com/ensembl/talk/commit/21cd97443af2030f3e7840b0183405f3659185dd)), closes [#401](https://gitlab.com/ensembl/talk/issues/401)
* **Comment:** Fix edit comment request/notify UI ([2109734](https://gitlab.com/ensembl/talk/commit/2109734648c61edbc7b972947314b708c1299787)), closes [#107](https://gitlab.com/ensembl/talk/issues/107)
* **Comment:** Fix editing a comment overflow ckeditor ([51b793f](https://gitlab.com/ensembl/talk/commit/51b793fc75f3acddcbef720b2d5de2c84c18476a))
* **Comment:** Fix scrolling to comment ([8bb3b7f](https://gitlab.com/ensembl/talk/commit/8bb3b7f11987240efe96e3fc7c39edb94f952a7f))
* **Comment:** Prevent ckeditor edit comment overflow ([c855b64](https://gitlab.com/ensembl/talk/commit/c855b640eb58175bb735c39388bc0f37b353658e))
* **Comment:** Remove notify/request UI after adding comment ([e5d887f](https://gitlab.com/ensembl/talk/commit/e5d887f45ccede19c3b864e158f2d8bdcc36e634)), closes [#106](https://gitlab.com/ensembl/talk/issues/106)
* **Comment onboarding:** fix it ([4baa0b4](https://gitlab.com/ensembl/talk/commit/4baa0b4d66a0503ddac0b659f306c46067431017))
* **CommentBody:** Fix image moving when hover over header ([adc26c4](https://gitlab.com/ensembl/talk/commit/adc26c42fd0dda02d04b66c7eff383a4c0320c71))
* **CommentCKeditor:** Wrap editor in null-check ([6483a25](https://gitlab.com/ensembl/talk/commit/6483a254b41d41035eb6c39282b2c76d980c9661))
* **Comments:** Wrap long links in comments ([9d6d384](https://gitlab.com/ensembl/talk/commit/9d6d3849e02e6e1c99c7feaf88603d35263adffb)), closes [#671](https://gitlab.com/ensembl/talk/issues/671)
* **ContentEditor:** Do not set config for read only editor ([e62aae4](https://gitlab.com/ensembl/talk/commit/e62aae4b4d7c799f0d9d1730e5c93bfc07bd6bbd))
* **copyUserToSpaces:** Copy only when have name and image ([bbda15c](https://gitlab.com/ensembl/talk/commit/bbda15c5c3790eb18960a3497b93228d32c74ecf))
* **createUser:** When creating user, set lastVisitedSpaceName ([d42c54f](https://gitlab.com/ensembl/talk/commit/d42c54fff7e8fee358b55d6b4e2a76cf6ee195a5))
* **DeleteModal:** Fix bug that sometimes talk is not deleted ([a36fcaa](https://gitlab.com/ensembl/talk/commit/a36fcaab8e0598aac0f4b916eb6938a6705d8c2c)), closes [#379](https://gitlab.com/ensembl/talk/issues/379)
* **Doc:** Don't show notify subscribers button on doc ([d8d5a46](https://gitlab.com/ensembl/talk/commit/d8d5a46bced0e04da8598d67ea8bfb998f94cbba))
* **Doc title:** Should not be corrupted with comment tags ([544d5c7](https://gitlab.com/ensembl/talk/commit/544d5c7b31328a4c4c2bc5939ee07b910b2228da))
* **Doc toolbar:** Fix comment button not appearing ([36e571b](https://gitlab.com/ensembl/talk/commit/36e571b8d248f941ec6f365fab016305b58f8e69))
* **DocEditor:** Focus on marker only if url has hash ([5b9de87](https://gitlab.com/ensembl/talk/commit/5b9de87e9e9b366b10486dc11d8f9f26261fee4e))
* **EditComment:** After submitting comment, should clear add comment ([046a3b1](https://gitlab.com/ensembl/talk/commit/046a3b13ca166108afd5ec6dadbc7305fb676c2c))
* **EditComment:** Fix displaying alert on addComment ([4130033](https://gitlab.com/ensembl/talk/commit/4130033f8a71627801467680bcb0e3fb4e01e11a))
* **EditComment:** Store attached image url to localstorage ([9538e1f](https://gitlab.com/ensembl/talk/commit/9538e1f33005548e39d653934d8456b23780bfa5)), closes [#487](https://gitlab.com/ensembl/talk/issues/487)
* **EditSubjectBeta:** Fix bug with not using editorContent ([a67d219](https://gitlab.com/ensembl/talk/commit/a67d219dea211b12fc98d990a76534a15e2e0500)), closes [#649](https://gitlab.com/ensembl/talk/issues/649)
* **Email:** Fix missing update to sender ([b26d573](https://gitlab.com/ensembl/talk/commit/b26d5738ee979e5c5ad45c1b13b847419498ec01))
* **FileName:** Change file names to PascalCase ([83d8d41](https://gitlab.com/ensembl/talk/commit/83d8d4126d09dbaf6807ffa33726678bf5ab2926))
* **FilterLabels:** Throw away invalid labelIds associated with user ([ee979bb](https://gitlab.com/ensembl/talk/commit/ee979bba68b31a4ff7ebd32568e55c4b84f59f41)), closes [#473](https://gitlab.com/ensembl/talk/issues/473)
* **firebase.json:** No cache ([f5f727d](https://gitlab.com/ensembl/talk/commit/f5f727d6263d21630d880f1b1f1268e9bd3c9925))
* **firebase.json:** Set max-age 0 for cache ([5f266ae](https://gitlab.com/ensembl/talk/commit/5f266aea30ccada61bbaba6e3129b8e833ad0683))
* **function:** Fix miss commit ([7bdbaf4](https://gitlab.com/ensembl/talk/commit/7bdbaf413400918264cc3937da367bc024df4bb3))
* **functions:** Filter comment creator from notification ([a56983c](https://gitlab.com/ensembl/talk/commit/a56983c32d345c334f39c7e93988313f68e4f1a6))
* **functions:** Fix getting subscribedUids from wrong doc ([98c713a](https://gitlab.com/ensembl/talk/commit/98c713ac3b44dad68dfc877db9d64a785d5aa608))
* **functions:** Fix toUids field type ([e0b5ceb](https://gitlab.com/ensembl/talk/commit/e0b5ceb850be8d3890bcfeffe15446bd38320b23))
* **functions:** Only create email doc when user is notified ([232e8d3](https://gitlab.com/ensembl/talk/commit/232e8d398920bcfe16f9008dbb448fa38b2f82b2))
* **functions:** Shorten function name ([9ccff85](https://gitlab.com/ensembl/talk/commit/9ccff854060572a509575ab1bedb3d7451fce42e))
* **Heap:** Fix appid not defined correctly ([4a11438](https://gitlab.com/ensembl/talk/commit/4a11438813694df37b417813d79df4dd0c249489))
* **Invite person and Create label links:** With spaceName ([9633abe](https://gitlab.com/ensembl/talk/commit/9633abec241dee58134edcc40a9df9fc0501b1ce))
* **Label:** Fix hovering background ([7744c09](https://gitlab.com/ensembl/talk/commit/7744c09893c403d1920bb0921e1091d799e17479))
* **Label:** Fix long label text to wrap ([e5d8260](https://gitlab.com/ensembl/talk/commit/e5d82605c366a5148655ceebd0ac90667a94dfb8))
* **LabelFirestore:** Rename file to use PascalCase ([bb49a3b](https://gitlab.com/ensembl/talk/commit/bb49a3ba6180e85e429c3afbf71b5c2dd728f665))
* **Layout:** Fix pages without padding ([522499c](https://gitlab.com/ensembl/talk/commit/522499c25e79f677170bbbb12a5a4b6705e99d66))
* **Layout:** Fix sidebar scrolling ([371f015](https://gitlab.com/ensembl/talk/commit/371f015aa0d8f7dabdce9644396b3bc0ec3c72eb))
* **Left navbar:** Talks menu should be highlighted when on talk page ([fb850bc](https://gitlab.com/ensembl/talk/commit/fb850bc14c9e2f38c49e9e10fb53f97026d00816)), closes [#741](https://gitlab.com/ensembl/talk/issues/741)
* **Left sidebar:** Don't show until finished loading combinedUser ([5559d91](https://gitlab.com/ensembl/talk/commit/5559d9155cf9167cf82a8f4c6f68a7b97e80ca34))
* **Log out:** Logging out broken ([2301754](https://gitlab.com/ensembl/talk/commit/2301754d001db386f767fa545ee940e344e44b49)), closes [#398](https://gitlab.com/ensembl/talk/issues/398)
* **Login:** Fix email address word break on Login page ([4d0dacd](https://gitlab.com/ensembl/talk/commit/4d0dacd63b289bdf35610c0fc94efc441b3a8289)), closes [#408](https://gitlab.com/ensembl/talk/issues/408)
* **Main area content:** Fix negative margins ([c55490d](https://gitlab.com/ensembl/talk/commit/c55490dbf317d63515fd55388ffecc7588be067c)), closes [#743](https://gitlab.com/ensembl/talk/issues/743)
* **Mentions:** Fix mentions not show in comments (pre-ckeditor) ([134bccc](https://gitlab.com/ensembl/talk/commit/134bccc043c846d8b6ed6560c0d8295697adb955)), closes [#566](https://gitlab.com/ensembl/talk/issues/566)
* **NewTalkPage:** Fix missing bootstrap styling ([24401f5](https://gitlab.com/ensembl/talk/commit/24401f51adb75821280e15d673fba8b9119d8a5c))
* Remove console log statement ([2065f58](https://gitlab.com/ensembl/talk/commit/2065f58bf011556c5a7b3c3cbfce8089520c2454))
* **Line height of talk select comment preview:** Fix line height of talk select preview ([2501797](https://gitlab.com/ensembl/talk/commit/25017973cbda3f33bcefdce4744c12b8989e6623)), closes [#594](https://gitlab.com/ensembl/talk/issues/594)
* **Loading spinners:** Fix jupming due to conditional reload code ([b7ef81f](https://gitlab.com/ensembl/talk/commit/b7ef81f3d4d7dfe4573aefd7e4026455e27916b1))
* **Login:** Fix error when user closes login popup ([3e785a9](https://gitlab.com/ensembl/talk/commit/3e785a97e8f6d5eb68f8aa5ccae7a062b1e80467))
* **Login:** Fix login page not loading ([66281f1](https://gitlab.com/ensembl/talk/commit/66281f129f9a77634dd802baf5b6ef7f36fb0d11))
* **Login Page:** Fix not routing to login page when logged out ([ec37b7b](https://gitlab.com/ensembl/talk/commit/ec37b7b66b603e9745f7d43954e32ffa48e103c6))
* **MediaEmbed:** Remove this plugin ([9de9f95](https://gitlab.com/ensembl/talk/commit/9de9f95bff5e31940977b3e3cb693adb314ec781))
* **Mention:** Fix notification message for ack and notify ([2cbe5cd](https://gitlab.com/ensembl/talk/commit/2cbe5cdb71a1bfe7d9fda2f56bc2d0923370d3f8))
* **Mention:** Fix users object for Mention ([064b014](https://gitlab.com/ensembl/talk/commit/064b014c5cd847ef22ad6d0054ff5c9d8bc5afd4))
* **MentionAction:** Reduce width of mention action select ([13d7064](https://gitlab.com/ensembl/talk/commit/13d7064ef3148cc52b7f1331d4c92a59809d5255))
* **Mentions:** Fix @ mentions ([0e89c0a](https://gitlab.com/ensembl/talk/commit/0e89c0aff95f1cf7744bf2f57abe2f67911597f4)), closes [#573](https://gitlab.com/ensembl/talk/issues/573)
* **MergeRequest:** Fix regex for title with more than 2 words ([b005f98](https://gitlab.com/ensembl/talk/commit/b005f98334ec58e242bd43b8320c76c99c4776d8))
* **MergeRequest:** Wait for DB update to finish ([b59a709](https://gitlab.com/ensembl/talk/commit/b59a709f160c30c0a22c0af59bbb4be55f6c0521))
* **Nav:** Fix links hovering effect ([bba8078](https://gitlab.com/ensembl/talk/commit/bba807829d6f97c3ed11b1d3c551897d29d9fbe2))
* **New Talk Blue Button:** Fix size ([97d2a96](https://gitlab.com/ensembl/talk/commit/97d2a9639661e10bb66d5aea7610898896852294))
* **NewestComment:** writtenAt should be taken from comment createdAt ([bd6dd37](https://gitlab.com/ensembl/talk/commit/bd6dd370a90a865a0d493d58e7a44d07caba9ffa))
* **NewestCommentPreview:** Should not show up in talkrows for docs ([2d6d114](https://gitlab.com/ensembl/talk/commit/2d6d114dfd480a68b364ae1e701ecefd143180a1))
* **NewTalk:** Fix comment not showing up in new talk ([38c9378](https://gitlab.com/ensembl/talk/commit/38c9378c6b18db3b354002aa7d7e45564818384e)), closes [#616](https://gitlab.com/ensembl/talk/issues/616)
* **NewTalk:** Fix create empty message when comment is empty ([baf0469](https://gitlab.com/ensembl/talk/commit/baf04695583ab6a02bfc9991188b5d1bf7db03f2))
* **NewTalk:** Replace history to not go back to create new talk ([0bc48ca](https://gitlab.com/ensembl/talk/commit/0bc48cad9104cf09e16ac5a8cdd6dd45d50c6a57))
* **NewTalksStrip:** Show scrollbar only if on mouse hover ([4f93ce6](https://gitlab.com/ensembl/talk/commit/4f93ce6aef4630c2706a9f66d3a844c087629e6e))
* **Notifications:** Differentiate between talk and doc in email nots ([8e7de45](https://gitlab.com/ensembl/talk/commit/8e7de45dda883b7e3f236fd4124c20bf3ccd0cdd))
* **Notify everyone:** Fix trailing s for everyone ([9e643a0](https://gitlab.com/ensembl/talk/commit/9e643a07a97a802349b8320758f7c7edc57ed8cb))
* **Notify Subscribers button:** Position to left of Saving UI ([cf24063](https://gitlab.com/ensembl/talk/commit/cf2406309706252c7bce8c2251987922ff7904a7))
* **Nots:** Fix typo ([bbb26e6](https://gitlab.com/ensembl/talk/commit/bbb26e6d682cc0338c15c30818498ba4e3fd54a5))
* **NotsMessage:** Default mentions for nots message is empty obj ([01763ae](https://gitlab.com/ensembl/talk/commit/01763ae58d7618073c879984a015c0e59e6463ca)), closes [#610](https://gitlab.com/ensembl/talk/issues/610)
* **Owner:** Redirect to InvitePerson in the dropdown ([6a9c329](https://gitlab.com/ensembl/talk/commit/6a9c329773a609bdc20bdd9f7b65d525d14f551c))
* **PageLayout middle area:** For Talk Page and narrower screens, fix overflow ([47b0a6d](https://gitlab.com/ensembl/talk/commit/47b0a6d22c4cee39e0dc2da1021013ed47eec139))
* **Passwordless Login:** Workaround so that user is not redirected ([102a853](https://gitlab.com/ensembl/talk/commit/102a853f5e786e2607ac36e9ab592d04769e5ac9))
* **ProfilePage:** Fix shrink when name field is empty ([df52a05](https://gitlab.com/ensembl/talk/commit/df52a05ecbd16da313cc08782b810a8bf0788f45))
* **ProfilePage:** Remove debug code ([ecf2f68](https://gitlab.com/ensembl/talk/commit/ecf2f687ec462d8fce24f037c92f1b4787fbffae))
* **replaceAll:** Don't use it ([dd2b062](https://gitlab.com/ensembl/talk/commit/dd2b062faeb761f4c33889f1fde910341672d11f))
* **Resend invitee email:** Fix space name not being passed ([a53e492](https://gitlab.com/ensembl/talk/commit/a53e4921e6db4ea77f82b9e873783e313d669d5c))
* **Right sidebar:** Background color fix ([0331e95](https://gitlab.com/ensembl/talk/commit/0331e95dd49601690828d9982aed23c10544d670)), closes [#772](https://gitlab.com/ensembl/talk/issues/772)
* **Routing:** / goes to home ([3756db6](https://gitlab.com/ensembl/talk/commit/3756db61b9a5d54ec96220ec8129cf0d8f9f6b68))
* **rules:** Allow fields to update for onboarding message ([6407e8e](https://gitlab.com/ensembl/talk/commit/6407e8e22b0cf14bed888c052ca9c4b71a85a36c))
* **rules:** Fix unable to delete Talk ([6b30780](https://gitlab.com/ensembl/talk/commit/6b307806bec8ff7c0067a2c5412e3ffd20759480))
* **Rules:** Allow user in mentions to update comment ([f70db39](https://gitlab.com/ensembl/talk/commit/f70db39628b018bd06c5aa68e42d1974cb767863))
* **Rules:** Prevent user from changing its own space ([8f28e63](https://gitlab.com/ensembl/talk/commit/8f28e63a5783d0c9b3136c4c18e008387c014e5a))
* **Scrolling:** Temporary fix for deep link comment ([6ac6886](https://gitlab.com/ensembl/talk/commit/6ac6886fabcb8ed7ef40df5094c3ab399438c916))
* **Search:** Remove scrolling to the top on seach change ([136576e](https://gitlab.com/ensembl/talk/commit/136576ecbf6aa3b0ca17b6f2a511a4faea38eeb4))
* **Send Talk Approval Nots:** Fix null array ([46f692a](https://gitlab.com/ensembl/talk/commit/46f692aca4476d88fd37e41828a662cd489c762b))
* **sendCmtAddedNots:** Don't send notifications to yourself for @ everyone ([01392d1](https://gitlab.com/ensembl/talk/commit/01392d1cb8796b30e7c305f08f0f21671fa7d2ef))
* **sendCmtAddedNots:** FIx sendCmtAddedNots not named in index.js ([2cc47e9](https://gitlab.com/ensembl/talk/commit/2cc47e98c9bfb6842e3211faac1c6202df8b82a4)), closes [#417](https://gitlab.com/ensembl/talk/issues/417)
* **sendTalkApprovalNots:** Fix getting keys in approval email notification ([5226da1](https://gitlab.com/ensembl/talk/commit/5226da1f8a661a4f9adf5a2f0d0481c9bd54700b)), closes [#391](https://gitlab.com/ensembl/talk/issues/391)
* **sendTalkSubjectUpdatedNots:** Fix bug with printing htmlSubject in error message ([7c6d797](https://gitlab.com/ensembl/talk/commit/7c6d797baee28203cf25e5ddb508eab84729db96)), closes [#541](https://gitlab.com/ensembl/talk/issues/541)
* **Sidebar:** Make sidebar scrollable when overflowed ([0b023c2](https://gitlab.com/ensembl/talk/commit/0b023c2be7b259de80d0cb37536d3694b6f12c4e))
* **Sign In Page:** Check window.location.href is not email link ([e4e563a](https://gitlab.com/ensembl/talk/commit/e4e563a65c5c98ee312bc9957af8a401a1f1d507))
* **Space switcher:** Should not flash with old space name after switch ([5b5e3e6](https://gitlab.com/ensembl/talk/commit/5b5e3e6cd9889b840e36c873dd5388d2e14b3f6f))
* **Style:** Fix scrolling to comment ([144689f](https://gitlab.com/ensembl/talk/commit/144689f919ea3048bb564170866947327160fcd1))
* **SubjectHeader:** Fix onboarding message having wrong state ([565f778](https://gitlab.com/ensembl/talk/commit/565f77895407f0e6c78c01c1d30dd6326652f36f)), closes [#612](https://gitlab.com/ensembl/talk/issues/612)
* **SubjectTimestamp:** Allow timestamp to wrap ([38bced4](https://gitlab.com/ensembl/talk/commit/38bced4efab66fd4d3e403f29c525e94d90983af))
* **SubjectTimestamp:** Fix span breaking up ([8da661c](https://gitlab.com/ensembl/talk/commit/8da661c45d5254c00101064cd47f1f1393d5e454))
* **Tabs:** Resolved tab only show resolved talks ([9a0edaf](https://gitlab.com/ensembl/talk/commit/9a0edaf6bfb8656ee860ed2aca85243c325fbf27))
* **Talk:** Fix conflicts ([8a54728](https://gitlab.com/ensembl/talk/commit/8a54728e2da75e6c05f2d17308378ac3fcc1508c)), closes [#382](https://gitlab.com/ensembl/talk/issues/382)
* **Talk:** Fix mention not getting the correct data ([2d5ca32](https://gitlab.com/ensembl/talk/commit/2d5ca3276983b9b20e18164be6076e5686c3e0bf))
* **TalkLabels:** Remove unused import ([95507a5](https://gitlab.com/ensembl/talk/commit/95507a5f887e6714103823b962e8d5415f7f5960)), closes [#579](https://gitlab.com/ensembl/talk/issues/579)
* **TalkPage:** Fix anchor tag ([2d08f58](https://gitlab.com/ensembl/talk/commit/2d08f58a543324cf1a7ff90bd189516ba17f03cb))
* **TalkPage:** Scroll to talk top automatically when loading page ([674b9f7](https://gitlab.com/ensembl/talk/commit/674b9f72a97aa128ab84dc1143b49b7ffaa69539)), closes [#674](https://gitlab.com/ensembl/talk/issues/674)
* **Talks:** Talkselects don't refresh after filter change ([689d79a](https://gitlab.com/ensembl/talk/commit/689d79a2a62f742a248769b51fb5f6f7ce769093)), closes [#457](https://gitlab.com/ensembl/talk/issues/457)
* **TalkSelect:** Fix margin when no talk comment preview ([e13a7e5](https://gitlab.com/ensembl/talk/commit/e13a7e59e288e36bc66123a4778067e9d0453b51)), closes [#630](https://gitlab.com/ensembl/talk/issues/630)
* **TalkSelect:** Fix too much top padding TalkSelect due to labels ([47719a2](https://gitlab.com/ensembl/talk/commit/47719a21af92dc5e3976b05c3bf2e67a49ba7142)), closes [#453](https://gitlab.com/ensembl/talk/issues/453)
* **TalkSelectLabels:** Fix labels not wrapping when overflow ([994d5eb](https://gitlab.com/ensembl/talk/commit/994d5eb52b5766f283dca1f275ca4b6afdcbeaa6))
* **TalkSelectNewestComment:** Fix line cut off between lines ([d5d76fa](https://gitlab.com/ensembl/talk/commit/d5d76fa770f12301ee40aca08e9a3ac68e38fdde)), closes [#392](https://gitlab.com/ensembl/talk/issues/392)
* **TalkSelects:** Fix bug with required PropTypes missing ([003e84a](https://gitlab.com/ensembl/talk/commit/003e84a789aced977b14735eb568e8dbf60fe54f)), closes [#415](https://gitlab.com/ensembl/talk/issues/415)
* **TalkSelects:** Fix pagination not showing ([a80c479](https://gitlab.com/ensembl/talk/commit/a80c479886ac195f6dedb047f4a99bdece11a6e8)), closes [#404](https://gitlab.com/ensembl/talk/issues/404)
* **TalksPageHeader:** Fix missing styling ([1cd19e8](https://gitlab.com/ensembl/talk/commit/1cd19e8ee515d5d3112bb0b196aa55be741bcb37)), closes [#576](https://gitlab.com/ensembl/talk/issues/576)
* Fix placeholder text being left out in merge ([1e7696c](https://gitlab.com/ensembl/talk/commit/1e7696c7f4d4ae53418f9806629bef5f8a330f5a))
* **tooltipDatetimeFormatter:** Fix bug rendering timestamp in tooltip ([e652601](https://gitlab.com/ensembl/talk/commit/e652601c91e7789c9e2f5abd6483ca34b701c51b)), closes [#425](https://gitlab.com/ensembl/talk/issues/425)
* **updateAlgoliaTalks:** Fix not sending avatarUrls to Algolia ([23852a0](https://gitlab.com/ensembl/talk/commit/23852a0ea1ac84e23b14d894782c91b5e2926a6f)), closes [#365](https://gitlab.com/ensembl/talk/issues/365)
* **updateAlgoliaTalks:** Send commenterAvatarUrls to Algolia ([8f540fa](https://gitlab.com/ensembl/talk/commit/8f540fa1fc8bb47534f83680af2364180309495b)), closes [#368](https://gitlab.com/ensembl/talk/issues/368)
* Add quotes for space with spaces ([59b2684](https://gitlab.com/ensembl/talk/commit/59b26846eacc5414ca79dd2faddebf6f53417044))
* Fix delete talk on update ([fa8c939](https://gitlab.com/ensembl/talk/commit/fa8c93931b223834d982f7d1f80e46890f74f956))
* Fix incorrect index name ([b1355b8](https://gitlab.com/ensembl/talk/commit/b1355b8ecb048698a01d7ae25293f0e7b727ed8f))
* Fix linting error. ([ff276ec](https://gitlab.com/ensembl/talk/commit/ff276ecfd18d4dc08e4f2bea489db216489bdddb))
* Fix new Talk to have mentions ([cff2e05](https://gitlab.com/ensembl/talk/commit/cff2e05ddf5da4633d91ca41153b67e4b6c6f84f)), closes [#207](https://gitlab.com/ensembl/talk/issues/207)
* Fix not able to add comment ([0de61c7](https://gitlab.com/ensembl/talk/commit/0de61c7d17598fe03039384226ca865119562a32))
* Fix on create not updating the index ([f6b4d1f](https://gitlab.com/ensembl/talk/commit/f6b4d1fab4beed673c2f2b6a73a178ded159b7d2))
* Fix semantic and key ([2793687](https://gitlab.com/ensembl/talk/commit/2793687930dbc5535c21d19bf9f82a27ac66cfd6))
* Make subscription toggle reactive ([353a08e](https://gitlab.com/ensembl/talk/commit/353a08e013d22bd79f4ebcdb8aa6448eb6309203))
* **Talks:** Fix Talks page not showing when sort by preview ([47de4b2](https://gitlab.com/ensembl/talk/commit/47de4b243311c7e096db9a30787f86feb3d18757))
* Remove Home component. ([5eb77dc](https://gitlab.com/ensembl/talk/commit/5eb77dc9ce56c5e8e4b0bb3d3386effe93ab22c6))
* Remove test from CI ([cd73e01](https://gitlab.com/ensembl/talk/commit/cd73e018f2719c49ba6ec63d73459c5f89a16ed4))
* Update mentionedUserMap to mentions ([b75a973](https://gitlab.com/ensembl/talk/commit/b75a973d3ce38506145adc010cbd185ad6c62b98))
* **Talk:** Fix linting error ([ecd98cc](https://gitlab.com/ensembl/talk/commit/ecd98ccb38aae5f34e583dd99102aa684cc1ef13)), closes [#33](https://gitlab.com/ensembl/talk/issues/33)
* **Talk header title:** Fix overflow when long titles ([30c7d61](https://gitlab.com/ensembl/talk/commit/30c7d61c0373e3b03c7eb671fcf0e7183d236166)), closes [#811](https://gitlab.com/ensembl/talk/issues/811)
* **Talk sidebar:** Fix width styling ([92b045f](https://gitlab.com/ensembl/talk/commit/92b045fb41589dbee39dc761fa1a01ab42b9a536)), closes [#771](https://gitlab.com/ensembl/talk/issues/771)
* **TalkPage:** Fix scrolling to talk top ([add48c0](https://gitlab.com/ensembl/talk/commit/add48c06aa4186bb414d1cd284db2a53e528ce73))
* **TalkPage Desktop Header:** Fix vertical centering ([bd0b83c](https://gitlab.com/ensembl/talk/commit/bd0b83c23136980eb856fcb1ec0e5d6614fd1560)), closes [#769](https://gitlab.com/ensembl/talk/issues/769)
* **TalkPge:** Fix no talk found missing ([32284b4](https://gitlab.com/ensembl/talk/commit/32284b4858932bdb9157339764d9037ec5995adf))
* **TalkRow:** Title and Newest Comment Urls contain spaceName ([a63c191](https://gitlab.com/ensembl/talk/commit/a63c19196feab976d7996120feb248816e3953be))
* **TalkSelect labels:** Space between labels in TalkSelect ([2ce515a](https://gitlab.com/ensembl/talk/commit/2ce515a05b6908c87710e9e8e6c1ab82a8f3d859)), closes [#754](https://gitlab.com/ensembl/talk/issues/754)
* **TalksPage:** Clicking tabs should not blow away labels select ([2b4d1e5](https://gitlab.com/ensembl/talk/commit/2b4d1e5b10f71a66d8f1f59152a5571f470aef4e))
* **TalksPage:** Jump to top after clicking pagination ([3300335](https://gitlab.com/ensembl/talk/commit/330033539400e4de2cc8ae43fa78c8e87bd43df1)), closes [#767](https://gitlab.com/ensembl/talk/issues/767)
* **TalksPage:** Title should be be 'Home' ([372cde0](https://gitlab.com/ensembl/talk/commit/372cde013c44547faf63b3afc1a18b780717d190))
* **Title:** Encode & symbol correctly ([121673d](https://gitlab.com/ensembl/talk/commit/121673de663775d64ba14954f2d3a34b553ec4c6))
* **useEmulator:** Set useEmulator back to false ([5a53d92](https://gitlab.com/ensembl/talk/commit/5a53d921cf72fdc59dd9dc413b5a5c3dd05c0004)), closes [#519](https://gitlab.com/ensembl/talk/issues/519)
